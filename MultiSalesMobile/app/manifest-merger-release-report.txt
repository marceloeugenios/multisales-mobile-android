-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionCode
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.CAMERA
ADDED from AndroidManifest.xml:4:5
	android:name
		ADDED from AndroidManifest.xml:4:22
uses-feature#android.hardware.camera
ADDED from AndroidManifest.xml:6:5
	android:name
		ADDED from AndroidManifest.xml:6:19
uses-feature#android.hardware.camera.autofocus
ADDED from AndroidManifest.xml:7:5
	android:name
		ADDED from AndroidManifest.xml:7:19
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
permission#multisales.mobile.nx.com.br.permission.MAPS_RECEIVE
ADDED from AndroidManifest.xml:17:5
	android:protectionLevel
		ADDED from AndroidManifest.xml:18:9
	android:name
		ADDED from AndroidManifest.xml:17:17
uses-permission#multisales.mobile.nx.com.br.permission.MAPS_RECEIVE
ADDED from AndroidManifest.xml:20:5
	android:name
		ADDED from AndroidManifest.xml:20:22
uses-permission#android.permission.USE_CREDENTIALS
ADDED from AndroidManifest.xml:21:5
	android:name
		ADDED from AndroidManifest.xml:21:22
uses-permission#android.permission.GET_ACCOUNTS
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:22
uses-permission#android.permission.READ_PROFILE
ADDED from AndroidManifest.xml:23:5
	android:name
		ADDED from AndroidManifest.xml:23:22
uses-permission#android.permission.READ_CONTACTS
ADDED from AndroidManifest.xml:24:5
	android:name
		ADDED from AndroidManifest.xml:24:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:26:5
	android:name
		ADDED from AndroidManifest.xml:26:22
uses-permission#com.google.android.providers.gsf.permission.READ_GSERVICES
ADDED from AndroidManifest.xml:27:5
	android:name
		ADDED from AndroidManifest.xml:27:22
uses-permission#android.permission.ACCESS_COARSE_LOCATION
ADDED from AndroidManifest.xml:32:5
	android:name
		ADDED from AndroidManifest.xml:32:22
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:33:5
	android:name
		ADDED from AndroidManifest.xml:33:22
uses-permission#android.permission.WRITE_SETTINGS
ADDED from AndroidManifest.xml:34:5
	android:name
		ADDED from AndroidManifest.xml:34:22
application
ADDED from AndroidManifest.xml:36:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.google.android.gms:play-services:6.5.87:20:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:40:9
	android:allowBackup
		ADDED from AndroidManifest.xml:38:9
	android:icon
		ADDED from AndroidManifest.xml:39:9
	android:theme
		ADDED from AndroidManifest.xml:41:9
	android:name
		ADDED from AndroidManifest.xml:37:9
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.LoginActivity
ADDED from AndroidManifest.xml:44:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:47:13
	android:label
		ADDED from AndroidManifest.xml:46:13
	android:name
		ADDED from AndroidManifest.xml:45:13
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:50:9
MERGED from com.google.android.gms:play-services:6.5.87:21:9
	android:name
		ADDED from AndroidManifest.xml:51:13
	android:value
		ADDED from AndroidManifest.xml:52:13
meta-data#com.google.android.maps.v2.API_KEY
ADDED from AndroidManifest.xml:54:9
	android:name
		ADDED from AndroidManifest.xml:55:13
	android:value
		ADDED from AndroidManifest.xml:56:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.MainActivity
ADDED from AndroidManifest.xml:58:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:62:13
	android:label
		ADDED from AndroidManifest.xml:60:13
	android:name
		ADDED from AndroidManifest.xml:59:13
	android:logo
		ADDED from AndroidManifest.xml:61:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaClienteActivity
ADDED from AndroidManifest.xml:64:9
	android:label
		ADDED from AndroidManifest.xml:67:13
	android:icon
		ADDED from AndroidManifest.xml:66:13
	android:name
		ADDED from AndroidManifest.xml:65:13
intent-filter#TABULACAO+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:68:13
action#TABULACAO
ADDED from AndroidManifest.xml:69:17
	android:name
		ADDED from AndroidManifest.xml:69:25
category#android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:71:17
	android:name
		ADDED from AndroidManifest.xml:71:27
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.NaoVendaActivity
ADDED from AndroidManifest.xml:74:9
	android:label
		ADDED from AndroidManifest.xml:77:13
	android:icon
		ADDED from AndroidManifest.xml:76:13
	android:name
		ADDED from AndroidManifest.xml:75:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaEnderecoActivity
ADDED from AndroidManifest.xml:84:9
	android:label
		ADDED from AndroidManifest.xml:86:13
	android:name
		ADDED from AndroidManifest.xml:85:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaPacoteActivity
ADDED from AndroidManifest.xml:88:9
	android:label
		ADDED from AndroidManifest.xml:90:13
	android:name
		ADDED from AndroidManifest.xml:89:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.AgendamentosActivity
ADDED from AndroidManifest.xml:92:9
	android:label
		ADDED from AndroidManifest.xml:95:13
	android:icon
		ADDED from AndroidManifest.xml:94:13
	android:name
		ADDED from AndroidManifest.xml:93:13
	android:logo
		ADDED from AndroidManifest.xml:96:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.AgendamentoDetalhesActivity
ADDED from AndroidManifest.xml:98:9
	android:label
		ADDED from AndroidManifest.xml:100:13
	android:name
		ADDED from AndroidManifest.xml:99:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.NaoVendasActivity
ADDED from AndroidManifest.xml:102:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:105:13
	android:label
		ADDED from AndroidManifest.xml:104:13
	android:name
		ADDED from AndroidManifest.xml:103:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaTvActivity
ADDED from AndroidManifest.xml:108:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:111:13
	android:label
		ADDED from AndroidManifest.xml:110:13
	android:name
		ADDED from AndroidManifest.xml:109:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.NaoVendaDetalhesActivity
ADDED from AndroidManifest.xml:113:9
	android:label
		ADDED from AndroidManifest.xml:115:13
	android:name
		ADDED from AndroidManifest.xml:114:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendasActivity
ADDED from AndroidManifest.xml:117:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:120:13
	android:label
		ADDED from AndroidManifest.xml:119:13
	android:name
		ADDED from AndroidManifest.xml:118:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaTvAdicionalActivity
ADDED from AndroidManifest.xml:122:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:125:13
	android:label
		ADDED from AndroidManifest.xml:124:13
	android:name
		ADDED from AndroidManifest.xml:123:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaFinalizarActivity
ADDED from AndroidManifest.xml:127:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:130:13
	android:label
		ADDED from AndroidManifest.xml:129:13
	android:name
		ADDED from AndroidManifest.xml:128:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaDetalhesActivity
ADDED from AndroidManifest.xml:132:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:135:13
	android:label
		ADDED from AndroidManifest.xml:134:13
	android:name
		ADDED from AndroidManifest.xml:133:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.AgendamentoActivity
ADDED from AndroidManifest.xml:137:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:140:13
	android:label
		ADDED from AndroidManifest.xml:139:13
	android:name
		ADDED from AndroidManifest.xml:138:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.SplashActivity
ADDED from AndroidManifest.xml:142:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:145:13
	android:label
		ADDED from AndroidManifest.xml:144:13
	android:name
		ADDED from AndroidManifest.xml:143:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:146:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:147:17
	android:name
		ADDED from AndroidManifest.xml:147:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:149:17
	android:name
		ADDED from AndroidManifest.xml:149:27
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaCelularActivity
ADDED from AndroidManifest.xml:153:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:156:13
	android:label
		ADDED from AndroidManifest.xml:155:13
	android:name
		ADDED from AndroidManifest.xml:154:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaCelularDependenteActivity
ADDED from AndroidManifest.xml:158:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:161:13
	android:label
		ADDED from AndroidManifest.xml:160:13
	android:name
		ADDED from AndroidManifest.xml:159:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.PapActivity
ADDED from AndroidManifest.xml:164:9
	android:label
		ADDED from AndroidManifest.xml:166:13
	android:name
		ADDED from AndroidManifest.xml:165:13
receiver#multisales.mobile.nx.com.br.multisalesmobile.utils.VerificadorInternet
ADDED from AndroidManifest.xml:168:9
	android:name
		ADDED from AndroidManifest.xml:168:19
intent-filter#android.net.conn.CONNECTIVITY_CHANGE
ADDED from AndroidManifest.xml:169:13
action#android.net.conn.CONNECTIVITY_CHANGE
ADDED from AndroidManifest.xml:170:17
	android:name
		ADDED from AndroidManifest.xml:170:25
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaPacoteNaoVendaActivity
ADDED from AndroidManifest.xml:174:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:177:13
	android:label
		ADDED from AndroidManifest.xml:176:13
	android:name
		ADDED from AndroidManifest.xml:175:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.CondominioActivity
ADDED from AndroidManifest.xml:180:9
	android:label
		ADDED from AndroidManifest.xml:182:13
	android:name
		ADDED from AndroidManifest.xml:181:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.CondominiosActivity
ADDED from AndroidManifest.xml:185:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:188:13
	android:label
		ADDED from AndroidManifest.xml:187:13
	android:name
		ADDED from AndroidManifest.xml:186:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.CondominioDetalheActivity
ADDED from AndroidManifest.xml:191:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:194:13
	android:label
		ADDED from AndroidManifest.xml:193:13
	android:name
		ADDED from AndroidManifest.xml:192:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.CondominioPlantoesActivity
ADDED from AndroidManifest.xml:197:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:200:13
	android:label
		ADDED from AndroidManifest.xml:199:13
	android:name
		ADDED from AndroidManifest.xml:198:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.CondominioPlantaoActivity
ADDED from AndroidManifest.xml:203:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:206:13
	android:label
		ADDED from AndroidManifest.xml:205:13
	android:name
		ADDED from AndroidManifest.xml:204:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.CondominioEstatisticaActivity
ADDED from AndroidManifest.xml:209:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:212:13
	android:label
		ADDED from AndroidManifest.xml:211:13
	android:name
		ADDED from AndroidManifest.xml:210:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.CondominioPlantaoEstatisticaActivity
ADDED from AndroidManifest.xml:215:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:218:13
	android:label
		ADDED from AndroidManifest.xml:217:13
	android:name
		ADDED from AndroidManifest.xml:216:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaFoneActivity
ADDED from AndroidManifest.xml:221:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:224:13
	android:label
		ADDED from AndroidManifest.xml:223:13
	android:name
		ADDED from AndroidManifest.xml:222:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaFonePortabilidadeActivity
ADDED from AndroidManifest.xml:227:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:230:13
	android:label
		ADDED from AndroidManifest.xml:229:13
	android:name
		ADDED from AndroidManifest.xml:228:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaInternetActivity
ADDED from AndroidManifest.xml:233:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:236:13
	android:label
		ADDED from AndroidManifest.xml:235:13
	android:name
		ADDED from AndroidManifest.xml:234:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.VendaFormaPagamentoActivity
ADDED from AndroidManifest.xml:239:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:242:13
	android:label
		ADDED from AndroidManifest.xml:241:13
	android:name
		ADDED from AndroidManifest.xml:240:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.RascunhosActivity
ADDED from AndroidManifest.xml:245:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:248:13
	android:label
		ADDED from AndroidManifest.xml:247:13
	android:name
		ADDED from AndroidManifest.xml:246:13
activity#multisales.mobile.nx.com.br.multisalesmobile.activities.ClientesActivity
ADDED from AndroidManifest.xml:251:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:254:13
	android:label
		ADDED from AndroidManifest.xml:253:13
	android:name
		ADDED from AndroidManifest.xml:252:13
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.android.gms:play-services:6.5.87:18:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
