package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cidade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ETipoAgendamento;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Estado;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogDiario;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TabulacaoAgendamento;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.servico.LogDiarioServico;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class AgendamentoActivity extends ActionBarActivity implements View.OnFocusChangeListener, View.OnClickListener {

    private EntityManager entityManager;

    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    public AsyncTask<Void, Void, ResponseD2D> taskNaoVenda;

    private Tabulacao tabulacao;
    private TabulacaoAgendamento tabulacaoAgendamento;
    private ETipoAgendamento[] tiposAgendamento;
    private Hp hp;
    private Cidade cidade;
    private MotivoTabulacao motivoAgendamento;
    private List<MotivoTabulacao> motivosTabulacao;
    private List<Estado> estados;
    private List<Cidade> cidades;
    private EditText nomeClienteET;
    private EditText telResidencialET;
    private EditText telComercialET;
    private EditText telCelularET;
    private EditText enderecoET;
    private EditText complementoET;
    private EditText cepET;
    private EditText dataET;
    private EditText horaET;
    private EditText bairroET;
    private Spinner  estadoSP;
    private Spinner  cidadeSP;
    private EditText observacaoET;
    private Spinner  motivoAgendamentoSP;
    private Spinner  tipoAgendamentoSP;

    private DatePickerDialog dataDPD;
    private TimePickerDialog horaTPD;

    private SimpleDateFormat formatoData;
    private SimpleDateFormat formatoHora;

    private Calendar dataHoraAgendamento;

    private LogDiarioServico logDiarioServico;

    private MultiSales multiSales;

    private Integer idCondominio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendamento);
        multiSales = (MultiSales) getApplication();
        dataHoraAgendamento = Calendar.getInstance();
        formatoData = new SimpleDateFormat("dd/MM/yyyy");
        formatoHora = new SimpleDateFormat("HH:mm");
        entityManager = new EntityManager(this);
        logDiarioServico = new LogDiarioServico(this);
        idCondominio = getIntent().getIntExtra("idCondominio", 0);

        instanciarComponentes();

        carregarMotivosTabulacao();
        carregarTiposAgedamento();
        carregarEstados();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setDateTimeField();

    }

    private void instanciarComponentes() {
        nomeClienteET        = (EditText) findViewById(R.id.nomeClienteET);
        telResidencialET     = (EditText) findViewById(R.id.telResidencialET);
        UtilMask.setMascaraTelefone(telResidencialET);
        telComercialET       = (EditText) findViewById(R.id.telComercialET);
        UtilMask.setMascaraTelefone(telComercialET);
        telCelularET         = (EditText) findViewById(R.id.telCelularET);
        UtilMask.setMascaraTelefone(telCelularET);
        enderecoET           = (EditText) findViewById(R.id.enderecoET);
        complementoET        = (EditText) findViewById(R.id.complementoET);
        cepET                = (EditText) findViewById(R.id.cepET);
        UtilMask.setMascaraCep(cepET);
        bairroET             = (EditText) findViewById(R.id.bairroET);
        dataET               = (EditText) findViewById(R.id.dataET);
        horaET               = (EditText) findViewById(R.id.horaET);
        estadoSP             = (Spinner) findViewById(R.id.estadoSP);
        cidadeSP             = (Spinner) findViewById(R.id.cidadeSP);
        observacaoET         = (EditText) findViewById(R.id.observacaoET);
        motivoAgendamentoSP  = (Spinner) findViewById(R.id.motivoAgendamentoSP);
        tipoAgendamentoSP    = (Spinner) findViewById(R.id.tipoAgendamentoSP);

    }

    private void setDateTimeField() {
        dataET.setOnFocusChangeListener(this);
        horaET.setOnFocusChangeListener(this);
        dataET.setOnClickListener(this);
        horaET.setOnClickListener(this);

        final Calendar dataHoraAtual = Calendar.getInstance();
        dataDPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dataHoraAgendamento.set(year, monthOfYear, dayOfMonth);
                dataET.setText(formatoData.format(dataHoraAgendamento.getTime()));
            }

        },dataHoraAtual.get(Calendar.YEAR), dataHoraAtual.get(Calendar.MONTH), dataHoraAtual.get(Calendar.DAY_OF_MONTH));

        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                dataHoraAgendamento.set(Calendar.HOUR_OF_DAY, hourOfDay);
                dataHoraAgendamento.set(Calendar.MINUTE, minute);
                horaET.setText(formatoHora.format(dataHoraAgendamento.getTime()));
            }
        };

        horaTPD = new TimePickerDialog(this, onTimeSetListener, dataHoraAtual.get(Calendar.HOUR_OF_DAY), dataHoraAtual.get(Calendar.MINUTE), true);

    }

    private void carregarMotivosTabulacao() {
        try {
            List<MotivoTabulacaoTipo> motivosTabulacaoTipo = entityManager.getByWhere(MotivoTabulacaoTipo.class, "descricao = '" + EMotivoTabulacaoTipo.AGENDAMENTO.getDescricao() + "'", null);
            if (motivosTabulacaoTipo != null
                    && !motivosTabulacaoTipo.isEmpty()) {

                MotivoTabulacaoTipo motivoNaoVenda = motivosTabulacaoTipo.get(SistemaConstantes.ZERO);

                motivosTabulacao = entityManager.getByWhere(MotivoTabulacao.class,
                        "_motivo_tabulacao_tipo = " + motivoNaoVenda.getId(), null);
                final List<String> descricoes = new ArrayList<>();
                if (motivosTabulacao != null && !motivosTabulacao.isEmpty()) {
                    descricoes.add(UtilActivity.SELECIONE);
                    for(MotivoTabulacao mot : motivosTabulacao){
                        descricoes.add(mot.getDescricao());
                    }
                }
                setAdapter(motivoAgendamentoSP, descricoes);
            }
        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }

    private void carregarTiposAgedamento() {
        try {
            tiposAgendamento = ETipoAgendamento.values();
            List<String> tipos = new ArrayList<>();
            tipos.add(UtilActivity.SELECIONE);

            for(ETipoAgendamento tipo : tiposAgendamento) {
                tipos.add(tipo.getLabel());
            }
            UtilActivity.setAdapter(this, tipoAgendamentoSP, tipos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void carregarEstados() {
        try {
            estados = entityManager.getAll(Estado.class);
            List<String> nomesEstados = new ArrayList<>();
            nomesEstados.add(UtilActivity.SELECIONE);

            for(Estado estado : estados){
                nomesEstados.add(estado.getNome());
            }
            setAdapter(estadoSP, nomesEstados);

            estadoSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    List<String> nomesCidades = new ArrayList<>();

                    if (position > SistemaConstantes.ZERO) {
                        Estado estadoSelecionado = estados.get(position - SistemaConstantes.UM);

                        try {
                            cidades = entityManager.getByWhere(Cidade.class, "_estado = \"" + estadoSelecionado.getUf()+"\"", null);
                            nomesCidades.add(UtilActivity.SELECIONE);
                            if (cidades != null && !cidades.isEmpty()) {
                                for (Cidade cidade : cidades) {
                                    nomesCidades.add(cidade.getNome());
                                }
                            }
                            cidadeSP.setEnabled(true);
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                            makeShortToast("Ops! Ocorreu um ERRO ao listar as cidades! :(");
                            cidades = new ArrayList<>();
                        }

                    } else {
                        nomesCidades.add("SELECIONE O ESTADO");
                        cidadeSP.setEnabled(false);
                    }
                    setAdapter(cidadeSP, nomesCidades);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nao_venda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            this.finish();
        }

        if (id == R.id.action_salvar_nao_venda) {

            if (!validarCamposObrigatorios()) {
                return false;
            }

            try {
                popularHp();
                popularTabulacao();
                tabulacao.setHp(entityManager.save(hp));
                tabulacao = entityManager.save(tabulacao);
                entityManager.initialize(tabulacao.getHp());
                if (idCondominio != 0) {
                    entityManager.initialize(tabulacao.getHp().getCondominio());
                }
                popularTabulacaoAgendamento();
                tabulacaoAgendamento = entityManager.save(tabulacaoAgendamento);
                tabulacao.setCompleta(EBoolean.TRUE);
                entityManager.atualizar(tabulacao);

                try {
                    LogDiario logDiario = logDiarioServico.obterLogDiarioAtualPorUsuario(multiSales.getLogin());
                    logDiario.setAgendamentos(logDiario.getAgendamentos() + 1);
                    entityManager.atualizar(logDiario);
                } catch (Exception e) {
                    UtilActivity.makeShortToast("ERRO ao atualizar contadores!", getApplicationContext());
                }

                taskNaoVenda = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.AGENDAMENTO.getCodigo());
                            requestD2D.setTabulacao(tabulacao);
                            requestD2D.setTabulacaoAgendamento(tabulacaoAgendamento);
                            requestD2D.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                            requestD2D.setIdUsuario(multiSales.getIdUsuario());
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        Intent mainIntent = new Intent(AgendamentoActivity.this, MainActivity.class);
                        if(resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                tabulacao.setId((Integer) resposta.getExtras().get("idAgendamento"));
                                entityManager.atualizar(tabulacao);
                                tabulacaoAgendamento.setId((Integer) resposta.getExtras().get("idAgendamento"));
                                entityManager.atualizar(tabulacaoAgendamento);
                                mainIntent.putExtra("mensagem", "Tabulação " + tabulacao.getIdLocal() + " salva com SUCESSO!");
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("ERRO ao salvar Tabulação!", getApplicationContext());
                            }
                        } else {
                            mainIntent.putExtra("mensagem", "Tabulação salva mas não sincronizada!");
                        }
                        startActivity(mainIntent);
                        AgendamentoActivity.this.finish();
                    }
                };
                taskNaoVenda.execute();

            } catch (DataBaseException e) {
                e.printStackTrace();
                makeShortToast("ERRO ao salvar Tabulação!");
                return false;
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean validarCamposObrigatorios() {

        if (nomeClienteET.getText().toString().isEmpty()) {

            //TODO decidir como ficarão marcados campos obrigatórios não preenchidos
            setError(nomeClienteET);
            nomeClienteET.setHint("Informe o nome");
            Drawable x = this.getResources().getDrawable(R.drawable.abc_edit_text_material);
            x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());
            nomeClienteET.setCompoundDrawables(null, null, x, null);
            return false;
        }

        if (telResidencialET.getText().toString().isEmpty()
                && telCelularET.getText().toString().isEmpty()
                && telComercialET.getText().toString().isEmpty()) {
            makeShortToast("Informe ao menos um telefone.");
            return false;
        }

        if (cidadeSP.getSelectedItemPosition() == SistemaConstantes.ZERO) {
            makeShortToast("Informe a cidade");
            return false;
        } else {
            this.cidade = cidades.get(cidadeSP.getSelectedItemPosition() - SistemaConstantes.UM);
        }

        if (motivoAgendamentoSP.getSelectedItemPosition() == SistemaConstantes.ZERO) {
            makeShortToast("Informe o motivo do agendamento");
            return false;
        } else {
            this.motivoAgendamento = motivosTabulacao
                    .get(motivoAgendamentoSP.getSelectedItemPosition() - SistemaConstantes.UM);
        }

        if (tipoAgendamentoSP.getSelectedItemPosition() == SistemaConstantes.ZERO) {
            makeShortToast("Informe o tipo do agendamento");
            return false;
        }

        if(dataET.getText().toString().equals("__/__/____") ||
                dataET.getText().toString().isEmpty() ||
                horaET.getText().toString().equals("__:__") ||
                horaET.getText().toString().isEmpty()){
            makeShortToast("Informe a data e hora de retorno");
            return false;
        }

        return true;

    }

    private void setError(TextView textView) {
        textView.setError("Campo obrigatório");
    }


    private Hp popularHp() throws DataBaseException {

        this.hp = new Hp();
        hp.setNome(nomeClienteET.getText().toString());
        hp.setTelefone1(UtilMask.unmask(telResidencialET.getText().toString()));
        hp.setTelefone2(UtilMask.unmask(telComercialET.getText().toString()));
        hp.setTelefone3(UtilMask.unmask(telCelularET.getText().toString()));
        hp.setLogradouro(enderecoET.getText().toString());
        hp.setCidade(cidade);
        hp.setComplemento(complementoET.getText().toString());
        hp.setCep(UtilMask.unmask(cepET.getText().toString()));
        hp.setBairro(bairroET.getText().toString());
        hp.setUltimoMotivoTabulacao(motivoAgendamento);
        hp.setObservacao(observacaoET.getText().toString());
        if (idCondominio != 0) {
            hp.setCondominio(entityManager.getById(Condominio.class, idCondominio));
        }

        return hp;
    }

    //TODO setar usuário de inclusão da tabulação
    private Tabulacao popularTabulacao() {
        tabulacao = new Tabulacao();
        tabulacao.setMotivoTabulacao(motivoAgendamento);
        tabulacao.setDataCadastro(Calendar.getInstance());
        return tabulacao;
    }

    private TabulacaoAgendamento popularTabulacaoAgendamento() {
        tabulacaoAgendamento = new TabulacaoAgendamento();
        tabulacaoAgendamento.setTabulacao(tabulacao);
        tabulacaoAgendamento.setDataRetorno(dataHoraAgendamento);
        tabulacaoAgendamento.setTipoAgendamento(tiposAgendamento[tipoAgendamentoSP.getSelectedItemPosition() - 1]);
        return tabulacaoAgendamento;
    }

    private void makeShortToast(String mensagem) {
        Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();
    }

    private void setAdapter(Spinner spinner, List<String> strings) {
        ArrayAdapter adapter = new ArrayAdapter<> (this, android.R.layout.simple_spinner_item, strings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(hasFocus) {
            showPicker(view);
        }
    }

    @Override
    public void onClick(View view) {
        showPicker(view);
    }

    private void showPicker(View view) {
        if(view == dataET) {
            dataDPD.show();
        } else if(view == horaET) {
            horaTPD.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
