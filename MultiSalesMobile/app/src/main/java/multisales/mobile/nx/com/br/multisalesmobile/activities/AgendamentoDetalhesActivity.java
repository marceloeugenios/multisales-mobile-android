package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TabulacaoAgendamento;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class AgendamentoDetalhesActivity extends ActionBarActivity {

    private EntityManager entityManager;

    private TextView nomeHpTV;
    private TextView dataTV;
    private TextView horaTV;
    private TextView tipoTV;
    private TextView enderecoTV;
    private TextView cidadeEstadoTV;
    private TextView cepTV;
    private TextView residencialTV;
    private TextView celularTV;
    private TextView comercialTV;
    private TextView motivoTV;

    private Integer idAgendamento;
    private TabulacaoAgendamento tabulacaoAgendamento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendamento_detalhes);
        entityManager = new EntityManager(this);

        idAgendamento = getIntent().getIntExtra("idAgendamento", SistemaConstantes.ZERO);
        atribuirComponentes();
        try {
            tabulacaoAgendamento = entityManager.getById(TabulacaoAgendamento.class, idAgendamento);
            entityManager.initialize(tabulacaoAgendamento.getTabulacao());
            entityManager.initialize(tabulacaoAgendamento.getTabulacao().getHp());
            entityManager.initialize(tabulacaoAgendamento.getTabulacao().getMotivoTabulacao());
            entityManager.initialize(tabulacaoAgendamento.getTabulacao().getHp().getCidade());
            entityManager.initialize(tabulacaoAgendamento.getTabulacao().getHp().getCidade().getEstado());

            SimpleDateFormat simpleDateFormatData = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat("HH:mm");

            nomeHpTV.setText(tabulacaoAgendamento.getTabulacao().getHp().getNome());
            dataTV.setText(simpleDateFormatData.format(tabulacaoAgendamento.getDataRetorno().getTime()));
            horaTV.setText(simpleDateFormatHora.format(tabulacaoAgendamento.getDataRetorno().getTime()));
            tipoTV.setText(tabulacaoAgendamento.getTipoAgendamento().getLabel());
            motivoTV.setText(tabulacaoAgendamento.getTabulacao().getMotivoTabulacao().getDescricao());

            StringBuilder enderecoCompleto = new StringBuilder();
            int aux = 0 ;
            if(tabulacaoAgendamento.getTabulacao().getHp().getLogradouro() != null && !tabulacaoAgendamento.getTabulacao().getHp().getLogradouro().isEmpty()){
                enderecoCompleto.append(tabulacaoAgendamento.getTabulacao().getHp().getLogradouro());
                enderecoCompleto.append(",");
                aux ++ ;
            }
            if(tabulacaoAgendamento.getTabulacao().getHp().getComplemento() != null && !tabulacaoAgendamento.getTabulacao().getHp().getComplemento().isEmpty()){
                enderecoCompleto.append(tabulacaoAgendamento.getTabulacao().getHp().getComplemento());
                if(aux > 0) {
                    enderecoCompleto.append(",");
                }
                aux ++;
            }
            if(tabulacaoAgendamento.getTabulacao().getHp().getBairro() != null && !tabulacaoAgendamento.getTabulacao().getHp().getBairro().isEmpty()){
                enderecoCompleto.append(tabulacaoAgendamento.getTabulacao().getHp().getBairro());
                aux++;
            }

            if(aux > 0 ){
                enderecoTV.setText(enderecoCompleto.toString());
            } else {
                enderecoTV.setText("-");
            }


            StringBuilder cidadeEstado = new StringBuilder();
            cidadeEstado.append(tabulacaoAgendamento.getTabulacao().getHp().getCidade().getNome());
            cidadeEstado.append("/");
            cidadeEstado.append(tabulacaoAgendamento.getTabulacao().getHp().getCidade().getEstado().getUf());
            cidadeEstadoTV.setText(cidadeEstado.toString());

            if(tabulacaoAgendamento.getTabulacao().getHp().getCep() != null
                    && !tabulacaoAgendamento.getTabulacao().getHp().getCep().isEmpty()){


            String cep = tabulacaoAgendamento.getTabulacao().getHp().getCep();
                if(cep != null && !cep.isEmpty() && cep.length() > 7 ){
                    String part1 = cep.substring(0, 5);
                    String part2 = cep.substring(5, 8);
                    cep = part1 + "-" + part2;
                } else {
                    cep =tabulacaoAgendamento.getTabulacao().getHp().getCep().toString();
                }
                cepTV.setText(cep);
            } else {
                cepTV.setText("-");
            }

            SpannableString content;
            if(tabulacaoAgendamento.getTabulacao().getHp().getTelefone1() != null
                    && !tabulacaoAgendamento.getTabulacao().getHp().getTelefone1().isEmpty()) {
                content = new SpannableString(tabulacaoAgendamento.getTabulacao().getHp().getTelefone1());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

                residencialTV.setText(UtilActivity.formatarTelefone(content.toString()));
            } else {
                residencialTV.setText("-");
            }

            if(tabulacaoAgendamento.getTabulacao().getHp().getTelefone2() != null
                    && !tabulacaoAgendamento.getTabulacao().getHp().getTelefone2().isEmpty()) {
                content = new SpannableString(tabulacaoAgendamento.getTabulacao().getHp().getTelefone2());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                comercialTV.setText(UtilActivity.formatarTelefone(content.toString()));
            } else {
                comercialTV.setText("-");
            }

            if(tabulacaoAgendamento.getTabulacao().getHp().getTelefone3() != null
                    && !tabulacaoAgendamento.getTabulacao().getHp().getTelefone3().isEmpty()){
                content = new SpannableString(tabulacaoAgendamento.getTabulacao().getHp().getTelefone3());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                celularTV.setText(UtilActivity.formatarTelefone(content.toString()));
            } else {
                celularTV.setText("-");
            }

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }

    private void atribuirComponentes() {
        nomeHpTV = (TextView) findViewById(R.id.nomeHpTV);
        dataTV = (TextView) findViewById(R.id.dataTV);
        horaTV = (TextView) findViewById(R.id.horaTV);
        tipoTV = (TextView) findViewById(R.id.tipoTV);
        enderecoTV  = (TextView) findViewById(R.id.enderecoTV);
        cidadeEstadoTV  = (TextView) findViewById(R.id.cidadeEstadoTV);
        cepTV  = (TextView) findViewById(R.id.cepTV);
        residencialTV = (TextView) findViewById(R.id.residencialTV);
        comercialTV  = (TextView) findViewById(R.id.comercialTV);
        celularTV  = (TextView) findViewById(R.id.celularTV);
        motivoTV  = (TextView) findViewById(R.id.motivoTV);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_agendamento_detalhes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Id correspondente ao botão Up/Home da actionbar
            case android.R.id.home:
                finish();
                break;

            case R.id.tabular:
                Intent intentTabulacao = new Intent("TABULACAO");
                startActivity(Intent.createChooser(intentTabulacao, null));
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    public void discar(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        if (view.equals(residencialTV)) {
            intent.setData(Uri.parse("tel:" + tabulacaoAgendamento.getTabulacao().getHp().getTelefone1()));
            startActivity(intent);
        } else if (view.equals(comercialTV)) {
            intent.setData(Uri.parse("tel:" + tabulacaoAgendamento.getTabulacao().getHp().getTelefone2()));
            startActivity(intent);
        } else if (view.equals(celularTV)) {
            intent.setData(Uri.parse("tel:" + tabulacaoAgendamento.getTabulacao().getHp().getTelefone3()));
            startActivity(intent);
        }
    }
}
