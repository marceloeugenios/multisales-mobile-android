package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TabulacaoAgendamento;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class AgendamentosActivity extends ActionBarActivity implements AdapterView.OnItemClickListener{


    private ListView listView;
    private EntityManager entityManager;
    private List<TabulacaoAgendamento> tabulacoesAgendamento;
    private TabulacaoAgendamento tabulacaoAtual;
    private List<TabulacaoAgendamento> naoSincronizadas;
    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    private int contadorSincronizacao = 0;
    private boolean syncRLIsVisible = false;
    private RelativeLayout syncRL;
    private Button sincronizarBT;
    private Integer alturaSync;
    private MultiSales multiSales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendamentos);
        multiSales = (MultiSales) getApplication();
        syncRLIsVisible = true;
        entityManager = new EntityManager(this);

        TabulacaoListAdapter adapter = new TabulacaoListAdapter(this, listarAgendamentos());

        listView = (ListView) findViewById(R.id.listview);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        syncRL = (RelativeLayout) findViewById(R.id.syncRL);
        sincronizarBT = (Button) findViewById(R.id.sincronizarBT);

        naoSincronizadas = new ArrayList<>();
        for (TabulacaoAgendamento tabulacaoAgendamento : tabulacoesAgendamento) {
            if (tabulacaoAgendamento.getId() == null) {
                naoSincronizadas.add(tabulacaoAgendamento);
            }
        }

        if (naoSincronizadas.size() > 0 && UtilActivity.isOnline(this)) {

            ViewTreeObserver vto = syncRL.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    alturaSync = syncRL.getMeasuredHeight();
                }
            });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(syncRLIsVisible) {
                        syncRL.animate().setDuration(500);
                        syncRL.animate().translationY(alturaSync);
                    }
                }
            }, 5000);

        } else {

            syncRL.setVisibility(View.INVISIBLE);

        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private List<Tabulacao> listarAgendamentos() {
        List<Tabulacao> tabulacoes = new ArrayList<>();
        try {
            tabulacoesAgendamento = entityManager.getAll(TabulacaoAgendamento.class);

            for (TabulacaoAgendamento tabulacao : tabulacoesAgendamento) {
                entityManager.initialize(tabulacao.getTabulacao());
                entityManager.initialize(tabulacao.getTabulacao().getHp());
                entityManager.initialize(tabulacao.getTabulacao().getMotivoTabulacao());
                tabulacoes.add(tabulacao.getTabulacao());
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao listar os agendamentos", this);
            finish();
        }
        return tabulacoes;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_agendamentos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        if (id == R.id.novo) {
            Intent intentAgendamento = new Intent(this, AgendamentoActivity.class);
            startActivity(intentAgendamento);
            this.finish();
        }

        if (id == R.id.action_sincronizar) {
            sincronizarAgendamento();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentDetalhes = new Intent(this, AgendamentoDetalhesActivity.class);
        intentDetalhes.putExtra("idAgendamento", tabulacoesAgendamento.get(position).getIdLocal());
        startActivity(intentDetalhes);
    }

    public void sincronizar(View view) {
        sincronizarBT.setEnabled(false);
        syncRL.animate().setDuration(500);
        syncRL.animate().translationY(alturaSync);
        sincronizarAgendamento();
    }

    private void sincronizarAgendamento() {
        try {
            if (naoSincronizadas.size() > 0) {
                tabulacaoAtual = naoSincronizadas.get(contadorSincronizacao);

                AsyncTask<Void, Void, ResponseD2D> taskAgendamento = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.AGENDAMENTO.getCodigo());
                            requestD2D.setTabulacao(tabulacaoAtual.getTabulacao());
                            requestD2D.setTabulacaoAgendamento(tabulacaoAtual);
                            requestD2D.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                            requestD2D.setIdUsuario(multiSales.getIdUsuario());
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        if (resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                tabulacaoAtual.getTabulacao().setId((Integer) resposta.getExtras().get("idAgendamento"));
                                entityManager.atualizar(tabulacaoAtual.getTabulacao());
                                tabulacaoAtual.setId((Integer) resposta.getExtras().get("idAgendamento"));
                                entityManager.atualizar(tabulacaoAtual);

                                contadorSincronizacao++;
                                if (naoSincronizadas.size() == contadorSincronizacao) {
                                    naoSincronizadas = new ArrayList<>();
                                    tabulacoesAgendamento = entityManager.getAll(TabulacaoAgendamento.class);
                                    for (TabulacaoAgendamento tabulacao : tabulacoesAgendamento) {
                                        if (tabulacao.getId() == null) {
                                            naoSincronizadas.add(tabulacao);
                                        }
                                    }
                                    if (naoSincronizadas.size() == 0) {
                                        UtilActivity.makeShortToast("Todas tabulações sincronizadas com sucesso!", getApplicationContext());
                                    }

                                    List<Tabulacao> tabulacoes = new ArrayList<>();
                                    for (TabulacaoAgendamento tabulacaoAgendamento : tabulacoesAgendamento) {
                                        tabulacoes.add(tabulacaoAgendamento.getTabulacao());
                                    }

                                    TabulacaoListAdapter adapter = new TabulacaoListAdapter(AgendamentosActivity.this, tabulacoes);
                                    listView.setAdapter(adapter);
                                } else {
                                    sincronizarAgendamento();
                                }
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("ERRO ao salvar Tabulação!", getApplicationContext());
                            }
                        }
                    }
                };
                taskAgendamento.execute();
            } else {
                UtilActivity.makeShortToast("Não há tabulações para sincronizar", getApplicationContext());
            }

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar Tabulação!", this);
        }

    }
}
