package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;

/**
 * Created by eric on 26-02-2015.
 */
public class ClienteListAdapter extends BaseAdapter {
    private Context context;
    private List<Tabulacao> tabulacoes;

    public ClienteListAdapter(Context context, List<Tabulacao> tabulacoes){
        this.context = context;
        this.tabulacoes = tabulacoes;
    }

    @Override
    public int getCount() {
        if (tabulacoes != null) {
            return tabulacoes.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return tabulacoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_tabulacao, null);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

        TextView nomeClienteTV = (TextView) convertView.findViewById(R.id.nomeClienteTV);
        TextView motivoTabulacaoTV = (TextView) convertView.findViewById(R.id.motivoTabulacaoTV);
        TextView naoSincronizadoTV = (TextView) convertView.findViewById(R.id.naoSincronizadoTV);
        TextView dataHoraAgendamentoTV = (TextView) convertView.findViewById(R.id.dataHoraAgendamentoTV);

        if(tabulacoes.get(position).getVenda() == null) {
            nomeClienteTV.setText(tabulacoes.get(position).getHp().getNome());
        } else {
            nomeClienteTV.setText(tabulacoes.get(position).getVenda().getCliente().getNome());
        }

        String motivoTabulacao = tabulacoes.get(position).getMotivoTabulacao().getMotivoTabulacaoTipo().getDescricao();

        if (motivoTabulacao.equals(EMotivoTabulacaoTipo.VENDA.getDescricao())) {
            naoSincronizadoTV.setText("VENDA");
            if (tabulacoes.get(position).getVenda().getCombinacaoProdutoTipo() != null) {
                motivoTabulacaoTV.setText(tabulacoes.get(position).getVenda().getCombinacaoProdutoTipo().getDescricao());
            } else {
                motivoTabulacaoTV.setText("SEM PRODUTO");
            }
        } else {
            motivoTabulacaoTV.setText(tabulacoes.get(position).getMotivoTabulacao().getDescricao());
            if (motivoTabulacao.equals(EMotivoTabulacaoTipo.AGENDAMENTO.getDescricao())) {
                naoSincronizadoTV.setText("AGENDAMENTO");
            } else if(motivoTabulacao.equals(EMotivoTabulacaoTipo.NAO_VENDA.getDescricao())) {
                naoSincronizadoTV.setText("NÃO VENDA");
            }
        }

        naoSincronizadoTV.setVisibility(View.VISIBLE);

        dataHoraAgendamentoTV.setText(simpleDateFormat.format(tabulacoes.get(position).getDataCadastro().getTime()));

        return convertView;
    }
}
