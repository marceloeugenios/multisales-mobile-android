package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class ClientesActivity extends ActionBarActivity implements AdapterView.OnItemClickListener  {

    private ListView listView;
    private EntityManager entityManager;
    private List<Tabulacao> tabulacoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);
        entityManager = new EntityManager(this);
        listView = (ListView) findViewById(R.id.listView);

        ClienteListAdapter adapter = new ClienteListAdapter(this, listarVendas());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private List<Tabulacao> listarVendas() {
        try {
            tabulacoes = new ArrayList<>();
            StringBuilder stringBuilder =  new StringBuilder();
            stringBuilder.append("SELECT tab.* ");
            stringBuilder.append("FROM ");
            stringBuilder.append("tabulacao tab ");
            stringBuilder.append("WHERE ");
            stringBuilder.append("tab.completa =  \"" + EBoolean.TRUE.toString() + "\"");
            tabulacoes = entityManager.select(Tabulacao.class, stringBuilder.toString());

            for (Tabulacao tabulacao : tabulacoes) {
                entityManager.initialize(tabulacao.getMotivoTabulacao());
                entityManager.initialize(tabulacao.getMotivoTabulacao().getMotivoTabulacaoTipo());
                entityManager.initialize(tabulacao.getVenda());
                if (tabulacao.getVenda() != null) {
                    entityManager.initialize(tabulacao.getVenda().getCliente());
                    if (tabulacao.getVenda().getCombinacaoProdutoTipo() != null) {
                        entityManager.initialize(tabulacao.getVenda().getCombinacaoProdutoTipo());
                    }
                } else {
                    entityManager.initialize(tabulacao.getHp());
                }
            }

            return tabulacoes;
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao listar os clientes", this);
            finish();
        }

        return new ArrayList<>();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_clientes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String motivoTabulacao = tabulacoes.get(position).getMotivoTabulacao().getMotivoTabulacaoTipo().getDescricao();
        if (motivoTabulacao.equals(EMotivoTabulacaoTipo.VENDA.getDescricao())) {
            Intent intentVenda = new Intent(this, VendaDetalhesActivity.class);
            intentVenda.putExtra("idVenda", tabulacoes.get(position).getVenda().getId());
            startActivity(intentVenda);
        } else if (motivoTabulacao.equals(EMotivoTabulacaoTipo.AGENDAMENTO.getDescricao())) {
            Intent intentAgendamento = new Intent(this, AgendamentoDetalhesActivity.class);
            intentAgendamento.putExtra("idAgendamento", tabulacoes.get(position).getIdLocal());
            startActivity(intentAgendamento);
        } else if(motivoTabulacao.equals(EMotivoTabulacaoTipo.NAO_VENDA.getDescricao())) {
            Intent intentNaovenda = new Intent(this, NaoVendaDetalhesActivity.class);
            intentNaovenda.putExtra("idNaoVenda", tabulacoes.get(position).getIdLocal());
            startActivity(intentNaovenda);
        }


    }
}
