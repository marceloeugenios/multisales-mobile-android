package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cidade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContato;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContatoCargo;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.SlidingTabLayout;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class CondominioActivity extends ActionBarActivity {

    // Declaring Your View and Variables
    ViewPager pager;
    TabAdapter adapter;
    SlidingTabLayout tabs;
    private List<Fragment> fragments;

    List<String> titles;
    private EntityManager entityManager;
    private Condominio condominio;
    private Endereco endereco;
    private List<CondominioContato> condominioContatos;
    public AsyncTask<Void, Void, ResponseD2D> taskCondominio;
    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    private boolean validado = false;

    private CondominioInformacoesBasicasFragment    condominioInformacoesBasicasFragment;
    private CondominioEnderecoFragment              condominioEnderecoFragment;
    private CondominioInformacoesTecnicasFragment   condominioInformacoesTecnicasFragment;
    private CondominioSindicoFragment               condominioContatoFragment;
    private CondominioZeladorFragment               condominioZeladorFragment;
    MenuItem menuSalvar;
    MenuItem menuAvancar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condominio);

        entityManager       = new EntityManager(this);
        condominio          = new Condominio();
        endereco            = new Endereco();
        condominioContatos  = new ArrayList<>();

        condominio.setEndereco(endereco);

        fragments = new ArrayList<>();
        titles = new ArrayList<>();
        condominioInformacoesBasicasFragment    = new CondominioInformacoesBasicasFragment();
        condominioEnderecoFragment              = new CondominioEnderecoFragment();
        condominioInformacoesTecnicasFragment   = new CondominioInformacoesTecnicasFragment();
        condominioContatoFragment               = new CondominioSindicoFragment();
        condominioZeladorFragment               = new CondominioZeladorFragment();

        titles.add("INFORMAÇÕES BÁSICAS");
        fragments.add(condominioInformacoesBasicasFragment);
        titles.add("ENDEREÇO");
        fragments.add(condominioEnderecoFragment);
        titles.add("INFORMAÇÕES TECNICAS");
        fragments.add(condominioInformacoesTecnicasFragment);
        titles.add("SINDICO");
        fragments.add(condominioContatoFragment);
        titles.add("ZELADOR");
        fragments.add(condominioZeladorFragment);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new TabAdapter(getSupportFragmentManager(),titles,fragments);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(false); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.cor_destaque);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_condominio, menu);
//        menuSalvar = menu.getItem(0);
//        menuAvancar = menu.getItem(1);
//        menuSalvar.setVisible(false);
//        menuAvancar.setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_salvar_condominio) {

            validado = popularCondominio();

            if (!validarCamposObrigatorios() || !validado) {
                return false;
            }

            try {
                Cidade cidade = condominio.getEndereco().getCidade();
                List<CondominioContato> contatos = condominioContatos;

                condominio.setCondominioContatos(condominioContatos);
                endereco = entityManager.save(condominio.getEndereco());
                condominio.setEndereco(endereco);
                condominio = entityManager.save(condominio);
                condominio.getEndereco().setNullId();
                for(CondominioContato contato : contatos){
                    contato.setCondominio(condominio);
                    entityManager.save(contato);
                    contato.setCondominio(null);
                    contato.setNullId();
                }
                condominio.getEndereco().setCidade(cidade);
                condominio.setCondominioContatos(contatos);
                taskCondominio = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.CONDOMINIO.getCodigo());
                            requestD2D.setCondominio(condominio);
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        Intent mainIntent = new Intent(CondominioActivity.this, MainActivity.class);
                        if(resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                condominio.setId((Integer)resposta.getExtras().get("idCondominio"));
                                entityManager.atualizar(condominio);
                                mainIntent.putExtra("mensagem", "Condomínio " + condominio.getIdLocal() + " salva com SUCESSO!");
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("ERRO ao salvar Condomínio!", getApplicationContext());
                            }
                        } else {
                            mainIntent.putExtra("mensagem", "Condomínio salvo mas não sincronizado!");
                        }
                        startActivity(mainIntent);
                        CondominioActivity.this.finish();
                    }
                };
                taskCondominio.execute();

            } catch (DataBaseException e) {
                e.printStackTrace();
                UtilActivity.makeShortToast("ERRO ao salvar Condomínio!",this);
                return false;
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private Boolean popularCondominio() {

        try {
            //INFORMAÇÕES BASICAS
            condominio.setNome(condominioInformacoesBasicasFragment.getNomeCondominioET().getText().toString());
            condominio.setClasseSocial(condominioInformacoesBasicasFragment.getClasseSocialET().getText().toString());
            condominio.setTipoCondominio(condominioInformacoesBasicasFragment.getTipoCondominioET().getText().toString());
            condominio.setSegmento(condominioInformacoesBasicasFragment.getSegmentoET().getText().toString());
            condominio.setQuantidadeBloco(Integer.valueOf(validarVazioCampo(condominioInformacoesBasicasFragment.getQtdeBlocoET().getText().toString())));
            condominio.setQuantidadeApartamento(condominioInformacoesBasicasFragment.getQtdeAptosET().getText().toString());
            condominio.setQuantidadeAndaresBloco(Integer.valueOf(validarVazioCampo(condominioInformacoesBasicasFragment.getQtdeAndarBlocoET().getText().toString())));
            condominio.setQuantidadeApartamentoAndar(Integer.valueOf(validarVazioCampo(condominioInformacoesBasicasFragment.getQtdeAptoAndarET().getText().toString())));
            condominio.setQuantidadeDormitorioApartamento(Integer.valueOf(validarVazioCampo(condominioInformacoesBasicasFragment.getQtdeDormAptoET().getText().toString())));
            condominio.setMetragemApartamento(Double.valueOf(validarVazioCampo(condominioInformacoesBasicasFragment.getMetragemAptoET().getText().toString())));
            condominio.setIdadeEdificio(Integer.valueOf(validarVazioCampo(condominioInformacoesBasicasFragment.getIdadeEdificilET().getText().toString())));
            condominio.setObservacao(condominioInformacoesBasicasFragment.getObservacaoET().getText().toString());

            if (condominioInformacoesBasicasFragment.getSpinnerLayoutCondominio().getSelectedItemPosition() > SistemaConstantes.ZERO) {
                condominio.setLayout(condominioInformacoesBasicasFragment.getCondominioLayouts().get(
                        condominioInformacoesBasicasFragment.getSpinnerLayoutCondominio().getSelectedItemPosition() - SistemaConstantes.UM));
            }

            //ENDERECO
            endereco.setLogradouro(condominioEnderecoFragment.getEnderecoET().getText().toString());
            endereco.setNumero(condominioEnderecoFragment.getNumeroET().getText().toString());
            endereco.setCep(UtilMask.unmask(condominioEnderecoFragment.getCepET().getText().toString()));
            endereco.setBairro(condominioEnderecoFragment.getBairroET().getText().toString());
            endereco.setPontoReferencia(condominioEnderecoFragment.getRegiaoET().getText().toString());
            if(condominioEnderecoFragment.getSpinnerCidade().getSelectedItemPosition() > 0 ){
                endereco.setCidade(condominioEnderecoFragment.getCidades().get(condominioEnderecoFragment.getSpinnerCidade().getSelectedItemPosition() - SistemaConstantes.UM));
            }


            //INFORMAÇÕES TECNICAS
            condominio.setCodFttx(condominioInformacoesTecnicasFragment.getCodFttxET().getText().toString());
            condominio.setChaveEndereco(condominioInformacoesTecnicasFragment.getChaveEndET().getText().toString());
            condominio.setAreaTelefonica(condominioInformacoesTecnicasFragment.getAreaTelefonicaET().getText().toString());
            condominio.setPrioridade(condominioInformacoesTecnicasFragment.getPrioridadeET().getText().toString());
            condominio.setSegmentacaoMkt(condominioInformacoesTecnicasFragment.getSegmentacaoMktET().getText().toString());
            condominio.setCodLog(condominioInformacoesTecnicasFragment.getCodLogET().getText().toString());
            condominio.setChaveCep(condominioInformacoesTecnicasFragment.getChaveCepET().getText().toString());
            condominio.setCnl(condominioInformacoesTecnicasFragment.getCnlET().getText().toString());
            condominio.setDataConclInfra(UtilActivity.ConverterStringParaCalendar(condominioInformacoesTecnicasFragment.getDataConclInfraET().getText().toString()));
            condominio.setCapacidadeEquipamento(Integer.valueOf(validarVazioCampo(condominioInformacoesTecnicasFragment.getCapacidadeEquipamentoET().getText().toString())));
            condominio.setOcupacaoEquipamento(Integer.valueOf(validarVazioCampo(condominioInformacoesTecnicasFragment.getOcupacaoEquipamentoET().getText().toString())));
            condominio.setFibraLivre(Integer.valueOf(validarVazioCampo(condominioInformacoesTecnicasFragment.getFibraLivreET().getText().toString())));
            condominio.setPercentualOcupacao(Double.valueOf(validarVazioCampo(condominioInformacoesTecnicasFragment.getPercentualOcupacaoET().getText().toString())));
            if(condominioInformacoesTecnicasFragment.getSpinnerStatusInfra().getSelectedItemPosition() > 0){
                condominio.setCondominioStatusInfra(condominioInformacoesTecnicasFragment.getStatusInfras().get(condominioInformacoesTecnicasFragment.getSpinnerStatusInfra().getSelectedItemPosition() - SistemaConstantes.UM));
            }


            //SINDICO
            if(condominioContatoFragment.getNomeET() != null && !condominioContatoFragment.getNomeET().getText().toString().trim().equals("")){
                CondominioContato sindico = new CondominioContato();
                sindico.setNome(condominioContatoFragment.getNomeET().getText().toString());
                sindico.setDataNascimento(UtilActivity.ConverterStringParaCalendar(condominioContatoFragment.getDataNascimentoET().getText().toString()));
                sindico.setDataLimiteMandato(UtilActivity.ConverterStringParaCalendar(condominioContatoFragment.getDataLimiteMandatoET().getText().toString()));
                sindico.setClube(condominioContatoFragment.getTimeET().getText().toString());
                sindico.setNumeroApartamento(condominioContatoFragment.getApartamentoET().getText().toString());

                if(validarTelefone(UtilMask.unmask(condominioContatoFragment.getTelResidencialET().getText().toString()),"telefone residencial do Sindico")){
                    sindico.setTelefoneResidencial(UtilMask.unmask(condominioContatoFragment.getTelResidencialET().getText().toString()));
                }else{
                    return false;
                }

                if(validarTelefone(UtilMask.unmask(condominioContatoFragment.getTelCelularET().getText().toString()),"telefone celular do Sindico")){
                    sindico.setTelefoneCelular(UtilMask.unmask(condominioContatoFragment.getTelCelularET().getText().toString()));
                }else{
                    return false;
                }
                if(validarTelefone(UtilMask.unmask(condominioContatoFragment.getTelComercialET().getText().toString()),"telefone comercial do Sindico")){
                    sindico.setTelefoneComercial(UtilMask.unmask(condominioContatoFragment.getTelComercialET().getText().toString()));
                }else{
                    return false;
                }
                sindico.setCondominioContatoCargo(entityManager.getByWhere(CondominioContatoCargo.class,"descricao = 'SINDICO' ",null).get(0));
                condominioContatos.add(sindico);
            }


            //ZELADOR
            if(condominioContatoFragment.getNomeET() != null && !condominioZeladorFragment.getNomeET().getText().toString().trim().equals("")) {
                CondominioContato zelador = new CondominioContato();
                zelador.setNome(condominioZeladorFragment.getNomeET().getText().toString());
                zelador.setDataNascimento(UtilActivity.ConverterStringParaCalendar(condominioZeladorFragment.getDataNascimentoET().getText().toString()));
                zelador.setNumeroApartamento(condominioZeladorFragment.getApartamentoET().getText().toString());
                zelador.setClube(condominioZeladorFragment.getTimeET().getText().toString());
                if(validarTelefone(UtilMask.unmask(condominioZeladorFragment.getTelResidencialET().getText().toString()),"telefone residencial do zelador")){
                    zelador.setTelefoneResidencial(UtilMask.unmask(condominioZeladorFragment.getTelResidencialET().getText().toString()));
                }else{
                    return false;
                }
                if(validarTelefone(UtilMask.unmask(condominioZeladorFragment.getTelCelularET().getText().toString()),"telefone celular do zelador")){
                    zelador.setTelefoneCelular(UtilMask.unmask(condominioZeladorFragment.getTelCelularET().getText().toString()));
                }else{
                    return false;
                }
                if(validarTelefone(UtilMask.unmask(condominioZeladorFragment.getTelComercialET().getText().toString() ),"telefone comercial do zelador")){
                    zelador.setTelefoneComercial(UtilMask.unmask(condominioZeladorFragment.getTelComercialET().getText().toString()));
                }else{
                    return false;
                }
                if(validarTelefone(UtilMask.unmask(condominioZeladorFragment.getTelPortariaET().getText().toString() ),"telefone portaria do zelador")){
                    zelador.setTelefonePortaria(UtilMask.unmask(condominioZeladorFragment.getTelPortariaET().getText().toString()));
                } else {
                    return false;
                }
                zelador.setCondominioContatoCargo(entityManager.getByWhere(CondominioContatoCargo.class, "descricao = 'ZELADOR' ", null).get(0));
                condominioContatos.add(zelador);
            }

            return true;
        }catch (Exception e){
            UtilActivity.makeShortToast("Erro ao cadastrar condominio.", getApplicationContext());
            e.printStackTrace();
            return false;
        }
    }


    private String validarVazioCampo(String campo){
        return campo.trim().equals("")? "0" : campo;
    }

    private boolean validarCamposObrigatorios() {
        if(condominio.getNome() == null || condominio.getNome().trim().equals("")){
            UtilActivity.makeShortToast("O campo nome do condomínio é obrigatório.", getApplicationContext());
            return false;
        }
        if(condominio.getEndereco().getLogradouro() == null || condominio.getEndereco().getLogradouro().trim().equals("")){
            UtilActivity.makeShortToast("O campo logradouro do condomínio é obrigatório.", getApplicationContext());
            return false;
        }
        if(condominio.getEndereco().getCidade() == null || condominio.getEndereco().getCidade() == null){
            UtilActivity.makeShortToast("O campo cidade do condomínio é obrigatório.", getApplicationContext());
            return false;
        }
        return true;
    }

    private boolean validarTelefone(String campo, String msg){
        if(campo != null && !campo.trim().equals("") && campo.length() > 11) {
            UtilActivity.makeShortToast("O campo "+msg+" inválido.", getApplicationContext());
            return false;
        }
        return true;
    }

    public Condominio getCondominio() {
        return condominio;
    }

    public void setCondominio(Condominio condominio) {
        this.condominio = condominio;
    }

    public List<CondominioContato> getCondominioContatos() {
        return condominioContatos;
    }

    public void setCondominioContatos(List<CondominioContato> condominioContatos) {
        this.condominioContatos = condominioContatos;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public MenuItem getMenuAvancar() {
        return menuAvancar;
    }

    public MenuItem getMenuSalvar() {
        return menuSalvar;
    }
}
