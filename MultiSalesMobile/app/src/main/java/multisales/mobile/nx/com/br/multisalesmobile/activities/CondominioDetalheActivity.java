package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContato;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.SlidingTabLayout;

public class CondominioDetalheActivity extends ActionBarActivity {


    ViewPager pager;
    TabAdapter adapter;
    SlidingTabLayout tabs;
    private List<Fragment> fragments;
    List<String> titles;

    private Integer idCondominio;

    private EntityManager entityManager;

    private Condominio condominio;

    private CondominioInformacoesBasicasDetalheFragment    condominioInformacoesBasicasDetalheFragment;
    private CondominioEnderecoDetalheFragment              condominioEnderecoDetalheFragment;
    private CondominioInformacoesTecnicasDetalheFragment   condominioInformacoesTecnicasDetalheFragment;
    private CondominioSindicoDetalheFragment               condominioContatoDetalheFragment;
    private CondominioZeladorDetalheFragment               condominioZeladorDetalheFragment;

    private MultiSales multiSales;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condominio_detalhe);
        multiSales = (MultiSales) getApplication();

        entityManager = new EntityManager(this);
        idCondominio = getIntent().getIntExtra("idCondominio", SistemaConstantes.ZERO);
        try {
            setCondominio(entityManager.getById(Condominio.class, idCondominio));
            entityManager.initialize(getCondominio().getEndereco());
            entityManager.initialize(getCondominio().getEndereco().getCidade());
            entityManager.initialize(getCondominio().getEndereco().getCidade().getEstado());
            entityManager.initialize(getCondominio().getCondominioStatusInfra());
            getCondominio().setCondominioContatos(entityManager.select(CondominioContato.class,"SELECT cc.* FROM condominio_contato cc WHERE cc._condominio =  " + getCondominio().getIdLocal()));

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
//            StringBuilder endereco = new StringBuilder();
//            endereco.append(tabulacao.getHp().getLogradouro());
//            endereco.append(", ");
//            if (tabulacao.getHp().getComplemento() != null && tabulacao.getHp().getComplemento().equals("")) {
//                endereco.append(tabulacao.getHp().getComplemento());
//                endereco.append(", ");
//            }
//            endereco.append(tabulacao.getHp().getBairro());
//            enderecoTV.setText(endereco.toString());
//            StringBuilder cidadeEstado = new StringBuilder();
//            cidadeEstado.append(tabulacao.getHp().getCidade().getNome());
//            cidadeEstado.append("/");
//            cidadeEstado.append(tabulacao.getHp().getCidade().getEstado().getNome());
//            cidadeEstadoTV.setText(cidadeEstado.toString());
//            cepTV.setText(tabulacao.getHp().getCep());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            fragments = new ArrayList<>();
            titles = new ArrayList<>();

            condominioInformacoesBasicasDetalheFragment    = new CondominioInformacoesBasicasDetalheFragment();
            condominioEnderecoDetalheFragment              = new CondominioEnderecoDetalheFragment();
            condominioInformacoesTecnicasDetalheFragment   = new CondominioInformacoesTecnicasDetalheFragment();
            condominioContatoDetalheFragment               = new CondominioSindicoDetalheFragment();
            condominioZeladorDetalheFragment               = new CondominioZeladorDetalheFragment();

            titles.add("INFORMAÇÕES BÁSICAS");
            fragments.add(condominioInformacoesBasicasDetalheFragment);
            titles.add("ENDEREÇO");
            fragments.add(condominioEnderecoDetalheFragment);
            titles.add("INFORMAÇÕES TÉCNICAS");
            fragments.add(condominioInformacoesTecnicasDetalheFragment);
            titles.add("SÍNDICO");
            fragments.add(condominioContatoDetalheFragment);
            titles.add("ZELADOR");
            fragments.add(condominioZeladorDetalheFragment);

            // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
            adapter =  new TabAdapter(getSupportFragmentManager(),titles,fragments);

            // Assigning ViewPager View and setting the adapter
            pager = (ViewPager) findViewById(R.id.pager);
            pager.setAdapter(adapter);

            // Assiging the Sliding Tab Layout View
            tabs = (SlidingTabLayout) findViewById(R.id.tabs);
            tabs.setDistributeEvenly(false); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

            // Setting Custom Color for the Scroll bar indicator of the Tab View
            tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
                @Override
                public int getIndicatorColor(int position) {
                    return getResources().getColor(R.color.cor_destaque);
                }
            });

            // Setting the ViewPager For the SlidingTabsLayout
            tabs.setViewPager(pager);


        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_condominio_detalhe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent intentListaCondominios = new Intent(this, CondominiosActivity.class);
            startActivity(intentListaCondominios);
            this.finish();
        }

        if (id == R.id.action_plantao) {
            Intent intentPlantoes = new Intent(this, CondominioPlantoesActivity.class);
            intentPlantoes.putExtra("idCondominio", condominio.getIdLocal());
            startActivity(intentPlantoes);
        }

        if (id == R.id.action_venda) {
            Intent intentVenda = new Intent(this, VendaClienteActivity.class);
            intentVenda.putExtra("idCondominio", condominio.getIdLocal());
            multiSales.setIdCondominio(condominio.getIdLocal());
            startActivity(intentVenda);
        }

        if (id == R.id.action_nao_venda) {
            Intent intentNaoVenda = new Intent(this, NaoVendaActivity.class);
            intentNaoVenda.putExtra("idCondominio", condominio.getIdLocal());
            startActivity(intentNaoVenda);
        }

        if (id == R.id.action_agendamento) {
            Intent intentAgendamento = new Intent(this, AgendamentoActivity.class);
            intentAgendamento.putExtra("idCondominio", condominio.getIdLocal());
            startActivity(intentAgendamento);
        }

        if (id == R.id.action_estatisticas) {
            Intent intentEstatistica = new Intent(this, CondominioEstatisticaActivity.class);
            intentEstatistica.putExtra("idCondominio", condominio.getIdLocal());
            startActivity(intentEstatistica);
        }

        return super.onOptionsItemSelected(item);
    }

    public Condominio getCondominio() {
        return condominio;
    }

    public void setCondominio(Condominio condominio) {
        this.condominio = condominio;
    }
}
