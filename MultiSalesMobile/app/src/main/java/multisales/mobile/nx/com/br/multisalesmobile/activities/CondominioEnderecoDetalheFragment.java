package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.ECondominioLayout;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;

public class CondominioEnderecoDetalheFragment extends Fragment{

    private EntityManager entityManager;
    private List<ECondominioLayout> condominioLayouts;
    private RelativeLayout relativeLayout;
    private Condominio condominio;
    private TextView cepTV;
    private TextView cidadeTV;
    private TextView regiaoTV;
    private TextView enderecoTV;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.condominio = ((CondominioDetalheActivity) getActivity()).getCondominio();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }
        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_endereco_detalhe, container, false);
        criarCamposEntrada();
        return relativeLayout;
    }


    public void criarCamposEntrada() {

        this.cepTV = (TextView) relativeLayout.findViewById(R.id.cepTV);
        if(condominio.getEndereco().getCep() != null && !condominio.getEndereco().getCep().isEmpty() && condominio.getEndereco().getCep().length() > 7 ){
            String part1 = condominio.getEndereco().getCep().substring(0, 5);
            String part2 = condominio.getEndereco().getCep().substring(5, 8);
            String cep = part1 + "-" + part2;
            cepTV.setText(cep);
        } else {
            cepTV.setText("N/D");
        }

        this.enderecoTV = (TextView) relativeLayout.findViewById(R.id.enderecoTV);
        StringBuilder enderecoCompleto = new StringBuilder();
        int aux = 0 ;
        if(condominio.getEndereco().getLogradouro() != null && !condominio.getEndereco().getLogradouro().isEmpty()){
            enderecoCompleto.append(condominio.getEndereco().getLogradouro());
            aux ++ ;
        }
        if(condominio.getEndereco().getNumero() != null && !condominio.getEndereco().getNumero().isEmpty()){
            if(condominio.getEndereco().getNumero() == null ){
                enderecoCompleto.append(" ");
            } else {
                enderecoCompleto.append(", " + condominio.getEndereco().getNumero());
            }
            aux ++ ;
        }
        if(condominio.getEndereco().getBairro() != null && !condominio.getEndereco().getBairro().isEmpty()){
            enderecoCompleto.append(", " + condominio.getEndereco().getBairro());
            aux ++ ;
        }

        if(aux == 0 ){
            enderecoTV.setText("N/D");
        } else {
            enderecoTV.setText(enderecoCompleto);
        }

        this.regiaoTV = (TextView) relativeLayout.findViewById(R.id.pontoReferenciaTV);
        if(condominio.getEndereco().getPontoReferencia() != null && !condominio.getEndereco().getPontoReferencia().isEmpty()){
            regiaoTV.setText("Regiao " + condominio.getEndereco().getPontoReferencia());
        } else {
            regiaoTV.setText("N/D");
        }

        this.cidadeTV = (TextView) relativeLayout.findViewById(R.id.cidadeEstadoTV);
//        cidadeTV.setText(condominio.getEndereco().getCidade().getNome() + "/" + condominio.getEndereco().getCidade().getEstado().getUf());
        cidadeTV.setText("");
    }
}

