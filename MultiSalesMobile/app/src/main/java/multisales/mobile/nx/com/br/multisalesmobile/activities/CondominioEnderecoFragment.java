package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cidade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Estado;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class CondominioEnderecoFragment extends Fragment{

    private EntityManager entityManager;
    private List<Estado> estados;
    private List<Cidade> cidades;

    private EditText enderecoET;
    private EditText numeroET;
    private EditText cepET;
    private EditText bairroET;
    private Spinner  spinnerEstado;
    private Spinner  spinnerCidade;
    private EditText regiaoET;
    private RelativeLayout relativeLayout;
    Condominio condominio;
    Endereco endereco;
    private MenuItem menuSalvar;
    private MenuItem menuAvancar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_endereco, container, false);
        entityManager  = new EntityManager(getActivity());
        inicializar();
        carregarSpinnerEstado();
//        menuSalvar =  ((CondominioActivity) getActivity()).getMenuSalvar();
//        menuAvancar = ((CondominioActivity) getActivity()).getMenuAvancar();
//        menuSalvar.setVisible(true);
//        menuAvancar.setVisible(false);

        return relativeLayout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        condominio  = ((CondominioActivity) getActivity()).getCondominio();
        endereco    = condominio.getEndereco();
    }

    private void inicializar() {
        setEnderecoET((EditText) relativeLayout.findViewById(R.id.enderecoET));
        setNumeroET((EditText) relativeLayout.findViewById(R.id.numeroET));
        setCepET((EditText) relativeLayout.findViewById(R.id.cepET));
        UtilMask.setMascaraCep(cepET);
        setBairroET((EditText) relativeLayout.findViewById(R.id.bairroET));
        setSpinnerEstado((Spinner)  relativeLayout.findViewById(R.id.spinnerEstado));
        setSpinnerCidade((Spinner)  relativeLayout.findViewById(R.id.spinnerCidade));
        setRegiaoET((EditText) relativeLayout.findViewById(R.id.regiaoET));
    }

    private void carregarSpinnerEstado() {
        try {
            setEstados(entityManager.getAll(Estado.class));
            List<String> nomesEstados = new ArrayList<>();
            nomesEstados.add(UtilActivity.SELECIONE);

            for(Estado estado : getEstados()){
                nomesEstados.add(estado.getNome());
            }
            UtilActivity.setAdapter((ActionBarActivity)getActivity(), getSpinnerEstado(), nomesEstados);

            getSpinnerEstado().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    List<String> nomesCidades = new ArrayList<>();

                    if (position > SistemaConstantes.ZERO) {
                        Estado estadoSelecionado = getEstados().get(position - SistemaConstantes.UM);

                        try {
                            setCidades(entityManager.getByWhere(Cidade.class, "_estado = '" + estadoSelecionado.getUf() + "'", null));
                            nomesCidades.add(UtilActivity.SELECIONE);
                            if (getCidades() != null && !getCidades().isEmpty()) {
                                for (Cidade cidade : getCidades()) {
                                    nomesCidades.add(cidade.getNome());
                                }
                            }
                            getSpinnerCidade().setEnabled(true);
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                            UtilActivity.makeShortToast("Ops! Ocorreu um ERRO ao listar as cidades!", getActivity().getApplicationContext());
                            cidades = new ArrayList<>();
                        }
                    } else {
                        nomesCidades.add("SELECIONE O ESTADO");
                        getSpinnerCidade().setEnabled(false);
                    }
                    ItemComboAdapter adapter = new ItemComboAdapter(getActivity(), R.layout.item_combo, nomesCidades);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    getSpinnerCidade().setAdapter(adapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }
    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public EditText getEnderecoET() {
        return enderecoET;
    }

    public void setEnderecoET(EditText enderecoET) {
        this.enderecoET = enderecoET;
    }

    public EditText getNumeroET() {
        return numeroET;
    }

    public void setNumeroET(EditText numeroET) {
        this.numeroET = numeroET;
    }

    public EditText getCepET() {
        return cepET;
    }

    public void setCepET(EditText cepET) {
        this.cepET = cepET;
    }

    public EditText getBairroET() {
        return bairroET;
    }

    public void setBairroET(EditText bairroET) {
        this.bairroET = bairroET;
    }

    public Spinner getSpinnerEstado() {
        return spinnerEstado;
    }

    public void setSpinnerEstado(Spinner spinnerEstado) {
        this.spinnerEstado = spinnerEstado;
    }

    public Spinner getSpinnerCidade() {
        return spinnerCidade;
    }

    public void setSpinnerCidade(Spinner spinnerCidade) {
        this.spinnerCidade = spinnerCidade;
    }

    public EditText getRegiaoET() {
        return regiaoET;
    }

    public void setRegiaoET(EditText regiaoET) {
        this.regiaoET = regiaoET;
    }
}
