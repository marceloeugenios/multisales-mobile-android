package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.ECondominioLayout;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class CondominioInformacoesBasicasDetalheFragment extends Fragment{

    private EntityManager entityManager;
    private List<ECondominioLayout> condominioLayouts;

    private TextView nomeCondominioTV;
    private TextView classeSocialTV;
    private TextView tipoCondominioTV;
    private TextView segmentoTV;
    private TextView layoutCondominioTV;
    private TextView qtdeBlocoTV;
    private TextView qtdeAptosTV;
    private TextView qtdeAndarBlocoTV;
    private TextView qtdeAptoAndarTV;
    private TextView qtdeDormAptoTV;
    private TextView metragemAptoTV;
    private TextView idadeEdificilTV;
    private TextView observacaoTV;
    private RelativeLayout relativeLayout;
    private Condominio condominio;
    private TextView qtdApartBlocoTV;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.condominio = ((CondominioDetalheActivity) getActivity()).getCondominio();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }
        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_informacoes_basicas_detalhe, container, false);
        criarCamposEntrada();
        return relativeLayout;
    }


    public void criarCamposEntrada(){

        this.nomeCondominioTV = (TextView) relativeLayout.findViewById(R.id.nomeCondominioTV);
        if (condominio.getNome() != null
                && !condominio.getNome().isEmpty()){
            this.nomeCondominioTV.setText(condominio.getNome());
        } else {
            this.nomeCondominioTV.setText("-");
        }

        this.classeSocialTV = (TextView) relativeLayout.findViewById(R.id.classeSocialTV);
        if (condominio.getClasseSocial() != null
                && !condominio.getClasseSocial().isEmpty()){
            this.classeSocialTV.setText(condominio.getClasseSocial());
        } else {
            this.classeSocialTV.setText("-");
        }

        this.tipoCondominioTV = (TextView) relativeLayout.findViewById(R.id.tipoCondominioTV);
        if (condominio.getTipoCondominio() != null
                && !condominio.getTipoCondominio().isEmpty()){
            this.classeSocialTV.setText(condominio.getTipoCondominio());
        } else {
            this.classeSocialTV.setText("-");
        }

        this.tipoCondominioTV = (TextView) relativeLayout.findViewById(R.id.tipoCondominioTV);
        if (condominio.getTipoCondominio() != null
                && !condominio.getTipoCondominio().isEmpty()){
            this.tipoCondominioTV.setText(condominio.getTipoCondominio());
        } else {
            this.tipoCondominioTV.setText("-");
        }

        this.segmentoTV = (TextView) relativeLayout.findViewById(R.id.segmentoTV);
        if(condominio.getSegmento() != null
                && !condominio.getSegmento().isEmpty()){
            this.segmentoTV.setText(condominio.getSegmento());
        } else {
            this.segmentoTV.setText("-");
        }

        this.layoutCondominioTV = (TextView) relativeLayout.findViewById(R.id.layoutTV);
        if(condominio.getLayout() != null
                && !condominio.getLayout().toString().isEmpty()){
            this.layoutCondominioTV.setText(condominio.getLayout().toString());
        } else {
            this.layoutCondominioTV.setText("-");
        }

        this.qtdeBlocoTV = (TextView) relativeLayout.findViewById(R.id.qtdBlocoTV);
        if (condominio.getQuantidadeBloco() != null
                && !condominio.getQuantidadeBloco().toString().isEmpty()){
            this.qtdeBlocoTV.setText(condominio.getQuantidadeBloco().toString());
        } else {
            this.qtdeBlocoTV.setText("-");
        }

        this.qtdeAptosTV = (TextView) relativeLayout.findViewById(R.id.qtdeAptosTV);
        if(condominio.getQuantidadeApartamento() != null
                && !condominio.getQuantidadeApartamento().toString().isEmpty()){
            this.qtdeAptosTV.setText(condominio.getQuantidadeApartamento().toString());
        } else {
            this.qtdeAptosTV.setText("-");
        }

      this.qtdeAndarBlocoTV = (TextView) relativeLayout.findViewById(R.id.qtdeAndarBlocoTV);
        if(condominio.getQuantidadeAndaresBloco() != null
                && !condominio.getQuantidadeAndaresBloco().toString().isEmpty()){
            this.qtdeAndarBlocoTV.setText(condominio.getQuantidadeAndaresBloco().toString());
        } else {
            this.qtdeAndarBlocoTV.setText("-");
        }


        this.qtdeAptoAndarTV = (TextView) relativeLayout.findViewById(R.id.qtdeAptoAndarTV);
        if(condominio.getQuantidadeApartamentoAndar() != null
                && !condominio.getQuantidadeApartamentoAndar().toString().isEmpty()) {
            this.qtdeAptoAndarTV.setText(condominio.getQuantidadeApartamentoAndar().toString());
        } else {
            this.qtdeAptoAndarTV.setText("-");
        }



        this.qtdeDormAptoTV = (TextView) relativeLayout.findViewById(R.id.qtdeDormAptoTV);
        if(condominio.getQuantidadeDormitorioApartamento() != null
                && !condominio.getQuantidadeDormitorioApartamento().toString().isEmpty()){
            this.qtdeDormAptoTV.setText(condominio.getQuantidadeDormitorioApartamento().toString());
        } else {
            this.qtdeDormAptoTV.setText("-");
        }

        this.metragemAptoTV = (TextView) relativeLayout.findViewById(R.id.metragemAptoTV);
        if(condominio.getMetragemApartamento() != null){
            this.metragemAptoTV.setText(condominio.getMetragemApartamento().toString());
        } else {
            this.metragemAptoTV.setText("-");
        }

        this.idadeEdificilTV = (TextView) relativeLayout.findViewById(R.id.idadeEdificioTV);
        if(condominio.getIdadeEdificio() != null) {
            this.idadeEdificilTV.setText(condominio.getIdadeEdificio().toString());
        } else {
            this.idadeEdificilTV.setText("-");
        }

        this.observacaoTV  = (TextView) relativeLayout.findViewById(R.id.observacaoTV);
        if (condominio.getObservacao() != null
                && !condominio.getObservacao().isEmpty()) {
            this.observacaoTV.setText(condominio.getObservacao());
        } else {
            this.observacaoTV.setText("-");
        }

    }

}
