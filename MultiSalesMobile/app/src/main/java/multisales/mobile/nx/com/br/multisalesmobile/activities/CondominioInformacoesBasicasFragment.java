package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.ECondominioLayout;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class CondominioInformacoesBasicasFragment extends Fragment{

    private EntityManager entityManager;
    private List<ECondominioLayout> condominioLayouts;

    private EditText nomeCondominioET;
    private EditText classeSocialET;
    private EditText tipoCondominioET;
    private EditText segmentoET;
    private Spinner  spinnerLayoutCondominio;
    private EditText qtdeBlocoET;
    private EditText qtdeAptosET;
    private EditText qtdeAndarBlocoET;
    private EditText qtdeAptoAndarET;
    private EditText qtdeDormAptoET;
    private EditText metragemAptoET;
    private EditText idadeEdificilET;
    private EditText observacaoET;
    private RelativeLayout relativeLayout;
    Condominio condominio;
    private MenuItem menuSalvar;
    private MenuItem menuAvancar;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_informacoes_basicas, container, false);

        inicializar();
        carregarSpinnerLayoutCondominio();

        return relativeLayout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        menuSalvar =  ((CondominioActivity) getActivity()).getMenuSalvar();
//        menuAvancar = ((CondominioActivity) getActivity()).getMenuAvancar();
//        menuSalvar.setVisible(true);
//        menuAvancar.setVisible(false);
        condominio = ((CondominioActivity) getActivity()).getCondominio();
    }

    private void inicializar() {
        setNomeCondominioET((EditText) relativeLayout.findViewById(R.id.nomeCondominioET));
        classeSocialET          = (EditText) relativeLayout.findViewById(R.id.classeSocialET);
        tipoCondominioET        = (EditText) relativeLayout.findViewById(R.id.tipoCondominioET);
        segmentoET              = (EditText) relativeLayout.findViewById(R.id.segmentoET);
        spinnerLayoutCondominio = (Spinner)  relativeLayout.findViewById(R.id.spinnerLayoutCondominio);
        qtdeBlocoET             = (EditText) relativeLayout.findViewById(R.id.qtdeBlocoET);
        qtdeAptosET             = (EditText) relativeLayout.findViewById(R.id.qtdeAptosET);
        qtdeAndarBlocoET        = (EditText) relativeLayout.findViewById(R.id.qtdeAndarBlocoET);
        qtdeAptoAndarET         = (EditText) relativeLayout.findViewById(R.id.qtdeAptoAndarET);
        qtdeDormAptoET          = (EditText) relativeLayout.findViewById(R.id.qtdeDormAptoET);
        metragemAptoET          = (EditText) relativeLayout.findViewById(R.id.metragemAptoET);
        idadeEdificilET         = (EditText) relativeLayout.findViewById(R.id.idadeEdificilET);
        observacaoET            = (EditText) relativeLayout.findViewById(R.id.observacaoET);
    }

    private void carregarSpinnerLayoutCondominio() {
        setCondominioLayouts(Arrays.asList(ECondominioLayout.values()));
        List<String> descricoesCondominioLayouts = new ArrayList<>();
        descricoesCondominioLayouts.add(UtilActivity.SELECIONE);

        for (ECondominioLayout condominioLayout : getCondominioLayouts()) {
            descricoesCondominioLayouts.add(condominioLayout.toString());
        }

        UtilActivity.setAdapter((ActionBarActivity)getActivity(), spinnerLayoutCondominio, descricoesCondominioLayouts);
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        if (this.isVisible()) {
//            if (!isVisibleToUser) {
//                popularCondominioComInformacoesBasicas();
//            }
//        }
//        super.setUserVisibleHint(isVisibleToUser);
//    }

//    private void popularCondominioComInformacoesBasicas() {
//
//        try {
//            condominio.setNome(getNomeCondominioET().getText().toString());
//            condominio.setClasseSocial(classeSocialET.getText().toString());
//            condominio.setTipoCondominio(tipoCondominioET.getText().toString());
//            condominio.setSegmento(segmentoET.getText().toString());
//            condominio.setQuantidadeBloco(Integer.valueOf(validarVazioCampo(qtdeBlocoET.getText().toString())));
//            condominio.setQuantidadeApartamento(qtdeAptosET.getText().toString());
//            condominio.setQuantidadeAndaresBloco(Integer.valueOf(validarVazioCampo(qtdeAndarBlocoET.getText().toString())));
//            condominio.setQuantidadeApartamentoAndar(Integer.valueOf(validarVazioCampo(qtdeAptoAndarET.getText().toString())));
//            condominio.setQuantidadeDormitorioApartamento(Integer.valueOf(validarVazioCampo(qtdeDormAptoET.getText().toString())));
//            condominio.setMetragemApartamento(Double.valueOf(validarVazioCampo(metragemAptoET.getText().toString())));
//            condominio.setIdadeEdificio(Integer.valueOf(validarVazioCampo(idadeEdificilET.getText().toString())));
//
//            if (spinnerLayoutCondominio.getSelectedItemPosition() > SistemaConstantes.ZERO) {
//                condominio.setLayout(getCondominioLayouts().get(
//                        spinnerLayoutCondominio.getSelectedItemPosition() - SistemaConstantes.UM));
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

//    private String validarVazioCampo(String campo){
//        return campo.trim().equals("")? "0" : campo;
//    }

    public EditText getNomeCondominioET() {
        return nomeCondominioET;
    }

    public void setNomeCondominioET(EditText nomeCondominioET) {
        this.nomeCondominioET = nomeCondominioET;
    }

    public EditText getClasseSocialET() {
        return classeSocialET;
    }

    public void setClasseSocialET(EditText classeSocialET) {
        this.classeSocialET = classeSocialET;
    }

    public EditText getTipoCondominioET() {
        return tipoCondominioET;
    }

    public void setTipoCondominioET(EditText tipoCondominioET) {
        this.tipoCondominioET = tipoCondominioET;
    }

    public EditText getSegmentoET() {
        return segmentoET;
    }

    public void setSegmentoET(EditText segmentoET) {
        this.segmentoET = segmentoET;
    }

    public Spinner getSpinnerLayoutCondominio() {
        return spinnerLayoutCondominio;
    }

    public void setSpinnerLayoutCondominio(Spinner spinnerLayoutCondominio) {
        this.spinnerLayoutCondominio = spinnerLayoutCondominio;
    }

    public EditText getQtdeBlocoET() {
        return qtdeBlocoET;
    }

    public void setQtdeBlocoET(EditText qtdeBlocoET) {
        this.qtdeBlocoET = qtdeBlocoET;
    }

    public EditText getQtdeAptosET() {
        return qtdeAptosET;
    }

    public void setQtdeAptosET(EditText qtdeAptosET) {
        this.qtdeAptosET = qtdeAptosET;
    }

    public EditText getQtdeAndarBlocoET() {
        return qtdeAndarBlocoET;
    }

    public void setQtdeAndarBlocoET(EditText qtdeAndarBlocoET) {
        this.qtdeAndarBlocoET = qtdeAndarBlocoET;
    }

    public EditText getQtdeAptoAndarET() {
        return qtdeAptoAndarET;
    }

    public void setQtdeAptoAndarET(EditText qtdeAptoAndarET) {
        this.qtdeAptoAndarET = qtdeAptoAndarET;
    }

    public EditText getQtdeDormAptoET() {
        return qtdeDormAptoET;
    }

    public void setQtdeDormAptoET(EditText qtdeDormAptoET) {
        this.qtdeDormAptoET = qtdeDormAptoET;
    }

    public EditText getMetragemAptoET() {
        return metragemAptoET;
    }

    public void setMetragemAptoET(EditText metragemAptoET) {
        this.metragemAptoET = metragemAptoET;
    }

    public EditText getIdadeEdificilET() {
        return idadeEdificilET;
    }

    public void setIdadeEdificilET(EditText idadeEdificilET) {
        this.idadeEdificilET = idadeEdificilET;
    }

    public List<ECondominioLayout> getCondominioLayouts() {
        return condominioLayouts;
    }

    public void setCondominioLayouts(List<ECondominioLayout> condominioLayouts) {
        this.condominioLayouts = condominioLayouts;
    }

    public EditText getObservacaoET() {
        return observacaoET;
    }

    public void setObservacaoET(EditText observacaoET) {
        this.observacaoET = observacaoET;
    }
}
