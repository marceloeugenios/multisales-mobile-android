package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.ECondominioLayout;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;

public class CondominioInformacoesTecnicasDetalheFragment extends Fragment{

    private EntityManager entityManager;
    private List<ECondominioLayout> condominioLayouts;
    private RelativeLayout relativeLayout;
    private Condominio condominio;
    private TextView codigoFttxTV;
    private TextView chaveEndTV;
    private TextView classeSocialTV;
    private TextView prioridadeTV;
    private TextView segmentacaoTV;
    private TextView codLogTV;
    private TextView chaveCepTV;
    private TextView cnlTV;
    private TextView statusInfraTV;
    private TextView dataConclInfraTV;
    private TextView capacidadeEquipamentoTV;
    private TextView fibraLivreTV;
    private TextView percentualOcupacaoTV;
    private TextView areaTelefonicaTV;
    private TextView ocupacaoEquipamentoTV;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.condominio = ((CondominioDetalheActivity) getActivity()).getCondominio();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }
        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_informacoes_tecnicas_detalhe, container, false);
        criarCamposEntrada();
        return relativeLayout;
    }


    public void criarCamposEntrada(){

        this.codigoFttxTV = (TextView) relativeLayout.findViewById(R.id.codfttxTV);
        if( condominio.getCodFttx() != null
                && !condominio.getCodFttx().isEmpty()){
            this.codigoFttxTV.setText(condominio.getCodFttx());
        } else {
            this.codigoFttxTV.setText("-");
        }

        this.chaveEndTV = (TextView) relativeLayout.findViewById(R.id.chaveEndTV);
        if (condominio.getChaveEndereco() != null
                && !condominio.getChaveEndereco().isEmpty()){
            this.chaveEndTV.setText(condominio.getChaveEndereco());
        } else {
            this.chaveEndTV.setText("-");
        }

        this.areaTelefonicaTV = (TextView) relativeLayout.findViewById(R.id.areaTelefonicaTV);
        if (condominio.getAreaTelefonica() != null
                && !condominio.getAreaTelefonica().isEmpty()) {
            this.areaTelefonicaTV.setText(condominio.getAreaTelefonica());
        } else {
            this.areaTelefonicaTV.setText("-");
        }

        this.prioridadeTV = (TextView)  relativeLayout.findViewById(R.id.prioridadeTV);
        if (condominio.getPrioridade() != null
                && !condominio.getPrioridade().isEmpty()) {
            this.prioridadeTV.setText(condominio.getPrioridade());
        } else {
            this.prioridadeTV.setText("-");
        }

        this.segmentacaoTV = (TextView) relativeLayout.findViewById(R.id.segmentacaoMktTV);
        if (condominio.getSegmento() != null
                && !condominio.getSegmento().isEmpty()) {
            this.segmentacaoTV.setText(condominio.getSegmento());
        } else {
            this.segmentacaoTV.setText("-");
        }

        this.codLogTV = (TextView) relativeLayout.findViewById(R.id.codLogTV);
        if (condominio.getCodLog() != null
                && !condominio.getCodLog().isEmpty()) {
            this.codLogTV.setText(condominio.getCodLog());
        } else {
            this.codLogTV.setText("-");
        }

        this.chaveCepTV = (TextView) relativeLayout.findViewById(R.id.chaveCepTV);
        if ( condominio.getChaveCep() != null
                && !condominio.getChaveCep().isEmpty() ) {
            this.chaveCepTV.setText(condominio.getChaveCep());
        } else {
            this.chaveCepTV.setText("-");
        }

        this.cnlTV = (TextView) relativeLayout.findViewById(R.id.cnlTV);
        if (condominio.getCnl() != null
                && !condominio.getCnl().isEmpty()){
            this.cnlTV.setText(condominio.getCnl());
        } else {
            this.cnlTV.setText("-");
        }

        this.statusInfraTV = (TextView) relativeLayout.findViewById(R.id.statusInfraTV);
        if( condominio.getCondominioStatusInfra() != null
                && !condominio.getCondominioStatusInfra().getDescricao().isEmpty()) {
            this.statusInfraTV.setText(condominio.getCondominioStatusInfra().getDescricao());
        } else {
            this.statusInfraTV.setText("-");
        }

        this.dataConclInfraTV = (TextView) relativeLayout.findViewById(R.id.dataConclInfraTV);
        if ( condominio.getDataConclInfra() != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");
            String dataConclInfra = simpleDateFormat.format(condominio.getDataConclInfra().getTime());
            this.dataConclInfraTV.setText(dataConclInfra);
        } else {
            this.dataConclInfraTV.setText("-");
        }


        this.capacidadeEquipamentoTV = (TextView) relativeLayout.findViewById(R.id.capacidadeEquipamentoTV);
        if (condominio.getCapacidadeEquipamento() != null ){
            this.capacidadeEquipamentoTV.setText(condominio.getCapacidadeEquipamento().toString());
        } else {
            this.capacidadeEquipamentoTV.setText("-");
        }

        this.ocupacaoEquipamentoTV = (TextView) relativeLayout.findViewById(R.id.ocupacaoEquipamentoTV);
        if (condominio.getOcupacaoEquipamento() != null ){
            this.ocupacaoEquipamentoTV.setText(condominio.getOcupacaoEquipamento().toString());
        } else {
            this.ocupacaoEquipamentoTV.setText("-");
        }

        this.fibraLivreTV = (TextView) relativeLayout.findViewById(R.id.fibraLivreTV);
        if(condominio.getFibraLivre() != null ){
            this.fibraLivreTV.setText(condominio.getFibraLivre().toString());
        } else {
            this.fibraLivreTV.setText("-");
        }

        this.percentualOcupacaoTV = (TextView) relativeLayout.findViewById(R.id.percentualOcupacaoTV);
        if(condominio.getPercentualOcupacao() != null) {
            this.percentualOcupacaoTV.setText(condominio.getPercentualOcupacao().toString());
        } else {
            this.percentualOcupacaoTV.setText("-");
        }

    }

}

