package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioStatusInfra;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class CondominioInformacoesTecnicasFragment extends Fragment{

    private EntityManager   entityManager;
    private Condominio      condominio;
    private Endereco        endereco;
    private RelativeLayout  relativeLayout;
    private List<CondominioStatusInfra> statusInfras;

    private EditText codFttxET;
    private EditText chaveEndET;
    private EditText classeSocialET;
    private EditText prioridadeET;
    private EditText segmentacaoMktET;
    private EditText codLogET;
    private EditText chaveCepET;
    private EditText cnlET;
    private EditText dataConclInfraET;
    private EditText capacidadeEquipamentoET;
    private EditText ocupacaoEquipamentoET;
    private EditText fibraLivreET;
    private EditText percentualOcupacaoET;
    private Spinner spinnerStatusInfra;
    private EditText areaTelefonicaET;
    private MenuItem menuSalvar;
    private MenuItem menuAvancar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_informacoes_tecnicas, container, false);
        entityManager  = new EntityManager(getActivity());
        inicializar();
        carregarSpinnerStatusInfra();
//        menuSalvar =  ((CondominioActivity) getActivity()).getMenuSalvar();
//        menuAvancar = ((CondominioActivity) getActivity()).getMenuAvancar();
//        menuSalvar.setVisible(true);
//        menuAvancar.setVisible(false);

        return relativeLayout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        condominio = ((CondominioActivity) getActivity()).getCondominio();
    }

    private void inicializar() {

        setCodFttxET((EditText) relativeLayout.findViewById(R.id.codFttxET));
        setChaveEndET((EditText) relativeLayout.findViewById(R.id.chaveEndET));
        setAreaTelefonicaET((EditText) relativeLayout.findViewById(R.id.areaTelefonicaET));
        setPrioridadeET((EditText) relativeLayout.findViewById(R.id.prioridadeET));
        setSegmentacaoMktET((EditText) relativeLayout.findViewById(R.id.segmentacaoMktET));
        setCodLogET((EditText) relativeLayout.findViewById(R.id.codLogET));
        setChaveCepET((EditText) relativeLayout.findViewById(R.id.chaveCepET));
        setCnlET((EditText) relativeLayout.findViewById(R.id.cnlET));
        setDataConclInfraET((EditText) relativeLayout.findViewById(R.id.dataConclInfraET));
        setCapacidadeEquipamentoET((EditText) relativeLayout.findViewById(R.id.capacidadeEquipamentoET));
        setOcupacaoEquipamentoET((EditText) relativeLayout.findViewById(R.id.ocupacaoEquipamentoET));
        setFibraLivreET((EditText) relativeLayout.findViewById(R.id.fibraLivreET));
        setPercentualOcupacaoET((EditText) relativeLayout.findViewById(R.id.percentualOcupacaoET));
        setSpinnerStatusInfra((Spinner)  relativeLayout.findViewById(R.id.spinnerStatusInfra));

    }

    private void carregarSpinnerStatusInfra() {
        try {
            setStatusInfras(entityManager.getAll(CondominioStatusInfra.class));
            List<String> nomesStatusInfra = new ArrayList<>();
            nomesStatusInfra.add(UtilActivity.SELECIONE);

            for (CondominioStatusInfra statusInfra : getStatusInfras()) {
                nomesStatusInfra.add(statusInfra.getDescricao());
            }
            UtilActivity.setAdapter((ActionBarActivity) getActivity(), getSpinnerStatusInfra(), nomesStatusInfra);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        if (this.isVisible()) {
//            if (!isVisibleToUser) {
//                popularCondominioComInformacoesTecnicas();
//            }
//        }
//        super.setUserVisibleHint(isVisibleToUser);
//    }
//
//    private void popularCondominioComInformacoesTecnicas() {
//        try {
//            setSpinnerStatusInfra((Spinner)  relativeLayout.findViewById(R.id.spinnerStatusInfra));
//
//            condominio.setCodFttx(getCodFttxET().getText().toString());
//            condominio.setChaveEndereco(getChaveEndET().getText().toString());
//            condominio.setClasseSocial(getClasseSocialET().getText().toString());
//            condominio.setPrioridade(getPrioridadeET().getText().toString());
//            condominio.setSegmentacaoMkt(getSegmentacaoMktET().getText().toString());
//            condominio.setCodLog(getCodLogET().getText().toString());
//            condominio.setChaveCep(getChaveCepET().getText().toString());
//            condominio.setCnl(getCnlET().getText().toString());
//            condominio.setDataConclInfra(UtilActivity.ConverterStringParaCalendar(getDataConclInfraET().getText().toString()));
//            condominio.setCapacidadeEquipamento(Integer.valueOf(validarVazioCampo(getCapacidadeEquipamentoET().getText().toString())));
//            condominio.setOcupacaoEquipamento(Integer.valueOf(validarVazioCampo(getOcupacaoEquipamentoET().getText().toString())));
//            condominio.setFibraLivre(Integer.valueOf(validarVazioCampo(getFibraLivreET().getText().toString())));
//            condominio.setPercentualOcupacao(Double.valueOf(validarVazioCampo(getPercentualOcupacaoET().getText().toString())));
//            condominio.setCondominioStatusInfra(getStatusInfras().get(getSpinnerStatusInfra().getSelectedItemPosition() - SistemaConstantes.UM));
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

    private String validarVazioCampo(String campo){
        return campo.trim().equals("")? "0" : campo;
    }

    public List<CondominioStatusInfra> getStatusInfras() {
        return statusInfras;
    }

    public void setStatusInfras(List<CondominioStatusInfra> statusInfras) {
        this.statusInfras = statusInfras;
    }

    public EditText getCodFttxET() {
        return codFttxET;
    }

    public void setCodFttxET(EditText codFttxET) {
        this.codFttxET = codFttxET;
    }

    public EditText getChaveEndET() {
        return chaveEndET;
    }

    public void setChaveEndET(EditText chaveEndET) {
        this.chaveEndET = chaveEndET;
    }

    public EditText getClasseSocialET() {
        return classeSocialET;
    }

    public void setClasseSocialET(EditText classeSocialET) {
        this.classeSocialET = classeSocialET;
    }

    public EditText getPrioridadeET() {
        return prioridadeET;
    }

    public void setPrioridadeET(EditText prioridadeET) {
        this.prioridadeET = prioridadeET;
    }

    public EditText getSegmentacaoMktET() {
        return segmentacaoMktET;
    }

    public void setSegmentacaoMktET(EditText segmentacaoMktET) {
        this.segmentacaoMktET = segmentacaoMktET;
    }

    public EditText getCodLogET() {
        return codLogET;
    }

    public void setCodLogET(EditText codLogET) {
        this.codLogET = codLogET;
    }

    public EditText getChaveCepET() {
        return chaveCepET;
    }

    public void setChaveCepET(EditText chaveCepET) {
        this.chaveCepET = chaveCepET;
    }

    public EditText getCnlET() {
        return cnlET;
    }

    public void setCnlET(EditText cnlET) {
        this.cnlET = cnlET;
    }

    public EditText getDataConclInfraET() {
        return dataConclInfraET;
    }

    public void setDataConclInfraET(EditText dataConclInfraET) {
        this.dataConclInfraET = dataConclInfraET;
    }

    public EditText getCapacidadeEquipamentoET() {
        return capacidadeEquipamentoET;
    }

    public void setCapacidadeEquipamentoET(EditText capacidadeEquipamentoET) {
        this.capacidadeEquipamentoET = capacidadeEquipamentoET;
    }

    public EditText getOcupacaoEquipamentoET() {
        return ocupacaoEquipamentoET;
    }

    public void setOcupacaoEquipamentoET(EditText ocupacaoEquipamentoET) {
        this.ocupacaoEquipamentoET = ocupacaoEquipamentoET;
    }

    public EditText getFibraLivreET() {
        return fibraLivreET;
    }

    public void setFibraLivreET(EditText fibraLivreET) {
        this.fibraLivreET = fibraLivreET;
    }

    public EditText getPercentualOcupacaoET() {
        return percentualOcupacaoET;
    }

    public void setPercentualOcupacaoET(EditText percentualOcupacaoET) {
        this.percentualOcupacaoET = percentualOcupacaoET;
    }

    public Spinner getSpinnerStatusInfra() {
        return spinnerStatusInfra;
    }

    public void setSpinnerStatusInfra(Spinner spinnerStatusInfra) {
        this.spinnerStatusInfra = spinnerStatusInfra;
    }

    public EditText getAreaTelefonicaET() {
        return areaTelefonicaET;
    }

    public void setAreaTelefonicaET(EditText areaTelefonicaET) {
        this.areaTelefonicaET = areaTelefonicaET;
    }
}
