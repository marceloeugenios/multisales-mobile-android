package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;

/**
 * Created by eric on 26-02-2015.
 */
public class CondominioListAdapter extends BaseAdapter {
    private Context context;
    private List<Condominio> condominios;

    public CondominioListAdapter(Context context, List<Condominio> condominios){
        this.context = context;
        this.condominios = condominios;
    }

    @Override
    public int getCount() {
        if (condominios != null) {
            return condominios.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return condominios.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_condominio, null);
        }

        TextView nomeCondominio = (TextView) convertView.findViewById(R.id.nomeCondominio);

        nomeCondominio.setText(condominios.get(position).getNome());


        return convertView;
    }
}
