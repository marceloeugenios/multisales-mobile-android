package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogDiario;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioPlantao;
import multisales.mobile.nx.com.br.multisalesmobile.servico.LogDiarioServico;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class CondominioPlantaoActivity extends ActionBarActivity {


    private EntityManager entityManager;
    private CondominioPlantao condominioPlantao;
    public AsyncTask<Void, Void, ResponseD2D> taskCondominio;
    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    private LogDiarioServico logDiarioServico;
    private String login;
    private Condominio condominio;

    private TextView condominioNomeTV;
    private EditText dataInicioET;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condominio_plantao);

        entityManager       = new EntityManager(this);
        logDiarioServico    = new LogDiarioServico(this);
        login               = getIntent().getStringExtra("login");
        condominioPlantao   = new CondominioPlantao();
        try{
            condominio = entityManager.getById(Condominio.class,getIntent().getIntExtra("idCondominio", SistemaConstantes.ZERO));
        } catch (Exception e ){

        }
        this.condominioNomeTV = (TextView) this.findViewById(R.id.condominioNomeTV);
        dataInicioET = ((EditText) this.findViewById(R.id.dataInicioET));
        this.condominioNomeTV.setText(condominio.getNome());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_condominio_plantao, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_salvar_condominio_plantao) {
            try {
                condominioPlantao.setDataInicio(UtilActivity.ConverterStringParaCalendar(this.dataInicioET.getText().toString()));
                condominioPlantao.setCondominio(condominio);

            }catch (Exception e){
                UtilActivity.makeShortToast("Erro ao cadastrar plantão.", getApplicationContext());
                e.printStackTrace();
                return false;
            }

            try {
                condominioPlantao = entityManager.save(condominioPlantao);
                taskCondominio = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D = null;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.CONDOMINIO_PLANTAO.getCodigo());
                            requestD2D.setCondominioPlantao(condominioPlantao);
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        Intent mainIntent = new Intent(CondominioPlantaoActivity.this, MainActivity.class);
                        if(resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                condominioPlantao.setId((Integer)resposta.getExtras().get("idCondominioPlantao"));
                                entityManager.atualizar(condominioPlantao);
                                mainIntent.putExtra("mensagem", "Plantão " + condominioPlantao.getIdLocal() + " salvo com SUCESSO!");
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("ERRO ao salvar Plantão!", getApplicationContext());
                            }
                        } else {
                            mainIntent.putExtra("mensagem", "Plantão salvo mas não sincronizado!");
                        }
                        startActivity(mainIntent);
                        CondominioPlantaoActivity.this.finish();
                    }
                };
                taskCondominio.execute();

            } catch (DataBaseException e) {
                e.printStackTrace();
                UtilActivity.makeShortToast("ERRO ao salvar Plantão!",this);
                return false;
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
