package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import multisales.mobile.nx.com.br.multisalesmobile.R;

/**
 * Created by emanoela on 24/03/15.
 */
public class CondominioPlantaoEstatisticaActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condominio_plantao_estatistica);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_condominio_plantao_estatistica, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_iniciar_trabalho) {
            Toast.makeText(getApplicationContext(), "Checkin realizado com sucesso!", Toast.LENGTH_SHORT).show();
            this.finish();
        }

        if (id == R.id.action_finalizar_trabalho) {
            Toast.makeText(getApplicationContext(), "Checkout realizado com sucesso!", Toast.LENGTH_SHORT).show();
            this.finish();
        }


        return super.onOptionsItemSelected(item);
    }


}
