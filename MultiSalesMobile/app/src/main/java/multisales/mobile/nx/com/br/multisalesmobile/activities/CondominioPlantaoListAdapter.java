package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioPlantao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class CondominioPlantaoListAdapter extends BaseAdapter {
    private Context context;
    private List<CondominioPlantao> condominioPlantoes;

    public CondominioPlantaoListAdapter(Context context, List<CondominioPlantao> condominioPlantoes){
        this.context = context;
        this.condominioPlantoes = condominioPlantoes;
    }

    @Override
    public int getCount() {
        if (condominioPlantoes != null) {
            return condominioPlantoes.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return condominioPlantoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleDateFormat simpleDateFormatData = new SimpleDateFormat("dd/MM/yyyy");

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_condominio_plantao, null);
        }

        TextView dataInicioTV = (TextView) convertView.findViewById(R.id.dataInicioTV);

        dataInicioTV.setText(simpleDateFormatData.format(condominioPlantoes.get(position).getDataInicio().getTime()));


        return convertView;
    }
}
