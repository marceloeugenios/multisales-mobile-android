package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContato;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioPlantao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class CondominioPlantoesActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private ListView            listView;
    private EntityManager       entityManager;
    private Condominio          condominio;
    private RelativeLayout      syncRL;
    private Button              sincronizarBT;
    private Integer             alturaSync;
    private RequestD2D          requestD2D = new RequestD2D();
    private JsonParser          jsonParser = new JsonParser();
    private D2DRestClient       d2DRestClient = new D2DRestClient();
    private CondominioPlantao   condominioPlantaoAtual;
    private int                 contadorSincronizacao = 0;
    private boolean             syncRLIsVisible = false;

    private List<CondominioPlantao> naoSincronizados;
    private List<CondominioPlantao> condominioPlantoes;
    private AsyncTask<Void, Void, ResponseD2D> taskCondominioPlantao;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condominio_plantoes);

        syncRLIsVisible     = true;
        entityManager       = new EntityManager(this);
        condominioPlantoes  = listarCondominioPlantoes();
        listView            = (ListView) findViewById(R.id.listView);

        try{
            condominio = entityManager.getById(Condominio.class,getIntent().getIntExtra("idCondominio", SistemaConstantes.ZERO));
        } catch (Exception e ){

        }
        CondominioPlantaoListAdapter adapter = new CondominioPlantaoListAdapter(this, condominioPlantoes);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        syncRL           = (RelativeLayout) findViewById(R.id.syncRL);
        sincronizarBT    = (Button) findViewById(R.id.sincronizarBT);
        naoSincronizados = new ArrayList<>();
        if(condominioPlantoes != null ){
            for (CondominioPlantao condominioPlantao : condominioPlantoes) {
            if (condominioPlantao.getId() == null) {
                naoSincronizados.add(condominioPlantao);
            }
            }
        }


        if (naoSincronizados.size() > 0 && UtilActivity.isOnline(this)) {

            ViewTreeObserver vto = syncRL.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    alturaSync = syncRL.getMeasuredHeight();
                }
            });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(syncRLIsVisible) {
                        syncRL.animate().setDuration(500);
                        syncRL.animate().translationY(alturaSync);
                    }
                }
            }, 5000);

        } else {

            syncRL.setVisibility(View.INVISIBLE);

        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private List<CondominioPlantao> listarCondominioPlantoes() {
        try {
            List<CondominioPlantao> condominioPlantoes = entityManager.getAll(CondominioPlantao.class);

            for (CondominioPlantao condominioPlanta : condominioPlantoes) {
                entityManager.initialize(condominioPlanta.getCondominio());
            }
            return condominioPlantoes;
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao listar os plantões", this);
            finish();
        }

        return null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_condominio_plantoes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        if (id == R.id.action_sincronizar) {

            sincronizarCondominioPlantoes();
        }

        if (id == R.id.action_condominio_plantao) {
            Intent intentCondominioPlantao = new Intent(this, CondominioPlantaoActivity.class);
            intentCondominioPlantao.putExtra("idCondominio", condominio.getIdLocal());
            startActivity(intentCondominioPlantao);
            CondominioPlantoesActivity.this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentCondominioPlantaoEstatistica = new Intent(this, CondominioPlantaoEstatisticaActivity.class);
        startActivity(intentCondominioPlantaoEstatistica);
        this.finish();
    }

    public void sincronizar(View view) {
        sincronizarBT.setEnabled(false);
        syncRL.animate().setDuration(500);
        syncRL.animate().translationY(alturaSync);
        sincronizarCondominioPlantoes();
    }

    private void sincronizarCondominioPlantoes() {
        try {
            if (naoSincronizados.size() > 0) {
                condominioPlantaoAtual = naoSincronizados.get(contadorSincronizacao);


                taskCondominioPlantao = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D = null;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.CONDOMINIO.getCodigo());
                            requestD2D.setCondominioPlantao(condominioPlantaoAtual);
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        if (resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                condominioPlantaoAtual.setId((Integer) resposta.getExtras().get("idPlantao"));
                                entityManager.atualizar(condominioPlantaoAtual);
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("ERRO ao salvar Condomínio!", getApplicationContext());
                            }
                            contadorSincronizacao++;
                            if (naoSincronizados.size() == contadorSincronizacao) {
                                naoSincronizados = new ArrayList<>();
                                condominioPlantoes = listarCondominioPlantoes();
                                for (CondominioPlantao condominioPlantao : condominioPlantoes) {
                                    if (condominioPlantao.getId() == null) {
                                        naoSincronizados.add(condominioPlantao);
                                    }
                                }
                                if (naoSincronizados.size() == 0) {
                                    UtilActivity.makeShortToast("Todos plantões sincronizados com sucesso!", getApplicationContext());
                                }

                                CondominioPlantaoListAdapter adapter = new CondominioPlantaoListAdapter(CondominioPlantoesActivity.this, condominioPlantoes);
                                listView.setAdapter(adapter);
                            } else {
                                sincronizarCondominioPlantoes();
                            }
                        }
                    }
                };
                taskCondominioPlantao.execute();
            } else {
                UtilActivity.makeShortToast("Não há plantões para sincronizar", getApplicationContext());
            }

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar plantão!", this);
        }

    }
}
