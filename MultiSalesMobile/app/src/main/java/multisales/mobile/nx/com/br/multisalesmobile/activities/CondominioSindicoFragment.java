package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContato;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContatoCargo;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class CondominioSindicoFragment extends Fragment  implements View.OnClickListener {

    private RelativeLayout      relativeLayout;
    private EntityManager       entityManager;
    private Condominio          condominio;
    private CondominioContato   condominioContato;
    private List<CondominioContato> condominioContatos;

    private EditText nomeET;
    private EditText dataNascimentoET;
    private EditText apartamentoET;
    private EditText telResidencialET;
    private EditText telCelularET;
    private EditText telComercialET;
    private EditText dataLimiteMandatoET;
    private EditText timeET;
    private DatePickerDialog dataDPD;
    private MenuItem menuSalvar;
    private MenuItem menuAvancar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_sindico, container, false);
        entityManager  = new EntityManager(getActivity());
        inicializar();
//        menuSalvar =  ((CondominioActivity) getActivity()).getMenuSalvar();
//        menuAvancar = ((CondominioActivity) getActivity()).getMenuAvancar();
//        menuSalvar.setVisible(true);
//        menuAvancar.setVisible(false);

        return relativeLayout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        condominio = ((CondominioActivity) getActivity()).getCondominio();
        condominioContatos  = ((CondominioActivity) getActivity()).getCondominioContatos();
        condominioContato = new CondominioContato();
    }

    private void inicializar() {
        setNomeET((EditText) relativeLayout.findViewById(R.id.nomeET));
        setDataNascimentoET((EditText) relativeLayout.findViewById(R.id.dataNascimentoET));
        setApartamentoET((EditText) relativeLayout.findViewById(R.id.apartamentoET));
        setTelResidencialET((EditText) relativeLayout.findViewById(R.id.telResidencialET));
        UtilMask.setMascaraTelefone(telResidencialET);
        setTelCelularET((EditText) relativeLayout.findViewById(R.id.telCelularET));
        UtilMask.setMascaraTelefone(telCelularET);
        setTelComercialET((EditText) relativeLayout.findViewById(R.id.telComercialET));
        UtilMask.setMascaraTelefone(telComercialET);
        setDataLimiteMandatoET((EditText) relativeLayout.findViewById(R.id.dataLimiteMandatoET));
        setTimeET((EditText) relativeLayout.findViewById(R.id.timeET));
    }


    @Override
    public void onClick(View view) {
        showPicker(view);
    }

    private void showPicker(View view) {
        if(view == dataNascimentoET) {
            dataDPD.show();
        }
    }
    private String validarVazioCampo(String campo){
        return campo.trim().equals("")? "0" : campo;
    }

    public EditText getNomeET() {
        return nomeET;
    }

    public void setNomeET(EditText nomeET) {
        this.nomeET = nomeET;
    }

    public EditText getDataNascimentoET() {
        return dataNascimentoET;
    }

    public void setDataNascimentoET(EditText dataNascimentoET) {
        this.dataNascimentoET = dataNascimentoET;
    }

    public EditText getApartamentoET() {
        return apartamentoET;
    }

    public void setApartamentoET(EditText apartamentoET) {
        this.apartamentoET = apartamentoET;
    }

    public EditText getTelResidencialET() {
        return telResidencialET;
    }

    public void setTelResidencialET(EditText telResidencialET) {
        this.telResidencialET = telResidencialET;
    }

    public EditText getTelCelularET() {
        return telCelularET;
    }

    public void setTelCelularET(EditText telCelularET) {
        this.telCelularET = telCelularET;
    }

    public EditText getTelComercialET() {
        return telComercialET;
    }

    public void setTelComercialET(EditText telComercialET) {
        this.telComercialET = telComercialET;
    }

    public EditText getDataLimiteMandatoET() {
        return dataLimiteMandatoET;
    }

    public void setDataLimiteMandatoET(EditText dataLimiteMandatoET) {
        this.dataLimiteMandatoET = dataLimiteMandatoET;
    }

    public EditText getTimeET() {
        return timeET;
    }

    public void setTimeET(EditText timeET) {
        this.timeET = timeET;
    }
}
