package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContato;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class CondominioZeladorDetalheFragment extends Fragment{

    private EntityManager entityManager;
    private RelativeLayout relativeLayout;
    private Condominio condominio;
    private CondominioContato condominioContato;
    private TextView nomeZeladorTV;
    private TextView dataNascimentoTV;
    private TextView apartamentoTV;
    private TextView telefoneResidencialTV;
    private TextView telefoneCelularTV;
    private TextView telefoneComercialTV;
    private TextView telefonePortariaTV;
    private TextView timeTV;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        entityManager = new EntityManager(getActivity());
        this.condominio = ((CondominioDetalheActivity) getActivity()).getCondominio();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }
        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_zelador_detalhe, container, false);

        criarCamposEntrada();

        return relativeLayout;
    }


    public void criarCamposEntrada(){


        if(condominio.getCondominioContatos().size() > 0) {

            int aux = 0;

            for (int i = 0; i < condominio.getCondominioContatos().size(); i++) {

                condominioContato = condominio.getCondominioContatos().get(i);
                try {
                    entityManager.initialize(condominioContato.getCondominioContatoCargo());

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (condominioContato.getCondominioContatoCargo().getDescricao().equals("ZELADOR")) {

                    aux++;

                    this.nomeZeladorTV = (TextView) relativeLayout.findViewById(R.id.nomeZeladorTV);
                    if (condominioContato.getNome() != null
                            && !condominioContato.getNome().isEmpty()) {
                        this.nomeZeladorTV.setText(condominioContato.getNome());
                    } else {
                        this.nomeZeladorTV.setText("-");
                    }

                    this.dataNascimentoTV = (TextView) relativeLayout.findViewById(R.id.dataNascimentoTV);
                    if (condominioContato.getDataNascimento() != null) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String dataNascimento = simpleDateFormat.format(condominioContato.getDataNascimento().getTime());
                        this.dataNascimentoTV.setText(dataNascimento);
                    } else {
                        this.dataNascimentoTV.setText("-");
                    }

                    this.apartamentoTV = (TextView) relativeLayout.findViewById(R.id.apartamentoTV);
                    if (condominioContato.getNumeroApartamento() != null
                            && !condominioContato.getNumeroApartamento().isEmpty()) {
                        this.apartamentoTV.setText(condominioContato.getNumeroApartamento());
                    } else {
                        this.apartamentoTV.setText("-");
                    }

                    this.telefoneResidencialTV = (TextView) relativeLayout.findViewById(R.id.telefoneResidencialTV);
                    if (condominioContato.getTelefoneResidencial() != null
                            && !condominioContato.getTelefoneResidencial().isEmpty()) {
                        this.telefoneResidencialTV.setText(UtilActivity.formatarTelefone(condominioContato.getTelefoneResidencial()));
                    } else {
                        this.telefoneResidencialTV.setText("-");
                    }

                    this.telefoneCelularTV = (TextView) relativeLayout.findViewById(R.id.telefoneCelularTV);
                    if (condominioContato.getTelefoneCelular() != null
                            && !condominioContato.getTelefoneCelular().isEmpty()) {
                        this.telefoneCelularTV.setText(UtilActivity.formatarTelefone(condominioContato.getTelefoneCelular()));
                    } else {
                        this.telefoneCelularTV.setText("-");
                    }

                    this.telefoneComercialTV = (TextView) relativeLayout.findViewById(R.id.telefoneComercialTV);
                    if (condominioContato.getTelefoneComercial() != null
                            && !condominioContato.getTelefoneComercial().isEmpty()) {
                        this.telefoneComercialTV.setText(UtilActivity.formatarTelefone(condominioContato.getTelefoneComercial()));
                    } else {
                        this.telefoneComercialTV.setText("-");
                    }

                    this.telefonePortariaTV = (TextView) relativeLayout.findViewById(R.id.telefonePortariaTV);
                    if(condominioContato.getTelefonePortaria() != null
                            && !condominioContato.getTelefonePortaria().isEmpty()){
                        this.telefonePortariaTV.setText(UtilActivity.formatarTelefone(condominioContato.getTelefonePortaria()));
                    }else {
                        this.telefonePortariaTV.setText("-");
                    }

                    this.timeTV = (TextView) relativeLayout.findViewById(R.id.timeTV);
                    if(condominioContato.getClube() != null
                            && !condominioContato.getClube().isEmpty()){
                        this.timeTV.setText(condominioContato.getClube());
                    }else {
                        this.timeTV.setText("-");
                    }

                }
            }
            if(aux == 0){
                this.nomeZeladorTV = (TextView) relativeLayout.findViewById(R.id.nomeZeladorTV);
                this.nomeZeladorTV.setText("-");
                this.dataNascimentoTV = (TextView) relativeLayout.findViewById(R.id.dataNascimentoTV);
                this.dataNascimentoTV.setText("-");
                this.apartamentoTV = (TextView) relativeLayout.findViewById(R.id.apartamentoTV);
                this.apartamentoTV.setText("-");
                this.telefoneResidencialTV = (TextView) relativeLayout.findViewById(R.id.telefoneResidencialTV);
                this.telefoneResidencialTV.setText("-");
                this.telefoneCelularTV = (TextView) relativeLayout.findViewById(R.id.telefoneCelularTV);
                this.telefoneCelularTV.setText("-");
                this.telefoneComercialTV = (TextView) relativeLayout.findViewById(R.id.telefoneComercialTV);
                this.telefoneComercialTV.setText("-");
                this.telefonePortariaTV = (TextView) relativeLayout.findViewById(R.id.telefonePortariaTV);
                this.telefonePortariaTV.setText("-");
                this.timeTV = (TextView) relativeLayout.findViewById(R.id.timeTV);
                this.timeTV.setText("-");
            }

        } else {
            this.nomeZeladorTV = (TextView) relativeLayout.findViewById(R.id.nomeZeladorTV);
            this.nomeZeladorTV.setText("-");
            this.dataNascimentoTV = (TextView) relativeLayout.findViewById(R.id.dataNascimentoTV);
            this.dataNascimentoTV.setText("-");
            this.apartamentoTV = (TextView) relativeLayout.findViewById(R.id.apartamentoTV);
            this.apartamentoTV.setText("-");
            this.telefoneResidencialTV = (TextView) relativeLayout.findViewById(R.id.telefoneResidencialTV);
            this.telefoneResidencialTV.setText("-");
            this.telefoneCelularTV = (TextView) relativeLayout.findViewById(R.id.telefoneCelularTV);
            this.telefoneCelularTV.setText("-");
            this.telefoneComercialTV = (TextView) relativeLayout.findViewById(R.id.telefoneComercialTV);
            this.telefoneComercialTV.setText("-");
            this.telefonePortariaTV = (TextView) relativeLayout.findViewById(R.id.telefonePortariaTV);
            this.telefonePortariaTV.setText("-");
            this.timeTV = (TextView) relativeLayout.findViewById(R.id.timeTV);
            this.timeTV.setText("-");
           }
    }


    public TextView getTimeTV() {
        return timeTV;
    }

    public void setTimeTV(TextView timeTV) {
        this.timeTV = timeTV;
    }
}

