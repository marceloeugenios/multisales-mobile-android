package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContato;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContatoCargo;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class CondominioZeladorFragment extends Fragment{

    private RelativeLayout      relativeLayout;
    private EntityManager       entityManager;
    private Condominio          condominio;
    private CondominioContato   condominioContato;
    private List<CondominioContato> condominioContatos;

    private EditText nomeET;
    private EditText dataNascimentoET;
    private EditText apartamentoET;
    private EditText telResidencialET;
    private EditText telCelularET;
    private EditText telComercialET;
    private EditText telPortariaET;
    private EditText timeET;
    private MenuItem menuSalvar;
    private MenuItem menuAvancar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_condominio_zelador, container, false);
        entityManager  = new EntityManager(getActivity());
//        menuSalvar =  ((CondominioActivity) getActivity()).getMenuSalvar();
//        menuAvancar = ((CondominioActivity) getActivity()).getMenuAvancar();
//        menuSalvar.setVisible(true);
//        menuAvancar.setVisible(false);
        inicializar();

        return relativeLayout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        condominio = ((CondominioActivity) getActivity()).getCondominio();
        condominioContatos  = ((CondominioActivity) getActivity()).getCondominioContatos();
        condominioContato = new CondominioContato();
    }

    private void inicializar() {
        setNomeET((EditText) relativeLayout.findViewById(R.id.nomeET));
        setDataNascimentoET((EditText) relativeLayout.findViewById(R.id.dataNascimentoET));
        setApartamentoET((EditText) relativeLayout.findViewById(R.id.apartamentoET));
        setTelResidencialET((EditText) relativeLayout.findViewById(R.id.telResidencialET));
        UtilMask.setMascaraTelefone(telResidencialET);
        setTelCelularET((EditText) relativeLayout.findViewById(R.id.telCelularET));
        UtilMask.setMascaraTelefone(telCelularET);
        setTelComercialET((EditText) relativeLayout.findViewById(R.id.telComercialET));
        UtilMask.setMascaraTelefone(telComercialET);
        setTelPortariaET((EditText) relativeLayout.findViewById(R.id.telPortariaET));
        UtilMask.setMascaraTelefone(telPortariaET);
        setTimeET((EditText) relativeLayout.findViewById(R.id.timeET));
    }

    private String validarVazioCampo(String campo){
        return campo.trim().equals("")? "0" : campo;
    }

    public EditText getNomeET() {
        return nomeET;
    }

    public void setNomeET(EditText nomeET) {
        this.nomeET = nomeET;
    }

    public EditText getDataNascimentoET() {
        return dataNascimentoET;
    }

    public void setDataNascimentoET(EditText dataNascimentoET) {
        this.dataNascimentoET = dataNascimentoET;
    }

    public EditText getApartamentoET() {
        return apartamentoET;
    }

    public void setApartamentoET(EditText apartamentoET) {
        this.apartamentoET = apartamentoET;
    }

    public EditText getTelResidencialET() {
        return telResidencialET;
    }

    public void setTelResidencialET(EditText telResidencialET) {
        this.telResidencialET = telResidencialET;
    }

    public EditText getTelCelularET() {
        return telCelularET;
    }

    public void setTelCelularET(EditText telCelularET) {
        this.telCelularET = telCelularET;
    }

    public EditText getTelComercialET() {
        return telComercialET;
    }

    public void setTelComercialET(EditText telComercialET) {
        this.telComercialET = telComercialET;
    }

    public EditText getTelPortariaET() {
        return telPortariaET;
    }

    public void setTelPortariaET(EditText telPortariaET) {
        this.telPortariaET = telPortariaET;
    }

    public EditText getTimeET() {
        return timeET;
    }

    public void setTimeET(EditText timeET) {
        this.timeET = timeET;
    }
}
