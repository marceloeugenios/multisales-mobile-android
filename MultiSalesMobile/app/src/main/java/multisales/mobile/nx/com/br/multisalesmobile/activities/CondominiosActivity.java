package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContato;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class CondominiosActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private ListView listView;
    private EntityManager entityManager;
    private List<Condominio> condominios;
    private RelativeLayout syncRL;
    private Button sincronizarBT;
    private Integer alturaSync;
    private AsyncTask<Void, Void, ResponseD2D> taskCondominio;
    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    private Condominio condominioAtual;
    private List<Condominio> naoSincronizados;
    private int contadorSincronizacao = 0;
    private boolean syncRLIsVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condominios);
        syncRLIsVisible = true;
        entityManager = new EntityManager(this);

        condominios = listarCondominios();

        CondominioListAdapter adapter = new CondominioListAdapter(this, condominios);

        listView = (ListView) findViewById(R.id.listView);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        syncRL = (RelativeLayout) findViewById(R.id.syncRL);
        sincronizarBT = (Button) findViewById(R.id.sincronizarBT);

        naoSincronizados = new ArrayList<>();
        for (Condominio condominio : condominios) {
            if (condominio.getId() == null) {
                naoSincronizados.add(condominio);
            }
        }

        if (naoSincronizados.size() > 0 && UtilActivity.isOnline(this)) {

            ViewTreeObserver vto = syncRL.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    alturaSync = syncRL.getMeasuredHeight();
                }
            });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(syncRLIsVisible) {
                        syncRL.animate().setDuration(500);
                        syncRL.animate().translationY(alturaSync);
                    }
                }
            }, 5000);

        } else {

            syncRL.setVisibility(View.INVISIBLE);

        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private List<Condominio> listarCondominios() {
        try {
            List<Condominio> condominios = entityManager.getAll(Condominio.class);

            for (Condominio condominio : condominios) {
                entityManager.initialize(condominio.getEndereco());
                entityManager.initialize(condominio.getCondominioContatos());
            }
            return condominios;
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao listar os condomínios", this);
            finish();
        }

        return null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_condominios, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            
        }

        if (id == R.id.action_sincronizar) {

            sincronizarCondominios();
        }

        if (id == R.id.action_condominio) {
            Intent intentCondominio = new Intent(this, CondominioActivity.class);
            startActivity(intentCondominio);
            CondominiosActivity.this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentDetalhes = new Intent(this, CondominioDetalheActivity.class);
        intentDetalhes.putExtra("idCondominio", condominios.get(position).getIdLocal());
        startActivity(intentDetalhes);
        this.finish();
    }

    public void sincronizar(View view) {
        sincronizarBT.setEnabled(false);
        syncRL.animate().setDuration(500);
        syncRL.animate().translationY(alturaSync);
        sincronizarCondominios();
    }

    private void sincronizarCondominios() {
        try {
            if (naoSincronizados.size() > 0) {
                condominioAtual = naoSincronizados.get(contadorSincronizacao);
                condominioAtual.setCondominioContatos(entityManager.getByWhere(CondominioContato.class,"_condominio = "+condominioAtual.getIdLocal(),null));
                entityManager.initialize(condominioAtual.getEndereco());
                condominioAtual.getEndereco().setNullId();
                if(condominioAtual.getCondominioContatos() != null){
                    for(CondominioContato contato : condominioAtual.getCondominioContatos()){
                        contato.setNullId();
                    }
                }


                taskCondominio = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D = null;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.CONDOMINIO.getCodigo());
                            requestD2D.setCondominio(condominioAtual);
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        if (resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                condominioAtual.setId((Integer) resposta.getExtras().get("idCondominio"));
                                entityManager.atualizar(condominioAtual);
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("ERRO ao salvar Condomínio!", getApplicationContext());
                            }
                            contadorSincronizacao++;
                            if (naoSincronizados.size() == contadorSincronizacao) {
                                naoSincronizados = new ArrayList<>();
                                condominios = listarCondominios();
                                for (Condominio condominio : condominios) {
                                    if (condominio.getId() == null) {
                                        naoSincronizados.add(condominio);
                                    }
                                }
                                if (naoSincronizados.size() == 0) {
                                    UtilActivity.makeShortToast("Todos condomínios sincronizados com sucesso!", getApplicationContext());
                                }

                                CondominioListAdapter adapter = new CondominioListAdapter(CondominiosActivity.this, condominios);
                                listView.setAdapter(adapter);
                            } else {
                                sincronizarCondominios();
                            }
                        }
                    }
                };
                taskCondominio.execute();
            } else {
                UtilActivity.makeShortToast("Não há condomínios para sincronizar", getApplicationContext());
            }

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar Condomínio!", this);
        }

    }
}
