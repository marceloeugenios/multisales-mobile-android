package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.animation.Animator;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Atualizacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ENotificacaoCodigo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VersaoPacote;
import multisales.mobile.nx.com.br.multisalesmobile.servico.AtualizacaoServico;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class ConfiguracoesActivity extends ActionBarActivity {

    private TextView atualizacaoTV;
    private TextView dataAtualizacaoTV;
    private TextView labelAtualizacaoTV;
    private TextView conexaoTV;
    private TextView modeloTextoTV;
    private TextView versaoTextoTV;
    private TextView pontosAtualizacaoTV;
    private ProgressBar atualizacaoPB;
    private ProgressBar conexaoPB;
    private EntityManager entityManager;
    private MultiSales multiSales;
    private BroadcastReceiver mReceiver;

    private RequestD2D requestD2D;
    private JsonParser jsonParser;
    private D2DRestClient d2DRestClient;
    private Float ultimaVersao;
    private AtualizacaoServico atualizacaoServico;
    private SimpleDateFormat simpleDateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracoes);
        multiSales = (MultiSales) getApplication();
        atualizacaoServico = new AtualizacaoServico(this);
        entityManager = new EntityManager(this);
        jsonParser = new JsonParser();
        d2DRestClient = new D2DRestClient();
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        instanciarComponentes();
        modeloTextoTV.setText(Build.MANUFACTURER.toUpperCase() + " " + Build.MODEL);
        try {
            versaoTextoTV.setText(Build.VERSION.RELEASE);
        } catch (Exception e) {
            versaoTextoTV.setText("");
            e.printStackTrace();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void instanciarComponentes() {
        atualizacaoTV = (TextView) findViewById(R.id.atualizacaoTV);
        dataAtualizacaoTV = (TextView) findViewById(R.id.dataAtualizacaoTV);
        atualizacaoPB = (ProgressBar) findViewById(R.id.atualizacaoPB);
        labelAtualizacaoTV = (TextView) findViewById(R.id.labelAtualizacaoTV);
        pontosAtualizacaoTV = (TextView) findViewById(R.id.pontosAtualizacaoTV);
        modeloTextoTV = (TextView) findViewById(R.id.modeloTextoTV);
        versaoTextoTV = (TextView) findViewById(R.id.versaoTextoTV);
        conexaoTV = (TextView) findViewById(R.id.conexaoTV);
        conexaoPB = (ProgressBar) findViewById(R.id.conexaoPB);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_configuracoes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void verificarAtualizacoes(View view){
        atualizacaoPB.setVisibility(View.VISIBLE);
        pontosAtualizacaoTV.animate().setDuration(300);
        pontosAtualizacaoTV.animate().setStartDelay(0);
        pontosAtualizacaoTV.animate().alpha(1);
        UtilActivity.makeNotification(this,
                ENotificacaoCodigo.ATUALIZACAO,
                R.drawable.ic_stat_notify_app,
                "Multisales",
                "Atualizações: Verificando",
                "Atualizações: Verificando",
                getIntent(),
                false);
        atualizacaoTV.setText("Verificando");
        atualizacaoTV.animate().setDuration(300);
        atualizacaoTV.animate().setStartDelay(0);
        atualizacaoTV.animate().alpha(1);
        try {
            AsyncTask<Void, Void, EBoolean> taskVerificarAtualizacoes = new AsyncTask<Void, Void, EBoolean>() {
                @Override
                protected EBoolean doInBackground(Void... params) {
                    JSONObject jsonEnvio;
                    JSONObject jsonRetorno;
                    ResponseD2D resposta;
                    EBoolean retorno;
                    try {
                        requestD2D = new RequestD2D();
                        List<VersaoPacote> versoes = entityManager.select(VersaoPacote.class, "SELECT versao FROM versao_pacote WHERE id = (SELECT MAX(id) FROM versao_pacote)");
                        if(versoes.size() > 0) {
                            ultimaVersao = versoes.get(0).getVersao();
                            requestD2D.setVersaoPacote(ultimaVersao);
                        } else {
                            requestD2D.setVersaoPacote(0f);
                        }
                        requestD2D.setIdUsuario(multiSales.getIdUsuario());
                        requestD2D.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                        requestD2D.setOperacao(ECodigoOperacaoD2D.VERIFICAR_ATUALIZACOES.getCodigo());
                        jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                        jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                        resposta = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);

                        if (resposta != null) {
                            retorno = EBoolean.valueOf(resposta.getExtras().get("pacotes").toString());
                        } else {
                            retorno = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        retorno = null;
                    }

                    return retorno;
                }

                @Override
                protected void onPostExecute(EBoolean resposta) {
                    if (resposta != null) {
                        if (resposta.equals(EBoolean.FALSE)) {
                            atualizacaoPB.setVisibility(View.INVISIBLE);
                            atualizacaoTV.animate().setDuration(300);
                            atualizacaoTV.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    UtilActivity.makeNotification(ConfiguracoesActivity.this,
                                            ENotificacaoCodigo.ATUALIZACAO,
                                            R.drawable.ic_stat_notify_app,
                                            "Multisales",
                                            "Atualizações: Atualizado",
                                            "Atualizações: Atualizado",
                                            new Intent(ConfiguracoesActivity.this, MainActivity.class),
                                            false);
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            UtilActivity.cancelNotification(ConfiguracoesActivity.this, ENotificacaoCodigo.ATUALIZACAO);
                                        }
                                    }, 10000);
                                    atualizacaoTV.setText("Atualizado");
                                    atualizacaoTV.animate().setDuration(300);
                                    atualizacaoTV.animate().setListener(null);
                                    atualizacaoTV.animate().alpha(1);
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });
                        } else {
                            atualizacaoTV.animate().setDuration(300);
                            atualizacaoTV.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    UtilActivity.makeNotification(ConfiguracoesActivity.this,
                                            ENotificacaoCodigo.ATUALIZACAO,
                                            R.drawable.ic_stat_notify_app,
                                            "Multisales",
                                            "Atualizações: Baixando",
                                            "Atualizações: Baixando",
                                            getIntent(),
                                            false);
                                    atualizacaoTV.setText("Baixando");
                                    atualizacaoTV.animate().setDuration(300);
                                    atualizacaoTV.animate().setListener(null);
                                    atualizacaoTV.animate().alpha(1);
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });
                            verificarPacotes();
                        }
                    } else {
                        atualizacaoPB.setVisibility(View.INVISIBLE);
                        atualizacaoTV.animate().setDuration(300);
                        atualizacaoTV.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                atualizacaoPB.setVisibility(View.INVISIBLE);
                                UtilActivity.makeNotification(ConfiguracoesActivity.this,
                                        ENotificacaoCodigo.ATUALIZACAO,
                                        R.drawable.ic_stat_notify_app,
                                        "Multisales",
                                        "Atualizações: Erro",
                                        "Atualizações: Erro",
                                        getIntent(),
                                        false);
                                atualizacaoTV.setText("Erro");
                                atualizacaoTV.animate().setDuration(300);
                                atualizacaoTV.animate().alpha(1).setListener(new Animator.AnimatorListener() {

                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        pontosAtualizacaoTV.animate().setDuration(300);
                                        pontosAtualizacaoTV.animate().setStartDelay(2000);
                                        pontosAtualizacaoTV.animate().setListener(null);
                                        pontosAtualizacaoTV.animate().alpha(0);
                                        atualizacaoTV.animate().setDuration(300);
                                        atualizacaoTV.animate().setStartDelay(2000);
                                        atualizacaoTV.animate().setListener(null);
                                        atualizacaoTV.animate().alpha(0);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                        UtilActivity.makeShortToast("Erro ao obter versão", ConfiguracoesActivity.this);
                    }
                    super.onPostExecute(resposta);
                }
            };
            taskVerificarAtualizacoes.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verificarPacotes(){
        try {
            AsyncTask<Void, Void, VersaoPacote> taskVerificarPacotes = new AsyncTask<Void, Void, VersaoPacote>() {
                @Override
                protected VersaoPacote doInBackground(Void... params) {
                    JSONObject jsonEnvio;
                    JSONObject jsonRetorno;
                    VersaoPacote resposta;
                    try {
                        requestD2D = new RequestD2D();
                        entityManager.dropTable("versao_pacote");
                        entityManager.createBasicTables();
                        List<VersaoPacote> versoes = entityManager.select(VersaoPacote.class, "SELECT versao FROM versao_pacote WHERE id = (SELECT MAX(id) FROM versao_pacote)");
                        if(versoes.size() > 0) {
                            ultimaVersao = versoes.get(0).getVersao();
                            requestD2D.setVersaoPacote(ultimaVersao);
                        } else {
                            requestD2D.setVersaoPacote(0f);
                        }
                        requestD2D.setIdUsuario(multiSales.getIdUsuario());
                        requestD2D.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                        requestD2D.setOperacao(ECodigoOperacaoD2D.VERIFICAR_PACOTES.getCodigo());
                        jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                        jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                        resposta = jsonParser.parseJsonToObject(jsonRetorno, VersaoPacote.class);

                    } catch (Exception e) {
                        e.printStackTrace();
                        resposta = null;
                    }

                    return resposta;
                }

                @Override
                protected void onPostExecute(VersaoPacote resposta) {
                    if (resposta != null) {
                        atualizacaoTV.animate().setDuration(300);
                        atualizacaoTV.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                UtilActivity.makeNotification(ConfiguracoesActivity.this,
                                        ENotificacaoCodigo.ATUALIZACAO,
                                        R.drawable.ic_stat_notify_app,
                                        "Multisales",
                                        "Atualizações: Instalando",
                                        "Atualizações: Instalando",
                                        getIntent(),
                                        false);
                                atualizacaoTV.setText("Instalando");
                                atualizacaoTV.animate().setDuration(300);
                                atualizacaoTV.animate().setListener(null);
                                atualizacaoTV.animate().alpha(1);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                        instalarPacotes(resposta);
                    } else {
                        atualizacaoPB.setVisibility(View.INVISIBLE);
                        atualizacaoTV.animate().setDuration(300);
                        atualizacaoTV.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                atualizacaoPB.setVisibility(View.INVISIBLE);
                                UtilActivity.makeNotification(ConfiguracoesActivity.this,
                                        ENotificacaoCodigo.ATUALIZACAO,
                                        R.drawable.ic_stat_notify_app,
                                        "Multisales",
                                        "Atualizações: Erro",
                                        "Atualizações: Erro",
                                        getIntent(),
                                        false);
                                atualizacaoTV.setText("Erro");
                                atualizacaoTV.animate().setDuration(300);
                                atualizacaoTV.animate().alpha(1).setListener(new Animator.AnimatorListener() {

                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        pontosAtualizacaoTV.animate().setDuration(300);
                                        pontosAtualizacaoTV.animate().setStartDelay(2000);
                                        pontosAtualizacaoTV.animate().setListener(null);
                                        pontosAtualizacaoTV.animate().alpha(0);
                                        atualizacaoTV.animate().setDuration(300);
                                        atualizacaoTV.animate().setStartDelay(2000);
                                        atualizacaoTV.animate().setListener(null);
                                        atualizacaoTV.animate().alpha(0);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                        UtilActivity.makeShortToast("Erro ao obter versão", ConfiguracoesActivity.this);
                    }

                    super.onPostExecute(resposta);
                }
            };
            taskVerificarPacotes.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void instalarPacotes(final VersaoPacote versaoPacote){
        try {
            AsyncTask<Void, Void, Boolean> taskVerificarPacotes = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    Boolean retorno;
                    try {
                        atualizacaoServico.instalarPacotes(versaoPacote);
                        retorno = Boolean.TRUE;
                    } catch (Exception e) {
                        retorno = Boolean.FALSE;
                    }

                    return retorno;
                }

                @Override
                protected void onPostExecute(Boolean resposta) {
                    atualizacaoPB.setVisibility(View.INVISIBLE);
                    atualizacaoTV.animate().setDuration(300);
                    if (resposta) {
                        Toast.makeText(getApplicationContext(), "Pacotes atualizados com sucesso!", Toast.LENGTH_SHORT).show();
                        try {
                            Atualizacao atualizacao = new Atualizacao();
                            atualizacao.setLogin(multiSales.getLogin());
                            atualizacao.setDataHora(Calendar.getInstance());
                            entityManager.save(atualizacao);
                            atualizacao = atualizacaoServico.obterUltimaAtualizacao(multiSales.getLogin());
                            if (atualizacao != null) {
                                dataAtualizacaoTV.setText(simpleDateFormat.format(atualizacao.getDataHora().getTime()));
                            } else {
                                dataAtualizacaoTV.setText("-");
                            }
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                        }
                        atualizacaoTV.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                atualizacaoPB.setVisibility(View.INVISIBLE);
                                UtilActivity.makeNotification(ConfiguracoesActivity.this,
                                        ENotificacaoCodigo.ATUALIZACAO,
                                        R.drawable.ic_stat_notify_app,
                                        "Multisales",
                                        "Atualizações: Atualizado",
                                        "Atualizações: Atualizado",
                                        new Intent(ConfiguracoesActivity.this, MainActivity.class),
                                        false);
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        UtilActivity.cancelNotification(ConfiguracoesActivity.this, ENotificacaoCodigo.ATUALIZACAO);
                                    }
                                }, 10000);
                                atualizacaoTV.setText("Atualizado");
                                atualizacaoTV.animate().setDuration(300);
                                atualizacaoTV.animate().alpha(1).setListener(new Animator.AnimatorListener() {

                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        pontosAtualizacaoTV.animate().setDuration(300);
                                        pontosAtualizacaoTV.animate().setStartDelay(2000);
                                        pontosAtualizacaoTV.animate().setListener(null);
                                        pontosAtualizacaoTV.animate().alpha(0);
                                        atualizacaoTV.animate().setDuration(300);
                                        atualizacaoTV.animate().setStartDelay(2000);
                                        atualizacaoTV.animate().setListener(null);
                                        atualizacaoTV.animate().alpha(0);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "Erro ao obter versão", Toast.LENGTH_SHORT).show();
                        atualizacaoTV.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                atualizacaoPB.setVisibility(View.INVISIBLE);
                                UtilActivity.makeNotification(ConfiguracoesActivity.this,
                                        ENotificacaoCodigo.ATUALIZACAO,
                                        R.drawable.ic_stat_notify_app,
                                        "Multisales",
                                        "Atualizações: Erro",
                                        "Atualizações: Erro",
                                        getIntent(),
                                        false);
                                atualizacaoTV.setText("Erro");
                                atualizacaoTV.animate().setDuration(300);
                                atualizacaoTV.animate().alpha(1).setListener(new Animator.AnimatorListener() {

                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        pontosAtualizacaoTV.animate().setDuration(300);
                                        pontosAtualizacaoTV.animate().setStartDelay(2000);
                                        pontosAtualizacaoTV.animate().setListener(null);
                                        pontosAtualizacaoTV.animate().alpha(0);
                                        atualizacaoTV.animate().setDuration(300);
                                        atualizacaoTV.animate().setStartDelay(2000);
                                        atualizacaoTV.animate().setListener(null);
                                        atualizacaoTV.animate().alpha(0);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    }
                    super.onPostExecute(resposta);
                }
            };
            taskVerificarPacotes.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intentMain = new Intent(this, MainActivity.class);
        startActivity(intentMain);
    }

    public void redefinir(View view) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        try {
                            if (entityManager.resetDatabase()) {
                                Intent i = getBaseContext().getPackageManager()
                                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                            } else {
                                UtilActivity.makeShortToast("Erro ao redefinir", ConfiguracoesActivity.this);
                            }
                        } catch (Exception e) {
                            UtilActivity.makeShortToast("Erro ao redefinir", ConfiguracoesActivity.this);
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Isso apagará todos os dados do aplicativo. Tem certeza que deseja continuar?").setPositiveButton("OK", dialogClickListener)
                .setNegativeButton("Cancelar", dialogClickListener).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter("ALTERACAO_REDE");

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                verificarConexao();
            }
        };
        //registering our receiver
        this.registerReceiver(mReceiver, intentFilter);
        try {
            Atualizacao atualizacao = atualizacaoServico.obterUltimaAtualizacao(multiSales.getLogin());
            if (atualizacao != null) {
                dataAtualizacaoTV.setText(simpleDateFormat.format(atualizacao.getDataHora().getTime()));
            } else {
                dataAtualizacaoTV.setText("-");
            }
        } catch (DataBaseException e) {
            dataAtualizacaoTV.setText("-");
            e.printStackTrace();
        }
        verificarConexao();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(this.mReceiver);
    }

    private void verificarConexao() {
        try {
            if (!multiSales.getRedeDados()) {
                conexaoTV.setTextColor(getResources().getColor(R.color.corErro));
                conexaoTV.setText("Verifique sua conexão de dados");
            } else {
                conexaoPB.setVisibility(View.VISIBLE);
                conexaoTV.setTextColor(getResources().getColor(R.color.corInfo));
                conexaoTV.setText("Verificando");
                AsyncTask<Void, Void, ResponseD2D> taskVerificarConexao = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D resposta;
                        try {
                            requestD2D = new RequestD2D();
                            requestD2D.setIdUsuario(multiSales.getIdUsuario());
                            requestD2D.setOperacao(ECodigoOperacaoD2D.VERIFICAR_CONEXAO.getCodigo());
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            resposta = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            e.printStackTrace();
                            resposta = null;
                        }

                        return resposta;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        conexaoPB.setVisibility(View.INVISIBLE);
                        if (resposta != null) {
                            if (resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                                conexaoTV.setTextColor(getResources().getColor(R.color.corOk));
                                conexaoTV.setText("OK");
                            } else {
                                conexaoTV.setTextColor(getResources().getColor(R.color.corErro));
                                conexaoTV.setText("ERRO");
                            }
                        } else {
                            conexaoTV.setText("ERRO");
                            conexaoTV.setTextColor(getResources().getColor(R.color.corErro));
                        }

                        super.onPostExecute(resposta);
                    }
                };
                taskVerificarConexao.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


