package multisales.mobile.nx.com.br.multisalesmobile.activities;

/**
 * Created by marceloeugenio on 3/14/15.
 */
public enum EFormaPagamento {
    BOLETO ("BOLETO"),
    DEBITO_CC("DEBITO C/C");

    private final String descricao;

    private EFormaPagamento(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
