package multisales.mobile.nx.com.br.multisalesmobile.activities;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EAcaoAcesso;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Localizacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioPlantao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class GmFragment extends Fragment implements LocationListener{

    public static String TAG = "NX_GOOGLE_MAPS";
    public static long TEMPO = 0;
    public static long DISTANCIA = 1000;
    public static String PROVIDER_INICIAL = LocationManager.NETWORK_PROVIDER;
    public static String PROVIDER_FINAL = LocationManager.GPS_PROVIDER;

    public GoogleMap googleMap;

    private LocationManager locationManager;
    private Location meuLocal;
    private PapSource papSource;
    private HpsFragment hpsFragment;
    private float zoom = 15;


    private String title;
    private int page;
    private PapActivity papActivity;
    private RelativeLayout relativeLayout;
    private View view;
    private ListView listViewHp;
    private Location localInicial;

    private List<Marker> marcadores = new ArrayList<Marker>();
    private AsyncTask<Void, Void, ResponseD2D> taskLocalizacao;
    private RequestD2D requestD2D;
    private JsonParser jsonParser;
    private D2DRestClient d2DRestClient;
    private EntityManager entityManager;


    public static GmFragment newInstance(int page,String title){
        page = page;
        title = title;
        GmFragment gmFragment = new GmFragment();
        Bundle args = new Bundle();
        args.putInt("someInt",page);
        return gmFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG,"ON CREATE VIEW");
        // Inflate the layout for this fragment
        papActivity = (PapActivity) getActivity();
        jsonParser = new JsonParser();
        d2DRestClient = new D2DRestClient();
        locationManager = (LocationManager) papActivity.getSystemService(Context.LOCATION_SERVICE);
        view =  inflater.inflate(R.layout.fragment_gm, container, false);
        listViewHp = (ListView) view.findViewById(R.id.listview_gm_hps);
        papSource = new PapSource(papActivity,googleMap);
        googleMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        entityManager       = new EntityManager(papActivity);

        return view;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "ON RESUME");
        super.onResume();
        if(!locationManager.isProviderEnabled(PROVIDER_INICIAL) ||
                !locationManager.isProviderEnabled(PROVIDER_FINAL)){
            turnGPSOn();
        } else {
            papActivity.esconderOverlay();
            if (locationManager.isProviderEnabled(PROVIDER_INICIAL) ||
                    locationManager.isProviderEnabled(PROVIDER_FINAL)) {
                setUpMapIfNeeded();

            }
        }
    }

    @Override
    public void onPause() {
        Log.d(TAG,"onPause");
        super.onPause();
        //locationManager.removeUpdates(this);
    }

    private void setUpMapIfNeeded() {
        Log.d(TAG,"setUpMapIfNeeded");
        // Do a null check to confirm that we have not already instantiated the map.
        if (googleMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            googleMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        }
        // Check if we were successful in obtaining the map.
        if (googleMap != null) {
            setUpMap();

        }
    }


    private void setUpMap() {
        papActivity.mostrarOverlay();
        googleMap.setMyLocationEnabled(true);
        googleMap.setLocationSource(papSource);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            // Evento de click nos marcadores do mapa
            @Override
            public boolean onMarkerClick(Marker marker) {
                for(Marker item : marcadores){
                    item.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

                listViewHp.setVisibility(View.VISIBLE);

                List<Object> oneListMarcadores = new ArrayList<Object>();

                if(PapActivity.LAYOUT == "NET"){
                    Hp hp = new Hp();
                    hp.setNome(marker.getTitle());
                    hp.setLogradouro(marker.getSnippet());
                    oneListMarcadores.add(hp);
                } else if (PapActivity.LAYOUT == "VIVO"){;
                    Condominio condominio = new Condominio();
                    condominio.setNome(marker.getTitle());
                    Endereco endereco = new Endereco();
                    endereco.setLogradouro(marker.getSnippet());
                    condominio.setEndereco(endereco);
                    oneListMarcadores.add(condominio);

                }

                GmHpAdapter gmHpAdapter = new GmHpAdapter(getActivity(),oneListMarcadores);
                listViewHp.setAdapter(gmHpAdapter);
                listViewHp.animate().setDuration(500);

                CameraPosition position = new CameraPosition.Builder()
                        .target(marker.getPosition())
                        .zoom(googleMap.getCameraPosition().zoom)
                        .build();
                CameraUpdate update = CameraUpdateFactory.
                        newCameraPosition(position);
                googleMap.animateCamera(update);
                return true;
            }
        });

        //Pegar localizaçao pele rede apenas 1 vez
        if(localInicial == null) {
            locationManager.requestSingleUpdate(PROVIDER_INICIAL, this, null);
        }
        locationManager.requestLocationUpdates(PROVIDER_FINAL,TEMPO,DISTANCIA,this);
        Log.d(TAG,"setUpMap");
    }

    private void adicionarMarcadores(Location location, String label,String info){
        LatLng pontoMarcador  = new LatLng(location.getLatitude(),location.getLongitude());
        Marker marcador = googleMap.addMarker(new MarkerOptions().position(pontoMarcador).title(label).snippet(info));
        marcadores.add(marcador);
    }

    private void adicionarMarcadores(LatLng latLng, String label,String info){
        Marker marcador = googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(label)
                        .snippet(info)
        );
        marcadores.add(marcador);
    }

    private void adicionarMarcadores(Location location, String label,String info,boolean isPlantao){
        LatLng pontoMarcador  = new LatLng(location.getLatitude(),location.getLongitude());
        Marker marcador = googleMap.addMarker(new MarkerOptions().position(pontoMarcador).title(label).snippet(info));

        if(isPlantao){
            marcador.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else{
            marcador.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }
        marcadores.add(marcador);
    }

    private void adicionarMarcadores(LatLng latLng, String label,String info,boolean isPlantao){
        Marker marcador = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(label)
                .snippet(info)
        );
        if(isPlantao){
            marcador.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else{
            marcador.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        marcadores.add(marcador);
    }

    public Location getMeuLocal() {
        return meuLocal;
    }

    public void setMeuLocal(Location meuLocal) {
        this.meuLocal = meuLocal;
    }

    private void turnGPSOn() {
        // Get Location Manager and check for GPS & Network location services
        //locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            papActivity.mostrarOverlay();
            // Build the alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(papActivity);
            builder.setTitle("GPS desligado");
            builder.setMessage("Por favor, ligue seu GPS!");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Show location settings when the user acknowledges the alert dialog
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }
    }

    @Override
    public void onLocationChanged(final Location location) {
        Log.d(TAG,"Novo Local encontrado");
        papActivity.mostrarOverlay();
        localInicial = location;
        papActivity.meuLocal = location;

        //Ajusta Mapa
        listViewHp.setVisibility(View.INVISIBLE);
        for(Marker item : marcadores){
            item.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        // Atualiza a bolinha azul para a nova coordenada
        papSource.setLocation(location);

        Toast.makeText(papActivity,"Atualizando Local...",Toast.LENGTH_LONG).show();
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(location.getLatitude(),location.getLongitude()))
                .zoom(15)
                .build();
        CameraUpdate update = CameraUpdateFactory.
                newCameraPosition(position);
        googleMap.animateCamera(update);
        obterHps(location);
        //MockCondominio mock = new MockCondominio();
        //obterHps(mock.listaCondominio);
        //obterHps(new MockHp().listahps);
        Log.d(TAG,"local atualizado");
    }



    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        //papActivity.esconderOverlay();
    }

    @Override
    public void onProviderDisabled(String provider) {
        turnGPSOn();
    }

    public void obterHps(final Location location){
        try {
            taskLocalizacao = new AsyncTask<Void, Void, ResponseD2D>() {
                @Override
                protected ResponseD2D doInBackground(Void... params) {
                    JSONObject jsonEnvio;
                    JSONObject jsonRetorno;
                    ResponseD2D responseD2D = null;
                    try {
                        requestD2D = new RequestD2D();
                        Localizacao localizacao = new Localizacao();
                        localizacao.setLatitude(location.getLatitude());
                        localizacao.setLongitude(location.getLongitude());
                        localizacao.setRaio(2);
                        requestD2D.setLocalizacao(localizacao);

                        if(PapActivity.LAYOUT == "VIVO"){
                            requestD2D.setOperacao(ECodigoOperacaoD2D.PAP_CODIGO_CONDOMINIO.getCodigo());
                        } else if(PapActivity.LAYOUT == "NET"){
                            requestD2D.setOperacao(ECodigoOperacaoD2D.PAP_CODIGO_HP.getCodigo());
                        }

                        jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                        jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                        responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                    } catch (Exception e) {
                        responseD2D = new ResponseD2D();
                        responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                        responseD2D.setMensagem(e.getMessage());
                        return responseD2D;
                    }
                    return responseD2D;
                }

                @Override
                protected void onPostExecute(ResponseD2D resposta) {
                    super.onPostExecute(resposta);
                    if(resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                        try {
                            // Limpa Dados
                            papActivity.pontos.clear();
                            googleMap.clear();
                            marcadores.clear();
                            //relativeLayout.removeAllViewsInLayout();

                            //Recupera Hps
                            //papActivity.hps = resposta.getHps();

                            if(PapActivity.LAYOUT == "VIVO"){
                                List<Condominio> condominios = resposta.getCondominios();

                                for(Condominio item : condominios){
                                    List<Condominio> condominioBancoLocal = entityManager.getByWhere(Condominio.class, " id = "+ item.getId() , null);
                                    if(condominioBancoLocal == null || condominioBancoLocal.isEmpty()){
                                        Endereco endereco = item.getEndereco();
                                        endereco.setNullId();
                                        endereco = entityManager.save(endereco);
                                        item.setEndereco(endereco);
                                        item = entityManager.save(item);
                                    }else {
                                        item.setIdLocal(condominioBancoLocal.get(0).getIdLocal());
                                    }
                                    boolean temPlantao = false;
                                    List<CondominioPlantao> plantoes = entityManager.getByWhere(CondominioPlantao.class, "_condominio = "+item.getIdLocal(),null);
                                    if(plantoes != null && !plantoes.isEmpty()){
                                        temPlantao = true;
                                    }

                                    papActivity.pontos.add(item);
                                    adicionarMarcadores(new LatLng(
                                                    item.getLatitude(),
                                                    item.getLongitude()),
                                            item.getNome(),
                                            item.getEndereco().getLogradouro() + " , " + item.getEndereco().getNumero(),
                                            temPlantao
                                    );
                                }

                            } else if(PapActivity.LAYOUT == "NET"){
                               List<Hp> hps = resposta.getHps();

                                for(Hp item : hps){

                                    papActivity.pontos.add(item);
                                    adicionarMarcadores(new LatLng(
                                                    item.getLatitude(),
                                                    item.getLongitude()),
                                            item.getNome(),
                                            item.getLogradouro());
                                }

                            }

                            hpsFragment = HpsFragment.newInstance(1,"2");

                            if(papActivity.listView != null){
                                Log.d(TAG,"HPSFRAGMENT");
                                PapListAdapter papListAdapter = new PapListAdapter(getActivity(),papActivity.pontos);
                                ListView listView = papActivity.listView;
                                listView.setAdapter(papListAdapter);
                            }
                            papActivity.esconderOverlay();

                        } catch (Exception e) {
                            papActivity.esconderOverlay();
                           // Log.d(TAG,e.getMessage());
                            UtilActivity.makeShortToast("Falha em obter os HPs", papActivity);
                        }
                    } else {
                        papActivity.esconderOverlay();
                        UtilActivity.makeShortToast(resposta.getMensagem(), papActivity);
                    }
                }
            };
            taskLocalizacao.execute();
        } catch (Exception e) {
            papActivity.esconderOverlay();
            UtilActivity.makeShortToast("Falha em obter os HPs", papActivity);
        }

    }

    public void obterHps(final Location location,final Integer raio){

        if(PapActivity.LAYOUT == "NET"){
            Toast.makeText(papActivity,"Atualizando Hps...",Toast.LENGTH_LONG).show();
        } else if(PapActivity.LAYOUT == "VIVO"){
            Toast.makeText(papActivity,"Atualizando Condominios...",Toast.LENGTH_LONG).show();
        }

        papActivity.mostrarOverlay();
        try {
            taskLocalizacao = new AsyncTask<Void, Void, ResponseD2D>() {
                @Override
                protected ResponseD2D doInBackground(Void... params) {
                    JSONObject jsonEnvio;
                    JSONObject jsonRetorno;
                    ResponseD2D responseD2D = null;
                    try {
                        requestD2D = new RequestD2D();
                        Localizacao localizacao = new Localizacao();
                        localizacao.setLatitude(location.getLatitude());
                        localizacao.setLongitude(location.getLongitude());
                        localizacao.setRaio(raio);
                        requestD2D.setLocalizacao(localizacao);

                        if(PapActivity.LAYOUT == "VIVO"){
                            requestD2D.setOperacao(ECodigoOperacaoD2D.PAP_CODIGO_CONDOMINIO.getCodigo());
                        } else if(PapActivity.LAYOUT == "NET"){
                            requestD2D.setOperacao(ECodigoOperacaoD2D.PAP_CODIGO_HP.getCodigo());
                        }

                        jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                        jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                        responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                    } catch (Exception e) {
                        responseD2D = new ResponseD2D();
                        responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                        responseD2D.setMensagem(e.getMessage());
                        return responseD2D;
                    }
                    return responseD2D;
                }

                @Override
                protected void onPostExecute(ResponseD2D resposta) {
                    super.onPostExecute(resposta);
                    if(resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                        try {
                            // Limpa Dados
                            papActivity.pontos.clear();
                            googleMap.clear();
                            marcadores.clear();

                            if(PapActivity.LAYOUT == "VIVO"){
                                List<Condominio> condominios = resposta.getCondominios();

                                for(Condominio item : condominios){
                                    List<Condominio> condominioBancoLocal = entityManager.getByWhere(Condominio.class, " id = "+ item.getId() , null);
                                    if(condominioBancoLocal == null || condominioBancoLocal.isEmpty()){
                                        Endereco endereco = item.getEndereco();
                                        endereco.setNullId();
                                        endereco = entityManager.save(endereco);
                                        item.setEndereco(endereco);
                                        item = entityManager.save(item);
                                    }else {
                                        item.setIdLocal(condominioBancoLocal.get(0).getIdLocal());
                                    }
                                    boolean temPlantao = false;
                                    List<CondominioPlantao> plantoes = entityManager.getByWhere(CondominioPlantao.class, "_condominio = "+item.getIdLocal(),null);
                                    if(plantoes != null && !plantoes.isEmpty()){
                                        temPlantao = true;
                                    }

                                    papActivity.pontos.add(item);
                                    adicionarMarcadores(new LatLng(
                                                    item.getLatitude(),
                                                    item.getLongitude()),
                                            item.getNome(),
                                            item.getEndereco().getLogradouro() + " , " + item.getEndereco().getNumero(),
                                            temPlantao
                                    );
                                }

                            } else if(PapActivity.LAYOUT == "NET"){
                                List<Hp> hps = resposta.getHps();

                                for(Hp item : hps){

                                    papActivity.pontos.add(item);
                                    adicionarMarcadores(new LatLng(
                                                    item.getLatitude(),
                                                    item.getLongitude()),
                                            item.getNome(),
                                            item.getLogradouro());
                                }

                            }


                            hpsFragment = HpsFragment.newInstance(1,"2");

                            if(papActivity.listView != null){
                                Log.d(TAG,"CRIANDO HPSFRAGMENT");
                                PapListAdapter papListAdapter = new PapListAdapter(getActivity(),papActivity.pontos);
                                ListView listView = papActivity.listView;
                                listView.setAdapter(papListAdapter);
                            }
                            papActivity.esconderOverlay();

                        } catch (Exception e) {
                            papActivity.esconderOverlay();
                            // Log.d(TAG,e.getMessage());
                            UtilActivity.makeShortToast("Falha em obter os HPs", papActivity);
                        }
                    } else {
                        papActivity.esconderOverlay();
                        UtilActivity.makeShortToast(resposta.getMensagem(), papActivity);
                    }
                }
            };
            taskLocalizacao.execute();
        } catch (Exception e) {
            papActivity.esconderOverlay();
            UtilActivity.makeShortToast("Falha em obter os HPs", papActivity);
        }


    }

    public void obterHps(List<Hp> hps) {
        // Limpa Dados
        papActivity.pontos.clear();
        googleMap.clear();
        marcadores.clear();
        //relativeLayout.removeAllViewsInLayout();

        for(Hp item : hps){
            papActivity.pontos.add(item);
            adicionarMarcadores(new LatLng(item.getLatitude(), item.getLongitude()), item.getNome(),item.getLogradouro(),false);
        }

        Log.d(TAG,"HPS - papActivity.hps = " + Integer.toString(papActivity.pontos.size()));

        hpsFragment = HpsFragment.newInstance(1,"2");

        if(papActivity.listView != null){
            Log.d(TAG,"HPSFRAGMENT");
            PapListAdapter papListAdapter = new PapListAdapter(getActivity(),papActivity.pontos);
            ListView listView = papActivity.listView;
            listView.setAdapter(papListAdapter);
        }
        papActivity.esconderOverlay();
    }

}
