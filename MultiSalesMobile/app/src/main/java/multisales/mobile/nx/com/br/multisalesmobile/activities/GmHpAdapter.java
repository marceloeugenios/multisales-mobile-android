package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;

/**
 * Created by fabionx on 19/03/15.
 */
public class GmHpAdapter extends BaseAdapter {

    private final List<Object> itens;
    private final Activity activity;


    public GmHpAdapter(Activity activity,List<Object> itens){
        this.itens = itens;
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return  itens.size();
    }

    @Override
    public Object getItem(int position) {

        if(PapActivity.LAYOUT == "NET"){
            Hp hp = (Hp) itens.get(position);
            return (Object) hp;
        } else if (PapActivity.LAYOUT == "VIVO"){
            Condominio condominio = (Condominio) itens.get(position);
            return (Object) condominio;
        } else {
            return false;
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_pap_gm, null);

        LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.linear_layout_item_gm);

        TextView textNome = (TextView) linearLayout.findViewById(R.id.nomeGmHp);
        TextView texInfo = (TextView) linearLayout.findViewById(R.id.infoGmHp);

        if(PapActivity.LAYOUT == "NET"){
            Hp hp = (Hp) itens.get(position);
            textNome.setText(hp.getNome());
            texInfo.setText(hp.getLogradouro());
        } else if(PapActivity.LAYOUT == "VIVO"){
            Condominio condominio = (Condominio) itens.get(position);
            textNome.setText(condominio.getNome());
            texInfo.setText(condominio.getEndereco().getLogradouro());
        }









        return v;
    }
}
