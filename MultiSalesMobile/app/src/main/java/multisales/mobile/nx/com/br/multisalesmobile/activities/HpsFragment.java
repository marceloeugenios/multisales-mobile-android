package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;

/**
 * A simple {@link Fragment} subclass.
 */
public class HpsFragment extends Fragment implements AdapterView.OnItemClickListener {


    public static String TAG = "NX_LISTA_HPS";
    private String title;
    private int page;
    private PapActivity papActivity;
    private ArrayAdapter<Hp> arrayAdapter;
    private View view;
    public  ListView listView;


    public static HpsFragment newInstance(int page,String title){
        HpsFragment hpsFragment = new HpsFragment();
        Bundle args = new Bundle();
        args.putInt("someInt",page);
        return hpsFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        papActivity = (PapActivity) getActivity();
        Log.d(TAG,"onCreateView");
        view = inflater.inflate(R.layout.fragment_hps, container, false);
        PapListAdapter papListAdapter = new PapListAdapter(getActivity(),papActivity.pontos);
        Log.d(TAG," Tamanho papActivity.hps  =" + papActivity.pontos.size());
        listView = (ListView) view.findViewById(R.id.listview_hps);
        listView.setAdapter(papListAdapter);
        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        papActivity.listView = listView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(papActivity.LAYOUT == "VIVO"){
            TextView textId = (TextView) view.findViewById(R.id.idHp);
            Integer idCondominio = Integer.parseInt(textId.getText().toString());

            Intent intentCondominioDetalhes = new Intent(papActivity, CondominioDetalheActivity.class);
            intentCondominioDetalhes.putExtra("idCondominio",idCondominio);
            startActivity(intentCondominioDetalhes);
        } else if (papActivity.LAYOUT == "NET"){
            return;
        }

    }
}
