package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;

/**
 * Created by eric on 18-03-2015.
 */
public class ItemComboAdapter extends ArrayAdapter<String> {

    private Activity context;
    private List<String> values;

    public ItemComboAdapter(Activity ctx, int txtViewResourceId, List<String> objects) {
        super(ctx, txtViewResourceId, objects);
        this.context = ctx;
        this.values = objects;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);
    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View mySpinner = inflater.inflate(R.layout.item_combo, parent, false);
        TextView main_text = (TextView) mySpinner .findViewById(R.id.text1);
        main_text.setText(values.get(position));
        return mySpinner;
    }

}
