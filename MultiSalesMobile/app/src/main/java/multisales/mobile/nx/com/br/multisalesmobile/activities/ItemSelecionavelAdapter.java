package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.IItemSelecionavel;
import multisales.mobile.nx.com.br.multisalesmobile.utils.OnItemSelectListener;

/**
 * Created by eric on 06-03-2015.
 */
public class ItemSelecionavelAdapter<T extends IItemSelecionavel> extends ArrayAdapter<T> {
    private LayoutInflater inflater;
    private OnItemSelectListener onItemSelectListener;
    private List<T> itens;

    public ItemSelecionavelAdapter(Context context, List<T> itemList)
    {
        super(context, R.layout.item_selecionavel, R.id.label, itemList);
        // Cache the LayoutInflate to avoid asking for a new one each time.
        itens = itemList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        // Planet to display
        IItemSelecionavel item = (IItemSelecionavel) this.getItem(position);

        // The child views in each row.
        CheckBox checkBox;
        TextView textView;

        // Create a new row view
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.item_selecionavel, null);

            // Find the child views.
            textView = (TextView) convertView
                    .findViewById(R.id.label);
            checkBox = (CheckBox) convertView.findViewById(R.id.check);

            convertView.setTag(new ItemSelecionavelHolder(textView, checkBox));

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    CheckBox cb = (CheckBox) buttonView;
                    IItemSelecionavel item = (IItemSelecionavel) cb.getTag();
                    item.setSelecionado(isChecked);
                    if (onItemSelectListener != null) {
                        onItemSelectListener.onItemSelect(position);
                    }
                }
            });
        }
        // Reuse existing row view
        else
        {
            // Because we use a ViewHolder, we avoid having to call
            // findViewById().
            ItemSelecionavelHolder viewHolder = (ItemSelecionavelHolder) convertView
                    .getTag();
            checkBox = viewHolder.getCheckBox();
            textView = viewHolder.getTextView();
        }

        // Tag the CheckBox with the Planet it is displaying, so that we can
        // access the planet in onClick() when the CheckBox is toggled.
        checkBox.setTag(item);

        // Display planet data
        checkBox.setChecked(item.isSelecionado());
        textView.setText(item.getLabel());

        return convertView;
    }

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        this.onItemSelectListener = onItemSelectListener;
    }

    public List<T> getItens() {
        return itens;
    }
}
