package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by eric on 06-03-2015.
 */
public class ItemSelecionavelHolder {
    private CheckBox checkBox;
    private TextView textView;

    public ItemSelecionavelHolder()
    {
    }

    public ItemSelecionavelHolder(TextView textView, CheckBox checkBox)
    {
        this.checkBox = checkBox;
        this.textView = textView;
    }

    public CheckBox getCheckBox()
    {
        return checkBox;
    }

    public void setCheckBox(CheckBox checkBox)
    {
        this.checkBox = checkBox;
    }

    public TextView getTextView()
    {
        return textView;
    }

    public void setTextView(TextView textView)
    {
        this.textView = textView;
    }
}
