package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EAcaoAcesso;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ENotificacaoCodigo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogAcesso;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.StatusSincronizacao;
import multisales.mobile.nx.com.br.multisalesmobile.servico.LogAcessoServico;
import multisales.mobile.nx.com.br.multisalesmobile.servico.StatusSincronizacaoServico;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;


public class LoginActivity extends Activity {

    private EditText loginET;
    private EditText senhaET;
    private Button loginBT;
    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    private FrameLayout logoFL;
    private LinearLayout camposLogin;
    private FrameLayout preLoaderLoginFL;
    private Boolean showAnimations = true;
    private LogAcessoServico logAcessoServico;
    private StatusSincronizacaoServico statusSincronizacaoServico;
    private EntityManager entityManager;
    MultiSales multiSales;

    public AsyncTask<Void, Void, ResponseD2D> taskLogar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        carregarCamposEntrada();
        logAcessoServico = new LogAcessoServico(this);
        statusSincronizacaoServico = new StatusSincronizacaoServico(this);
        entityManager = new EntityManager(this);
        multiSales = (MultiSales)getApplication();
        Calendar hoje = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        if (UtilActivity.isOnline(this)) {
            multiSales.setRedeDados(true);
        } else {
            multiSales.setRedeDados(false);
        }

        try {
            LogAcesso logAcesso = logAcessoServico.obterUltimoLogAcesso();

            StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.NAO_VENDA);
            if (statusSincronizacao.getEmSincronizacao().equals(EBoolean.TRUE)) {
                statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                entityManager.atualizar(statusSincronizacao);
            }
            statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.AGENDAMENTO);
            if (statusSincronizacao.getEmSincronizacao().equals(EBoolean.TRUE)) {
                statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                entityManager.atualizar(statusSincronizacao);
            }

            if (logAcesso != null && logAcesso.getAcaoLogin().equals(EAcaoAcesso.LOGIN)) {
                multiSales.setLogin(logAcesso.getLogin());
                multiSales.setIdAgenteAutorizado(logAcesso.getIdAgenteAutorizado());
                multiSales.setIdUsuario(logAcesso.getIdUsuario());
                multiSales.setImei(logAcesso.getImei());
                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainIntent);
                if (!UtilActivity.isOnline(this)) {
                    multiSales.setRedeDados(false);
                    UtilActivity.makeNotification(this,
                            ENotificacaoCodigo.CONEXAO,
                            R.drawable.ic_stat_notify_app,
                            "Multisales",
                            "Trabalhando offline",
                            "Trabalhando offline",
                            null,
                            true);
                } else {
                    UtilActivity.cancelNotification(this, ENotificacaoCodigo.CONEXAO);
                }
                LoginActivity.this.finish();
            }

            if (showAnimations) {
                camposLogin.setVisibility(View.GONE);
                camposLogin.animate().setDuration(0);
                camposLogin.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (showAnimations) {
                            camposLogin.setVisibility(View.VISIBLE);
                            camposLogin.animate().setStartDelay(900);
                            camposLogin.animate().setDuration(450);
                            camposLogin.animate().alpha(1);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                logoFL = (FrameLayout) findViewById(R.id.logoFL);
                logoFL.setVisibility(View.GONE);
                logoFL.animate().setDuration(0);
                logoFL.animate().translationY(250).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (showAnimations) {
                            logoFL.animate().setDuration(0);
                            logoFL.animate().alpha(0).setListener(new Animator.AnimatorListener() {

                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    if (showAnimations) {
                                        logoFL.setVisibility(View.VISIBLE);
                                        logoFL.animate().setDuration(750);
                                        logoFL.animate().alpha(1).setListener(new Animator.AnimatorListener() {

                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                if (showAnimations) {
                                                    logoFL.animate().setDuration(450);
                                                    logoFL.animate().translationY(0);
                                                    showAnimations = false;
                                                }
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
            }
        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }

    public void carregarCamposEntrada() {
        loginET = (EditText) findViewById(R.id.loginET);
        senhaET = (EditText) findViewById(R.id.senhaET);
        loginBT = (Button) findViewById(R.id.loginBT);
        preLoaderLoginFL = (FrameLayout) findViewById(R.id.preLoaderLoginFL);
        camposLogin = (LinearLayout) findViewById(R.id.camposLoginLL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    public void logar(View view){
        requestD2D.setUsuario(loginET.getText().toString());
        requestD2D.setSenha(senhaET.getText().toString());
        camposLogin.setVisibility(View.GONE);
        preLoaderLoginFL.setVisibility(View.VISIBLE);

        try {
            if (multiSales.getRedeDados()) {
                taskLogar = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D = null;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.AUTENTICAR.getCodigo());
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        if (resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                logAcessoServico.salvarAcaoAcesso(resposta.getIdUsuario(), resposta.getIdAgenteAutorizado(), requestD2D.getUsuario(), EAcaoAcesso.LOGIN);
                                TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                                multiSales.setIdUsuario(resposta.getIdUsuario());
                                multiSales.setIdAgenteAutorizado(resposta.getIdAgenteAutorizado());
                                multiSales.setImei(mngr.getDeviceId());
                                multiSales.setLogin(requestD2D.getUsuario());

                                Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(mainIntent);
                                LoginActivity.this.finish();
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("Falha na autenticação", getApplicationContext());
                            }
                        } else {
                            UtilActivity.makeShortToast(resposta.getMensagem(), getApplicationContext());
                            camposLogin.setVisibility(View.VISIBLE);
                            preLoaderLoginFL.setVisibility(View.GONE);
                        }
                    }
                };
                taskLogar.execute();
            } else {
                UtilActivity.makeShortToast("Sem conexão de dados", getApplicationContext());
                camposLogin.setVisibility(View.VISIBLE);
                preLoaderLoginFL.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Falha na autenticação", getApplicationContext());
            camposLogin.setVisibility(View.VISIBLE);
            preLoaderLoginFL.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        camposLogin.setVisibility(View.VISIBLE);
        preLoaderLoginFL.setVisibility(View.GONE);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
        System.exit(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
