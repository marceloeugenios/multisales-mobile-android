package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EAcaoAcesso;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ENotificacaoCodigo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.FotoPerfilUsuario;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogDiario;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.servico.LogAcessoServico;
import multisales.mobile.nx.com.br.multisalesmobile.servico.LogDiarioServico;
import multisales.mobile.nx.com.br.multisalesmobile.servico.SincronizacaoServico;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class MainActivity extends Activity {

    private int tempoAccordion = 750;

    private BroadcastReceiver mReceiver;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ListView mDrawerListRight;
    private ListView listChat;

    private RelativeLayout menuAgendamentosRL;
    private RelativeLayout menuChatRL;
    private RelativeLayout agendamentosRL;
    private RelativeLayout menuDireitoRL;
    private RelativeLayout menuEsquerdoRL;
    private RelativeLayout chatRL;
    private RelativeLayout aguardeRL;
    private Boolean menuAgendamentosAberto = false;
    private Boolean menuChatAberto = true;
    private ImageView setaAgendamentosIV;
    private ImageView setaChatIV;
    private TextView nomeUsuarioTV;
    private ImageView botaoSyncIV;
    private ImageView imageView;

    private Integer alturaTituloAgendamentos;
    private Integer alturaTituloChat;
    private Integer alturaAgendamentos;
    private Integer alturaMenuDireito;
    private boolean alturasRecuperadas = false;
    private boolean mDisplayBlocked = false;

    List<Map<String, Object>> itens;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<MenuLateralItem> navDrawerItems;
    private static boolean alreadyOpen = false;

    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    private EntityManager entityManager;
    private Float ultimaVersao;

    private LogAcessoServico logAcessoServico;
    private LogDiarioServico logDiarioServico;
    private SincronizacaoServico sincronizacaoServico;

    private MultiSales multiSales;

    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;
    protected static final int PIC_CROP = 2;
    private Intent pictureActionIntent = null;
    private Uri picUri;
    FotoPerfilUsuario fotoPerfilUsuario;
    private String selectedImagePath;

    private static final int CAMERA_REQUISICAO = 1888;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        invalidateOptionsMenu();
        entityManager = new EntityManager(this);
        multiSales = (MultiSales)getApplication();

        menuAgendamentosRL = (RelativeLayout) findViewById(R.id.menuAgendamentosRL);
        menuChatRL = (RelativeLayout) findViewById(R.id.menuChatRL);
        agendamentosRL = (RelativeLayout) findViewById(R.id.agendamentosRL);
        menuDireitoRL = (RelativeLayout) findViewById(R.id.menuDireitoRL);
        menuEsquerdoRL = (RelativeLayout) findViewById(R.id.menuEsquerdoRL);
        nomeUsuarioTV = (TextView) findViewById(R.id.nomeUsuarioTV);
        chatRL = (RelativeLayout) findViewById(R.id.chatRL);
        aguardeRL = (RelativeLayout) findViewById(R.id.aguardeRL);
        setaAgendamentosIV = (ImageView) findViewById(R.id.setaAgendamentosIV);
        setaChatIV = (ImageView) findViewById(R.id.setaChatIV);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        mDrawerListRight = (ListView) findViewById(R.id.list_slidermenuright);
        listChat = (ListView) findViewById(R.id.listChat);
        botaoSyncIV = (ImageView) findViewById(R.id.botaoSyncIV);
        imageView = (ImageView) findViewById(R.id.fotoUsuario);

        try {
            List<FotoPerfilUsuario> imagens = entityManager.getByWhere(FotoPerfilUsuario.class, "login = '" + multiSales.getLogin() + "'", null);
            if (imagens != null && imagens.size() > 0) {
                fotoPerfilUsuario = imagens.get(0);
                imageView.setImageBitmap(UtilActivity.deserializarImagem(fotoPerfilUsuario.getImagem()));
            }

            logAcessoServico = new LogAcessoServico(this);
            logDiarioServico = new LogDiarioServico(this);

            nomeUsuarioTV.setText(multiSales.getLogin());
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao recuperar usuario logado", this);
        }

        ViewTreeObserver observer = menuChatRL.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                recuperarAlturas();
            }
        });

        String[] deAgendamento = {"nome", "razao", "horario"};

        int[] paraAgendamento = {R.id.nomeClienteTV, R.id.motivoTabulacaoTV, R.id.dataHoraAgendamentoTV};

        SimpleAdapter adapterAgendamentos = new SimpleAdapter(this, listarAgendamentos(), R.layout.item_tabulacao_agendamento_main, deAgendamento, paraAgendamento);

        mDrawerListRight.setAdapter(adapterAgendamentos);

        String[] deChat = {"nomeContato", "status"};

        int[] paraChat = {R.id.nomeContatoTV, R.id.statusContatoTV};

        SimpleAdapter adapterContatos = new SimpleAdapter(this, listarContatos(), R.layout.item_contato_chat, deChat, paraChat);

        listChat.setAdapter(adapterContatos);

        /*listChat.setAdapter(adapterAgendamentos);*/

        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(android.R.drawable.ic_menu_more);*/

        String mensagem = getIntent().getStringExtra("mensagem");
        if (mensagem != null) {
            UtilActivity.makeShortToast(mensagem, this);
        }
    }

    public void recuperarAlturas() {
        if (!alturasRecuperadas) {
            alturaTituloAgendamentos = menuAgendamentosRL.getHeight();
            alturaTituloChat = menuChatRL.getHeight();
            alturaMenuDireito = menuDireitoRL.getHeight();
            alturaAgendamentos = alturaMenuDireito - alturaTituloChat - alturaTituloAgendamentos;
            ViewGroup.LayoutParams params = agendamentosRL.getLayoutParams();
            params.height = alturaAgendamentos;

            ViewGroup.LayoutParams paramsChat = chatRL.getLayoutParams();
            paramsChat.height = alturaAgendamentos;
            fecharAccordionChat();
            alturasRecuperadas = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

        }

        return super.onOptionsItemSelected(item);
    }

    public void acionarMenu(View view) {
        if (mDrawerLayout.isDrawerOpen(menuEsquerdoRL)) {
            mDrawerLayout.closeDrawer(menuEsquerdoRL);
        } else {
            if (mDrawerLayout.isDrawerOpen(menuDireitoRL)) {
                mDrawerLayout.closeDrawer(menuDireitoRL);
            }
            mDrawerLayout.openDrawer(menuEsquerdoRL);
        }
    }

    private void mostrarOverlayCarga() {
        aguardeRL.setVisibility(View.VISIBLE);
        mDisplayBlocked = true;
    }

    private void esconderOverlayCarga() {
        aguardeRL.setVisibility(View.GONE);
        mDisplayBlocked = false;
    }

    private List<Map<String, Object>> listarAgendamentos() {
        itens = new ArrayList<>();

        Map<String, Object> item = new HashMap<>();
        item.put("nome", "Nome de Teste");
        item.put("razao", "Nao esta em casa");
        item.put("horario", "17:30");

        itens.add(item);

        return itens;
    }

    private List<Map<String, Object>> listarContatos() {
        itens = new ArrayList<>();

        Map<String, Object> item = new HashMap<>();
        item.put("nomeContato", "Nome do Contato");
        item.put("status", "Online");

        itens.add(item);

        item = new HashMap<>();
        item.put("nomeContato", "Nome do Contato");
        item.put("status", "Online");
        itens.add(item);

        return itens;
    }

    public void clickAccordionAgendamento(View view) {
        if (menuAgendamentosAberto) {
            fecharAccordionAgendamento();
        } else {
            abrirAccordionAgendamento();
        }
    }

    private void fecharAccordionAgendamento() {
        menuAgendamentosAberto = false;
        setaAgendamentosIV.setImageResource(android.R.drawable.arrow_down_float);
        agendamentosRL.animate().translationY(-alturaAgendamentos);
        if (!menuChatAberto) {
            abrirAccordionChat();
        }
    }

    private void abrirAccordionAgendamento() {
        menuAgendamentosAberto = true;
        agendamentosRL.setVisibility(View.VISIBLE);
        setaAgendamentosIV.setImageResource(android.R.drawable.arrow_up_float);
        agendamentosRL.animate().translationY(0);
        if (menuChatAberto) {
            fecharAccordionChat();
        }
    }

    public void clickAccordionChat(View view) {
        if (menuChatAberto) {
            fecharAccordionChat();
        } else {
            abrirAccordionChat();
        }
    }

    private void fecharAccordionChat() {
        menuChatAberto = false;
        setaChatIV.setImageResource(android.R.drawable.arrow_down_float);
        menuChatRL.animate().translationY(alturaAgendamentos);
        chatRL.animate().translationY(alturaAgendamentos);
        if (!menuAgendamentosAberto) {
            abrirAccordionAgendamento();
        }
    }

    private void abrirAccordionChat() {
        menuChatAberto = true;
        setaChatIV.setImageResource(android.R.drawable.arrow_up_float);
        menuChatRL.animate().translationY(0);
        chatRL.animate().translationY(0);
        if (menuAgendamentosAberto) {
            fecharAccordionAgendamento();
        }
    }

    public void carregarImagemUsuario(View view) {
        startDialog();
        /*Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUISICAO);*/
    }

    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
        myAlertDialog.setTitle("Upload de Imagem");
        myAlertDialog.setMessage("Como deseja selecionar sua foto?");

        myAlertDialog.setPositiveButton("Galeria",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        pictureActionIntent = new Intent();
                        pictureActionIntent.setAction(Intent.ACTION_GET_CONTENT);
                        pictureActionIntent = new Intent(
                                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pictureActionIntent,
                                GALLERY_PICTURE);
                    }
                });

        myAlertDialog.setNegativeButton("Câmera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "New Picture");
                        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                        picUri = getContentResolver().insert(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        pictureActionIntent = new Intent(
                                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
                        startActivityForResult(pictureActionIntent,
                                CAMERA_REQUEST);

                    }
                });
        myAlertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_PICTURE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    picUri = data.getData();
                    performCrop();
                }
            }
        } else if (requestCode == CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                performCrop();
            }
        } else if(requestCode == PIC_CROP){
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap thePic = extras.getParcelable("data");
                try {
                    String imagemSerializada = UtilActivity.serializarImagem(thePic);
                    if (fotoPerfilUsuario != null) {
                        fotoPerfilUsuario.setImagem(imagemSerializada);
                        entityManager.atualizar(fotoPerfilUsuario);
                    } else {
                        fotoPerfilUsuario = new FotoPerfilUsuario();
                        fotoPerfilUsuario.setImagem(imagemSerializada);
                        fotoPerfilUsuario.setLogin(multiSales.getLogin());
                        entityManager.save(fotoPerfilUsuario);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                imageView.setImageBitmap(thePic);

            }
        }

        /*if(requestCode == CAMERA_REQUISICAO && resultCode == RESULT_OK){

            Bitmap foto = (Bitmap) data.getExtras().get("data");
            ImageView imageView = (ImageView) findViewById(R.id.fotoUsuario);
            imageView.setImageBitmap(foto);

//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            foto.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            byte[] byteArray = stream.toByteArray();
//
//            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
//            str = enc.GetString(dBytes);
//
//            ImagemUsuario imagem = new ImagemUsuario();
//            imagem.setLogin(multiSales.getLogin());
//            imagem.setImagem(byteArray);

            //TODO salvar no banco*//*
        }*/
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void performCrop(){
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        //set crop properties
        cropIntent.putExtra("crop", "true");
        //indicate aspect of desired crop
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        //indicate output X and Y
        cropIntent.putExtra("outputX", 512);
        cropIntent.putExtra("outputY", 512);
        //retrieve data on return
        cropIntent.putExtra("return-data", true);
        //start the activity - we handle returning in onActivityResult
        startActivityForResult(cropIntent, PIC_CROP);
    }

    public void abrirConfiguracoes(View view) {
        Intent intentConf = new Intent(this, ConfiguracoesActivity.class);
        startActivity(intentConf);
        finish();
    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
        // update the main content by replacing fragments
        mDrawerLayout.closeDrawer(menuEsquerdoRL);
        switch (position)
        {
            case 0:
                abrirAgendamentos(null);
                break;
            case 1:
                Intent intentVendas = new Intent(this, VendasActivity.class);
                startActivity(intentVendas);
                break;
            case 2:
                Intent intentNaoVendas = new Intent(this, NaoVendasActivity.class);
                startActivity(intentNaoVendas);
                break;
            case 3:
                Intent intentRascunhos = new Intent(this, RascunhosActivity.class);
                startActivity(intentRascunhos);
                break;
            case 4:
                abrirClientes(null);
                break;
            case 5:
                Intent intentCondominio = new Intent(this, CondominiosActivity.class);
                startActivity(intentCondominio);
                break;
            case 6:
                abrirPAP(null);
                break;

            case 7:
                AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
                myAlertDialog.setTitle("Upload de Imagem");
                myAlertDialog.setMessage("Como deseja selecionar sua imagem?");

                myAlertDialog.setPositiveButton("Galeria",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                pictureActionIntent = new Intent();
                                pictureActionIntent.setAction(Intent.ACTION_GET_CONTENT);
                                pictureActionIntent = new Intent(
                                        Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pictureActionIntent,
                                        -1);
                            }
                        });

                myAlertDialog.setNegativeButton("Câmera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                ContentValues values = new ContentValues();
                                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                                picUri = getContentResolver().insert(
                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                pictureActionIntent = new Intent(
                                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
                                startActivityForResult(pictureActionIntent,
                                        -1);

                            }
                        });
                myAlertDialog.show();
                break;
            default:
                break;
        }
    }

    public void abrirClientes(View view) {
        Intent intentClientes = new Intent(this, ClientesActivity.class);
        startActivity(intentClientes);
    }

    public void abrirAgendamentos(View view) {
        Intent intentAgendamentos = new Intent(this, AgendamentosActivity.class);
        startActivity(intentAgendamentos);
    }

    public void abrirPAP(View view) {
        Intent intentPap = new Intent(this, PapActivity.class);
        startActivity(intentPap);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(menuEsquerdoRL)) {
            mDrawerLayout.closeDrawer(menuEsquerdoRL);
        } else if (mDrawerLayout.isDrawerOpen(menuDireitoRL)) {
            mDrawerLayout.closeDrawer(menuDireitoRL);
        } else {
            logoff(null);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent pEvent) {
        if (!mDisplayBlocked) {
            return super.dispatchTouchEvent(pEvent);
        }
        return mDisplayBlocked;
    }

    public void logoff (View view) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        try {
                            logAcessoServico.salvarAcaoAcesso(multiSales.getIdUsuario(), multiSales.getIdAgenteAutorizado(), multiSales.getLogin(), EAcaoAcesso.LOGOFF);
                            UtilActivity.cancelNotification(MainActivity.this, ENotificacaoCodigo.CONEXAO);
                            Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intentLogin);
                            MainActivity.this.finish();
                        } catch (Exception e) {
                            UtilActivity.makeShortToast("Erro ao efetuar logoff", MainActivity.this);
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Deseja Realmente Realizar Logoff?").setPositiveButton("Sim", dialogClickListener)
                .setNegativeButton("Não", dialogClickListener).show();
    }

    public void atualizarIconeSincr() {
        sincronizacaoServico = new SincronizacaoServico(getApplication());
        if (!sincronizacaoServico.verificarSincronizacoesPendentes()) {
            botaoSyncIV.setImageResource(R.drawable.ic_sincronizacao_tick);
        } else {
            botaoSyncIV.setImageResource(R.drawable.ic_sincronizacao_sincronizar);
        }
    }

    @Override
    protected void onResume() {
        mDrawerLayout.closeDrawer(menuEsquerdoRL);
        multiSales = (MultiSales)getApplication();
        IntentFilter intentFilter = new IntentFilter("SINCRONIZACAO_CONCLUIDA");

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                atualizarIconeSincr();
            }
        };
        //registering our receiver
        this.registerReceiver(mReceiver, intentFilter);

        atualizarIconeSincr();

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        navDrawerItems = new ArrayList<>();

        LogDiario logDiario = new LogDiario();
        LogDiario logMes = new LogDiario();
        try {
            logDiarioServico.apagarLogsMesAnterior();
            logDiario = logDiarioServico.obterLogDiarioAtualPorUsuario(multiSales.getLogin());
            logMes = logDiarioServico.obterLogDiarioPorMes(Calendar.getInstance(), multiSales.getLogin());
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao recuperar tabulações", this);
        }

        // adding nav drawer items to array
        navDrawerItems.add(new MenuLateralItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1), true, Integer.toString(logDiario.getAgendamentos()) + "/" + Integer.toString(logMes.getAgendamentos())));
        navDrawerItems.add(new MenuLateralItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1), true, Integer.toString(logDiario.getVendas()) + "/" + Integer.toString(logMes.getVendas())));
        navDrawerItems.add(new MenuLateralItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1), true, Integer.toString(logDiario.getNaoVendas()) + "/" + Integer.toString(logMes.getNaoVendas())));
        navDrawerItems.add(new MenuLateralItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1), true, listarVendasIncompletas().size()+""));
        navDrawerItems.add(new MenuLateralItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1), true, Integer.toString(logDiario.getAgendamentos() + logDiario.getVendas() + logDiario.getNaoVendas())));
        navDrawerItems.add(new MenuLateralItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1), true, ""));
        navDrawerItems.add(new MenuLateralItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1), true, ""));
        navDrawerItems.add(new MenuLateralItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1), true, ""));

        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav drawer list adapter
        MenuLateralListAdapter menuLateralListAdapter = new MenuLateralListAdapter(getApplicationContext(), navDrawerItems);
        menuLateralListAdapter.notifyDataSetChanged();
        mDrawerList.setAdapter(menuLateralListAdapter);
        super.onResume();
    }

    private List<Tabulacao> listarVendasIncompletas() {
        try {
            List<MotivoTabulacaoTipo> motivosTabulacaoTipo = entityManager.getByWhere(MotivoTabulacaoTipo.class, "descricao = '" + EMotivoTabulacaoTipo.VENDA.getDescricao() + "'", null);
            if (motivosTabulacaoTipo != null
                    && !motivosTabulacaoTipo.isEmpty()) {

                MotivoTabulacaoTipo motivoVenda = motivosTabulacaoTipo.get(SistemaConstantes.ZERO);

                List<Tabulacao> tabulacoes = new ArrayList<>();
                StringBuilder stringBuilder =  new StringBuilder();
                stringBuilder.append("SELECT tab.* ");
                stringBuilder.append("FROM ");
                stringBuilder.append("tabulacao tab ");
                stringBuilder.append("INNER JOIN ");
                stringBuilder.append("motivo_tabulacao mtab ");
                stringBuilder.append("ON ");
                stringBuilder.append("tab._motivo_tabulacao = mtab.id ");
                stringBuilder.append("WHERE ");
                stringBuilder.append("mtab._motivo_tabulacao_tipo =  " + motivoVenda.getId() + " ");
                stringBuilder.append("AND ");
                stringBuilder.append("tab.completa =  \"" + EBoolean.FALSE.toString() + "\"");
                tabulacoes = entityManager.select(Tabulacao.class, stringBuilder.toString());

                for (Tabulacao tabulacao : tabulacoes) {
                    entityManager.initialize(tabulacao.getMotivoTabulacao());
                    entityManager.initialize(tabulacao.getVenda());
                    entityManager.initialize(tabulacao.getVenda().getCliente());
                    if (tabulacao.getVenda().getCombinacaoProdutoTipo() != null) {
                        entityManager.initialize(tabulacao.getVenda().getCombinacaoProdutoTipo());
                    }
                }

                return tabulacoes;
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao listar as vendas", this);
            finish();
        }

        return new ArrayList<>();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(this.mReceiver);
    }
}
