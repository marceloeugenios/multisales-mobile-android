package multisales.mobile.nx.com.br.multisalesmobile.activities;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;

/**
 * Created by fabionx on 21/03/15.
 */
public class MockCondominio {

    public List<Condominio> listaCondominio;

    public MockCondominio(){

        Condominio condominio1 = new Condominio();
        condominio1.setNome("JOSE - Tizen SPA");
        condominio1.setLatitude(-23.3368492);
        condominio1.setLongitude(-51.16326);
        Endereco endereco1 = new Endereco();
        endereco1.setLogradouro("Rua das Couves");
        condominio1.setEndereco(endereco1);
        listaCondominio.add(condominio1);

        Condominio condominio2 = new Condominio();
        condominio2.setNome("Instituto de Laser e Hospitalar do Paraná");
        condominio2.setLatitude(-23.3384665);
        condominio2.setLongitude(-51.1632371);
        Endereco endereco2 = new Endereco();
        endereco2.setLogradouro("Rua Das Abacaxis");
        condominio2.setEndereco(endereco2);
        listaCondominio.add(condominio2);

        Condominio condominio3 = new Condominio();
        condominio3.setNome("6º Distrito Policial de Londrina - Polícia Civil");
        condominio3.setLatitude(-23.3388045);
        condominio3.setLongitude(-51.1640626);
        Endereco endereco3 = new Endereco();
        endereco3.setLogradouro("Rua Das Alfaces");
        condominio3.setEndereco(endereco3);
        listaCondominio.add(condominio3);

        Condominio condominio4 = new Condominio();
        condominio4.setNome("Posto de Saúde Jardim do Sol");
        condominio4.setLatitude(-23.293626);
        condominio4.setLongitude(-51.180343);
        Endereco endereco4 = new Endereco();
        endereco4.setLogradouro("Rua Via Láctea, 877 - Jd do Sol\n" +
                "Londrina - PR +\n" +
                "86070-100");
        condominio4.setEndereco(endereco4);
        listaCondominio.add(condominio4);

        Condominio condominio5 = new Condominio();
        condominio5.setNome("Igreja Metodista do Jardim do Sol");
        condominio5.setLatitude(-23.293493);
        condominio5.setLongitude(-51.182537);
        Endereco endereco5 = new Endereco();
        endereco5.setLogradouro("R. Adulcino José Jordão, 553 - Jd do Sol\n" +
                "Londrina - PR\n" +
                "86070-150");
        condominio5.setEndereco(endereco5);
        listaCondominio.add(condominio5);

    }


}
