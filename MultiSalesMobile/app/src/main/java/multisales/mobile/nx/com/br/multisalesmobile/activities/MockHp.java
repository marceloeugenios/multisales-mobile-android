package multisales.mobile.nx.com.br.multisalesmobile.activities;

import com.google.android.gms.maps.MapView;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;

/**
 * Created by fabionx on 19/03/15.
 */
public class MockHp {

    public List<Hp> listahps = new ArrayList<Hp>();

    public MockHp(){

        Hp hp1 = new Hp();
        hp1.setNome("JOSE - Tizen SPA");
        hp1.setLatitude(-23.3368492);
        hp1.setLongitude(-51.16326);
        hp1.setLogradouro("Rua Das Couves");
        listahps.add(hp1);

        Hp hp2 = new Hp();
        hp2.setNome("Instituto de Laser e Hospitalar do Paraná");
        hp2.setLatitude(-23.3384665);
        hp2.setLongitude(-51.1632371);
        hp2.setLogradouro("Rua Das Abacaxis");
        listahps.add(hp2);

        Hp hp3 = new Hp();
        hp3.setNome("6º Distrito Policial de Londrina - Polícia Civil");
        hp3.setLatitude(-23.3388045);
        hp3.setLongitude(-51.1640626);
        hp3.setLogradouro("Rua Das alfaces");
        listahps.add(hp3);

        Hp hp4 = new Hp();
        hp4.setNome("Posto de Saúde Jardim do Sol");
        hp4.setLatitude(-23.293626);
        hp4.setLongitude(-51.180343);
        hp4.setLogradouro("Rua Via Láctea, 877 - Jd do Sol\n" +
                "Londrina - PR\n" +
                "86070-100");
        listahps.add(hp4);

        Hp hp5 = new Hp();
        hp5.setNome("Igreja Metodista do Jardim do Sol");
        hp5.setLatitude(-23.293493);
        hp5.setLongitude(-51.182537);
        hp5.setLogradouro("R. Adulcino José Jordão, 553 - Jd do Sol\n" +
                "Londrina - PR\n" +
                "86070-150");
        listahps.add(hp5);

    }
}
