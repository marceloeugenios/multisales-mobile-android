package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cidade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Concorrente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Estado;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogDiario;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoMigracao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.servico.LogDiarioServico;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class NaoVendaActivity extends ActionBarActivity {

    private EntityManager entityManager;

    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    public AsyncTask<Void, Void, ResponseD2D> taskNaoVenda;

    private Tabulacao tabulacao;
    private Hp hp;
    private Cidade cidade;
    private MotivoTabulacao motivoNaoVenda;
    private List<MotivoTabulacao> motivosTabulacao;
    private List<Estado> estados;
    private List<Cidade> cidades;
    private List<Concorrente> concorrentes;
    private List<MotivoMigracao> motivosMigracao;

    private EditText txtNomeCliente;
    private EditText txtTelResidencial;
    private EditText txtTelComercial;
    private EditText txtTelCelular;
    private EditText txtEndereco;
    private EditText txtComplemento;
    private EditText txtCep;
    private EditText txtBairro;
    private Spinner  spinnerEstado;
    private Spinner  spinnerCidade;
    private EditText txtObservacao;
    private Spinner  spinnerMotivoNaoVenda;
    private TextView txtViewConcorrente;
    private Spinner  spinnerConcorrente;
    private TextView txtViewMotivoMigracao;
    private Spinner  spinnerMotivoMigracao;
    private LogDiarioServico logDiarioServico;
    private MultiSales multiSales;
    private Integer idCondominio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nao_venda);
        instanciarComponentes();
        entityManager = new EntityManager(this);
        logDiarioServico = new LogDiarioServico(this);
        multiSales = (MultiSales)getApplication();

        idCondominio = getIntent().getIntExtra("idCondominio", 0);

        carregarMotivosTabulacao();
        carregarEstados();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void instanciarComponentes() {
        txtNomeCliente        = (EditText) findViewById(R.id.txtNomeCliente);
        txtTelResidencial     = (EditText) findViewById(R.id.txtTelResidencial);
        UtilMask.setMascaraTelefone(txtTelResidencial);
        txtTelComercial       = (EditText) findViewById(R.id.txtTelComercial);
        UtilMask.setMascaraTelefone(txtTelComercial);
        txtTelCelular         = (EditText) findViewById(R.id.txtTelCelular);
        UtilMask.setMascaraTelefone(txtTelCelular);
        txtEndereco           = (EditText) findViewById(R.id.txtEndereco);
        txtComplemento        = (EditText) findViewById(R.id.txtComplemento);
        txtCep                = (EditText) findViewById(R.id.txtCep);
        UtilMask.setMascaraCep(txtCep);
        txtBairro             = (EditText) findViewById(R.id.txtBairro);
        spinnerEstado         = (Spinner) findViewById(R.id.spinnerEstado);
        spinnerCidade         = (Spinner) findViewById(R.id.spinnerCidade);
        txtObservacao         = (EditText) findViewById(R.id.txtObservacao);
        spinnerMotivoNaoVenda = (Spinner) findViewById(R.id.spinnerMotivoNaoVenda);

        //Concorrência
        txtViewConcorrente    = (TextView) findViewById(R.id.txtViewConcorrente);
        spinnerConcorrente    = (Spinner) findViewById(R.id.spinnerConcorrente);
        txtViewMotivoMigracao = (TextView) findViewById(R.id.txtViewMotivoMigracao);
        spinnerMotivoMigracao = (Spinner) findViewById(R.id.spinnerMotivoMigracao);
        esconderCamposConcorrencia();
    }

    private void carregarMotivosTabulacao() {
        try {
            List<MotivoTabulacaoTipo> motivosTabulacaoTipo = entityManager.getByWhere(MotivoTabulacaoTipo.class, "descricao = '" + EMotivoTabulacaoTipo.NAO_VENDA.getDescricao() + "'", null);
            if (motivosTabulacaoTipo != null
                    && !motivosTabulacaoTipo.isEmpty()) {

                MotivoTabulacaoTipo motivoNaoVenda = motivosTabulacaoTipo.get(SistemaConstantes.ZERO);

                motivosTabulacao = entityManager.getByWhere(MotivoTabulacao.class,
                        "_motivo_tabulacao_tipo = " + motivoNaoVenda.getId(), null);
                final List<String> descricoes = new ArrayList<>();
                if (motivosTabulacao != null && !motivosTabulacao.isEmpty()) {
                    descricoes.add(UtilActivity.SELECIONE);
                    for(MotivoTabulacao mot : motivosTabulacao){
                        descricoes.add(mot.getDescricao());
                    }
                }
                setAdapter(spinnerMotivoNaoVenda, descricoes);

                spinnerMotivoNaoVenda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position > SistemaConstantes.ZERO) {
                            MotivoTabulacao cononcorrencia = motivosTabulacao.get(position - 1);
                            if (cononcorrencia.getConcorrencia() == EBoolean.TRUE) {

                                setVisible(txtViewConcorrente);
                                setVisible(spinnerConcorrente);
                                setVisible(txtViewMotivoMigracao);
                                setVisible(spinnerMotivoMigracao);

                                if (concorrentes == null || concorrentes.isEmpty()) {
                                    try {
                                        concorrentes = entityManager.getAll(Concorrente.class);
                                        List<String> descricoesConcorrentes = new ArrayList<String>();
                                        descricoesConcorrentes.add(UtilActivity.SELECIONE);
                                        for (Concorrente c : concorrentes) {
                                            descricoesConcorrentes.add(c.getDescricao());
                                        }
                                        setAdapter(spinnerConcorrente, descricoesConcorrentes);
                                    } catch (DataBaseException e) {
                                        e.printStackTrace();
                                        makeShortToast("ERRO ao listar Concorrentes!");
                                    }
                                }

                                if (motivosMigracao == null || motivosMigracao.isEmpty()) {
                                    try {
                                        motivosMigracao = entityManager.getAll(MotivoMigracao.class);
                                        List<String> motivosMigracaoDescricoes = new ArrayList<String>();
                                        motivosMigracaoDescricoes.add(UtilActivity.SELECIONE);
                                        for (MotivoMigracao motivo : motivosMigracao) {
                                            motivosMigracaoDescricoes.add(motivo.getDescricao());
                                        }
                                        setAdapter(spinnerMotivoMigracao, motivosMigracaoDescricoes);
                                    } catch (DataBaseException e) {
                                        e.printStackTrace();
                                        makeShortToast("ERRO ao listar Motivos de Migração!");
                                    }
                                }
                            } else {
                                esconderCamposConcorrencia();
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }

    private void carregarEstados() {
        try {
            estados = entityManager.getAll(Estado.class);
            List<String> nomesEstados = new ArrayList<>();
            nomesEstados.add(UtilActivity.SELECIONE);

            for(Estado estado : estados){
                nomesEstados.add(estado.getNome());
            };
            setAdapter(spinnerEstado, nomesEstados);

            spinnerEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    List<String> nomesCidades = new ArrayList<>();

                    if (position > SistemaConstantes.ZERO) {
                        Estado estadoSelecionado = estados.get(position - SistemaConstantes.UM);

                        try {
                            cidades = entityManager.getAll(Cidade.class);
                            nomesCidades.add(UtilActivity.SELECIONE);
                            if (cidades != null && !cidades.isEmpty()) {
                                for (Cidade cidade : cidades) {
                                    nomesCidades.add(cidade.getNome());
                                }
                            }
                            spinnerCidade.setEnabled(true);
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                            makeShortToast("Ops! Ocorreu um ERRO ao listar as cidades! :(");
                            cidades = new ArrayList<>();
                        }

                    } else {
                        nomesCidades.add("SELECIONE O ESTADO");
                        spinnerCidade.setEnabled(false);
                    }
                    setAdapter(spinnerCidade, nomesCidades);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nao_venda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            this.finish();
        }

        if (id == R.id.action_salvar_nao_venda) {

            if (!validarCamposObrigatorios()) {
                return false;
            }

            try {
                popularHp();
                popularTabulacao();

                tabulacao.setHp(entityManager.save(hp));
                tabulacao = entityManager.save(tabulacao);
                tabulacao.setCompleta(EBoolean.TRUE);
                entityManager.atualizar(tabulacao);
                entityManager.initialize(tabulacao.getHp());
                if (idCondominio != 0) {
                    entityManager.initialize(tabulacao.getHp().getCondominio());
                }

                try {
                    LogDiario logDiario = logDiarioServico.obterLogDiarioAtualPorUsuario(multiSales.getLogin());
                    logDiario.setNaoVendas(logDiario.getNaoVendas() + 1);
                    entityManager.atualizar(logDiario);
                } catch (Exception e) {
                    UtilActivity.makeShortToast("ERRO ao atualizar contadores!", getApplicationContext());
                }

                taskNaoVenda = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D = null;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.NAO_VENDA_CODIGO.getCodigo());
                            requestD2D.setTabulacao(tabulacao);
                            requestD2D.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                            requestD2D.setIdUsuario(multiSales.getIdUsuario());
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        Intent mainIntent = new Intent(NaoVendaActivity.this, MainActivity.class);
                        if(resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                tabulacao.setId((Integer)resposta.getExtras().get("idNaoVenda"));
                                entityManager.atualizar(tabulacao);
                                mainIntent.putExtra("mensagem", "Tabulação " + tabulacao.getIdLocal() + " salva com SUCESSO!");
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("ERRO ao salvar Tabulação!", getApplicationContext());
                            }
                        } else {
                            mainIntent.putExtra("mensagem", "Tabulação salva mas não sincronizada!");
                        }
                        startActivity(mainIntent);
                        NaoVendaActivity.this.finish();
                    }
                };
                taskNaoVenda.execute();

            } catch (DataBaseException e) {
                e.printStackTrace();
                makeShortToast("ERRO ao salvar Tabulação!");
                return false;
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean validarCamposObrigatorios() {

        if (txtNomeCliente.getText().toString().isEmpty()) {

            //TODO decidir como ficarão marcados campos obrigatórios não preenchidos
            setError(txtNomeCliente);
            txtNomeCliente.setHint("Informe o nome");
            Drawable x = this.getResources().getDrawable(R.drawable.abc_edit_text_material);
            x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());
            txtNomeCliente.setCompoundDrawables(null, null, x, null);
            return false;
        }

        if (txtTelResidencial.getText().toString().isEmpty()
                && txtTelCelular.getText().toString().isEmpty()
                && txtTelComercial.getText().toString().isEmpty()) {
            makeShortToast("Informe ao menos um telefone.");
            return false;
        }

        if (spinnerCidade.getSelectedItemPosition() == SistemaConstantes.ZERO) {
            makeShortToast("Informe a cidade");
            return false;
        } else {
            this.cidade = cidades.get(spinnerCidade.getSelectedItemPosition() - SistemaConstantes.UM);
        }

        if (spinnerMotivoNaoVenda.getSelectedItemPosition() == SistemaConstantes.ZERO) {
            makeShortToast("Informe o motivo da não venda");
            return false;
        } else {
            this.motivoNaoVenda = motivosTabulacao
                    .get(spinnerMotivoNaoVenda.getSelectedItemPosition() - SistemaConstantes.UM);
        }

        if (motivoNaoVenda.getConcorrencia() == EBoolean.TRUE) {
            if (spinnerConcorrente.getSelectedItemPosition() == SistemaConstantes.ZERO) {
                makeShortToast("Informe a Concorrente");
                return false;
            }
            if(spinnerMotivoMigracao.getSelectedItemPosition() == SistemaConstantes.ZERO) {
                makeShortToast("Informe o motivo de migração");
                return false;
            }
        }

        return true;

    }

    private void setError(TextView textView) {
        textView.setError("Campo obrigatório");
    }


    private Hp popularHp() throws DataBaseException {

        this.hp = new Hp();
        hp.setNome(txtNomeCliente.getText().toString());
        hp.setTelefone1(UtilMask.unmask(txtTelResidencial.getText().toString()));
        hp.setTelefone2(UtilMask.unmask(txtTelComercial.getText().toString()));
        hp.setTelefone3(UtilMask.unmask(txtTelCelular.getText().toString()));
        hp.setLogradouro(txtEndereco.getText().toString());
        hp.setCidade(cidade);
        hp.setComplemento(txtComplemento.getText().toString());
        hp.setCep(UtilMask.unmask(txtCep.getText().toString()));
        hp.setBairro(txtBairro.getText().toString());
        hp.setUltimoMotivoTabulacao(motivoNaoVenda);
        hp.setObservacao(txtObservacao.getText().toString());
        if (idCondominio != 0) {
            hp.setCondominio(entityManager.getById(Condominio.class, idCondominio));
        }

        return hp;
    }

    //TODO setar usuário de inclusão da tabulação
    private Tabulacao popularTabulacao() {
        tabulacao = new Tabulacao();
        tabulacao.setMotivoTabulacao(motivoNaoVenda);
        tabulacao.setDataCadastro(Calendar.getInstance());

        if (spinnerConcorrente.getSelectedItemPosition() > SistemaConstantes.ZERO
                && concorrentes != null) {
            tabulacao.setConcorrenteMigracao(
                    concorrentes.get(
                            spinnerConcorrente.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        if (spinnerMotivoMigracao.getSelectedItemPosition() > SistemaConstantes.ZERO
                && motivosMigracao != null) {
            tabulacao.setMotivoMigracao(
                    motivosMigracao.get(
                            spinnerMotivoMigracao.getSelectedItemPosition() - SistemaConstantes.UM));
        }
        return tabulacao;
    }

    private void makeShortToast(String mensagem) {
        Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();
    }

    private void setVisible(View v) {
        v.setVisibility(View.VISIBLE);
    }

    private void setInvisible(View v) {
        v.setVisibility(View.INVISIBLE);
    }

    private void setAdapter(Spinner spinner, List<String> strings) {
        ArrayAdapter adapter = new ArrayAdapter<> (this, android.R.layout.simple_spinner_item, strings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void esconderCamposConcorrencia() {
        setInvisible(txtViewConcorrente);
        setInvisible(spinnerConcorrente);
        setInvisible(txtViewMotivoMigracao);
        setInvisible(spinnerMotivoMigracao);
        spinnerConcorrente.setSelection(SistemaConstantes.ZERO);
        spinnerMotivoMigracao.setSelection(SistemaConstantes.ZERO);
    }

}
