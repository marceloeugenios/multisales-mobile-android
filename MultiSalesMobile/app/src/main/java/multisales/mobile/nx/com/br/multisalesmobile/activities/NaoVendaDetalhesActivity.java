package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.BandeiraSistemaProduto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.BandeiraSistemaProdutoPK;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class NaoVendaDetalhesActivity extends ActionBarActivity {

    private Integer idNaoVenda;

    private RelativeLayout concorrenteRL;

    private EntityManager entityManager;

    private TextView nomeHpTV;

    private TextView dataNaoVendaTV;

    private TextView motivoNaoVendaTV;

    private TextView enderecoTV;

    private TextView cidadeEstadoTV;

    private TextView cepTV;

    private TextView residencialTV;

    private TextView comercialTV;

    private TextView celularTV;

    private TextView concorrenteTV;

    private TextView motivoMigracaoTV;

    private Tabulacao tabulacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nao_venda_detalhes);
        entityManager = new EntityManager(this);
        concorrenteRL = (RelativeLayout) findViewById(R.id.concorrenteRL);
        concorrenteRL.setVisibility(View.GONE);
        idNaoVenda = getIntent().getIntExtra("idNaoVenda", SistemaConstantes.ZERO);
        atribuirComponentes();
        try {
            tabulacao = entityManager.getById(Tabulacao.class, idNaoVenda);
            entityManager.initialize(tabulacao.getHp());
            entityManager.initialize(tabulacao.getMotivoTabulacao());
            entityManager.initialize(tabulacao.getHp().getCidade());
            entityManager.initialize(tabulacao.getHp().getCidade().getEstado());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

            nomeHpTV.setText(tabulacao.getHp().getNome());
            dataNaoVendaTV.setText(simpleDateFormat.format(tabulacao.getDataCadastro().getTime()));
            motivoNaoVendaTV.setText(tabulacao.getMotivoTabulacao().getDescricao());

            StringBuilder enderecoCompleto = new StringBuilder();
            int aux = 0 ;
            if(tabulacao.getHp().getLogradouro() != null && !tabulacao.getHp().getLogradouro().isEmpty()){
                enderecoCompleto.append(tabulacao.getHp().getLogradouro());
                enderecoCompleto.append(",");
                aux ++ ;
            }
            if(tabulacao.getHp().getComplemento() != null && !tabulacao.getHp().getComplemento().isEmpty()){
                enderecoCompleto.append(tabulacao.getHp().getComplemento());
                if(aux > 0) {
                    enderecoCompleto.append(",");
                }
                aux ++;
            }
            if(tabulacao.getHp().getBairro() != null && !tabulacao.getHp().getBairro().isEmpty()){
                enderecoCompleto.append(tabulacao.getHp().getBairro());
                aux++;
            }

            if(aux > 0 ){
                enderecoTV.setText(enderecoCompleto.toString());
            } else {
                enderecoTV.setText("-");
            }

            StringBuilder cidadeEstado = new StringBuilder();
            cidadeEstado.append(tabulacao.getHp().getCidade().getNome());
            cidadeEstado.append("/");
            cidadeEstado.append(tabulacao.getHp().getCidade().getEstado().getUf());
            cidadeEstadoTV.setText(cidadeEstado.toString());

            String cep = tabulacao.getHp().getCep();
            if(tabulacao.getHp().getCep() != null
                    && !tabulacao.getHp().getCep().isEmpty()) {
                if (cep != null && !cep.isEmpty() && cep.length() > 7) {
                    String part1 = cep.substring(0, 5);
                    String part2 = cep.substring(5, 8);
                    cep = part1 + "-" + part2;
                } else {
                    cep = tabulacao.getHp().getCep().toString();
                }
                cepTV.setText(cep);
            }else{
                cepTV.setText("-");
            }


            SpannableString content;
            if(tabulacao.getHp().getTelefone1() != null
                    && !tabulacao.getHp().getTelefone1().isEmpty()  ){
                content = new SpannableString(tabulacao.getHp().getTelefone1());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                residencialTV.setText(UtilActivity.formatarTelefone(content.toString()));
            } else {
                residencialTV.setText("-");
            }

            if(tabulacao.getHp().getTelefone2() != null
                    && !tabulacao.getHp().getTelefone2().isEmpty()){
                content = new SpannableString(tabulacao.getHp().getTelefone2());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                comercialTV.setText(UtilActivity.formatarTelefone(content.toString()));
            } else {
                comercialTV.setText("-");
            }

            if(tabulacao.getHp().getTelefone3() != null
                    && !tabulacao.getHp().getTelefone3().isEmpty()){
                content = new SpannableString(tabulacao.getHp().getTelefone3());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                celularTV.setText(UtilActivity.formatarTelefone(content.toString()));
            } else {
                celularTV.setText("-");
            }

            if (tabulacao.getMotivoTabulacao().getConcorrencia().equals(EBoolean.TRUE)) {
                concorrenteRL.setVisibility(View.VISIBLE);
                entityManager.initialize(tabulacao.getConcorrenteMigracao());
                entityManager.initialize(tabulacao.getMotivoMigracao());
                if(tabulacao.getConcorrenteMigracao() != null){
                    concorrenteTV.setText(tabulacao.getConcorrenteMigracao().getDescricao());
                } else{
                    concorrenteTV.setText("-");
                }
                if(tabulacao.getConcorrenteMigracao() != null){
                    motivoMigracaoTV.setText(tabulacao.getMotivoMigracao().getDescricao());
                } else{
                    motivoMigracaoTV.setText("-");
                }

            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (DataBaseException e) {
            e.printStackTrace();
        }

    }

    private void atribuirComponentes() {
        nomeHpTV = (TextView) findViewById(R.id.nomeHpTV);
        dataNaoVendaTV  = (TextView) findViewById(R.id.dataNaoVendaTV);
        motivoNaoVendaTV  = (TextView) findViewById(R.id.motivoNaoVendaTV);
        enderecoTV  = (TextView) findViewById(R.id.enderecoTV);
        cidadeEstadoTV  = (TextView) findViewById(R.id.cidadeEstadoTV);
        cepTV  = (TextView) findViewById(R.id.cepTV);
        residencialTV = (TextView) findViewById(R.id.residencialTV);
        comercialTV  = (TextView) findViewById(R.id.comercialTV);
        celularTV  = (TextView) findViewById(R.id.celularTV);
        concorrenteTV  = (TextView) findViewById(R.id.concorrenteTV);
        motivoMigracaoTV  = (TextView) findViewById(R.id.motivoMigracaoTV);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nao_venda_detalhes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void discar(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        if (view.equals(residencialTV)) {
            intent.setData(Uri.parse("tel:" + tabulacao.getHp().getTelefone1()));
            startActivity(intent);
        } else if (view.equals(comercialTV)) {
            intent.setData(Uri.parse("tel:" + tabulacao.getHp().getTelefone2()));
            startActivity(intent);
        } else if (view.equals(celularTV)) {
            intent.setData(Uri.parse("tel:" + tabulacao.getHp().getTelefone3()));
            startActivity(intent);
        }
    }
}
