package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class NaoVendasActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private ListView listView;
    private EntityManager entityManager;
    private List<Tabulacao> tabulacoes;
    private RelativeLayout syncRL;
    private Button sincronizarBT;
    private Integer alturaSync;
    public  AsyncTask<Void, Void, ResponseD2D> taskNaoVenda;
    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    private Tabulacao tabulacaoAtual;
    private List<Tabulacao> naoSincronizadas;
    private int contadorSincronizacao = 0;
    private boolean syncRLIsVisible = false;
    private MultiSales multiSales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nao_vendas);
        multiSales = (MultiSales) getApplication();
        syncRLIsVisible = true;
        entityManager = new EntityManager(this);

        tabulacoes = listarNaoVendas();

        TabulacaoListAdapter adapter = new TabulacaoListAdapter(this, tabulacoes);

        listView = (ListView) findViewById(R.id.listView);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        syncRL = (RelativeLayout) findViewById(R.id.syncRL);
        sincronizarBT = (Button) findViewById(R.id.sincronizarBT);

        naoSincronizadas = new ArrayList<>();
        for (Tabulacao tabulacao : tabulacoes) {
            if (tabulacao.getId() == null) {
                naoSincronizadas.add(tabulacao);
            }
        }

        if (naoSincronizadas.size() > 0 && UtilActivity.isOnline(this)) {

            ViewTreeObserver vto = syncRL.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    alturaSync = syncRL.getMeasuredHeight();
                }
            });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(syncRLIsVisible) {
                        syncRL.animate().setDuration(500);
                        syncRL.animate().translationY(alturaSync);
                    }
                }
            }, 5000);

        } else {
            syncRL.setVisibility(View.INVISIBLE);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private List<Tabulacao> listarNaoVendas() {
        try {
            List<MotivoTabulacaoTipo> motivosTabulacaoTipo = entityManager.getByWhere(MotivoTabulacaoTipo.class, "descricao = '" + EMotivoTabulacaoTipo.NAO_VENDA.getDescricao() + "'", null);
            if (motivosTabulacaoTipo != null
                    && !motivosTabulacaoTipo.isEmpty()) {

                MotivoTabulacaoTipo motivoNaoVenda = motivosTabulacaoTipo.get(SistemaConstantes.ZERO);

                List<Tabulacao> tabulacoes = new ArrayList<>();
                StringBuilder stringBuilder =  new StringBuilder();
                stringBuilder.append("SELECT tab.* ");
                stringBuilder.append("FROM ");
                stringBuilder.append("tabulacao tab ");
                stringBuilder.append("INNER JOIN ");
                stringBuilder.append("motivo_tabulacao mtab ");
                stringBuilder.append("ON ");
                stringBuilder.append("tab._motivo_tabulacao = mtab.id ");
                stringBuilder.append("WHERE ");
                stringBuilder.append("mtab._motivo_tabulacao_tipo =  ");
                stringBuilder.append(motivoNaoVenda.getId());
                tabulacoes = entityManager.select(Tabulacao.class, stringBuilder.toString());

                for (Tabulacao tabulacao : tabulacoes) {
                    entityManager.initialize(tabulacao.getHp());
                    entityManager.initialize(tabulacao.getMotivoTabulacao());
                }

                return tabulacoes;
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao listar as não vendas", this);
            finish();
        }

        return new ArrayList<>();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nao_vendas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        if (id == R.id.action_sincronizar) {
            sincronizarNaoVendas();
        }

        if (id == R.id.novo) {
            Intent intentNaoVenda = new Intent(this, NaoVendaActivity.class);
            startActivity(intentNaoVenda);
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentDetalhes = new Intent(this, NaoVendaDetalhesActivity.class);
        intentDetalhes.putExtra("idNaoVenda", tabulacoes.get(position).getIdLocal());
        startActivity(intentDetalhes);
        this.finish();
    }

    public void sincronizar(View view) {
        sincronizarBT.setEnabled(false);
        syncRL.animate().setDuration(500);
        syncRL.animate().translationY(alturaSync);
        sincronizarNaoVendas();
    }

    private void sincronizarNaoVendas() {
        try {
            if (naoSincronizadas.size() > 0) {
                tabulacaoAtual = naoSincronizadas.get(contadorSincronizacao);
                entityManager.initialize(tabulacaoAtual.getHp());

                taskNaoVenda = new AsyncTask<Void, Void, ResponseD2D>() {
                    @Override
                    protected ResponseD2D doInBackground(Void... params) {
                        JSONObject jsonEnvio;
                        JSONObject jsonRetorno;
                        ResponseD2D responseD2D;
                        try {
                            requestD2D.setOperacao(ECodigoOperacaoD2D.NAO_VENDA_CODIGO.getCodigo());
                            requestD2D.setTabulacao(tabulacaoAtual);
                            requestD2D.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                            requestD2D.setIdUsuario(multiSales.getIdUsuario());
                            jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                            jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                            responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                        } catch (Exception e) {
                            responseD2D = new ResponseD2D();
                            responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                            responseD2D.setMensagem(e.getMessage());
                            return responseD2D;
                        }
                        return responseD2D;
                    }

                    @Override
                    protected void onPostExecute(ResponseD2D resposta) {
                        super.onPostExecute(resposta);
                        if (resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                            try {
                                tabulacaoAtual.setId((Integer) resposta.getExtras().get("idNaoVenda"));
                                entityManager.atualizar(tabulacaoAtual);
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("ERRO ao salvar Tabulação!", getApplicationContext());
                            }
                            contadorSincronizacao++;
                            if (naoSincronizadas.size() == contadorSincronizacao) {
                                naoSincronizadas = new ArrayList<>();
                                tabulacoes = listarNaoVendas();
                                for (Tabulacao tabulacao : tabulacoes) {
                                    if (tabulacao.getId() == null) {
                                        naoSincronizadas.add(tabulacao);
                                    }
                                }
                                if (naoSincronizadas.size() == 0) {
                                    UtilActivity.makeShortToast("Todas tabulações sincronizadas com sucesso!", getApplicationContext());
                                }

                                TabulacaoListAdapter adapter = new TabulacaoListAdapter(NaoVendasActivity.this, tabulacoes);
                                listView.setAdapter(adapter);
                            } else {
                                sincronizarNaoVendas();
                            }
                        }
                    }
                };
                taskNaoVenda.execute();
            } else {
                UtilActivity.makeShortToast("Não há tabulações para sincronizar", getApplicationContext());
            }

        } catch (DataBaseException e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar Tabulação!", this);
        }

    }
}
