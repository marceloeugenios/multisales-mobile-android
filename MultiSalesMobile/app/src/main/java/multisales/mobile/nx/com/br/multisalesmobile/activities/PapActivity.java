package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.utils.SlidingTabLayout;

public class PapActivity extends ActionBarActivity  {

    private static final int NUM_PAGINAS = 2;
    public static final String TAG = "NX_PAP_ACTIVITY";
    public static final String LAYOUT = "VIVO";
    //public static final String LAYOUT = "NET";

    private ViewPager viewPager;
    private PapPageAdapter papPageAdapter;
    private List<Fragment> fragmentList;
    private TabAdapter adapter;
    private SlidingTabLayout tabs;
    private List<String> titles;

    public static FragmentManager fragmentManager;
    public List<Object> pontos = new ArrayList<Object>();
    public ListView listView;
    public boolean isBlocked = false;
    public RelativeLayout aguardeLocal;
    public RelativeLayout aguardeGps;
    public Location meuLocal;
    public Integer raio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(TAG,"ON CREATE");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pap);
        fragmentManager = getSupportFragmentManager();
        fragmentList = getFragments();
        titles = getTitles();

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new TabAdapter(getSupportFragmentManager(), titles, fragmentList);

        //papPageAdapter = new PapPageAdapter(getSupportFragmentManager(),fragmentList);
        viewPager = (ViewPager) findViewById(R.id.view_pager_pap);

        aguardeLocal = (RelativeLayout) findViewById(R.id.aguardeLocal);
        viewPager.setAdapter(adapter);


        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabsPap);
        tabs.setDistributeEvenly(false); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.cor_destaque);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(viewPager);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pap,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        GmFragment gmFragment = (GmFragment)fragmentList.get(0);

        switch (id){
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.raio2km:
                raio = 2;
                gmFragment.obterHps(meuLocal, raio);
                return true;

            case R.id.raio5km:
                raio = 5;
                gmFragment.obterHps(meuLocal, raio);
                return true;

            case R.id.raio7km:
                raio = 7;
                gmFragment.obterHps(meuLocal, raio);
                return true;

            case R.id.raio9km:
                raio = 9;
                gmFragment.obterHps(meuLocal, raio);
                return true;

            case R.id.outro_raio:
                obterOutroRaio();
                return true;

            case R.id.novo_condominio:
                Intent intentCondominio = new Intent(this, CondominioActivity.class);
                startActivity(intentCondominio);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private List<Fragment> getFragments(){
        List<Fragment> fList = new ArrayList<Fragment>();

        fList.add(GmFragment.newInstance(0,"1"));
        fList.add(HpsFragment.newInstance(1, "2"));

        return fList;
    }

    private List<String> getTitles(){
        List<String> sList = new ArrayList<String>();
        sList.add("MAPA");

        if(LAYOUT == "VIVO"){
            sList.add("CONDOMINIOS");
        } else {
            sList.add("HPS");
        }

        return sList;
    }




    public void mostrarOverlay() {
        aguardeLocal.setVisibility(View.VISIBLE);
        isBlocked = true;
    }

    public void esconderOverlay() {
        aguardeLocal.setVisibility(View.GONE);
        isBlocked = false;
    }

    private void obterOutroRaio(){
        final GmFragment gmFragment = (GmFragment)fragmentList.get(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Raio");
        builder.setMessage("Selecione um novo raio");
        final NumberPicker numberPicker = new NumberPicker(this);
        numberPicker.setMaxValue(1000);
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setValue(9);
        builder.setView(numberPicker);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                // Show location settings when the user acknowledges the alert dialog
                //  Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                //  startActivity(intent);
                raio = numberPicker.getValue();
                gmFragment.obterHps(meuLocal, raio);
            }
        });
        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent pEvent) {
        if (!isBlocked) {
            return super.dispatchTouchEvent(pEvent);
        }
        return isBlocked;
    }
}

