package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioPlantao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;

/**
 * Created by fabionx on 16/03/15.
 */
public class PapListAdapter extends ArrayAdapter {

    //protected static final String CATEGORIA = "HPS";
    private Context context;
    private List<Object> lista;
    private String textoPlantao;
    private String corTextoPlantao;
    public PapListAdapter(Context context, List<Object> lista) {
        super(context, R.layout.item_pap_hps, lista);
        this.context = context;
        this.lista = lista;
    }
    public int getCount() {
        return lista.size();
    }
    public Object getItem(int posicao) {

        if(PapActivity.LAYOUT == "VIVO"){
            Condominio condominio = (Condominio) lista.get(posicao);
            return (Object) condominio;
        } else if(PapActivity.LAYOUT == "NET"){
            Hp hp = (Hp) lista.get(posicao);
            return (Object) hp;
        } else {
            return false;
        }

    }
    public long getItemId(int posicao) {
        return posicao;
    }

    public View getView(int posicao, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_pap_hps, null);
        TextView textNome = (TextView) v.findViewById(R.id.nomePapHp);
        TextView textNome2 = (TextView) v.findViewById(R.id.descricaoPapHp);
        TextView textNome3 = (TextView) v.findViewById(R.id.statusPlantao);
        TextView textNome4 = (TextView) v.findViewById(R.id.idHp);

        if(PapActivity.LAYOUT == "VIVO"){
            EntityManager entityManager = new EntityManager(getContext());

            Condominio condominio = (Condominio) lista.get(posicao);

            boolean temPlantao = false;
            List<CondominioPlantao> plantoes = null;

            try {
                plantoes = entityManager.getByWhere(CondominioPlantao.class, "_condominio = "+condominio.getIdLocal(),null);
            } catch (DataBaseException e) {
                e.printStackTrace();
            }
            if(plantoes != null && !plantoes.isEmpty()){
                temPlantao = true;
            }

            if(temPlantao){
                textoPlantao = "EXISTEM PLANTÕES NESTE CONDOMÍNIO" ;
                corTextoPlantao = "#3D9300";
            } else {
                textoPlantao = "NÃO EXISTEM PLANTÕES NESTE CONDOMÍNIO";
                corTextoPlantao = "#dd4433" ;
            }

            textNome.setText(condominio.getNome());
            textNome2.setText(condominio.getEndereco().getLogradouro() + " , " + condominio.getEndereco().getNumero());


            textNome3.setTextColor(Color.parseColor(corTextoPlantao));
            textNome3.setText(textoPlantao);
            textNome3.setVisibility(View.VISIBLE);



            // Atualiza o valor do Text para o nome do Smile

            textNome4.setText(Integer.toString(condominio.getIdLocal()));

        } else if (PapActivity.LAYOUT == "NET"){

            Hp hp = (Hp) lista.get(posicao);
            textNome.setText(hp.getNome());
            textNome2.setText(hp.getLogradouro());

        }

        return v;
    }
}
