package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by fabionx on 15/03/15.
 */
public class PapPageAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    public PapPageAdapter (FragmentManager fragmentManager, List<Fragment> fragments){
        super(fragmentManager);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }
}
