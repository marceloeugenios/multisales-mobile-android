package multisales.mobile.nx.com.br.multisalesmobile.activities;


import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by fabionx on 16/03/15.
 */
public class PapSource implements LocationSource {

    private OnLocationChangedListener listener;
    private LocationManager locationManager;
    private PapActivity papActivity;
    private GoogleMap googleMap;
    public static String TAG = "NX_PAP_SOURCE";


    public PapSource(PapActivity papActivity,GoogleMap googleMap){
        this.papActivity = papActivity;
        googleMap = googleMap;
        this.locationManager = (LocationManager) papActivity.getSystemService(Context.LOCATION_SERVICE);

    }

    @Override
    public void activate(OnLocationChangedListener listener) {
        this.listener = listener;
    }
    @Override
    public void deactivate() {
        this.listener = null;
    }

    public void setLocation(Location location) {
        Log.d(TAG,"Atualiza o Mylocation");
        if(this.listener != null) {
            this.listener.onLocationChanged(location);
        }
    }

    public void setLocation(LatLng latLng) {
        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        if(this.listener != null) {
            this.listener.onLocationChanged(location);
        }
    }

}
