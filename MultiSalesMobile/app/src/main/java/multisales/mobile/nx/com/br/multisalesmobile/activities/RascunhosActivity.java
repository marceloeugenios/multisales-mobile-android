package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class RascunhosActivity extends ActionBarActivity implements AdapterView.OnItemClickListener  {

    private ListView listView;
    private EntityManager entityManager;
    private List<Tabulacao> tabulacoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rascunhos);
        entityManager = new EntityManager(this);
        listView = (ListView) findViewById(R.id.listView);

        TabulacaoListAdapter adapter = new TabulacaoListAdapter(this, listarVendas());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private List<Tabulacao> listarVendas() {
        try {
            List<MotivoTabulacaoTipo> motivosTabulacaoTipo = entityManager.getByWhere(MotivoTabulacaoTipo.class, "descricao = '" + EMotivoTabulacaoTipo.VENDA.getDescricao() + "'", null);
            if (motivosTabulacaoTipo != null
                    && !motivosTabulacaoTipo.isEmpty()) {

                MotivoTabulacaoTipo motivoVenda = motivosTabulacaoTipo.get(SistemaConstantes.ZERO);

                tabulacoes = new ArrayList<>();
                StringBuilder stringBuilder =  new StringBuilder();
                stringBuilder.append("SELECT tab.* ");
                stringBuilder.append("FROM ");
                stringBuilder.append("tabulacao tab ");
                stringBuilder.append("INNER JOIN ");
                stringBuilder.append("motivo_tabulacao mtab ");
                stringBuilder.append("ON ");
                stringBuilder.append("tab._motivo_tabulacao = mtab.id ");
                stringBuilder.append("WHERE ");
                stringBuilder.append("mtab._motivo_tabulacao_tipo =  " + motivoVenda.getId() + " ");
                stringBuilder.append("AND ");
                stringBuilder.append("tab.completa =  \"" + EBoolean.FALSE.toString() + "\"");
                tabulacoes = entityManager.select(Tabulacao.class, stringBuilder.toString());

                for (Tabulacao tabulacao : tabulacoes) {
                    entityManager.initialize(tabulacao.getMotivoTabulacao());
                    entityManager.initialize(tabulacao.getVenda());
                    entityManager.initialize(tabulacao.getVenda().getCliente());
                    if (tabulacao.getVenda().getCombinacaoProdutoTipo() != null) {
                        entityManager.initialize(tabulacao.getVenda().getCombinacaoProdutoTipo());
                    }
                }

                return tabulacoes;
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao listar as vendas", this);
            finish();
        }

        return new ArrayList<>();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rascunhos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        if (id == R.id.apagar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            try {
                                for (Tabulacao tabulacao : tabulacoes) {
                                    UtilActivity.deletarVenda(entityManager, tabulacao.getIdLocal());
                                }
                                TabulacaoListAdapter adapter = new TabulacaoListAdapter(RascunhosActivity.this, listarVendas());

                                listView.setAdapter(adapter);
                                listView.setOnItemClickListener(RascunhosActivity.this);
                            } catch (Exception e) {
                                UtilActivity.makeShortToast("Erro ao apagar os rascunhos", RascunhosActivity.this);
                            }
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente apagar todos rascunhos?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentVenda = new Intent(this, VendaClienteActivity.class);
        intentVenda.putExtra("idVenda", tabulacoes.get(position).getVenda().getId());
        startActivity(intentVenda);
    }
}
