package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ETipoAquisicao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.OperadoraTelefonia;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TamanhoChip;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelular;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelularDependente;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaCelularActivity extends ActionBarActivity {

    private EntityManager entityManager;
    private Venda venda;
    private VendaCelular vendaCelular;

    //Extras
    private Integer idVenda;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private HashSet<EVendaFluxo> telasAnteriores;

    //
    private List<Produto> produtosCelular;
    private List<ETipoAquisicao> tiposAquisicao;
    private List<TamanhoChip> tamanhosChip;
    private List<OperadoraTelefonia> operadoras;
    private Produto produtoCelularSelecionado;
    private ETipoAquisicao tipoAquisicaoSelecionado;
    private OperadoraTelefonia operadoraSelecionada;
    private TamanhoChip tamanhoChipSelecionado;

    //Componentes
    private Spinner spinnerProdutoCelular;
    private Spinner spinnerTipoAquisicaoCelular;
    private TextView txtViewOperadoraCelular;
    private Spinner spinnerOperadoraCelular;
    private TextView txtViewNumeroCelular;
    private EditText txtNumeroCelular;
    private Spinner spinnerTamanhoChipCelular;
    private TextView txtViewExemplosAparelhosTitulo;
    private TextView txtViewExemplosAparelhos;
    private CheckBox checkBoxDependentes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_celular);
        criarCamposEntrada();
        entityManager = new EntityManager(this);
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>) getIntent().getExtras().get("produtosTipo");
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);

        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.CELULAR);

        carregarProdutosCelular();
        carregarTiposAquisicao();
        carregarTamanhosChip();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            venda = entityManager.getById(Venda.class, idVenda);
            entityManager.initialize(venda.getCliente());

            if (venda.getVendaCelular() != null
                    && venda.getVendaCelular().getId() != null
                    && !edicao) {
                isVoltar = true;
            }

            if (venda.getVendaCelular() != null
                    && venda.getVendaCelular().getId() != null
                    && (edicao || isVoltar)) {
                vendaCelular = entityManager.getById(VendaCelular.class, venda.getVendaCelular().getId());
                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }
    }

    private void popularValoresCamposEntrada() throws DataBaseException {

        if (vendaCelular.getProduto() != null) {
            for (Produto produto : produtosCelular) {
                if (produto.getId().equals(vendaCelular.getProduto().getId())) {
                    spinnerProdutoCelular
                            .setSelection(produtosCelular.indexOf(produto) + SistemaConstantes.UM);
                    break;
                }
            }
        }

        if (vendaCelular.getOperadoraPortabilidade() != null
                && vendaCelular.getTelefone() != null
                && !vendaCelular.getTelefone().trim().isEmpty()) {
            spinnerTipoAquisicaoCelular.setSelection(tiposAquisicao.indexOf(ETipoAquisicao.PORTABILIDADE) + SistemaConstantes.UM);
        } else {
            spinnerTipoAquisicaoCelular.setSelection(tiposAquisicao.indexOf(ETipoAquisicao.NOVO) + SistemaConstantes.UM);
        }

        if (vendaCelular.getTamanhoChip() != null) {
            for (TamanhoChip tamanhoChip : tamanhosChip) {
                if (tamanhoChip.getId().equals(vendaCelular.getTamanhoChip().getId())) {
                    spinnerTamanhoChipCelular
                            .setSelection(tamanhosChip.indexOf(tamanhoChip) + SistemaConstantes.UM);
                    txtViewExemplosAparelhosTitulo.setVisibility(View.VISIBLE);
                    txtViewExemplosAparelhos.setText(tamanhoChip.getModeloAparelho());
                    txtViewExemplosAparelhos.setVisibility(View.VISIBLE);
                    break;
                }
            }
        } else {
            txtViewExemplosAparelhosTitulo.setVisibility(View.GONE);
            txtViewExemplosAparelhos.setVisibility(View.GONE);
        }

        List<VendaCelularDependente> dependentes = entityManager.select(VendaCelularDependente.class,
                "SELECT vcd.* " +
                "FROM venda_celular_dependente vcd " +
                "WHERE vcd._venda_celular = " + vendaCelular.getId());
        if ((dependentes != null && !dependentes.isEmpty())
                || isMock) {
            checkBoxDependentes.setChecked(true);
        } else {
            checkBoxDependentes.setChecked(false);
        }
    }

    private void popularMock() throws Exception {
        isMock = true;
        vendaCelular = new VendaCelular();
        vendaCelular.setOperadoraPortabilidade(
                entityManager.select(OperadoraTelefonia.class,
                        "SELECT o.* FROM operadora_telefonia o WHERE o.situacao = 'ATIVO' ")
                        .get(SistemaConstantes.ZERO));
        vendaCelular.setProduto(entityManager.select(Produto.class,
                "SELECT p.* " +
                        "FROM produto p " +
                        "INNER JOIN produto_tipo pt " +
                        "ON pt.id = p._produto_tipo " +
                        "WHERE pt.descricao = '" + EProdutoTipo.MULTI.getDescricao() + "' " +
                        "AND p.situacao = 'ATIVO' ")
                .get(SistemaConstantes.ZERO));
        vendaCelular.setTamanhoChip(entityManager.select(
                TamanhoChip.class,
                "SELECT tc.* FROM tamanho_chip tc WHERE tc.situacao = 'ATIVO' ")
                .get(SistemaConstantes.ZERO));
        vendaCelular.setTelefone("4399999999");
    }

    private void criarCamposEntrada() {
        spinnerProdutoCelular          = (Spinner) findViewById(R.id.spinnerProdutoCelular);
        spinnerTipoAquisicaoCelular    = (Spinner) findViewById(R.id.spinnerTipoAquisicaoCelular);
        txtViewOperadoraCelular        = (TextView) findViewById(R.id.txtViewOperadoraCelular);
        spinnerOperadoraCelular        = (Spinner) findViewById(R.id.spinnerOperadoraCelular);
        txtViewNumeroCelular           = (TextView) findViewById(R.id.txtViewNumeroCelular);
        txtNumeroCelular               = (EditText) findViewById(R.id.txtNumeroCelular);
        UtilMask.setMascaraTelefone(txtNumeroCelular);
        spinnerTamanhoChipCelular      = (Spinner) findViewById(R.id.spinnerTamanhoChipCelular);
        txtViewExemplosAparelhosTitulo = (TextView) findViewById(R.id.txtViewExemplosAparelhosTitulo);
        txtViewExemplosAparelhos       = (TextView) findViewById(R.id.txtViewExemplosAparelhos);
        checkBoxDependentes            = (CheckBox) findViewById(R.id.checkBoxDependentes);

        txtViewOperadoraCelular.setVisibility(View.GONE);
        spinnerOperadoraCelular.setVisibility(View.GONE);
        txtViewNumeroCelular.setVisibility(View.GONE);
        txtNumeroCelular.setVisibility(View.GONE);
        txtViewExemplosAparelhosTitulo.setVisibility(View.GONE);
        txtViewExemplosAparelhos.setVisibility(View.GONE);

        txtViewExemplosAparelhosTitulo.setText("Exemplos de aparelhos: ");
    }

    private void carregarProdutosCelular() {
        try {
            String sql =
                    "SELECT p.* " +
                            "FROM produto p " +
                            "INNER JOIN produto_tipo pt " +
                            "ON pt.id = p._produto_tipo " +
                            "AND pt.descricao = " +
                            "'" + EProdutoTipo.MULTI.getDescricao() + "' " +
                            "WHERE p.situacao = 'ATIVO' " +
                            "ORDER BY p.descricao";
            produtosCelular = entityManager.select(Produto.class, sql);

            List<String> descricoesProdutosCelular = new ArrayList<>();
            descricoesProdutosCelular.add(UtilActivity.SELECIONE);
            for(Produto produto : produtosCelular) {
                descricoesProdutosCelular.add(produto.getDescricao());
            }
            UtilActivity.setAdapter(this, spinnerProdutoCelular, descricoesProdutosCelular);
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar Produtos: " + e.getMessage(), this);
        }
    }

    private void carregarTiposAquisicao() {
        tiposAquisicao = Arrays.asList(ETipoAquisicao.values());
        List<String> descricoesTiposAquisicoes = new ArrayList<>();
        descricoesTiposAquisicoes.add(UtilActivity.SELECIONE);

        for (ETipoAquisicao tipoAquisicao : tiposAquisicao) {
            descricoesTiposAquisicoes.add(tipoAquisicao.toString());
        }

        UtilActivity.setAdapter(this, spinnerTipoAquisicaoCelular, descricoesTiposAquisicoes);

        spinnerTipoAquisicaoCelular.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int visibility = View.GONE;
                if (position > SistemaConstantes.ZERO) {

                    if (tiposAquisicao.get(position - SistemaConstantes.UM) == ETipoAquisicao.PORTABILIDADE) {
                        visibility = View.VISIBLE;

                        if (operadoras == null) {
                            try {
                                String sql = "SELECT o.* " +
                                        "FROM operadora_telefonia o " +
                                        "WHERE o.situacao = 'ATIVO' " +
                                        "ORDER BY o.descricao ";
                                operadoras = entityManager.select(OperadoraTelefonia.class, sql);
                                List<String> descricoesOperadoras = new ArrayList<String>();
                                descricoesOperadoras.add(UtilActivity.SELECIONE);

                                for (OperadoraTelefonia operadora : operadoras) {
                                    descricoesOperadoras.add(operadora.getDescricao());
                                }

                                ArrayAdapter adapter = new ArrayAdapter<> (VendaCelularActivity.this, android.R.layout.simple_spinner_item, descricoesOperadoras);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinnerOperadoraCelular.setAdapter(adapter);

                                if (edicao || isMock || isVoltar) {

                                    if (vendaCelular.getOperadoraPortabilidade() != null
                                            && vendaCelular.getTelefone() != null
                                            && !vendaCelular.getTelefone().trim().isEmpty()) {
                                        for (OperadoraTelefonia operadora : operadoras) {
                                            if (operadora.getId().equals(vendaCelular.getOperadoraPortabilidade().getId())) {
                                                spinnerOperadoraCelular.setSelection(operadoras.indexOf(operadora) + SistemaConstantes.UM);
                                                break;
                                            }
                                        }
                                        txtNumeroCelular.setText(vendaCelular.getTelefone());
                                        txtViewOperadoraCelular.setVisibility(View.VISIBLE);
                                        spinnerOperadoraCelular.setVisibility(View.VISIBLE);
                                        txtViewNumeroCelular.setVisibility(View.VISIBLE);
                                        txtNumeroCelular.setVisibility(View.VISIBLE);
                                        spinnerTipoAquisicaoCelular.setSelection(tiposAquisicao.indexOf(ETipoAquisicao.PORTABILIDADE) + SistemaConstantes.UM);
                                    } else {
                                        spinnerTipoAquisicaoCelular.setSelection(tiposAquisicao.indexOf(ETipoAquisicao.NOVO) + SistemaConstantes.UM);
                                        txtViewOperadoraCelular.setVisibility(View.GONE);
                                        spinnerOperadoraCelular.setVisibility(View.GONE);
                                        txtViewNumeroCelular.setVisibility(View.GONE);
                                        txtNumeroCelular.setVisibility(View.GONE);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                UtilActivity.makeShortToast("ERRO ao listar Operadoras: "
                                        + e.getMessage(), getApplicationContext());
                            }
                        }
                    }
                }

                txtViewOperadoraCelular.setVisibility(visibility);
                spinnerOperadoraCelular.setVisibility(visibility);
                txtViewNumeroCelular.setVisibility(visibility);
                txtNumeroCelular.setVisibility(visibility);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void carregarTamanhosChip() {
        try {
            String sql = "SELECT t.* " +
                    "FROM tamanho_chip t " +
                    "WHERE t.situacao = 'ATIVO' " +
                    "ORDER BY t.descricao ";
            tamanhosChip = entityManager.select(TamanhoChip.class, sql);
            List<String> descricoesTamanhosChip = new ArrayList<>();
            descricoesTamanhosChip.add(UtilActivity.SELECIONE);

            for (TamanhoChip tamanhoChip : tamanhosChip) {
                descricoesTamanhosChip.add(tamanhoChip.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerTamanhoChipCelular, descricoesTamanhosChip);

            spinnerTamanhoChipCelular.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int visibility;
                    if (position > SistemaConstantes.ZERO) {
                        txtViewExemplosAparelhos.setText(
                                tamanhosChip.get(
                                        position - SistemaConstantes.UM).getModeloAparelho());
                        visibility = View.VISIBLE;
                    } else {
                        visibility = View.GONE;
                    }

                    txtViewExemplosAparelhosTitulo.setVisibility(visibility);
                    txtViewExemplosAparelhos.setVisibility(visibility);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar tamanhos de chip: " + e.getMessage(), this);
        }
    }

    private void avancar() {
        try {
            if (!validarCamposObrigatorios()) {
                return;
            }

            vendaCelular = entityManager.save(popularDadosVendaCelular());

            if (venda.getVendaCelular() != null && venda.getVendaCelular().getId() != null) {
                deletarVendaCelular();

                if (checkBoxDependentes.isChecked()) {
                    //atualiza os dependentes já existentes para a nova venda celular
                    entityManager.executeNativeQuery(
                            "UPDATE venda_celular_dependente SET _venda_celular = "
                                    + vendaCelular.getId() +
                                    " WHERE _venda_celular = " + venda.getVendaCelular().getId());
                } else {
                    entityManager.executeNativeQuery(
                            "DELETE FROM venda_celular_dependente " +
                                    "WHERE _venda_celular = " + venda.getVendaCelular().getId());
                }
            }

            venda.setVendaCelular(vendaCelular);
            entityManager.atualizar(venda);

            irParaProximaTela();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar dados do celular: " + e.getMessage(), this);
        }
    }

    private void deletarVendaCelular() throws DataBaseException {
        entityManager.executeNativeQuery(
                "DELETE FROM venda_celular WHERE id = " + venda.getVendaCelular().getId());
    }

    private boolean validarCamposObrigatorios() {

        if (UtilActivity.isValidSpinnerValue(spinnerProdutoCelular)) {
            produtoCelularSelecionado = produtosCelular.get(
                    spinnerProdutoCelular.getSelectedItemPosition() - SistemaConstantes.UM);
        } else {
            UtilActivity.makeShortToast("Selecione o Produto.", this);
            return false;
        }

        if (UtilActivity.isValidSpinnerValue(spinnerTipoAquisicaoCelular)) {
            tipoAquisicaoSelecionado = tiposAquisicao.get(
                    spinnerTipoAquisicaoCelular.getSelectedItemPosition() - SistemaConstantes.UM);
        } else {
            UtilActivity.makeShortToast("Selecione o Tipo de Aquisição.", this);
            return false;
        }

        if (UtilActivity.isValidSpinnerValue(spinnerTamanhoChipCelular)) {
            tamanhoChipSelecionado = tamanhosChip.get(
                    spinnerTamanhoChipCelular.getSelectedItemPosition() - SistemaConstantes.UM);
        } else {
            UtilActivity.makeShortToast("Selecione o Tamanho do Chip.", this);
            return false;
        }

        if (txtViewNumeroCelular.getVisibility() == View.VISIBLE) {

            if (UtilActivity.isValidSpinnerValue(spinnerOperadoraCelular)) {
                operadoraSelecionada = operadoras.get(
                        spinnerOperadoraCelular.getSelectedItemPosition() - SistemaConstantes.UM);
            } else {
                UtilActivity.makeShortToast("Selecione a Operadora.", this);
                return false;
            }

            if (txtNumeroCelular.getText() == null
                    || txtNumeroCelular.getText().toString().isEmpty()) {
                UtilActivity.makeShortToast("Informe o número do telefone.", this);
                return false;
            }
        } else {
            operadoraSelecionada = null;
            txtNumeroCelular.setText(null);
        }

        return true;
    }

    private VendaCelular popularDadosVendaCelular() {
        VendaCelular vendaCelular = new VendaCelular();
        vendaCelular.setProduto(produtoCelularSelecionado);
        vendaCelular.setOperadoraPortabilidade(operadoraSelecionada);
        vendaCelular.setTelefone(UtilMask.unmask(txtNumeroCelular.getText().toString()));
        vendaCelular.setTamanhoChip(tamanhoChipSelecionado);
        return vendaCelular;
    }


    private void irParaProximaTela() {
        Intent proximaTela = new Intent();
        proximaTela.putExtra("idVenda", idVenda);
        proximaTela.putExtra("produtosTipo", produtosTiposSelecionados);
        proximaTela.putExtra("edicao", edicao);
        proximaTela.putExtra("isMock", isMock);
        telasAnteriores.add(EVendaFluxo.CELULAR);
        proximaTela.putExtra("telasAnteriores", telasAnteriores);

        Class<?> cls = null;

        if (checkBoxDependentes.isChecked()) {
            cls = VendaCelularDependenteActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.TV)) {
            cls = VendaTvActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.FONE)) {
            cls = VendaFoneActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.INTERNET)) {
            cls = VendaInternetActivity.class;
        } else {
            cls = VendaFormaPagamentoActivity.class;
        }
        proximaTela.setComponent(new ComponentName(this, cls));
        startActivity(proximaTela);
        VendaCelularActivity.this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_celular, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            try {
                Intent activityAnterior = new Intent();
                activityAnterior.putExtra("idVenda", idVenda);
                activityAnterior.putExtra("edicao", edicao);
                activityAnterior.putExtra("isMock", isMock);
                activityAnterior.putExtra("isVoltar", true);
                activityAnterior.putExtra("telasAnteriores", telasAnteriores);

                Class<?> cls = VendaPacoteActivity.class;
                activityAnterior.setComponent(new ComponentName(this, cls));
                startActivity(activityAnterior);
                VendaCelularActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (id == R.id.action_avancar) {
            avancar();
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaCelularActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }
}