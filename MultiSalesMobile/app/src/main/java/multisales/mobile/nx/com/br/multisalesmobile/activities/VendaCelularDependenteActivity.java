package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ETipoAquisicao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.OperadoraTelefonia;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TamanhoChip;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TipoCompartilhamento;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelular;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelularDependente;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaCelularDependenteActivity extends ActionBarActivity {

    private EntityManager entityManager;

    private Venda venda;
    private VendaCelular vendaCelular;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private Integer idVenda;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private HashSet<EVendaFluxo> telasAnteriores;

    private TipoCompartilhamento tipoCompartilhamentoSelecionado;
    private ETipoAquisicao tipoAquisicaoSelecionado;
    private OperadoraTelefonia operadoraSelecionada;
    private TamanhoChip tamanhoChipSelecionado;

    private List<TipoCompartilhamento> tiposCompartilhamento;
    private List<ETipoAquisicao> tiposAquisicao;
    private List<OperadoraTelefonia> operadoras;
    private List<TamanhoChip> tamanhosChip;
    private List<VendaCelularDependente> vendasCelularDependente;

    //Componentes
    private Spinner spinnerTipoCompartilhamento;
    private Spinner spinnerTipoAquisicaoCelular;
    private TextView txtViewOperadoraCelular;
    private Spinner spinnerOperadoraCelular;
    private TextView txtViewNumeroCelular;
    private EditText txtNumeroCelular;
    private Spinner spinnerTamanhoChipCelular;
    private TextView txtViewExemplosAparelhosTitulo;
    private TextView txtViewExemplosAparelhos;
    private ListView listViewVendasCelularDependente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_celular_dependente);
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>) getIntent().getExtras().get("produtosTipo");
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);

        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.CELULAR_DEPENDENTE);

        criarCamposEntrada();
        entityManager = new EntityManager(this);

        carregarTiposCompartilhamento();
        carregarTiposAquisicao();
        carregarTamanhosChip();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        vendasCelularDependente = new ArrayList<>();

        try {
            venda = entityManager.getById(Venda.class, idVenda);
            vendaCelular = entityManager.getById(VendaCelular.class, venda.getVendaCelular().getId());

            vendasCelularDependente = entityManager.select(
                    VendaCelularDependente.class,
                    "SELECT vcd.* " +
                            "FROM venda_celular_dependente vcd " +
                            "WHERE vcd._venda_celular = " + vendaCelular.getId() + " " +
                            "ORDER BY vcd.id");

            if (vendasCelularDependente != null && !vendasCelularDependente.isEmpty() && !edicao) {
                isVoltar = true;
            }

            if (edicao || isVoltar) {
                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }
    }

    private void popularMock() throws Exception {
        VendaCelularDependente vcd = new VendaCelularDependente();
        vcd.setVendaCelular(vendaCelular);
        vcd.setTipoCompartilhamento(
                entityManager.select(TipoCompartilhamento.class,
                    "SELECT * FROM tipo_compartilhamento WHERE situacao = 'ATIVO'")
                    .get(SistemaConstantes.ZERO));
        vcd.setOperadoraPortabilidade(
                entityManager.select(OperadoraTelefonia.class,
                    "SELECT * FROM operadora_telefonia WHERE situacao = 'ATIVO'")
                    .get(SistemaConstantes.ZERO));
        vcd.setTelefone("4312345678");
        vcd.setTamanhoChip(
                entityManager.select(TamanhoChip.class,
                    "SELECT * FROM tamanho_chip WHERE situacao = 'ATIVO'")
                    .get(SistemaConstantes.ZERO));

        entityManager.save(vcd);
    }

    private void popularValoresCamposEntrada() throws Exception {
        if (vendasCelularDependente != null) {
            for (VendaCelularDependente vcd : vendasCelularDependente) {
                entityManager.initialize(vcd.getOperadoraPortabilidade());
                entityManager.initialize(vcd.getTamanhoChip());
                entityManager.initialize(vcd.getTipoCompartilhamento());
            }
        }

        listarVendasCelularDependentes();
    }

    private void criarCamposEntrada() {
        spinnerTipoCompartilhamento    = (Spinner) findViewById(R.id.spinnerTipoCompartilhamento);
        spinnerTipoAquisicaoCelular    = (Spinner) findViewById(R.id.spinnerTipoAquisicaoCelular);
        txtViewOperadoraCelular        = (TextView) findViewById(R.id.txtViewOperadoraCelular);
        spinnerOperadoraCelular        = (Spinner) findViewById(R.id.spinnerOperadoraCelular);
        txtViewNumeroCelular           = (TextView) findViewById(R.id.txtViewNumeroCelular);
        txtNumeroCelular               = (EditText) findViewById(R.id.txtNumeroCelular);
        UtilMask.setMascaraTelefone(txtNumeroCelular);
        spinnerTamanhoChipCelular      = (Spinner) findViewById(R.id.spinnerTamanhoChipCelular);
        txtViewExemplosAparelhosTitulo = (TextView) findViewById(R.id.txtViewExemplosAparelhosTitulo);
        txtViewExemplosAparelhos       = (TextView) findViewById(R.id.txtViewExemplosAparelhos);
        listViewVendasCelularDependente = (ListView) findViewById(R.id.listViewVendasCelularDependente);

        txtViewOperadoraCelular.setVisibility(View.GONE);
        spinnerOperadoraCelular.setVisibility(View.GONE);
        txtViewNumeroCelular.setVisibility(View.GONE);
        txtNumeroCelular.setVisibility(View.GONE);
        txtViewExemplosAparelhosTitulo.setVisibility(View.GONE);
        txtViewExemplosAparelhos.setVisibility(View.GONE);

        txtViewExemplosAparelhosTitulo.setText("Exemplos de aparelhos: ");

        listViewVendasCelularDependente.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                vendasCelularDependente.remove(position);
                listarVendasCelularDependentes();
                return true;
            }
        });
    }

    private void carregarTiposCompartilhamento() {
        try {
            String sql = "SELECT tc.* " +
                    "FROM tipo_compartilhamento tc " +
                    "WHERE tc.situacao = 'ATIVO' " +
                    "ORDER BY tc.descricao";
            tiposCompartilhamento = entityManager.select(TipoCompartilhamento.class, sql);
            List<String> descricoesTiposCompartilhamento = new ArrayList<>();
            descricoesTiposCompartilhamento.add(UtilActivity.SELECIONE);

            for (TipoCompartilhamento tipoCompartilhamento : tiposCompartilhamento) {
                descricoesTiposCompartilhamento.add(tipoCompartilhamento.getDescricao());
            }

            UtilActivity.setAdapter(this,
                    spinnerTipoCompartilhamento,
                    descricoesTiposCompartilhamento);

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar Tipos de Compartilhamento: "
                    + e.getMessage(), getApplicationContext());
        }
    }


    private void carregarTiposAquisicao() {
        tiposAquisicao = Arrays.asList(ETipoAquisicao.values());
        List<String> descricoesTiposAquisicoes = new ArrayList<>();
        descricoesTiposAquisicoes.add(UtilActivity.SELECIONE);

        for (ETipoAquisicao tipoAquisicao : tiposAquisicao) {
            descricoesTiposAquisicoes.add(tipoAquisicao.toString());
        }

        UtilActivity.setAdapter(this, spinnerTipoAquisicaoCelular, descricoesTiposAquisicoes);

        spinnerTipoAquisicaoCelular.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int visibility = View.GONE;
                if (position > SistemaConstantes.ZERO) {

                    if (tiposAquisicao.get(position - SistemaConstantes.UM) == ETipoAquisicao.PORTABILIDADE) {
                        visibility = View.VISIBLE;

                        if (operadoras == null) {
                            try {
                                String sql = "SELECT o.* " +
                                        "FROM operadora_telefonia o " +
                                        "WHERE o.situacao = 'ATIVO' " +
                                        "ORDER BY o.descricao ";
                                operadoras = entityManager.select(OperadoraTelefonia.class, sql);
                                List<String> descricoesOperadoras = new ArrayList<String>();
                                descricoesOperadoras.add(UtilActivity.SELECIONE);

                                for (OperadoraTelefonia operadora : operadoras) {
                                    descricoesOperadoras.add(operadora.getDescricao());
                                }

                                ArrayAdapter adapter = new ArrayAdapter<> (VendaCelularDependenteActivity.this, android.R.layout.simple_spinner_item, descricoesOperadoras);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinnerOperadoraCelular.setAdapter(adapter);

                            } catch (Exception e) {
                                e.printStackTrace();
                                UtilActivity.makeShortToast("ERRO ao listar Operadoras: "
                                        + e.getMessage(), getApplicationContext());
                            }

                        }
                    }

                }

                txtViewOperadoraCelular.setVisibility(visibility);
                spinnerOperadoraCelular.setVisibility(visibility);
                txtViewNumeroCelular.setVisibility(visibility);
                txtNumeroCelular.setVisibility(visibility);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void carregarTamanhosChip() {
        try {
            String sql = "SELECT t.* " +
                    "FROM tamanho_chip t " +
                    "WHERE t.situacao = 'ATIVO' " +
                    "ORDER BY t.descricao ";
            tamanhosChip = entityManager.select(TamanhoChip.class, sql);
            List<String> descricoesTamanhosChip = new ArrayList<>();
            descricoesTamanhosChip.add(UtilActivity.SELECIONE);

            for (TamanhoChip tamanhoChip : tamanhosChip) {
                descricoesTamanhosChip.add(tamanhoChip.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerTamanhoChipCelular, descricoesTamanhosChip);

            spinnerTamanhoChipCelular.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int visibility;
                    if (position > SistemaConstantes.ZERO) {
                        txtViewExemplosAparelhos.setText(
                                tamanhosChip.get(
                                        position - SistemaConstantes.UM).getModeloAparelho());
                        visibility = View.VISIBLE;
                    } else {
                        visibility = View.GONE;
                    }

                    txtViewExemplosAparelhosTitulo.setVisibility(visibility);
                    txtViewExemplosAparelhos.setVisibility(visibility);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar tamanhos de chip: " + e.getMessage(), this);
        }
    }

    private void avancar() {
        try {
            entityManager.executeNativeQuery("DELETE FROM venda_celular_dependente WHERE _venda_celular = " + vendaCelular.getId());
            for (VendaCelularDependente vcd : vendasCelularDependente) {
                vcd.setNullId();
                entityManager.save(vcd);
            }
            irParaProximaTela();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar dependentes: " + e.getMessage(), this);
        }
    }

    private void irParaProximaTela() {
        Intent proximaTela = new Intent();
        proximaTela.putExtra("idVenda", idVenda);
        proximaTela.putExtra("produtosTipo", produtosTiposSelecionados);
        proximaTela.putExtra("edicao", edicao);
        proximaTela.putExtra("isMock", isMock);

        telasAnteriores.add(EVendaFluxo.CELULAR_DEPENDENTE);
        proximaTela.putExtra("telasAnteriores", telasAnteriores);

        Class<?> cls = null;
        if (produtosTiposSelecionados.contains(EProdutoTipo.TV)) {
            cls = VendaTvActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.FONE)) {
            cls = VendaFoneActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.INTERNET)) {
            cls = VendaInternetActivity.class;
        } else {
            cls = VendaFormaPagamentoActivity.class;
        }
        proximaTela.setComponent(new ComponentName(this, cls));
        startActivity(proximaTela);
    }

    public void adicionarCelularDependente(View view) {
        if (validarCamposObrigatorios()) {
            VendaCelularDependente vcd = new VendaCelularDependente();
            vcd.setVendaCelular(vendaCelular);
            vcd.setTipoCompartilhamento(tipoCompartilhamentoSelecionado);
            vcd.setOperadoraPortabilidade(operadoraSelecionada);
            vcd.setTelefone(UtilMask.unmask(txtNumeroCelular.getText().toString()));
            vcd.setTamanhoChip(tamanhoChipSelecionado);
            vendasCelularDependente.add(vcd);

            listarVendasCelularDependentes();

            spinnerTipoCompartilhamento.setSelection(SistemaConstantes.ZERO);
            spinnerTipoAquisicaoCelular.setSelection(SistemaConstantes.ZERO);
            spinnerOperadoraCelular.setSelection(SistemaConstantes.ZERO);
            txtNumeroCelular.setText(null);
            spinnerTamanhoChipCelular.setSelection(SistemaConstantes.ZERO);
        }
    }

    private boolean validarCamposObrigatorios() {

        if (UtilActivity.isValidSpinnerValue(spinnerTipoCompartilhamento)) {
            tipoCompartilhamentoSelecionado = tiposCompartilhamento.get(
                    spinnerTipoCompartilhamento.getSelectedItemPosition() - SistemaConstantes.UM);
        } else {
            UtilActivity.makeShortToast("Selecione o tipo de Compartilhamento.", this);
            return false;
        }

        if (UtilActivity.isValidSpinnerValue(spinnerTipoAquisicaoCelular)) {
            tipoAquisicaoSelecionado = tiposAquisicao.get(
                    spinnerTipoAquisicaoCelular.getSelectedItemPosition() - SistemaConstantes.UM);
        } else {
            UtilActivity.makeShortToast("Selecione o Tipo de Aquisição.", this);
            return false;
        }

        if (UtilActivity.isValidSpinnerValue(spinnerTamanhoChipCelular)) {
            tamanhoChipSelecionado = tamanhosChip.get(
                    spinnerTamanhoChipCelular.getSelectedItemPosition() - SistemaConstantes.UM);
        } else {
            UtilActivity.makeShortToast("Selecione o Tamanho do Chip.", this);
            return false;
        }

        if (txtViewNumeroCelular.getVisibility() == View.VISIBLE) {

            if (UtilActivity.isValidSpinnerValue(spinnerOperadoraCelular)) {
                operadoraSelecionada = operadoras.get(
                        spinnerOperadoraCelular.getSelectedItemPosition() - SistemaConstantes.UM);
            } else {
                UtilActivity.makeShortToast("Selecione a Operadora.", this);
                return false;
            }

            if (txtNumeroCelular.getText() == null
                    || txtNumeroCelular.getText().toString().isEmpty()) {
                UtilActivity.makeShortToast("Informe o número do telefone.", this);
                return false;
            }
        } else {
            operadoraSelecionada = null;
            txtNumeroCelular.setText(null);
        }

        return true;
    }

    private void listarVendasCelularDependentes() {
        List<Map<String, Object>> itens = new ArrayList<Map<String, Object>>();

        if (vendasCelularDependente != null) {
            for (VendaCelularDependente vpa : vendasCelularDependente) {
                Map<String, Object> item = new HashMap<>();
                item.put("compartilhamento", vpa.getTipoCompartilhamento().getDescricao());
                item.put("aquisicao", ((vpa.getTelefone() != null && !vpa.getTelefone().isEmpty()) ? ETipoAquisicao.PORTABILIDADE : ETipoAquisicao.NOVO));
                item.put("operadora", (vpa.getOperadoraPortabilidade() != null ? vpa.getOperadoraPortabilidade().getDescricao() : "N/A"));
                item.put("telefone", ((vpa.getTelefone() != null && !vpa.getTelefone().isEmpty()) ? UtilActivity.formatarTelefone(vpa.getTelefone()) : "N/A"));
                item.put("tamanhoChip", vpa.getTamanhoChip().getDescricao());
                itens.add(item);
            }
        }

        String[] de = {"compartilhamento", "aquisicao", "operadora", "telefone", "tamanhoChip"};
        int[] para = {R.id.txtTipoCompartilhamento, R.id.txtTipoAquisicao, R.id.txtOperadora, R.id.txtTelefone, R.id.txtTamanhoChip};
        SimpleAdapter adapter = new SimpleAdapter(this, itens, R.layout.item_venda_celular_dependente, de, para);
        listViewVendasCelularDependente.setAdapter(adapter);

        UtilActivity.setListViewHeightBasedOnItems(listViewVendasCelularDependente);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_celular_dependente, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            try {
                Intent activityAnterior = new Intent();
                activityAnterior.putExtra("idVenda", idVenda);
                activityAnterior.putExtra("edicao", edicao);
                activityAnterior.putExtra("isMock", isMock);
                activityAnterior.putExtra("isVoltar", true);
                activityAnterior.putExtra("telasAnteriores", telasAnteriores);
                activityAnterior.putExtra("produtosTipo", produtosTiposSelecionados);
                Class<?> cls = VendaCelularActivity.class;
                activityAnterior.setComponent(new ComponentName(this, cls));
                startActivity(activityAnterior);
                VendaCelularDependenteActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (id == R.id.action_avancar) {
            avancar();
            return true;
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaCelularDependenteActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }
}