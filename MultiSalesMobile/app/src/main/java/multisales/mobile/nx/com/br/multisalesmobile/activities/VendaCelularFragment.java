package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.BreakIterator;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TamanhoChip;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelular;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelularDependente;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

/**
 * Created by eric on 16-03-2015.
 */
public class VendaCelularFragment extends Fragment {

    private VendaCelularDependente vendaCelularDependente;
    private VendaCelular vendaCelular;
    private RelativeLayout relativeLayout;
    private LinearLayout dependentesLayout;
    private TextView txtplanoProdutoTV;
    private TextView txtTipoAquisicaoTV;
    private TextView txtOperadoraTV;
    private TextView txtTelefoneTV;
    private TextView txtTamanhoChipTV;
    private TextView txtDependentesTV;
    private RelativeLayout txtDependenteRL;
    private RelativeLayout txtOperadoraRL;
    private RelativeLayout txtTelefoneRL;
    private EntityManager entityManager;
    private RelativeLayout relOperadoraRL;
    private RelativeLayout relTelefoneRL;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        entityManager = new EntityManager(getActivity());
        this.vendaCelular = ((VendaDetalhesActivity) getActivity()).getVenda().getVendaCelular();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_venda_celular, container, false);
        dependentesLayout = (LinearLayout) relativeLayout.findViewById(R.id.dependentesLL);
        criarCamposEntrada(inflater, container,savedInstanceState);
        return relativeLayout;
    }

    public void criarCamposEntrada(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState){
        txtplanoProdutoTV      = (TextView) relativeLayout.findViewById(R.id.planoProdutoTV);
        if(vendaCelular.getProduto() != null) {
            txtplanoProdutoTV.setText(vendaCelular.getProduto().getDescricao());
        } else {
            txtplanoProdutoTV.setText("N/D");
        }

        txtTipoAquisicaoTV     = (TextView)  relativeLayout.findViewById(R.id.tipoAquisicaoTV);
        if(vendaCelular.getOperadoraPortabilidade() != null ){
            txtTipoAquisicaoTV.setText("PORTABILIDADE");
        } else {
            txtTipoAquisicaoTV.setText("NOVO");
            txtOperadoraRL = (RelativeLayout) relativeLayout.findViewById(R.id.operadoraRL);
            txtTelefoneRL = (RelativeLayout) relativeLayout.findViewById(R.id.telefoneRL);
            txtOperadoraRL.setVisibility(View.GONE);
            txtTelefoneRL.setVisibility(View.GONE);
        }

        txtOperadoraTV         = (TextView) relativeLayout.findViewById(R.id.operadoraTV);
        if(vendaCelular.getOperadoraPortabilidade() != null){
            txtOperadoraTV.setText(vendaCelular.getOperadoraPortabilidade().getDescricao());
        }

        txtTelefoneTV          = (TextView) relativeLayout.findViewById(R.id.telefoneTV);
        if(vendaCelular.getTelefone() != null
                && !vendaCelular.getTelefone().isEmpty()) {
            txtTelefoneTV.setText(UtilActivity.formatarTelefone(vendaCelular.getTelefone()));
        }

        txtTamanhoChipTV      = (TextView) relativeLayout.findViewById(R.id.tamanhoChipTV);
        if(vendaCelular.getTamanhoChip() != null) {
            txtTamanhoChipTV.setText(vendaCelular.getTamanhoChip().getDescricao());
        } else {
            txtTamanhoChipTV.setText("N/D");
        }

        txtDependentesTV       = (TextView) relativeLayout.findViewById(R.id.dependentesTV);
        if(vendaCelular.getVendaCelularDependentes().isEmpty()){
            txtDependentesTV.setText("NÃO");
            txtDependenteRL = (RelativeLayout) relativeLayout.findViewById(R.id.dependenteRL);
            txtDependenteRL.setVisibility(View.GONE);
        } else {
            txtDependentesTV.setText("SIM");
        }

        for(int i = 0; i< vendaCelular.getVendaCelularDependentes().size();i++){
            vendaCelularDependente = vendaCelular.getVendaCelularDependentes().get(i);

            try {
                entityManager.initialize(vendaCelularDependente.getTipoCompartilhamento());
                entityManager.initialize(vendaCelularDependente.getOperadoraPortabilidade());
                entityManager.initialize(vendaCelularDependente.getTamanhoChip());
                entityManager.initialize(vendaCelularDependente.getVendaCelular());
            } catch (Exception e ) {
                e.printStackTrace();
            }

            RelativeLayout dependenteLayout = (RelativeLayout) inflater.inflate(R.layout.item_venda_celular_dependente, container, false);

            TextView txtTipoCompartilhamento = (TextView) dependenteLayout.findViewById(R.id.txtTipoCompartilhamento);
            if (vendaCelularDependente.getTipoCompartilhamento() != null ) {
               txtTipoCompartilhamento.setText(vendaCelularDependente.getTipoCompartilhamento().getDescricao());
            } else {
                txtTipoCompartilhamento.setText("N/D");
            }

            txtTipoCompartilhamento.setText(vendaCelularDependente.getTipoCompartilhamento().getDescricao());
            TextView txtTipoAquisicao = (TextView) dependenteLayout.findViewById(R.id.txtTipoAquisicao);
            if (vendaCelularDependente.getOperadoraPortabilidade() != null ) {
                txtTipoAquisicao.setText("PORTABILIDADE");
            } else {
                txtTipoAquisicao.setText("NOVO");
                relOperadoraRL = (RelativeLayout) dependenteLayout.findViewById(R.id.OperadoraRL);
                relOperadoraRL.setVisibility(View.GONE);
                relTelefoneRL = (RelativeLayout) dependenteLayout.findViewById(R.id.numeroTelefoneRL);
                relTelefoneRL.setVisibility(View.GONE);
            }


            TextView txtOperadora = (TextView) dependenteLayout.findViewById(R.id.txtOperadora);
            if(vendaCelularDependente.getOperadoraPortabilidade() != null){
                txtOperadora.setText(vendaCelularDependente.getOperadoraPortabilidade().getDescricao());
            }

            TextView txtTelefone = (TextView) dependenteLayout.findViewById(R.id.txtTelefone);
            if (vendaCelularDependente.getTelefone() != null
                    && !vendaCelularDependente.getTelefone().isEmpty()) {
               txtTelefone.setText(UtilActivity.formatarTelefone(vendaCelularDependente.getTelefone()));
            } else {
               txtTelefone.setText("N/D");
            }

            TextView txtTamanhoChip = (TextView) dependenteLayout.findViewById(R.id.txtTamanhoChip);
            if(vendaCelularDependente.getTamanhoChip() != null ){
                txtTamanhoChip.setText(vendaCelularDependente.getTamanhoChip().getDescricao());
            } else {
                txtTamanhoChip.setText("N/D");
            }

            dependentesLayout.addView(dependenteLayout);

            if (i != vendaCelular.getVendaCelularDependentes().size() - 1) {
                LinearLayout separador = (LinearLayout) inflater.inflate(R.layout.separador, container, false);
                    dependentesLayout.addView(separador);
                }
            }
    }

}
