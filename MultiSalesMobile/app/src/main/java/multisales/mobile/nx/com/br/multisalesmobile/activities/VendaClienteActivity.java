package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cliente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ESexo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Escolaridade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EstadoCivil;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaClienteActivity extends ActionBarActivity implements View.OnFocusChangeListener, View.OnClickListener {

    private List<Escolaridade> escolaridades;
    private List<EstadoCivil> estadosCivis;
    private ESexo[] sexos;

    private Cliente cliente;
    private Integer idVenda;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private HashSet<EVendaFluxo> telasAnteriores;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;

    private EntityManager entityManager;

    public EditText txtNomeCliente;
    public EditText txtDataNascimentoCliente;
    public Spinner spinnerSexo;
    public Spinner spinnerEstadoCivil;
    public EditText txtCpfCnpj;
    public EditText txtNomeMae;
    public EditText txtNomePai;
    public EditText txtRgInscEstadual;
    public EditText txtEmissorRG;
    public EditText txtDataEmissaoRG;
    public Spinner spinnerEscolaridade;
    public EditText txtNacionalidade;
    public EditText txtTelResidencial;
    public EditText txtTelComercial;
    public EditText txtTelCelular;
    public EditText txtEmail;
    public EditText txtProfissao;
    public EditText txtFaixaSalarial;
    private DatePickerDialog dataNascDPD;
    private DatePickerDialog dataEmissaoDPD;
    private SimpleDateFormat formatoData;
    public EditText txtRespLegal;
    public EditText txtProcurador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_cliente);
        cliente = new Cliente();
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);
        entityManager = new EntityManager(this);
        formatoData = new SimpleDateFormat("dd/MM/yyyy");

        try {
            telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        } catch (Exception e) {
            telasAnteriores = null;
        }


        if (telasAnteriores != null) {
            telasAnteriores.remove(EVendaFluxo.CLIENTE);
        } else {
            telasAnteriores = new HashSet<>();
        }
        criarCamposEntrada();
        carregarSexos();
        carregarEstadosCivis();
        carregarEscolaridades();
        setDateTimeField();

        txtNomeCliente.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equalsIgnoreCase("nx")) {
                    isMock = true;
                    popularMock();
                    popularValoresCamposEntrada();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (idVenda > SistemaConstantes.ZERO && !isVoltar) {
            edicao = true;
        }

        try {
            if (idVenda > SistemaConstantes.ZERO) {
                Venda venda = entityManager.getById(Venda.class, idVenda);
                entityManager.initialize(venda.getCliente());
                this.cliente = venda.getCliente();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao recuperar venda.", this);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void popularValoresCamposEntrada() {
        txtNomeCliente.setText(cliente.getNome());
        if (cliente.getDataNascimento() != null) {
            txtDataNascimentoCliente.setText(formatoData.format(cliente.getDataNascimento().getTime()));
        }
        int i;
        if (cliente.getSexo() != null) {
            for (i = 0; i < sexos.length; i++) {
                if (sexos[i].equals(cliente.getSexo())) {
                    spinnerSexo.setSelection(i + 1);
                    break;
                }
            }
        }
        if (cliente.getEstadoCivil() != null) {
            for (i = 0; i < estadosCivis.size(); i++) {
                if (estadosCivis.get(i).getId().equals(cliente.getEstadoCivil().getId())) {
                    spinnerEstadoCivil.setSelection(i + 1);
                    break;
                }
            }
        }
        if (cliente.getCpfCnpj() != null) {
            txtCpfCnpj.setText(cliente.getCpfCnpj());
        }
        if (cliente.getNomeMae() != null) {
            txtNomeMae.setText(cliente.getNomeMae());
        }
        if (cliente.getNomePai() != null) {
            txtNomePai.setText(cliente.getNomePai());
        }
        if (cliente.getRgInscricaoEstadual() != null) {
            txtRgInscEstadual.setText(cliente.getRgInscricaoEstadual());
        }
        if (cliente.getEmissor() != null) {
            txtEmissorRG.setText(cliente.getEmissor());
        }
        if (cliente.getDataEmissao() != null) {
            txtDataEmissaoRG.setText(formatoData.format(cliente.getDataEmissao().getTime()));
        }
        if (cliente.getEscolaridade() != null) {
            for (i = 0; i < escolaridades.size(); i++) {
                if (escolaridades.get(i).getId().equals(cliente.getEscolaridade().getId())) {
                    spinnerEscolaridade.setSelection(i + 1);
                    break;
                }
            }
        }

        if (cliente.getResponsavelLegal() != null) {
            txtRespLegal.setText(cliente.getResponsavelLegal());
        }
        if (cliente.getProcurador() != null) {
            txtProcurador.setText(cliente.getProcurador());
        }
        if (cliente.getNacionalidade() != null) {
            txtNacionalidade.setText(cliente.getNacionalidade());
        }
        if (cliente.getTelefoneResidencial() != null) {
            txtTelResidencial.setText(cliente.getTelefoneResidencial());
        }
        if (cliente.getTelefoneComercial() != null) {
            txtTelComercial.setText(cliente.getTelefoneComercial());
        }
        if (cliente.getTelefoneCelular() != null) {
            txtTelCelular.setText(cliente.getTelefoneCelular());
        }
        if (cliente.getEmail() != null) {
            txtEmail.setText(cliente.getEmail());
        }
        if (cliente.getProfissao() != null) {
            txtProfissao.setText(cliente.getProfissao());
        }
        if (cliente.getFaixaSalarial() != null) {
            txtFaixaSalarial.setText(cliente.getFaixaSalarial().toString());
        }
    }

    private void popularMock() {
        cliente.setNome("NOME CLIENTE TESTE");
        cliente.setDataNascimento(Calendar.getInstance());
        cliente.getDataNascimento().set(1987, 3, 14);
        cliente.setSexo(ESexo.F);
        cliente.setEstadoCivil(estadosCivis.get(0));
        cliente.setCpfCnpj("12312312312");
        cliente.setNomeMae("NOME MAE CLIENTE");
        cliente.setNomePai("NOME PAI CLIENTE");
        cliente.setRgInscricaoEstadual("12345678");
        cliente.setEmissor("SSP/PR");
        cliente.setDataEmissao(Calendar.getInstance());
        cliente.getDataEmissao().set(2015, 3, 7);
        cliente.setEscolaridade(escolaridades.get(0));
        cliente.setProcurador("PROCURADOR");
        cliente.setResponsavelLegal("RESPONSÁVEL LEGAL");
        cliente.setNacionalidade("BRASILEIRO");
        cliente.setTelefoneResidencial("1111111111");
        cliente.setTelefoneComercial("2222222222");
        cliente.setTelefoneCelular("3333333333");
        cliente.setEmail("ANDROID@NXMULTISERVICOS.COM.BR");
        cliente.setProfissao("VENDEDOR PAP");
        cliente.setFaixaSalarial(new BigDecimal(1500.00));
    }

    private void setDateTimeField() {
        final Calendar dataHoraAtual = Calendar.getInstance();

        txtDataNascimentoCliente.setOnFocusChangeListener(this);
        txtDataNascimentoCliente.setOnClickListener(this);

        dataNascDPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                cliente.setDataNascimento(Calendar.getInstance());
                cliente.getDataNascimento().set(year, monthOfYear, dayOfMonth);
                txtDataNascimentoCliente.setText(formatoData.format(cliente.getDataNascimento().getTime()));
            }

        },dataHoraAtual.get(Calendar.YEAR), dataHoraAtual.get(Calendar.MONTH), dataHoraAtual.get(Calendar.DAY_OF_MONTH));

        txtDataEmissaoRG.setOnFocusChangeListener(this);
        txtDataEmissaoRG.setOnClickListener(this);

        dataEmissaoDPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                cliente.setDataEmissao(Calendar.getInstance());
                cliente.getDataEmissao().set(year, monthOfYear, dayOfMonth);
                txtDataEmissaoRG.setText(formatoData.format(cliente.getDataEmissao().getTime()));
            }

        },dataHoraAtual.get(Calendar.YEAR), dataHoraAtual.get(Calendar.MONTH), dataHoraAtual.get(Calendar.DAY_OF_MONTH));
    }

    private void criarCamposEntrada() {
        txtNomeCliente           = (EditText) findViewById(R.id.txtNomeCliente);
        txtDataNascimentoCliente = (EditText) findViewById(R.id.txtDataNascimentoCliente);
        spinnerSexo              = (Spinner) findViewById(R.id.spinnerSexo);
        spinnerEstadoCivil       = (Spinner) findViewById(R.id.spinnerEstadoCivil);
        txtCpfCnpj               = (EditText) findViewById(R.id.txtCpfCnpj);
        UtilMask.setMascaraCpfCnpj(txtCpfCnpj);
        txtNomeMae               = (EditText) findViewById(R.id.txtNomeMae);
        txtNomePai               = (EditText) findViewById(R.id.txtNomePai);
        txtRgInscEstadual        = (EditText) findViewById(R.id.txtRgInscEstadual);
        txtEmissorRG             = (EditText) findViewById(R.id.txtEmissorRG);
        txtDataEmissaoRG         = (EditText) findViewById(R.id.txtDataEmissaoRG);
        spinnerEscolaridade      = (Spinner) findViewById(R.id.spinnerEscolaridade);
        txtNacionalidade         = (EditText) findViewById(R.id.txtNacionalidade);
        txtTelResidencial        = (EditText) findViewById(R.id.txtTelResidencial);
        UtilMask.setMascaraTelefone(txtTelResidencial);
        txtTelComercial          = (EditText) findViewById(R.id.txtTelComercial);
        UtilMask.setMascaraTelefone(txtTelComercial);
        txtTelCelular            = (EditText) findViewById(R.id.txtTelCelular);
        UtilMask.setMascaraTelefone(txtTelCelular);
        txtEmail                 = (EditText) findViewById(R.id.txtEmail);
        txtProfissao             = (EditText) findViewById(R.id.txtProfissao);
        txtFaixaSalarial         = (EditText) findViewById(R.id.txtFaixaSalarial);
        UtilMask.setMascaraMoeda(txtFaixaSalarial, 10);
        txtRespLegal             = (EditText) findViewById(R.id.txtRespLegal);
        txtProcurador            = (EditText) findViewById(R.id.txtProcurador);
    }

    private void carregarSexos() {
        try {
            sexos = ESexo.values();
            List<String> descricoesSexos = new ArrayList<>();
            descricoesSexos.add(UtilActivity.SELECIONE);

            for(ESexo sexo : sexos) {
                descricoesSexos.add(sexo.getDescricao());
            }
            UtilActivity.setAdapter(this, spinnerSexo, descricoesSexos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void carregarEstadosCivis() {
        try {
            estadosCivis = entityManager.getAll(EstadoCivil.class);
            List<String> descricoesEstadosCivis = new ArrayList<>();
            descricoesEstadosCivis.add(UtilActivity.SELECIONE);

            for(EstadoCivil estadoCivil : estadosCivis) {
                descricoesEstadosCivis.add(estadoCivil.getDescricao());
            }
            UtilActivity.setAdapter(this, spinnerEstadoCivil, descricoesEstadosCivis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void carregarEscolaridades() {
        try {
            escolaridades = entityManager.getAll(Escolaridade.class);
            List<String> descricoesEscolaridades = new ArrayList<>();
            descricoesEscolaridades.add(UtilActivity.SELECIONE);

            for(Escolaridade escolaridade : escolaridades){
                descricoesEscolaridades.add(escolaridade.getDescricao());
            }
            UtilActivity.setAdapter(this, spinnerEscolaridade, descricoesEscolaridades);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void cancelar(View view) {
        //Caso não seja edição de venda
        // e o registro temporário tenha sido persistido
        if (!edicao
                && idVenda > SistemaConstantes.ZERO) {
            try {
                UtilActivity.deletarVenda(entityManager, idVenda);
            } catch (Exception e) {
                UtilActivity.makeShortToast("ERRO: " + e.getMessage(), this);
                return;
            }
        }

        Intent mainIntent = new Intent(this, MainActivity.class);
        startActivity(mainIntent);
    }*/

    public void avancar(View view) {
        try {

            if (!validarCamposObrigatorios()) {
                return;
            }

            popularDadosCliente();
            cliente = entityManager.save(cliente);

            Venda venda;
            if (idVenda == SistemaConstantes.ZERO) {
                Tabulacao tabulacao = new Tabulacao();
                tabulacao.setDataCadastro(Calendar.getInstance());
                List<MotivoTabulacao> motivosVenda = entityManager.select(
                        MotivoTabulacao.class,
                        "SELECT m.* FROM motivo_tabulacao m WHERE m.descricao = 'VENDA' ");
                if (motivosVenda != null && !motivosVenda.isEmpty()) {
                    tabulacao.setMotivoTabulacao(motivosVenda.get(SistemaConstantes.ZERO));
                    tabulacao = entityManager.save(tabulacao);
                }
                venda = new Venda();
                venda.setId(tabulacao.getIdLocal());
                venda = entityManager.save(venda);
                idVenda = venda.getId();
                tabulacao.setVenda(venda);
                entityManager.atualizar(tabulacao);
            } else {
                venda = entityManager.getById(Venda.class, idVenda);
                entityManager.initialize(venda.getCliente());
                cliente.setEndereco(venda.getCliente().getEndereco());
                cliente.setEnderecoCobranca(venda.getCliente().getEnderecoCobranca());
                entityManager.atualizar(cliente);
                deletarCliente(venda.getCliente().getId());
            }

            venda.setCliente(cliente);
            entityManager.atualizar(venda);

            Intent intentEndereco = new Intent(this, VendaEnderecoActivity.class);
            intentEndereco.putExtra("idVenda", idVenda);
            intentEndereco.putExtra("isMock", isMock);
            if (telasAnteriores == null) {
                telasAnteriores = new HashSet<>();
            }
            telasAnteriores.add(EVendaFluxo.CLIENTE);
            intentEndereco.putExtra("telasAnteriores", telasAnteriores);

            if (edicao) {
                intentEndereco.putExtra("edicao", true);
            }
            startActivity(intentEndereco);
            VendaClienteActivity.this.finish();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Ocorreu um ERRO ao salvar dados do cliente!",
                    this.getApplicationContext());
        }
    }

    private boolean validarCamposObrigatorios() {
        if (txtNomeCliente.getText().toString().isEmpty()) {
            UtilActivity.makeShortToast("Informe o nome do cliente.", this);
            return false;
        }
        return true;
    }

    private void deletarCliente(Integer id) throws DataBaseException {
        entityManager.executeNativeQuery("DELETE FROM cliente WHERE id = " + id);
    }

    private Cliente popularDadosCliente() {
        cliente.setNullId();
        cliente.setNome(txtNomeCliente.getText().toString());
        cliente.setDataNascimento(UtilActivity.getCalendarValue(txtDataNascimentoCliente));

        if (spinnerSexo.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            cliente.setSexo(Arrays.asList(sexos)
                    .get(spinnerSexo.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        if (spinnerEstadoCivil.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            cliente.setEstadoCivil(estadosCivis
                    .get(spinnerEstadoCivil.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        cliente.setCpfCnpj(UtilMask.unmask(txtCpfCnpj.getText().toString()));
        cliente.setNomeMae(txtNomeMae.getText().toString());
        cliente.setNomePai(txtNomePai.getText().toString());
        cliente.setRgInscricaoEstadual(txtRgInscEstadual.getText().toString());
        cliente.setEmissor(txtEmissorRG.getText().toString());
        cliente.setDataEmissao(UtilActivity.getCalendarValue(txtDataEmissaoRG));

        if (spinnerEscolaridade.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            cliente.setEscolaridade(escolaridades
                    .get(spinnerEscolaridade.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        cliente.setResponsavelLegal(txtRespLegal.getText().toString());
        cliente.setProcurador(txtProcurador.getText().toString());
        cliente.setNacionalidade(txtNacionalidade.getText().toString());
        cliente.setTelefoneResidencial(UtilMask.unmask(txtTelResidencial.getText().toString()));
        cliente.setTelefoneComercial(UtilMask.unmask(txtTelComercial.getText().toString()));
        cliente.setTelefoneCelular(UtilMask.unmask(txtTelCelular.getText().toString()));
        cliente.setEmail(txtEmail.getText().toString());
        cliente.setProfissao(txtProfissao.getText().toString());
        cliente.setFaixaSalarial(new BigDecimal(UtilMask.unmaskMoeda(txtFaixaSalarial.getText().toString())));
        return cliente;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_cliente, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            UtilActivity.irParaHome(this);
            VendaClienteActivity.this.finish();
        }

        if (id == R.id.action_avancar) {
            avancar(null);
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaClienteActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(hasFocus) {
            showPicker(view);
        }
    }

    @Override
    public void onClick(View view) {
        showPicker(view);
    }

    private void showPicker(View view) {
        if(view == txtDataNascimentoCliente) {
            dataNascDPD.show();
        } else if(view == txtDataEmissaoRG) {
            dataEmissaoDPD.show();
        }

    }
}
