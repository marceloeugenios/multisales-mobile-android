package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cliente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;
import multisales.mobile.nx.com.br.multisalesmobile.utils.VendaActivityClienteUtil;

/**
 * Created by eric on 27-02-2015.
 */
public class VendaClienteFragment extends Fragment{

    private Cliente cliente;

    private TextView txtNomeCliente;
    private TextView txtDataNascimentoCliente;
    private TextView txtSexo;
    private TextView txtEstadoCivil;
    private TextView txtCpfCnpj;
    private TextView txtNomeMae;
    private TextView txtNomePai;
    private TextView txtRgInscEstadual;
    private TextView txtEmissorRG;
    private TextView txtDataEmissaoRG;
    private TextView txtEscolaridade;
    private TextView txtNacionalidade;
    private TextView txtTelResidencial;
    private TextView txtTelComercial;
    private TextView txtTelCelular;
    private TextView txtEmail;
    private TextView txtProfissao;
    private TextView txtFaixaSalarial;
    private RelativeLayout rel;
    private VendaActivityClienteUtil vendaActivityClienteUtil;
    private TextView txtResponsavelLegal;
    private TextView txtProcurador;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.cliente = ((VendaDetalhesActivity) getActivity()).getVenda().getCliente();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }
        rel = (RelativeLayout) inflater.inflate(R.layout.fragment_venda_cliente, container, false);
        criarCamposEntrada();
        return rel;
    }

    private void criarCamposEntrada() {

        txtNomeCliente           = (TextView) rel.findViewById(R.id.txtDetalheNomeCliente);
        if(cliente.getNome() != null
                && !cliente.getNome().isEmpty()) {
            txtNomeCliente.setText(cliente.getNome());
        } else {
            txtNomeCliente.setText("N/D");
        }

        txtDataNascimentoCliente = (TextView) rel.findViewById(R.id.txtDetalheDataNascimentoCliente);
        if (cliente.getDataNascimento() != null
                && !cliente.getDataNascimento().toString().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String dataNascimento = sdf.format(cliente.getDataNascimento().getTime());
            txtDataNascimentoCliente.setText(dataNascimento);
        } else {
            txtDataNascimentoCliente.setText("N/D");
        }


        txtSexo                  = (TextView) rel.findViewById(R.id.txtDetalheSexoCliente);
        if(cliente.getSexo() != null) {
            txtSexo.setText(cliente.getSexo().getDescricao());
        } else {
            txtSexo.setText("N/D");
        }


        txtEstadoCivil           = (TextView) rel.findViewById(R.id.txtDetalheEstadoCivilCliente);
        if(cliente.getEstadoCivil() != null ){
            txtEstadoCivil.setText(cliente.getEstadoCivil().getDescricao());
        } else {
            txtEstadoCivil.setText("N/D");
        }

        txtCpfCnpj               = (TextView) rel.findViewById(R.id.txtDetalheCpfCnpjCliente);
        if(cliente.getCpfCnpj() !=  null
                && !cliente.getCpfCnpj().isEmpty()){
            txtCpfCnpj.setText(UtilActivity.formatarCPF(cliente.getCpfCnpj()));
        } else {
            txtCpfCnpj.setText("N/D");
        }

        txtNomeMae               = (TextView) rel.findViewById(R.id.txtDetalheNomeMaeCliente);
        if(cliente.getNomeMae() != null
                && !cliente.getNomeMae().isEmpty()){
            txtNomeMae.setText(cliente.getNomeMae());
        } else {
            txtNomeMae.setText("N/D");
        }

        txtNomePai               = (TextView) rel.findViewById(R.id.txtDetalheNomePaiCliente);
        if(cliente.getNomePai() != null
                && !cliente.getNomePai().isEmpty()) {
            txtNomePai.setText(cliente.getNomePai());
        } else {
            txtNomePai.setText("N/D");
        }

        txtRgInscEstadual        = (TextView) rel.findViewById(R.id.txtDetalheRGInscricaoEstadualCliente);
        if (cliente.getRgInscricaoEstadual() != null
                && !cliente.getRgInscricaoEstadual().isEmpty()) {
            txtRgInscEstadual.setText(cliente.getRgInscricaoEstadual());
        } else {
            txtRgInscEstadual.setText("N/D");
        }

        txtEmissorRG             = (TextView) rel.findViewById(R.id.txtDetalheEmissorRgCliente);
        if(cliente.getEmissor() != null
                && !cliente.getEmissor().isEmpty()) {
            txtEmissorRG.setText(cliente.getEmissor());
        } else {
            txtEmissorRG.setText("N/D");
        }

        txtDataEmissaoRG         = (TextView) rel.findViewById(R.id.txtDetalheDataEmissaoRgCliente);
        if(cliente.getDataEmissao() != null
                && !cliente.getDataEmissao().toString().isEmpty()){
            SimpleDateFormat sdfDataEmissao = new SimpleDateFormat("dd/MM/yyyy");
            String dataEmissao = sdfDataEmissao.format(cliente.getDataEmissao().getTime());
            txtDataEmissaoRG.setText(dataEmissao);
        } else {
            txtDataEmissaoRG.setText("N/D");
        }

        txtEscolaridade          = (TextView) rel.findViewById(R.id.txtDetalheEscolaridadeCliente);
        if (cliente.getEscolaridade() != null) {
            txtEscolaridade.setText(cliente.getEscolaridade().getDescricao());
        } else {
            txtEscolaridade.setText("N/D");
        }

        txtResponsavelLegal = (TextView) rel.findViewById(R.id.responsavelLegalTV);
        if(cliente.getResponsavelLegal() != null
            && !cliente.getResponsavelLegal().isEmpty()){
            txtResponsavelLegal.setText(cliente.getResponsavelLegal());
        } else {
            txtResponsavelLegal.setText("N/D");
        }

        txtProcurador = (TextView) rel.findViewById(R.id.procuradorTV);
        if(cliente.getProcurador() != null
                && !cliente.getProcurador().isEmpty()){
            txtProcurador.setText(cliente.getProcurador());
        } else {
            txtProcurador.setText("N/D");
        }

        txtNacionalidade         = (TextView) rel.findViewById(R.id.txtDetalheNacionalidadeCliente);
        if(cliente.getNacionalidade()!= null
                && !cliente.getNacionalidade().isEmpty()) {
            txtNacionalidade.setText(cliente.getNacionalidade().toString());
        } else {
            txtNacionalidade.setText("N/D");
        }

        txtTelResidencial        = (TextView) rel.findViewById(R.id.txtDetalheTelefoneResidencialCliente);
        if(cliente.getTelefoneResidencial() != null
                && !cliente.getTelefoneResidencial().isEmpty()){
            txtTelResidencial.setText(UtilActivity.formatarTelefone(cliente.getTelefoneResidencial()));
        } else {
            txtTelResidencial.setText("N/D");
        }

        txtTelComercial          = (TextView) rel.findViewById(R.id.txtDetalheTelefoneComercialCliente);
        if(cliente.getTelefoneComercial() != null
                && !cliente.getTelefoneComercial().isEmpty()){
            txtTelComercial.setText(UtilActivity.formatarTelefone(cliente.getTelefoneComercial()));
        } else {
            txtTelComercial.setText("N/D");
        }

        txtTelCelular            = (TextView) rel.findViewById(R.id.txtDetalheTelefoneCelularCliente);
        if(cliente.getTelefoneCelular() != null
                && !cliente.getTelefoneCelular().isEmpty()) {
            txtTelCelular.setText(UtilActivity.formatarTelefone(cliente.getTelefoneCelular()));
        } else {
            txtTelCelular.setText("N/D");
        }

        txtEmail                 = (TextView) rel.findViewById(R.id.txtDetalheEmailCliente);
        if(cliente.getEmail() != null
                && !cliente.getEmail().isEmpty()) {
            txtEmail.setText(cliente.getEmail());
        } else {
            txtEmail.setText("N/D");
        }

        txtProfissao             = (TextView) rel.findViewById(R.id.txtDetalheProfissaoCliente);
        if(cliente.getProfissao() != null
                && !cliente.getProfissao().isEmpty() ){
            txtProfissao.setText(cliente.getProfissao());
        } else {
            txtProfissao.setText("N/D");
        }

        txtFaixaSalarial         = (TextView) rel.findViewById(R.id.txtDetalheFaixaSalarialCliente);
        if(cliente.getFaixaSalarial() != null
                && !cliente.getFaixaSalarial().toString().isEmpty()){
            txtFaixaSalarial.setText(UtilActivity.formatarMoeda(cliente.getFaixaSalarial().doubleValue()));
        } else {
            txtFaixaSalarial.setText("N/D");
        }
    }
}
