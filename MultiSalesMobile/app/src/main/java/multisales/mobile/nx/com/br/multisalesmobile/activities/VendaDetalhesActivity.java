package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelularDependente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFoneLinha;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFonePortabilidade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTvPontoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.SlidingTabLayout;

public class VendaDetalhesActivity extends ActionBarActivity {

    // Declaring Your View and Variables
    ViewPager pager;
    TabAdapter adapter;
    SlidingTabLayout tabs;
    private List<String> titles;
    private List<Fragment> fragments;
    private Tabulacao tabulacao;
    private EntityManager entityManager;
    private Venda venda;
    private Integer idVenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_detalhes);

        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        entityManager = new EntityManager(this);
        try{
            venda   = entityManager.getById(Venda.class, idVenda);
            entityManager.initialize(venda.getCliente());
            entityManager.initialize(venda.getCliente().getEndereco());
            if(venda.getCliente().getEnderecoCobranca() != null) {
                entityManager.initialize(venda.getCliente().getEnderecoCobranca());
                entityManager.initialize(venda.getCliente().getEnderecoCobranca().getCidade());
                entityManager.initialize(venda.getCliente().getEnderecoCobranca().getCidade().getEstado());
            }
            entityManager.initialize(venda.getCliente().getEndereco().getCidade());
            entityManager.initialize(venda.getCliente().getEndereco().getCidade().getEstado());
            entityManager.initialize(venda.getCliente().getEstadoCivil());
            entityManager.initialize(venda.getCliente().getEscolaridade());
            entityManager.initialize(venda.getTipoContrato());
            entityManager.initialize(venda.getCombinacaoProdutoTipo());

            if(venda.getVendaCelular() != null ){
                entityManager.initialize(venda.getVendaCelular());
                entityManager.initialize(venda.getVendaCelular().getProduto());
                entityManager.initialize(venda.getVendaCelular().getTamanhoChip());
                entityManager.initialize(venda.getVendaCelular().getOperadoraPortabilidade());
                venda.getVendaCelular().setVendaCelularDependentes(entityManager.getByWhere(VendaCelularDependente.class, "_venda_celular = " + venda.getVendaCelular().getId(), null));
            }

            if(venda.getVendaTv() != null) {
                entityManager.initialize(venda.getVendaTv());
                entityManager.initialize(venda.getVendaTv().getProduto());
                venda.getVendaTv().setProdutos(entityManager.select(Produto.class, "SELECT p.* FROM produto p INNER JOIN venda_tv_produto_adicional vtpa ON vtpa._produto = p.id and vtpa._venda_tv = " + getVenda().getVendaTv().getId()));
                entityManager.initialize(venda.getVendaTv().getConcorrenteMigracao());
                entityManager.initialize(venda.getVendaTv().getMotivoMigracao());
                venda.getVendaTv().setVendaTvPontosAdicionais(entityManager.getByWhere(VendaTvPontoAdicional.class, "_venda_tv =" + venda.getVendaTv().getId(), null));
            }

            if(venda.getVendaInternet() != null) {
                entityManager.initialize(venda.getVendaInternet());
                entityManager.initialize(venda.getVendaInternet().getProduto());
                entityManager.initialize(venda.getVendaInternet().getMotivoMigracao());
                entityManager.initialize(venda.getVendaInternet().getConcorrenteMigracao());
                venda.getVendaInternet().setProdutos(entityManager.select(Produto.class, "SELECT p.* FROM produto p INNER JOIN venda_internet_produto_adicional vipa ON vipa._produto = p.id and vipa._venda_internet = " + getVenda().getVendaInternet().getId()));
            }

            if(venda.getVendaFone() != null) {
                entityManager.initialize(venda.getVendaFone());
                entityManager.initialize(venda.getVendaFone().getTecnologiaDisponivel());
                venda.getVendaFone().setVendaFoneLinhas(entityManager.select(VendaFoneLinha.class,"SELECT vfl.* FROM venda_fone_linha vfl where vfl._venda_fone = " + venda.getVendaFone().getId() ));
                venda.getVendaFone().setVendaFonePortabilidades(entityManager.select(VendaFonePortabilidade.class,"SELECT vfp.* FROM venda_fone_portabilidade vfp WHERE vfp._venda_fone = " + venda.getVendaFone().getId()));
            }
            if (venda.getVendaFormaPagamento() != null) {
                entityManager.initialize(venda.getVendaFormaPagamento());
                entityManager.initialize(venda.getVendaFormaPagamento().getVencimentoFatura());
                entityManager.initialize(venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco());
                entityManager.initialize(venda.getMidia());
                entityManager.initialize(venda.getPeriodoInstalacao());
                if (venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco() != null){
                    entityManager.initialize(venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco());
                    entityManager.initialize(venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco().getBanco());
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }

        fragments = new ArrayList<>();
        titles = new ArrayList<>();
        fragments.add(new VendaClienteFragment());
        titles.add("DADOS DO CLIENTE");
        if (venda.getCliente().getEndereco() != null) {
            fragments.add(new VendaEnderecoFragment());
            titles.add("ENDEREÇO");
            fragments.add(new VendaPacoteFragment());
            titles.add("DADOS DO PACOTE");
        }
        if (venda.getVendaCelular() != null ) {
            fragments.add(new VendaCelularFragment());
            titles.add("CELULAR");
        }
        if (venda.getVendaTv() != null ) {
            fragments.add(new VendaTvFragment());
            titles.add("TV");
        }
        if (venda.getVendaFone() != null ) {
            fragments.add(new VendaFoneFragment());
            titles.add("FONE");
        }
        if (venda.getVendaInternet() != null ) {
            fragments.add(new VendaInternetFragment());
            titles.add("INTERNET");
        }
        if (venda.getVendaFormaPagamento() != null){
            fragments.add(new VendaPagamentoFragment ());
            titles.add("FORMA DE PAGAMENTO");
        }
        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new TabAdapter(getSupportFragmentManager(), titles, fragments);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(false); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.cor_destaque);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_detalhes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        if (id == R.id.editar) {
            Intent venda = new Intent(this, VendaClienteActivity.class);
            venda.putExtra("idVenda", idVenda);
            startActivity(venda);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void resgatarDadosDaVenda() {

    }

    public Tabulacao getTabulacao() {
        return tabulacao;
    }

    public void setTabulacao(Tabulacao tabulacao) {
        this.tabulacao = tabulacao;
    }

    public Venda getVenda() {
        return venda;
    }
}
