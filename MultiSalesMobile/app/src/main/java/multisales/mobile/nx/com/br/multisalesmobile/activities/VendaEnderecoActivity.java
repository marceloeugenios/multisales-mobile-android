package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cidade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Estado;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;


public class VendaEnderecoActivity extends ActionBarActivity {

    private EntityManager entityManager;

    private Integer idVenda;
    private Endereco endereco;
    private Venda venda;
    private boolean enderecoCobranca;
    private boolean edicao;
    private HashSet<EVendaFluxo> telasAnteriores;

    private List<Estado> estados;
    private List<Cidade> cidades;
    private Cidade cidade;

    private TextView txtViewTituloTelaEndereco;
    private EditText txtCep;
    private EditText txtEndereco;
    private EditText txtNumero;
    private EditText txtComplemento;
    private EditText txtApto;
    private EditText txtBloco;
    private EditText txtBairro;
    private EditText txtPtoRef;
    private Spinner spinnerEstado;
    private Spinner spinnerCidade;
    private CheckBox checkBoxEnderecoCobrancaDiferente;
    private boolean isMock;
    private boolean isVoltar;

    private ActionBarActivity thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        thisActivity = this;
        setContentView(R.layout.activity_venda_endereco);
        entityManager = new EntityManager(this);
        endereco = new Endereco();
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        enderecoCobranca = getIntent().getBooleanExtra("enderecoCobranca", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);
        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");

        if (enderecoCobranca) {
            telasAnteriores.remove(EVendaFluxo.ENDERECO_COBRANCA);
        } else {
            telasAnteriores.remove(EVendaFluxo.ENDERECO);
        }

        criarCamposEntrada();
        carregarEstados();
        try {
            venda = entityManager.getById(Venda.class, idVenda);
            entityManager.initialize(venda.getCliente());

            if ((venda.getCliente().getEndereco() != null
                    || venda.getCliente().getEnderecoCobranca() != null) && !edicao) {
                isVoltar = true;
            }

            if (edicao || isVoltar) {
                if (enderecoCobranca) {
                    entityManager.initialize(venda.getCliente().getEnderecoCobranca());
                    entityManager.initialize(venda.getCliente().getEnderecoCobranca().getCidade());
                    endereco = venda.getCliente().getEnderecoCobranca();
                } else {
                    entityManager.initialize(venda.getCliente().getEndereco());
                    entityManager.initialize(venda.getCliente().getEndereco().getCidade());
                    endereco = venda.getCliente().getEndereco();
                }
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao recuperar venda.", this);
        }
        if (isMock) {
            popularMock();
            popularValoresCamposEntrada();
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void popularValoresCamposEntrada() {
        if (endereco.getCep() != null) {
            txtCep.setText(endereco.getCep());
        }
        if (endereco.getLogradouro() != null) {
            txtEndereco.setText(endereco.getLogradouro());
        }
        if (endereco.getNumero() != null) {
            txtNumero.setText(endereco.getNumero());
        }
        if (endereco.getComplemento() != null) {
            txtComplemento.setText(endereco.getComplemento());
        }
        if (endereco.getApartamento() != null) {
            txtApto.setText(endereco.getApartamento());
        }
        if (endereco.getBloco() != null) {
            txtBloco.setText(endereco.getBloco());
        }
        if (endereco.getBairro() != null) {
            txtBairro.setText(endereco.getBairro());
        }
        if (endereco.getPontoReferencia() != null) {
            txtPtoRef.setText(endereco.getPontoReferencia());
        }
        int i;
        if (endereco.getCidade() != null) {
            for (i = 0; i < estados.size(); i++) {
                if (estados.get(i).getUf().equals(endereco.getCidade().getEstado().getUf())) {
                    spinnerEstado.setSelection(i + 1);
                    break;
                }
            }
        }
        if (venda.getCliente().getEnderecoCobranca() != null) {
            checkBoxEnderecoCobrancaDiferente.setChecked(true);
        }
    }

    private void popularMock() {
        isMock = true;
        try {
            if (enderecoCobranca) {
                endereco.setCep("12345678");
                endereco.setLogradouro("RUA DE COBRANCA");
                endereco.setNumero("123");
                endereco.setComplemento("COMPLEMENTO DE COBRANCA");
                endereco.setApartamento("AP COB");
                endereco.setBloco("BLC COB");
                endereco.setBairro("BAIRRO DE COBRANCA");
                endereco.setPontoReferencia("PONTO DE REFERENCIA COBRANCA");
                endereco.setCidade(entityManager.getAll(Cidade.class).get(0));
            } else {
                checkBoxEnderecoCobrancaDiferente.setChecked(true);
                endereco.setCep("87654321");
                endereco.setLogradouro("RUA DO ENDERECO");
                endereco.setNumero("321");
                endereco.setComplemento("COMPLEMENTO DO ENDERECO");
                endereco.setApartamento("AP END");
                endereco.setBloco("BLC END");
                endereco.setBairro("BAIRRO DO ENDERECO");
                endereco.setPontoReferencia("PONTO DE REFERENCIA ENDERECO");
                endereco.setCidade(entityManager.getAll(Cidade.class).get(0));
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao carregar mock", this);
        }
    }

    private void criarCamposEntrada() {
        txtViewTituloTelaEndereco         = (TextView) findViewById(R.id.txtViewTituloTelaEndereco);
        txtCep                            = (EditText) findViewById(R.id.txtCep);
        UtilMask.setMascaraCep(txtCep);
        txtEndereco                       = (EditText) findViewById(R.id.txtEndereco);
        txtNumero                         = (EditText) findViewById(R.id.txtNumero);
        txtComplemento                    = (EditText) findViewById(R.id.txtComplemento);
        txtApto                           = (EditText) findViewById(R.id.txtApto);
        txtBloco                          = (EditText) findViewById(R.id.txtBloco);
        txtBairro                         = (EditText) findViewById(R.id.txtBairro);
        txtPtoRef                         = (EditText) findViewById(R.id.txtPtoRef);
        spinnerEstado                     = (Spinner) findViewById(R.id.spinnerEstado);
        spinnerCidade                     = (Spinner) findViewById(R.id.spinnerCidade);
        checkBoxEnderecoCobrancaDiferente = (CheckBox) findViewById(R.id.checkBoxEnderecoCobrancaDiferente);

        if (enderecoCobranca) {
            checkBoxEnderecoCobrancaDiferente.setVisibility(View.INVISIBLE);
            checkBoxEnderecoCobrancaDiferente.setChecked(false);
            txtViewTituloTelaEndereco.setText("Endereço de Cobrança");
        } else {
            txtViewTituloTelaEndereco.setText("Endereço do Cliente");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_endereco, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent activityAnterior = new Intent();
            activityAnterior.putExtra("idVenda", idVenda);
            activityAnterior.putExtra("edicao", edicao);
            activityAnterior.putExtra("isMock", isMock);
            activityAnterior.putExtra("isVoltar", true);
            activityAnterior.putExtra("enderecoCobranca", false);
            activityAnterior.putExtra("telasAnteriores", telasAnteriores);

            Class<?> cls = null;

            if (enderecoCobranca) {
                cls = VendaEnderecoActivity.class;
            } else {
                cls = VendaClienteActivity.class;
            }
            activityAnterior.setComponent(new ComponentName(this, cls));
            startActivity(activityAnterior);
            VendaEnderecoActivity.this.finish();
        }

        if (id == R.id.action_avancar) {
            avancar(null);
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaEnderecoActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void carregarEstados() {
        try {
            estados = entityManager.getAll(Estado.class);
            List<String> nomesEstados = new ArrayList<>();
            nomesEstados.add(UtilActivity.SELECIONE);

            for(Estado estado : estados){
                nomesEstados.add(estado.getNome());
            }
            UtilActivity.setAdapter(this, spinnerEstado, nomesEstados);

            spinnerEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    List<String> nomesCidades = new ArrayList<>();

                    if (position > SistemaConstantes.ZERO) {
                        Estado estadoSelecionado = estados.get(position - SistemaConstantes.UM);

                        try {
                            cidades = entityManager.getByWhere(Cidade.class, "_estado = '" + estadoSelecionado.getUf() + "'", null);
                            nomesCidades.add(UtilActivity.SELECIONE);
                            if (cidades != null && !cidades.isEmpty()) {
                                for (Cidade cidade : cidades) {
                                    nomesCidades.add(cidade.getNome());
                                }
                            }
                            spinnerCidade.setEnabled(true);
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                            UtilActivity.makeShortToast("Ops! Ocorreu um ERRO ao listar as cidades!", getApplicationContext());
                            cidades = new ArrayList<>();
                        }
                    } else {
                        nomesCidades.add("SELECIONE O ESTADO");
                        spinnerCidade.setEnabled(false);
                    }
                    ArrayAdapter adapter = new ArrayAdapter<> (thisActivity, android.R.layout.simple_spinner_item, nomesCidades);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerCidade.setAdapter(adapter);
                    if (edicao || isMock || isVoltar) {
                        int i;
                        if (endereco.getCidade() != null) {
                            for (i = 0; i < cidades.size(); i++) {
                                if (cidades.get(i).getId().equals(endereco.getCidade().getId())) {
                                    spinnerCidade.setSelection(i + 1);
                                    break;
                                }
                            }
                        }
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (DataBaseException e) {
            e.printStackTrace();
        }
    }

    public boolean validarCamposObrigatorios() {
        if (spinnerCidade.getSelectedItemPosition() == SistemaConstantes.ZERO) {
            UtilActivity.makeShortToast("Informe a cidade", getApplicationContext());
            return false;
        } else {
            this.cidade = cidades.get(spinnerCidade.getSelectedItemPosition() - SistemaConstantes.UM);
        }
        return true;
    }

    public void popularDadosEndereco() {
        endereco.setCep(UtilMask.unmask(txtCep.getText().toString()));
        endereco.setApartamento(txtApto.getText().toString());
        endereco.setBloco(txtBloco.getText().toString());
        endereco.setBairro(txtBairro.getText().toString());
        endereco.setCidade(cidade);
        endereco.setComplemento(txtComplemento.getText().toString());
        endereco.setLogradouro(txtEndereco.getText().toString());
        endereco.setNumero(txtNumero.getText().toString());
        endereco.setPontoReferencia(txtPtoRef.getText().toString());
    }

    public void avancar(View view) {
        try {
            if (!validarCamposObrigatorios()) {
                return;
            }

            Venda venda = entityManager.getById(Venda.class, idVenda);
            entityManager.initialize(venda.getCliente());

            if (enderecoCobranca) {
                if (venda.getCliente().getEnderecoCobranca() != null) {
                    deletarEndereco(venda.getCliente().getEnderecoCobranca());
                }
                salvarEndereco();
                venda.getCliente().setEnderecoCobranca(endereco);
            } else {
                if (venda.getCliente().getEndereco() != null) {
                    deletarEndereco(venda.getCliente().getEndereco());
                }
                salvarEndereco();
                venda.getCliente().setEndereco(endereco);
            }

            entityManager.atualizar(venda.getCliente());

            Intent proximaActivity = new Intent();
            proximaActivity.putExtra("idVenda", idVenda);
            proximaActivity.putExtra("edicao", edicao);
            proximaActivity.putExtra("isMock", isMock);
            proximaActivity.putExtra("telasAnteriores", telasAnteriores);
            Class<?> cls = null;

            //Caso seja a activity de endereço de cobrança OU
            //a activity de endereço do cliente mas a opção de
            // endereço de cobrança diferente esteja desmarcada
            //redireciona para a activity de pacote
            if (checkBoxEnderecoCobrancaDiferente.getVisibility() == View.VISIBLE
                    && checkBoxEnderecoCobrancaDiferente.isChecked()) {
                proximaActivity.putExtra("enderecoCobranca", true);
                telasAnteriores.add(EVendaFluxo.ENDERECO_COBRANCA);
                cls = VendaEnderecoActivity.class;
            //Caso contrário reabre esta activity, no entanto com a flag de endereço de cobrança = true
            } else {
                telasAnteriores.add(EVendaFluxo.ENDERECO);
                if (venda.getCliente().getEnderecoCobranca() != null
                        && venda.getCliente().getEnderecoCobranca().getId() != null
                        && !enderecoCobranca) {
                    entityManager.executeNativeQuery("DELETE FROM endereco WHERE id = "
                            + venda.getCliente().getEnderecoCobranca().getId());
                    venda.getCliente().setEnderecoCobranca(null);
                    entityManager.atualizar(venda.getCliente());
                }
               cls = VendaPacoteActivity.class;
            }
            proximaActivity.setComponent(new ComponentName(this, cls));
            startActivity(proximaActivity);
            VendaEnderecoActivity.this.finish();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Ocorreu um ERRO ao salvar o endereço!",
                    this.getApplicationContext());
        }
    }

    private void salvarEndereco() throws DataBaseException {
        popularDadosEndereco();
        endereco = entityManager.save(endereco);
    }

    private void deletarEndereco(Endereco endereco) throws DataBaseException {
        if (endereco.getId() != null) {
            entityManager.executeNativeQuery("DELETE FROM endereco WHERE id = " + endereco.getId());
        }
    }
}
