package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cliente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

/**
 * Created by eric on 27-02-2015.
 */
public class VendaEnderecoFragment extends Fragment{

    private RelativeLayout relativeLayout;
    private TextView cepTV;
    private TextView cidadeEstadoTV;
    private TextView complementoTV;
    private TextView pontoReferenciaTV;
    private TextView enderecoTV;
    private Endereco endereco;
    private Endereco enderecoCobranca;
    private RelativeLayout relativeLayoutCobranca;
    private TextView cepCobrancaTV;
    private TextView cidadeEstadoCobrancaTV;
    private TextView pontoReferenciaCobrancaTV;
    private TextView complementoCobrancaTV;
    private TextView enderecoCobrancaTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.endereco = ((VendaDetalhesActivity) getActivity()).getVenda().getCliente().getEndereco();
        this.enderecoCobranca =((VendaDetalhesActivity) getActivity()).getVenda().getCliente().getEnderecoCobranca();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }
        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_venda_endereco, container, false);
        relativeLayoutCobranca = (RelativeLayout) relativeLayout.findViewById(R.id.enderecoCobrancaRL);
        criarCamposEntrada();
        return relativeLayout;
    }
    private void criarCamposEntrada() {

        this.cepTV = (TextView) relativeLayout.findViewById(R.id.cepTV);
        if(endereco.getCep() != null && !endereco.getCep().isEmpty() && endereco.getCep().length() > 7){
            String part1 = endereco.getCep().substring(0,5);
            String part2 = endereco.getCep().substring(5,8);
            String cep = part1 + "-" + part2;
            cepTV.setText(cep);
        } else {
            cepTV.setText("N/D");
        }

        this.cidadeEstadoTV = (TextView) relativeLayout.findViewById(R.id.cidadeEstadoTV);
        cidadeEstadoTV.setText(endereco.getCidade().getNome() + "/" + endereco.getCidade().getEstado().getUf());

        this.complementoTV = (TextView) relativeLayout.findViewById(R.id.complementoTV);
        if(endereco.getComplemento() != null
                && !endereco.getComplemento().isEmpty()) {
            complementoTV.setText(endereco.getComplemento());
        } else {
            complementoTV.setText("N/D");
        }

        this.pontoReferenciaTV = (TextView) relativeLayout.findViewById(R.id.pontoReferenciaTV);
        if(endereco.getPontoReferencia() != null && !endereco.getPontoReferencia().isEmpty()){
            pontoReferenciaTV.setText(endereco.getPontoReferencia());
        } else {
            pontoReferenciaTV.setText("N/D");
        }

        this.enderecoTV = (TextView) relativeLayout.findViewById(R.id.enderecoTV);
        StringBuilder enderecoCompleto = new StringBuilder();
        int aux = 0 ;
        if(endereco.getLogradouro() != null && !endereco.getLogradouro().isEmpty()){
            enderecoCompleto.append(endereco.getLogradouro());
            aux ++ ;
        }
        if(endereco.getNumero() != null && !endereco.getNumero().isEmpty()){
            if(endereco.getLogradouro() == null ){
               enderecoCompleto.append(" ");
            } else {
                enderecoCompleto.append(", " + endereco.getNumero());
            }
            aux ++ ;
        }
        if(endereco.getApartamento() != null && !endereco.getApartamento().isEmpty()){
            enderecoCompleto.append(", " + endereco.getApartamento());
            aux ++ ;
        }
        if(endereco.getBloco() != null && !endereco.getBloco().isEmpty()){
            enderecoCompleto.append(", " + endereco.getBairro());
            aux ++ ;
        }
        if(aux == 0 ){
            enderecoTV.setText("N/D");
        } else {
            enderecoTV.setText(enderecoCompleto);
        }




        if(enderecoCobranca != null ){
            this.cepCobrancaTV = (TextView) relativeLayout.findViewById(R.id.cepCobrancaTV);
            if(enderecoCobranca.getCep() != null && !enderecoCobranca.getCep().isEmpty() && enderecoCobranca.getCep().length() >7 ){
                String part1 = enderecoCobranca.getCep().substring(0,5);
                String part2 = enderecoCobranca.getCep().substring(5,8);
                String cep = part1 + "-" + part2;
                cepCobrancaTV.setText(cep);
            } else {
                cepCobrancaTV.setVisibility(View.GONE);
            }

            this.enderecoCobrancaTV = (TextView) relativeLayoutCobranca.findViewById(R.id.enderecoCobrancaTV);
            StringBuilder enderecoCompletoCombranca = new StringBuilder();
            int aux1 = 0 ;
            if(enderecoCobranca.getLogradouro() != null && !enderecoCobranca.getLogradouro().isEmpty()){
                enderecoCompletoCombranca.append(enderecoCobranca.getLogradouro());
                aux1 ++ ;
            }
            if(enderecoCobranca.getNumero() != null && !enderecoCobranca.getNumero().isEmpty()){
                if(enderecoCobranca.getLogradouro() != null ){
                    enderecoCompletoCombranca.append(", " + enderecoCobranca.getNumero());
                }
                aux1 ++ ;
            }
            if(enderecoCobranca.getApartamento() != null && !enderecoCobranca.getApartamento().isEmpty()){
                enderecoCompletoCombranca.append(", " + enderecoCobranca.getApartamento());
                aux1 ++ ;
            }
            if(enderecoCobranca.getBloco() != null && !enderecoCobranca.getBloco().isEmpty()){
                enderecoCompletoCombranca.append(", " + enderecoCobranca.getBairro());
                aux1 ++ ;
            }
            if(aux1 == 0 ){
                enderecoCobrancaTV.setVisibility(View.GONE);
            } else {
                enderecoCobrancaTV.setText(enderecoCompletoCombranca);
            }

            this.complementoCobrancaTV = (TextView) relativeLayoutCobranca.findViewById(R.id.complementoCobrancaTV);
            if(enderecoCobranca.getComplemento() != null
                    && !enderecoCobranca.getComplemento().isEmpty()) {
                complementoCobrancaTV.setText(enderecoCobranca.getComplemento());
            } else {
                complementoCobrancaTV.setVisibility(View.GONE);
            }

            this.pontoReferenciaCobrancaTV = (TextView) relativeLayoutCobranca.findViewById(R.id.pontoReferenciaCobrancaTV);
            if(enderecoCobranca.getPontoReferencia() != null
                    && !enderecoCobranca.getPontoReferencia().isEmpty()){
                pontoReferenciaCobrancaTV.setText(enderecoCobranca.getPontoReferencia());
            } else {
                pontoReferenciaTV.setVisibility(View.GONE);
            }

            if(enderecoCobranca.getCidade() != null ) {
                this.cidadeEstadoCobrancaTV = (TextView) relativeLayoutCobranca.findViewById(R.id.cidadeEstadoCobrancaTV);
                cidadeEstadoCobrancaTV.setText(enderecoCobranca.getCidade().getNome() + "/" + enderecoCobranca.getCidade().getEstado().getUf());
            } else {
                cidadeEstadoCobrancaTV.setVisibility(View.GONE);
            }

        } else {
           relativeLayoutCobranca.setVisibility(View.GONE);
        }

    }

}
