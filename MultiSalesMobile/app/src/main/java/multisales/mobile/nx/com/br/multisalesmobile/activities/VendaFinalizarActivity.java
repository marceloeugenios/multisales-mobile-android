package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Hp;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelularDependente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFoneLinha;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFonePortabilidade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaInternetProdutoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaMotivoNaoVenda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTvPontoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTvProdutoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class VendaFinalizarActivity extends ActionBarActivity {

    private EntityManager entityManager;

    private RequestD2D requestD2D = new RequestD2D();
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();

    private Integer idVenda;
    private boolean edicao;
    private boolean isMock;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private HashSet<EVendaFluxo> telasAnteriores;
    private MultiSales multiSales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_finalizar);
        multiSales = (MultiSales) getApplication();
        entityManager = new EntityManager(this);
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>) getIntent().getExtras().get("produtosTipo");
        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_finalizar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_salvar_venda) {
            try {
                salvarVenda();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                UtilActivity.makeShortToast("ERRO: " + e.getMessage(), this);
                return false;
            }
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaFinalizarActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        if (id == android.R.id.home) {
            Intent activityAnterior = new Intent();
            activityAnterior.putExtra("idVenda", idVenda);
            activityAnterior.putExtra("edicao", edicao);
            activityAnterior.putExtra("isMock", isMock);
            activityAnterior.putExtra("isVoltar", true);
            activityAnterior.putExtra("telasAnteriores", telasAnteriores);
            activityAnterior.putExtra("produtosTipo", produtosTiposSelecionados);
            Class<?> cls = VendaFormaPagamentoActivity.class;
            activityAnterior.setComponent(new ComponentName(this, cls));
            startActivity(activityAnterior);
            VendaFinalizarActivity.this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void salvarVenda() {
        try {

            Tabulacao tabulacao = entityManager.getById(Tabulacao.class, idVenda);
            entityManager.initialize(tabulacao.getVenda());
            tabulacao.getVenda().setNullId();
            entityManager.initialize(tabulacao.getVenda().getCombinacaoProdutoTipo());

            entityManager.initialize(tabulacao.getVenda().getCliente());
            tabulacao.getVenda().getCliente().setNullId();
            entityManager.initialize(tabulacao.getVenda().getCliente().getEndereco());
            tabulacao.getVenda().getCliente().getEndereco().setNullId();
            Hp hp = new Hp();
            if (multiSales.getIdCondominio() != null && multiSales.getIdCondominio() != 0) {
                hp.setCidade(tabulacao.getVenda().getCliente().getEndereco().getCidade());
                hp.setCondominio(entityManager.getById(Condominio.class, multiSales.getIdCondominio()));
            }
            tabulacao.setHp(hp);

            entityManager.initialize(tabulacao.getVenda().getCliente().getEnderecoCobranca());

            if (tabulacao.getVenda().getCliente().getEnderecoCobranca() != null) {
                tabulacao.getVenda().getCliente().getEnderecoCobranca().setNullId();
            }

            List<VendaMotivoNaoVenda> vendasMotivosNaoVenda =
                    entityManager.getByWhere(VendaMotivoNaoVenda.class, "_venda = " + idVenda, null);

            if (vendasMotivosNaoVenda != null) {
                tabulacao.getVenda().setVendaMotivosNaoVenda(vendasMotivosNaoVenda);
            }

            entityManager.initialize(tabulacao.getVenda().getVendaCelular());
            if (tabulacao.getVenda().getVendaCelular() != null
                    && produtosTiposSelecionados.contains(EProdutoTipo.MULTI)) {
                tabulacao.getVenda().getVendaCelular().setNullId();

                List<VendaCelularDependente> dependentes = entityManager.select(
                        VendaCelularDependente.class,
                        "SELECT vcd.* " +
                        "FROM venda_celular_dependente vcd " +
                        "WHERE vcd._venda_celular = " + idVenda);

                if (dependentes != null) {
                    for (VendaCelularDependente vcd : dependentes) {
                        vcd.setNullId();
                    }
                    tabulacao.getVenda().getVendaCelular().setVendaCelularDependentes(
                            new ArrayList<VendaCelularDependente>());
                    tabulacao.getVenda().getVendaCelular()
                            .getVendaCelularDependentes().addAll(dependentes);
                }
                tabulacao.getVenda().addVendaCelular();
            } else {
                tabulacao.getVenda().setVendaCelular(null);
                tabulacao.getVenda().setVendasCelular(null);
                UtilActivity.deletarCelular(entityManager, idVenda);
            }

            entityManager.initialize(tabulacao.getVenda().getVendaTv());
            if (tabulacao.getVenda().getVendaTv() != null
                    && produtosTiposSelecionados.contains(EProdutoTipo.TV)) {
                tabulacao.getVenda().getVendaTv().setNullId();

                List<VendaTvProdutoAdicional> produtosAdicionais = entityManager.select(
                        VendaTvProdutoAdicional.class,
                        "SELECT vtpa.* " +
                        "FROM venda_tv_produto_adicional vtpa " +
                        "WHERE vtpa._venda_tv = " + idVenda);

                if (produtosAdicionais != null) {
                    tabulacao.getVenda().getVendaTv().setProdutos(new ArrayList<Produto>());
                    for (VendaTvProdutoAdicional vendaTvProdutoAdicional : produtosAdicionais) {
                        tabulacao.getVenda().getVendaTv().getProdutos()
                                .add(new Produto(vendaTvProdutoAdicional.getId().getProduto()));
                    }
                }

                tabulacao.getVenda().getVendaTv().setVendaTvPontosAdicionais(
                        entityManager.select(
                                VendaTvPontoAdicional.class,
                                "SELECT vtpa.* " +
                                "FROM venda_tv_ponto_adicional vtpa " +
                                "WHERE vtpa._venda_tv = " + idVenda));

                for (VendaTvPontoAdicional vendaTvPontoAdicional
                        : tabulacao.getVenda().getVendaTv().getVendaTvPontosAdicionais()) {
                    vendaTvPontoAdicional.setNullId();
                }

                tabulacao.getVenda().addVendaTv();

            } else {
                tabulacao.getVenda().setVendaTv(null);
                tabulacao.getVenda().setVendasTv(null);
                UtilActivity.deletarTv(entityManager, idVenda);
            }

            entityManager.initialize(tabulacao.getVenda().getVendaFone());
            if (tabulacao.getVenda().getVendaFone() != null
                    && produtosTiposSelecionados.contains(EProdutoTipo.FONE)) {

                List<VendaFoneLinha> linhas = entityManager
                        .getByWhere(VendaFoneLinha.class,
                                "_venda_fone = " + tabulacao.getVenda().getVendaFone().getId(),
                                null);

                if (linhas != null) {
                    for (VendaFoneLinha linha : linhas) {
                        linha.setNullId();
                    }
                    tabulacao.getVenda().getVendaFone().setVendaFoneLinhas(linhas);
                }

                List<VendaFonePortabilidade> portabilidades = entityManager
                        .getByWhere(VendaFonePortabilidade.class,
                                "_venda_fone = " + tabulacao.getVenda().getVendaFone().getId(),
                                null);

                if (portabilidades != null) {
                    for (VendaFonePortabilidade portabilidade : portabilidades) {
                        portabilidade.setNullId();
                    }
                    tabulacao.getVenda().getVendaFone().setVendaFonePortabilidades(portabilidades);
                }

                tabulacao.getVenda().getVendaFone().setNullId();
                tabulacao.getVenda().addVendaFone();
            } else {
                tabulacao.getVenda().setVendaFone(null);
                tabulacao.getVenda().setVendasFone(null);
                UtilActivity.deletarFone(entityManager, idVenda);
            }

            entityManager.initialize(tabulacao.getVenda().getVendaInternet());
            if (tabulacao.getVenda().getVendaInternet() != null
                    && produtosTiposSelecionados.contains(EProdutoTipo.INTERNET)) {

                List<VendaInternetProdutoAdicional> vendasInternetProdutosAdicionais =
                        entityManager.select(VendaInternetProdutoAdicional.class,
                                "SELECT vipa.* " +
                                "FROM venda_internet_produto_adicional vipa " +
                                "WHERE vipa._venda_internet = " + tabulacao.getVenda().getVendaInternet().getId());

                if (vendasInternetProdutosAdicionais != null) {
                    tabulacao.getVenda().getVendaInternet().setProdutos(new ArrayList<Produto>());
                    for (VendaInternetProdutoAdicional vipa : vendasInternetProdutosAdicionais) {
                        tabulacao.getVenda().getVendaInternet().getProdutos().add(vipa.getProduto());
                    }
                }

                tabulacao.getVenda().getVendaInternet().setNullId();
                tabulacao.getVenda().addVendaInternet();
            } else {
                tabulacao.getVenda().setVendaInternet(null);
                tabulacao.getVenda().setVendasInternet(null);
                UtilActivity.deletarInternet(entityManager, idVenda);
            }

            entityManager.initialize(tabulacao.getVenda().getVendaFormaPagamento());
            entityManager.initialize(tabulacao.getVenda().getVendaFormaPagamento().getVendaFormaPagamentoBanco());
            tabulacao.getVenda().addVendaFormaPagamento();

            tabulacao.addVenda();
            requestD2D.setTabulacao(tabulacao);
        } catch (Exception e) {
            UtilActivity.makeShortToast("ERRO: " + e.getMessage(), this);
            return;
        }

        AsyncTask<Void, Void, ResponseD2D> taskSalvarVenda = new AsyncTask<Void, Void, ResponseD2D>() {
            @Override
            protected ResponseD2D doInBackground(Void... params) {
                JSONObject jsonEnvio;
                JSONObject jsonRetorno;
                ResponseD2D responseD2D = null;
                try {
                    requestD2D.setOperacao(ECodigoOperacaoD2D.VENDA_CODIGO.getCodigo());
                    requestD2D.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                    requestD2D.setIdUsuario(multiSales.getIdUsuario());
                    jsonEnvio = jsonParser.parseObjectToJson(requestD2D);
                    jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                    responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                } catch (Exception e) {
                    responseD2D = new ResponseD2D();
                    responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                    responseD2D.setMensagem(e.getMessage());
                    return responseD2D;
                }
                return responseD2D;
            }

            @Override
            protected void onPostExecute(ResponseD2D resposta) {
                Toast.makeText(getApplicationContext(), resposta.getMensagem(), Toast.LENGTH_SHORT).show();
                super.onPostExecute(resposta);
                if(resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                    UtilActivity.makeShortToast("Venda cadastrada com sucesso!", getApplicationContext());
                    Intent main = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(main);
                    try {
                        Tabulacao tabulacao = entityManager.getById(Tabulacao.class, idVenda);
                        tabulacao.setId((Integer) resposta.getExtras().get("idVenda"));
                        entityManager.atualizar(tabulacao);
                    } catch (Exception e) {
                        UtilActivity.makeShortToast("ERRO ao atualizar na base local!", getApplicationContext());
                    }
                }
            }
        };
        taskSalvarVenda.execute();
    }
}
