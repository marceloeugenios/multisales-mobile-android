package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TecnologiaDisponivel;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFone;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFoneLinha;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFonePortabilidade;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaFoneActivity extends ActionBarActivity {

    private EntityManager entityManager;
    private MultiSales multiSales;
    private Venda venda;
    private VendaFone vendaFone;

    //Extras
    private Integer idVenda;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private HashSet<EVendaFluxo> telasAnteriores;


    private List<Produto> produtosTelefone;
    private List<VendaFoneLinha> vendasFoneLinhas;
    private List<TecnologiaDisponivel> tecnologiasDisponiveis;

    //Componentes
    private Spinner spinnerPlanoTelefone;
    private NumberPicker qtdExtensoesNumberPicker;
    private CheckBox checkBoxPublicarNumero;
    private ListView listViewLinhas;
    private EditText txtLinhaTronco;
    private EditText txtObservacao;
    private CheckBox checkBoxAdicionarNumeroPortado;

    private EditText txtMensalidade;
    private EditText txtMinExcedidoFixo;
    private EditText txtHabilitacao;

    private EditText txtTaxaHabilitacao;
    private EditText txtValorPlano;
    private Spinner spinnerTecnologiaUtilizada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_fone);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>) getIntent()
                .getExtras().get("produtosTipo");
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);

        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.FONE);

        criarCamposEntrada();
        entityManager = new EntityManager(this);
        multiSales = (MultiSales) getApplication();
        vendasFoneLinhas = new ArrayList<>();
        carregarPlanosTelefone();
        carregarQuantidadesExtensoes();
        carregarTecnologiasDisponiveis();

        try {
            venda = entityManager.getById(Venda.class, idVenda);

            if (venda.getVendaFone() != null
                    && venda.getVendaFone().getId() != null
                    && !edicao) {
                isVoltar = true;
            }

            if (venda.getVendaFone() != null
                    && venda.getVendaFone().getId() != null
                    && (edicao || isVoltar)) {

                vendaFone = entityManager.getById(VendaFone.class, venda.getVendaFone().getId());

                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void carregarTecnologiasDisponiveis() {
        try {
            tecnologiasDisponiveis = entityManager.select(
                    TecnologiaDisponivel.class,
                    "SELECT td.* FROM tecnologia_disponivel td INNER JOIN produto_tipo pt ON pt.id = td._produto_tipo WHERE td.situacao = 'ATIVO' AND pt.descricao = 'FONE' ORDER BY descricao");
            List<String> descricoesTecnologias = new ArrayList<>();
            if (tecnologiasDisponiveis != null) {
                descricoesTecnologias.add(UtilActivity.SELECIONE);
                for (TecnologiaDisponivel tecnologia : tecnologiasDisponiveis) {
                    descricoesTecnologias.add(tecnologia.getDescricao());
                }
            }
            UtilActivity.setAdapter(this, spinnerTecnologiaUtilizada, descricoesTecnologias);
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }
    }

    private void popularMock() {
        vendaFone = new VendaFone();
        vendaFone.setObservacao("OBSERVAÇÃO VENDA FONE");
        vendaFone.setLinhaTronco("4333333333");
        vendaFone.setTaxaHabilitacao(new BigDecimal(SistemaConstantes.SESSENTA_QUATRO));
        vendaFone.setValorPlano(new BigDecimal(SistemaConstantes.OITENTA));
        vendaFone.setTecnologiaDisponivel(tecnologiasDisponiveis.get(SistemaConstantes.ZERO));

        VendaFoneLinha vfl = new VendaFoneLinha();
        vfl.setPublicarNumero(EBoolean.FALSE);
        vfl.setExtensoes(SistemaConstantes.ZERO);
        vfl.setProduto(produtosTelefone.get(SistemaConstantes.ZERO));
        vfl.setMensalidade(new BigDecimal(SistemaConstantes.SETENTA));
        vfl.setHabilitacao(new BigDecimal(SistemaConstantes.CINQUENTA));
        vfl.setMinutoExcedidoParaFixo(new BigDecimal(SistemaConstantes.DOIS));

        vendasFoneLinhas.add(vfl);
    }

    private void popularValoresCamposEntrada() throws DataBaseException {

        txtLinhaTronco.setText(vendaFone.getLinhaTronco());
        txtObservacao.setText(vendaFone.getObservacao());

        List<VendaFonePortabilidade> portabilidades = null;

        if (edicao || isVoltar) {
            vendasFoneLinhas = entityManager.select(
                    VendaFoneLinha.class,
                    "SELECT * FROM venda_fone_linha WHERE _venda_fone = " + vendaFone.getId());

            portabilidades = entityManager.select(
                    VendaFonePortabilidade.class,
                    "SELECT * FROM venda_fone_portabilidade WHERE _venda_fone = " + vendaFone.getId());
        }

        checkBoxAdicionarNumeroPortado
                .setChecked(((portabilidades != null && !portabilidades.isEmpty()) || isMock));

        if (vendaFone.getTaxaHabilitacao() != null) {
            txtTaxaHabilitacao.setText(UtilActivity.formatarMoeda(vendaFone.getTaxaHabilitacao().doubleValue()));
        }

        if (vendaFone.getValorPlano() != null) {
            txtValorPlano.setText(UtilActivity.formatarMoeda(vendaFone.getValorPlano().doubleValue()));
        }

        if (vendaFone.getTecnologiaDisponivel() != null) {
            for (TecnologiaDisponivel tecnologia : tecnologiasDisponiveis) {
                if (tecnologia.getId().equals(vendaFone.getTecnologiaDisponivel().getId())) {
                    spinnerTecnologiaUtilizada
                            .setSelection(tecnologiasDisponiveis.indexOf(tecnologia) + SistemaConstantes.UM);
                    break;
                }
            }
        }

        listarVendasFoneLinhas();
    }

    private void carregarQuantidadesExtensoes() {
        qtdExtensoesNumberPicker.setMaxValue(SistemaConstantes.QUATRO);
    }

    private void carregarPlanosTelefone() {
        try {
            produtosTelefone = new ArrayList<>();
            String sql =
                    "SELECT p.* " +
                            "FROM produto p " +
                            "INNER JOIN produto_tipo pt " +
                            "ON pt.id = p._produto_tipo " +
                            "WHERE pt.descricao = '" + EProdutoTipo.FONE.getDescricao() + "' " +
                            "AND p.situacao = 'ATIVO' " +
                            "ORDER BY p.descricao";
            produtosTelefone.addAll(entityManager.select(Produto.class, sql));

            List<String> descricoesProdutosTelefone = new ArrayList<>();
            descricoesProdutosTelefone.add(UtilActivity.SELECIONE);
            for (Produto produto : produtosTelefone) {
                descricoesProdutosTelefone.add(produto.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerPlanoTelefone, descricoesProdutosTelefone);
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar os planos de telefone: " + e.getMessage(), this);
        }
    }

    private void criarCamposEntrada() {
        spinnerPlanoTelefone           = (Spinner) findViewById(R.id.spinnerPlanoTelefone);
        qtdExtensoesNumberPicker       = (NumberPicker) findViewById(R.id.qtdExtensoesNumberPicker);
        checkBoxPublicarNumero         = (CheckBox) findViewById(R.id.checkBoxPublicarNumero);
        listViewLinhas                 = (ListView) findViewById(R.id.listViewLinhas);
        txtLinhaTronco                 = (EditText) findViewById(R.id.txtLinhaTronco);
        UtilMask.setMascaraTelefone(txtLinhaTronco);
        txtObservacao                  = (EditText) findViewById(R.id.txtObservacao);
        checkBoxAdicionarNumeroPortado = (CheckBox) findViewById(R.id.checkBoxAdicionarNumeroPortado);
        txtMensalidade                 = (EditText) findViewById(R.id.txtMensalidade);
        UtilMask.setMascaraMoeda(txtMensalidade, 10);
        txtMinExcedidoFixo             = (EditText) findViewById(R.id.txtMinExcedidoFixo);
        UtilMask.setMascaraMoeda(txtMinExcedidoFixo, 10);
        txtHabilitacao                 = (EditText) findViewById(R.id.txtHabilitacao);
        UtilMask.setMascaraMoeda(txtHabilitacao, 10);
        txtTaxaHabilitacao             = (EditText) findViewById(R.id.txtTaxaHabilitacao);
        UtilMask.setMascaraMoeda(txtTaxaHabilitacao, 10);
        txtValorPlano                  = (EditText) findViewById(R.id.txtValorPlano);
        UtilMask.setMascaraMoeda(txtValorPlano, 10);
        spinnerTecnologiaUtilizada     = (Spinner) findViewById(R.id.spinnerTecnologiaUtilizada);

        listViewLinhas.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                vendasFoneLinhas.remove(position);
                listarVendasFoneLinhas();
                return true;
            }
        });
    }

    public void adicionarLinha(View view) {
        if (validarCamposObrigatorios()) {
            VendaFoneLinha vfl = new VendaFoneLinha();
            vfl.setProduto(produtosTelefone.get(spinnerPlanoTelefone.getSelectedItemPosition() - SistemaConstantes.UM));
            vfl.setExtensoes(qtdExtensoesNumberPicker.getValue());
            vfl.setPublicarNumero((checkBoxPublicarNumero.isChecked() ? EBoolean.TRUE : EBoolean.FALSE));

            if (txtMensalidade.getText() != null && !txtMensalidade.getText().toString().isEmpty()) {
                vfl.setMensalidade(new BigDecimal(UtilMask.unmaskMoeda(txtMensalidade.getText().toString())));
            }

            if (txtHabilitacao.getText() != null && !txtHabilitacao.getText().toString().isEmpty()) {
                vfl.setHabilitacao(new BigDecimal(UtilMask.unmaskMoeda(txtHabilitacao.getText().toString())));
            }

            if (txtMinExcedidoFixo.getText() != null && !txtMinExcedidoFixo.getText().toString().isEmpty()) {
                vfl.setMinutoExcedidoParaFixo(new BigDecimal(UtilMask.unmaskMoeda(txtMinExcedidoFixo.getText().toString())));
            }

            vendasFoneLinhas.add(vfl);
            listarVendasFoneLinhas();

            spinnerPlanoTelefone.setSelection(SistemaConstantes.ZERO);
            qtdExtensoesNumberPicker.setValue(SistemaConstantes.ZERO);
            checkBoxPublicarNumero.setChecked(false);
            txtMensalidade.setText(null);
            txtHabilitacao.setText(null);
            txtMinExcedidoFixo.setText(null);
        }
    }

    private void listarVendasFoneLinhas() {
        List<Map<String, Object>> itens = new ArrayList<Map<String, Object>>();

        if (vendasFoneLinhas != null) {
            for (VendaFoneLinha vfl : vendasFoneLinhas) {
                Map<String, Object> item = new HashMap<>();
                item.put("produto", vfl.getProduto().getDescricao());
                item.put("qtdExtensoes", vfl.getExtensoes());
                item.put("publicarNumero", (vfl.getPublicarNumero() == EBoolean.TRUE ? "SIM" : "NÃO"));
                item.put("mensalidade", UtilActivity.formatarMoeda(vfl.getMensalidade().doubleValue()));
                item.put("minutoExcedidoParaFixo", UtilActivity.formatarMoeda(vfl.getMinutoExcedidoParaFixo().doubleValue()));
                item.put("habilitacao", UtilActivity.formatarMoeda(vfl.getHabilitacao().doubleValue()));
                itens.add(item);
            }
        }

        //String[] de = {"produto", "qtdExtensoes", "publicarNumero"};
        String[] de = {"produto", "qtdExtensoes", "publicarNumero", "mensalidade", "minutoExcedidoParaFixo", "habilitacao"};
        int[] para = {R.id.txtProduto, R.id.txtQtdExtensoes, R.id.txtPublicarNumero, R.id.mensalidadeTV, R.id.minutoExcedidoTV, R.id.habilitacaoTV};
        SimpleAdapter adapter = new SimpleAdapter(this, itens, R.layout.item_venda_fone_linha_vivo, de, para);
        listViewLinhas.setAdapter(adapter);
        UtilActivity.setListViewHeightBasedOnItems(listViewLinhas);
    }

    private boolean validarCamposObrigatorios() {
        if (!UtilActivity.isValidSpinnerValue(spinnerPlanoTelefone)) {
            UtilActivity.makeShortToast("Informe o plano de telefone.", this);
            return false;
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_fone, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            try {
                Intent activityAnterior = new Intent();
                activityAnterior.putExtra("idVenda", idVenda);
                activityAnterior.putExtra("edicao", edicao);
                activityAnterior.putExtra("isMock", isMock);
                activityAnterior.putExtra("isVoltar", true);
                activityAnterior.putExtra("telasAnteriores", telasAnteriores);
                activityAnterior.putExtra("produtosTipo", produtosTiposSelecionados);
                Class<?> cls = null;

                if (telasAnteriores.contains(EVendaFluxo.TV_ADICIONAL)) {
                    cls = VendaTvAdicionalActivity.class;
                } else if (telasAnteriores.contains(EVendaFluxo.TV)) {
                    cls = VendaTvActivity.class;
                } else if (telasAnteriores.contains(EVendaFluxo.CELULAR_DEPENDENTE)) {
                    cls = VendaCelularDependenteActivity.class;
                } else if (telasAnteriores.contains(EVendaFluxo.CELULAR)) {
                    cls = VendaCelularActivity.class;
                } else if (telasAnteriores.contains(EVendaFluxo.MOTIVOS_NAO_VENDA)) {
                    cls = VendaPacoteNaoVendaActivity.class;
                } else {
                    cls = VendaPacoteActivity.class;
                }

//                if (produtosTiposSelecionados.contains(EProdutoTipo.TV)) {
//                    entityManager.initialize(venda.getVendaTv());
//                    List<VendaTvPontoAdicional> pontosAdicionais =
//                            entityManager.select(
//                                    VendaTvPontoAdicional.class,
//                                    "SELECT * FROM venda_tv_ponto_adicional WHERE _venda_tv = "
//                                            + venda.getVendaTv().getId());
//
//                    List<VendaTvPontoAdicional> produtosAdicionais =
//                            entityManager.select(
//                                    VendaTvPontoAdicional.class,
//                                    "SELECT * FROM venda_tv_ponto_adicional WHERE _venda_tv = "
//                                            + venda.getVendaTv().getId());
//
//                    //verificar se tem adicionais TV
//                    if ((pontosAdicionais != null && !pontosAdicionais.isEmpty())
//                            || (produtosAdicionais != null && !produtosAdicionais.isEmpty())) {
//                        cls = VendaTvAdicionalActivity.class;
//                    } else {
//                        cls = VendaTvActivity.class;
//                    }
//                } else if (produtosTiposSelecionados.contains(EProdutoTipo.MULTI)) {
//                    entityManager.initialize(venda.getVendaCelular());
//                    List<VendaCelularDependente> dependentes =
//                            entityManager.select(
//                                    VendaCelularDependente.class,
//                                    "SELECT * FROM venda_celular_dependente WHERE _venda_celular = "
//                                            + venda.getVendaCelular().getId());
//                    //verificar se tem dependentes
//                    if (dependentes != null && !dependentes.isEmpty()) {
//                        cls = VendaCelularDependenteActivity.class;
//                    } else {
//                        cls = VendaCelularActivity.class;
//                    }
//                } else {
//                    //Caso bandeira seja vivo
//                    boolean isBandeiraVivo = false;
//                    for (BandeiraSistema bandeira : multiSales.getBandeiras()) {
//                        if (bandeira.getDescricao().contains("VIVO")) {
//                            isBandeiraVivo = true;
//                            break;
//                        }
//                    }
//                    if (isBandeiraVivo) {
//                        cls = VendaPacoteActivity.class;
//                    } else {
//                        cls = VendaPacoteNaoVendaActivity.class;
//                    }
//                }
                activityAnterior.setComponent(new ComponentName(this, cls));
                startActivity(activityAnterior);
                VendaFoneActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (id == R.id.action_avancar) {
            avancar();
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaFoneActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void avancar() {
        try {

            vendaFone = entityManager.save(popularDadosVendaFone());

            if (venda.getVendaFone() != null
                    && venda.getVendaFone().getId() != null) {

                entityManager.executeNativeQuery(
                        "DELETE FROM venda_fone_linha " +
                                "WHERE _venda_fone = "
                                + venda.getVendaFone().getId());

                entityManager.executeNativeQuery("DELETE FROM venda_fone WHERE id = "
                        + venda.getVendaFone().getId());

                if (checkBoxAdicionarNumeroPortado.isChecked()) {
                    entityManager.executeNativeQuery(
                            "UPDATE venda_fone_portabilidade SET _venda_fone = "
                            + vendaFone.getId() +
                            " WHERE _venda_fone = "
                            + venda.getVendaFone().getId());

                } else {
                    entityManager.executeNativeQuery(
                            "DELETE FROM venda_fone_portabilidade WHERE _venda_fone = "
                            + venda.getVendaFone().getId());
                }
            }

            for (VendaFoneLinha vfl : vendasFoneLinhas) {
                vfl.setNullId();
                vfl.setVendaFone(vendaFone);
                entityManager.save(vfl);
            }

            venda.setVendaFone(vendaFone);
            entityManager.atualizar(venda);
            irParaProximaTela();

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar linha(s): " + e.getMessage(), this);
        }
    }

    private VendaFone popularDadosVendaFone() {
        VendaFone vendaFone = new VendaFone();
        vendaFone.setObservacao(txtObservacao.getText().toString());
        vendaFone.setLinhaTronco(UtilMask.unmask(txtLinhaTronco.getText().toString()));

        if (txtTaxaHabilitacao.getText() != null && !txtTaxaHabilitacao.getText().toString().isEmpty()) {
            vendaFone.setTaxaHabilitacao(new BigDecimal(UtilMask.unmaskMoeda(txtTaxaHabilitacao.getText().toString())));
        }

        if (txtValorPlano.getText() != null && !txtValorPlano.getText().toString().isEmpty()) {
            vendaFone.setValorPlano(new BigDecimal(UtilMask.unmaskMoeda(txtValorPlano.getText().toString())));
        }

        if (UtilActivity.isValidSpinnerValue(spinnerTecnologiaUtilizada)) {
            vendaFone.setTecnologiaDisponivel(tecnologiasDisponiveis
                    .get(spinnerTecnologiaUtilizada.getSelectedItemPosition() - SistemaConstantes.UM));
        }
        return vendaFone;
    }

    private void irParaProximaTela() throws Exception {
        Intent proximaTela = new Intent();
        proximaTela.putExtra("idVenda", idVenda);
        proximaTela.putExtra("produtosTipo", produtosTiposSelecionados);
        proximaTela.putExtra("edicao", edicao);
        proximaTela.putExtra("isMock", isMock);
        telasAnteriores.add(EVendaFluxo.FONE);
        proximaTela.putExtra("telasAnteriores", telasAnteriores);
        Class<?> cls = null;

        if (checkBoxAdicionarNumeroPortado.isChecked()) {
            cls = VendaFonePortabilidadeActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.INTERNET)) {
            cls = VendaInternetActivity.class;
        } else {
            cls = VendaFormaPagamentoActivity.class;
        }
        proximaTela.setComponent(new ComponentName(this, cls));
        startActivity(proximaTela);
        VendaFoneActivity.this.finish();
    }
}
