package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaCelularDependente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFone;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFoneLinha;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFonePortabilidade;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

/**
 * Created by eric on 16-03-2015.
 */
public class VendaFoneFragment extends Fragment {

    private RelativeLayout relativeLayout;

    private LinearLayout linhasLayout;
    private LinearLayout numerosPortadosLL;

    private EntityManager entityManager;

    private TextView txtxperfilComboTV;

    private VendaFone vendaFone;
    private VendaFonePortabilidade vendaFonePortabilidades;
    private VendaFoneLinha vendaFoneLinha;
    private TextView txtPortadosTV;
    private RelativeLayout rlNumerosPortadosRL;
    private RelativeLayout rlLinhasRL;
    private TextView taxaHabilitacaoTV;
    private TextView valorPlanoTV;
    private TextView tecnologiaDisponivelTV;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        entityManager = new EntityManager(getActivity());
        this.vendaFone = ((VendaDetalhesActivity) getActivity()).getVenda().getVendaFone();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }


        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_venda_fone, container, false);

        linhasLayout = (LinearLayout) relativeLayout.findViewById(R.id.linhasLL);

        numerosPortadosLL = (LinearLayout) relativeLayout.findViewById(R.id.numerosPortadosLL);

        criarCamposEntrada(inflater,container,savedInstanceState);

        return relativeLayout;
    }

    public void criarCamposEntrada(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState){

        if(vendaFone.getVendaFoneLinhas().size() > 0) {

            for (int i = 0; i < vendaFone.getVendaFoneLinhas().size(); i++) {

                vendaFoneLinha = vendaFone.getVendaFoneLinhas().get(i);

                try {
                    entityManager.initialize(vendaFoneLinha.getProduto());
                    entityManager.initialize(vendaFoneLinha.getVendaFone());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                RelativeLayout linhaLayout = (RelativeLayout) inflater.inflate(R.layout.item_venda_fone_linha_vivo, container, false);

                TextView txtProduto = (TextView) linhaLayout.findViewById(R.id.txtProduto);
                if (vendaFoneLinha.getProduto() != null) {
                    txtProduto.setText(vendaFoneLinha.getProduto().getDescricao());
                } else {
                    txtProduto.setText("N/D");
                }

                TextView txtQtdExtensoes = (TextView) linhaLayout.findViewById(R.id.txtQtdExtensoes);
                if(vendaFoneLinha.getExtensoes() != null
                        && vendaFoneLinha.getExtensoes() > 0){
                    txtQtdExtensoes.setText(vendaFoneLinha.getExtensoes().toString());
                } else {
                    txtQtdExtensoes.setText("0");
                }

                TextView txtPublicarNumero = (TextView) linhaLayout.findViewById(R.id.txtPublicarNumero);
                if (vendaFoneLinha.getPublicarNumero() != null) {
                    if (vendaFoneLinha.getPublicarNumero().toString() == "FALSE") {
                        txtPublicarNumero.setText("NAO");
                    } else {
                        txtPublicarNumero.setText("SIM");
                    }
                } else {
                    txtPublicarNumero.setText("N/D");
                }

                TextView txtMensalidade = (TextView) linhaLayout.findViewById(R.id.mensalidadeTV);
                if(vendaFoneLinha.getMensalidade() != null ){
                    txtMensalidade.setText(UtilActivity.formatarMoeda(vendaFoneLinha.getMensalidade().doubleValue()));
                } else {
                    txtMensalidade.setText("N/D");
                }


                TextView txtExcedidoFixo = (TextView) linhaLayout.findViewById(R.id.minutoExcedidoTV);
                if(vendaFoneLinha.getMinutoExcedidoParaFixo() != null) {
                    txtExcedidoFixo.setText(UtilActivity.formatarMoeda(vendaFoneLinha.getMinutoExcedidoParaFixo().doubleValue()));
                } else {
                    txtExcedidoFixo.setText("N/D");
                }

                TextView txtHabilitacao = (TextView) linhaLayout.findViewById(R.id.habilitacaoTV);
                if(vendaFoneLinha.getHabilitacao() != null ){
                    txtHabilitacao.setText(UtilActivity.formatarMoeda(vendaFoneLinha.getHabilitacao().doubleValue()));
                } else {
                    txtHabilitacao.setText("N/D");
                }


                linhasLayout.addView(linhaLayout);

                if (i != vendaFone.getVendaFoneLinhas().size() - 1) {
                    LinearLayout separador = (LinearLayout) inflater.inflate(R.layout.separador, container, false);
                    linhasLayout.addView(separador);
                }
            }

        } else {
           rlLinhasRL = (RelativeLayout) relativeLayout.findViewById(R.id.linhasRL);
           rlLinhasRL.setVisibility(View.GONE);
        }


        txtPortadosTV = (TextView) relativeLayout.findViewById(R.id.portadosTV);
        if(vendaFone.getVendaFonePortabilidades().size() > 0) {
            txtPortadosTV.setText("SIM");
        } else {
            txtPortadosTV.setText("NÃO");
            rlNumerosPortadosRL = (RelativeLayout) relativeLayout.findViewById(R.id.numerosPortadosRL);
            rlNumerosPortadosRL.setVisibility(View.GONE);
        }

        taxaHabilitacaoTV = (TextView) relativeLayout.findViewById(R.id.taxaHabilitacaoTV);
        if (vendaFone.getTaxaHabilitacao() != null ){
            taxaHabilitacaoTV.setText(UtilActivity.formatarMoeda(vendaFone.getTaxaHabilitacao().doubleValue()));
        } else {
            taxaHabilitacaoTV.setText("N/D");
        }

        valorPlanoTV = (TextView) relativeLayout.findViewById(R.id.valorPlanoTV);
        if (vendaFone.getValorPlano() != null) {
            valorPlanoTV.setText(UtilActivity.formatarMoeda(vendaFone.getValorPlano().doubleValue()));
        } else {
            valorPlanoTV.setText("N/D");
        }

        tecnologiaDisponivelTV = (TextView) relativeLayout.findViewById(R.id.tecnologiaDisponivelTV);
        if (vendaFone.getTecnologiaDisponivel() != null ){
            tecnologiaDisponivelTV.setText(vendaFone.getTecnologiaDisponivel().getDescricao());
        } else {
            tecnologiaDisponivelTV.setText("N/D");
        }

        for (int i = 0; i < vendaFone.getVendaFonePortabilidades().size(); i++) {
            vendaFonePortabilidades = vendaFone.getVendaFonePortabilidades().get(i);

            try{
                entityManager.initialize(vendaFonePortabilidades.getVendaFone());
                entityManager.initialize(vendaFonePortabilidades.getOperadoraPortabilidade());
            } catch (Exception e ) {
                e.printStackTrace();
            }

            RelativeLayout linhaLayout = (RelativeLayout) inflater.inflate(R.layout.item_venda_fone_portabilidade, container, false);

            TextView txtOperadora = (TextView) linhaLayout.findViewById(R.id.txtOperadora);
            if(vendaFonePortabilidades.getOperadoraPortabilidade() != null ){
                txtOperadora.setText(vendaFonePortabilidades.getOperadoraPortabilidade().getDescricao());
            }

            TextView txtTelefone = (TextView) linhaLayout.findViewById(R.id.txtTelefone);
            if (vendaFonePortabilidades.getTelefonePortado() != null) {
                txtTelefone.setText(UtilActivity.formatarTelefone(vendaFonePortabilidades.getTelefonePortado()));
            } else {
                txtTelefone.setText("N/D");
            }


            numerosPortadosLL.addView(linhaLayout);

            if (i != vendaFone.getVendaFonePortabilidades().size()-1) {
                LinearLayout separador = (LinearLayout) inflater.inflate(R.layout.separador, container, false);
                numerosPortadosLL.addView(separador);
            }
        }
    }

}

