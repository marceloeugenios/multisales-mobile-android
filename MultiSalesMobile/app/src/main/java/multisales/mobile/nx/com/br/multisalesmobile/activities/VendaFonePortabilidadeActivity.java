package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.OperadoraTelefonia;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFone;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFonePortabilidade;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaFonePortabilidadeActivity extends ActionBarActivity {

    private EntityManager entityManager;

    private Integer idVenda;
    private VendaFone vendaFone;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private HashSet<EVendaFluxo> telasAnteriores;

    private List<OperadoraTelefonia> operadoras;
    private List<VendaFonePortabilidade> portabilidades;

    //Componentes
    private Spinner spinnerOperadoraPortabilidade;
    private EditText txtNumero;
    private ListView listViewPortabilidades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_fone_portabilidade);
        criarCamposEntrada();
        entityManager = new EntityManager(this);
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>) getIntent()
                .getExtras().get("produtosTipo");
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);

        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.FONE_PORTABILIDADE);

        carregarOperadoras();
        portabilidades = new ArrayList<>();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            Venda venda = entityManager.getById(Venda.class, idVenda);
            vendaFone = entityManager.getById(VendaFone.class, venda.getVendaFone().getId());

            if (vendaFone.getId() != null) {
                portabilidades = entityManager.select(
                        VendaFonePortabilidade.class,
                        "SELECT * FROM venda_fone_portabilidade WHERE _venda_fone = "
                                + vendaFone.getId());

                if (portabilidades != null && !portabilidades.isEmpty() && !edicao) {
                    isVoltar = true;
                }
            }

            if (edicao || isVoltar) {
                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }
    }

    private void popularValoresCamposEntrada() throws DataBaseException {
        listarPortabilidadesAdicionadas();
    }

    private void popularMock() {
        VendaFonePortabilidade vfp = new VendaFonePortabilidade();
        vfp.setTelefonePortado("4355555555");
        vfp.setOperadoraPortabilidade(operadoras.get(SistemaConstantes.ZERO));
        vfp.setVendaFone(vendaFone);
        portabilidades.add(vfp);
    }

    private void carregarOperadoras() {
        try {
            operadoras = entityManager.select(OperadoraTelefonia.class,
                    "SELECT o.* " +
                    "FROM operadora_telefonia o " +
                    "WHERE o.situacao = 'ATIVO' " +
                    "ORDER BY o.descricao ");

            List<String> descricoesOperadoras = new ArrayList<>();
            descricoesOperadoras.add(UtilActivity.SELECIONE);
            for (OperadoraTelefonia operadora : operadoras) {
                descricoesOperadoras.add(operadora.getDescricao());
            }
            UtilActivity.setAdapter(this, spinnerOperadoraPortabilidade, descricoesOperadoras);
         } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar Operadoras Telefonia: " + e.getMessage(), this);
        }
    }

    private void listarPortabilidadesAdicionadas() throws DataBaseException {
        List<Map<String, Object>> itens = new ArrayList<Map<String, Object>>();

        if (portabilidades != null) {

            for (VendaFonePortabilidade vfp : portabilidades) {
                Map<String, Object> item = new HashMap<>();
                entityManager.initialize(vfp.getOperadoraPortabilidade());
                item.put("operadora", vfp.getOperadoraPortabilidade().getDescricao());
                item.put("telefone", UtilActivity.formatarTelefone(vfp.getTelefonePortado()));
                itens.add(item);
            }
        }

        String[] de = {"operadora", "telefone"};
        int[] para = {R.id.txtOperadora,R.id.txtTelefone};
        SimpleAdapter adapter = new SimpleAdapter(this, itens, R.layout.item_venda_fone_portabilidade, de, para);
        listViewPortabilidades.setAdapter(adapter);

        UtilActivity.setListViewHeightBasedOnItems(listViewPortabilidades);
    }

    private void criarCamposEntrada() {
        spinnerOperadoraPortabilidade = (Spinner) findViewById(R.id.spinnerOperadoraPortabilidade);
        txtNumero                 = (EditText) findViewById(R.id.txtNumero);
        UtilMask.setMascaraTelefone(txtNumero);
        listViewPortabilidades    = (ListView) findViewById(R.id.listViewPortabilidades);
        listViewPortabilidades.setLongClickable(true);
        listViewPortabilidades.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    portabilidades.remove(position);
                    listarPortabilidadesAdicionadas();
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
    }

    private void avancar() {
        try {
            entityManager.executeNativeQuery(
                    "DELETE FROM venda_fone_portabilidade WHERE _venda_fone = " + vendaFone.getId());

            for (VendaFonePortabilidade portabilidade : portabilidades) {
                portabilidade.setNullId();
                entityManager.save(portabilidade);
            }
            irParaProximaTela();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar portabilidades: " + e.getMessage(), this);
        }
    }

    private void irParaProximaTela() {
        Intent proximaTela = new Intent();
        proximaTela.putExtra("idVenda", idVenda);
        proximaTela.putExtra("produtosTipo", produtosTiposSelecionados);
        proximaTela.putExtra("edicao", edicao);
        proximaTela.putExtra("isMock", isMock);

        telasAnteriores.add(EVendaFluxo.FONE_PORTABILIDADE);
        proximaTela.putExtra("telasAnteriores", telasAnteriores);

        Class<?> cls = null;

        if (produtosTiposSelecionados.contains(EProdutoTipo.INTERNET)) {
            cls = VendaInternetActivity.class;
        } else {
            cls = VendaFormaPagamentoActivity.class;
        }

        proximaTela.setComponent(new ComponentName(this, cls));
        startActivity(proximaTela);
        VendaFonePortabilidadeActivity.this.finish();
    }

    public void adicionarPortabilidade(View view) {
        try {
            if (!UtilActivity.isValidSpinnerValue(spinnerOperadoraPortabilidade)) {
                UtilActivity.makeShortToast("Selecione a operadora.", this);
                return;
            }

            if (txtNumero.getText() == null || txtNumero.getText().toString().isEmpty()) {
                UtilActivity.makeShortToast("Informe o número a ser portado.", this);
                return;
            }

            VendaFonePortabilidade vfp = new VendaFonePortabilidade();
            vfp.setOperadoraPortabilidade(operadoras
                    .get(spinnerOperadoraPortabilidade.getSelectedItemPosition() - SistemaConstantes.UM));
            vfp.setTelefonePortado(UtilMask.unmask(txtNumero.getText().toString()));
            vfp.setVendaFone(vendaFone);

            portabilidades.add(vfp);
            listarPortabilidadesAdicionadas();

            spinnerOperadoraPortabilidade.setSelection(SistemaConstantes.ZERO);
            txtNumero.setText("");
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao adicionar portabilidade: " + e.getMessage(), this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_fone_portabilidade, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            Intent activityAnterior = new Intent();
            activityAnterior.putExtra("idVenda", idVenda);
            activityAnterior.putExtra("edicao", edicao);
            activityAnterior.putExtra("isMock", isMock);
            activityAnterior.putExtra("isVoltar", true);
            activityAnterior.putExtra("telasAnteriores", telasAnteriores);
            activityAnterior.putExtra("produtosTipo", produtosTiposSelecionados);
            Class<?> cls = VendaFoneActivity.class;
            activityAnterior.setComponent(new ComponentName(this, cls));
            startActivity(activityAnterior);
            VendaFonePortabilidadeActivity.this.finish();
        }

        if (id == R.id.action_avancar) {
            avancar();
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaFonePortabilidadeActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }
}
