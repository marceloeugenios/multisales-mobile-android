package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Banco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogDiario;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Midia;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.PeriodoInstalacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VencimentoFatura;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFormaPagamento;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFormaPagamentoBanco;
import multisales.mobile.nx.com.br.multisalesmobile.servico.LogDiarioServico;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class VendaFormaPagamentoActivity extends ActionBarActivity implements View.OnFocusChangeListener, View.OnClickListener {

    private MultiSales multiSales;

    private EntityManager entityManager;
    private Venda venda;
    private VendaFormaPagamento vendaFormaPagamento;

    //Extras
    private Integer idVenda;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private HashSet<EVendaFluxo> telasAnteriores;

    private List<VencimentoFatura> vencimentos;
    private List<Banco> bancos;
    private List<Midia> midias;
    private List<PeriodoInstalacao> periodos;
    private List<String> formasPagamento;

    //Componentes
    private Spinner spinnerVencimentoFatura;
    private Spinner spinnerFormaPagamento;
    private Spinner spinnerBanco;

    private EditText txtAgencia;
    private EditText txtConta;
    private EditText dataInstalacaoET;
    private EditText txtNomeTitular;
    private CheckBox checkBoxReceberFaturaSomentePorEmail;
    private EditText txtEmailFatura;

    private Spinner spinnerPeriodoInstalacao;
    private Spinner spinnerMidia;
    private CheckBox checkBoxCPFNota;

    private CheckBox checkBoxInformouSobreMuta;
    private CheckBox checkBoxAceitaReceberCampanhaPublicitaria;
    private DatePickerDialog dataDPD;
    private Calendar dataInstalacao;
    private SimpleDateFormat formatoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_forma_pagamento);
        entityManager = new EntityManager(this);
        multiSales = (MultiSales) getApplication();
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>) getIntent().getExtras().get("produtosTipo");
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);

        isVoltar = getIntent().getBooleanExtra("isVoltar", false);

        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.PAGAMENTO);

        dataInstalacao = Calendar.getInstance();
        formatoData = new SimpleDateFormat("dd/MM/yyyy");
        criarCamposEntrada();
        carregarVencimentos();
        carregarMidias();
        carregarPeriodosInstalacao();
        setDateTimeField();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            venda = entityManager.getById(Venda.class, idVenda);
            entityManager.initialize(venda.getCliente());
            txtEmailFatura.setText(venda.getCliente().getEmail());

            if (venda.getVendaFormaPagamento() != null
                    && venda.getVendaFormaPagamento().getId() != null
                    && !edicao) {
                isVoltar = true;
            }

            if (venda.getVendaFormaPagamento() != null
                    && venda.getVendaFormaPagamento().getId() != null
                    && (edicao || isVoltar)) {

                vendaFormaPagamento = entityManager.getById(
                        VendaFormaPagamento.class,
                        venda.getVendaFormaPagamento().getId());
                entityManager.initialize(vendaFormaPagamento.getVendaFormaPagamentoBanco());
                entityManager.initialize(venda.getCliente());

                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }
    }

    private void popularMock() {
        vendaFormaPagamento = new VendaFormaPagamento();
        VendaFormaPagamentoBanco vfpb = new VendaFormaPagamentoBanco();
        vfpb.setBanco(bancos.get(SistemaConstantes.ZERO));
        vfpb.setAgencia("12345");
        vfpb.setConta("123456789");
        vfpb.setNomeTitular("NOME TITULAR");

        vendaFormaPagamento.setVendaFormaPagamentoBanco(vfpb);
        vendaFormaPagamento.setVencimentoFatura(vencimentos.get(SistemaConstantes.ZERO));
        venda.setMidia(midias.get(SistemaConstantes.ZERO));
    }

    private void popularValoresCamposEntrada() {
        if (vendaFormaPagamento.getVencimentoFatura() != null) {
            spinnerVencimentoFatura.setSelection(
                    vencimentos.indexOf(vendaFormaPagamento.getVencimentoFatura()) + SistemaConstantes.UM);
        }

        if (vendaFormaPagamento.getVendaFormaPagamentoBanco() != null) {
            spinnerFormaPagamento.setSelection(formasPagamento
                    .indexOf(EFormaPagamento.DEBITO_CC.getDescricao()));

            if (vendaFormaPagamento.getVendaFormaPagamentoBanco().getBanco() != null) {
                for (Banco banco : bancos) {
                    if (banco.getId()
                            .equals(vendaFormaPagamento.getVendaFormaPagamentoBanco().getBanco().getId())) {
                        spinnerBanco.setSelection(bancos.indexOf(banco) + SistemaConstantes.UM);
                        break;
                    }
                }
            }
            txtAgencia.setText(vendaFormaPagamento.getVendaFormaPagamentoBanco().getAgencia());
            txtConta.setText(vendaFormaPagamento.getVendaFormaPagamentoBanco().getConta());
            txtNomeTitular.setText(vendaFormaPagamento.getVendaFormaPagamentoBanco().getNomeTitular());
        } else {
            spinnerFormaPagamento.setSelection(formasPagamento
                    .indexOf(EFormaPagamento.BOLETO.getDescricao()));
        }

        txtEmailFatura.setText(venda.getCliente().getEmail());
        checkBoxAceitaReceberCampanhaPublicitaria.setChecked(
                vendaFormaPagamento.getReceberCampanhaPublicitaria() == EBoolean.TRUE);
        checkBoxCPFNota.setChecked(
                vendaFormaPagamento.getCpfNota() == EBoolean.TRUE);
        checkBoxInformouSobreMuta.setChecked
                (vendaFormaPagamento.getInformouSobreMulta() == EBoolean.TRUE);
        checkBoxReceberFaturaSomentePorEmail.setChecked(
                vendaFormaPagamento.getFaturaSomentePorEmail() == EBoolean.TRUE);

        if (venda.getDataInstalacao() != null) {
            dataInstalacao = venda.getDataInstalacao();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            dataInstalacaoET.setText(sdf.format(dataInstalacao.getTime()));
        }

        if (venda.getPeriodoInstalacao() != null) {
            for (PeriodoInstalacao periodoInstalacao : periodos) {
                if (periodoInstalacao.getId().equals(venda.getPeriodoInstalacao().getId())) {
                    spinnerPeriodoInstalacao.setSelection(
                            periodos.indexOf(periodoInstalacao) + SistemaConstantes.UM);
                    break;
                }
            }
        }

        if (venda.getMidia() != null) {
            for (Midia midia : midias) {
                if (midia.getId().equals(venda.getMidia().getId())) {
                    spinnerMidia.setSelection(midias.indexOf(midia) + SistemaConstantes.UM);
                    break;
                }
            }
        }
    }

    private void carregarMidias() {
        try {
            midias = entityManager.getByWhere(Midia.class, "situacao = 'ATIVO'", "descricao");

            List<String> descricaoMidias = new ArrayList<>();
            descricaoMidias.add(UtilActivity.SELECIONE);
            for (Midia midia : midias) {
                descricaoMidias.add(midia.getDescricao());
            }
            UtilActivity.setAdapter(this, spinnerMidia, descricaoMidias);
        } catch (Exception e) {
            UtilActivity.makeShortToast("ERRO ao listar midias: " + e.getMessage(), this);
        }
    }

    private void carregarPeriodosInstalacao() {
        try {
            periodos = entityManager.getByWhere(PeriodoInstalacao.class, "situacao = 'ATIVO'", "descricao");

            List<String> descricaoPeriodos = new ArrayList<>();
            descricaoPeriodos.add(UtilActivity.SELECIONE);
            for (PeriodoInstalacao periodoInstalacao : periodos) {
                descricaoPeriodos.add(periodoInstalacao.getDescricao());
            }
            UtilActivity.setAdapter(this, spinnerPeriodoInstalacao, descricaoPeriodos);
        } catch (Exception e) {
            UtilActivity.makeShortToast("ERRO ao listar periodos instalacao: " + e.getMessage(), this);
        }
    }

    private void carregarVencimentos() {
        try {
            vencimentos = entityManager.getByWhere(VencimentoFatura.class, "situacao = 'ATIVO'", "vencimento");
            bancos = entityManager.getByWhere(Banco.class, "situacao = 'ATIVO'", "descricao");

            List<String> numerosVencimentos = new ArrayList<>();
            numerosVencimentos.add(UtilActivity.SELECIONE);
            for (VencimentoFatura bs : vencimentos) {
                numerosVencimentos.add(bs.getVencimento().toString());
            }
            UtilActivity.setAdapter(this, spinnerVencimentoFatura, numerosVencimentos);

            List<String> descricoesBancos = new ArrayList<>();
            descricoesBancos.add(UtilActivity.SELECIONE);
            for (Banco bs : bancos) {
                descricoesBancos.add(bs.getDescricao().toString());
            }
            UtilActivity.setAdapter(this, spinnerBanco, descricoesBancos);

            formasPagamento = Arrays.asList(UtilActivity.SELECIONE, EFormaPagamento.BOLETO.getDescricao(), EFormaPagamento.DEBITO_CC.getDescricao());
            UtilActivity.setAdapter(this, spinnerFormaPagamento, formasPagamento);
            spinnerFormaPagamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (position > SistemaConstantes.ZERO) {
                        if (isVendaComDebitoContaCorrente()) {
                            mudarExibicaoPagamentoComContaCorrente(View.VISIBLE);
                        } else {
                            mudarExibicaoPagamentoComContaCorrente(View.GONE);
                        }
                    }
                    findViewById(R.id.vendaFormaPagamentoLayout).invalidate();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            UtilActivity.makeShortToast("ERRO ao listar bandeiras: " + e.getMessage(), this);
        }
    }

    private void mudarExibicaoPagamentoComContaCorrente(int visible) {
        txtAgencia.setVisibility(visible);
        spinnerBanco.setVisibility(visible);
        txtConta.setVisibility(visible);
        txtNomeTitular.setVisibility(visible);
        findViewById(R.id.txtViewAgencia).setVisibility(visible);
        findViewById(R.id.txtViewBanco).setVisibility(visible);
        findViewById(R.id.txtViewConta).setVisibility(visible);
        findViewById(R.id.txtViewNomeTitular).setVisibility(visible);
    }

    private void criarCamposEntrada() {
        spinnerVencimentoFatura              = (Spinner) findViewById(R.id.spinnerVencimentoFatura);
        spinnerFormaPagamento                = (Spinner) findViewById(R.id.spinnerFormaPagamento);
        spinnerBanco                         = (Spinner) findViewById(R.id.spinnerBanco);
        txtAgencia                           = (EditText) findViewById(R.id.txtAgencia);
        txtConta                             = (EditText) findViewById(R.id.txtConta);
        txtNomeTitular                       = (EditText) findViewById(R.id.txtNomeTitular);
        checkBoxReceberFaturaSomentePorEmail = (CheckBox) findViewById(R.id.checkBoxReceberFaturaSomentePorEmail);
        checkBoxInformouSobreMuta            = (CheckBox) findViewById(R.id.checkBoxInformouSobreMulta);
        checkBoxAceitaReceberCampanhaPublicitaria   = (CheckBox) findViewById(R.id.checkBoxAutorizaReceberCampanhaPublicitaria);
        txtEmailFatura                              = (EditText) findViewById(R.id.txtEmailFatura);
        checkBoxCPFNota                             = (CheckBox) findViewById(R.id.checkBoxCPFNota);
        spinnerPeriodoInstalacao                    = (Spinner) findViewById(R.id.spinnerPeriodoInstalacao);
        spinnerMidia                                = (Spinner) findViewById(R.id.spinnerMidia);
        dataInstalacaoET                            = (EditText) findViewById(R.id.dataInstalacaoET);
        mudarExibicaoPagamentoComContaCorrente(View.GONE);
    }

    private void setDateTimeField() {
        dataInstalacaoET.setOnFocusChangeListener(this);
        dataInstalacaoET.setOnClickListener(this);

        final Calendar dataHoraAtual = Calendar.getInstance();
        dataDPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dataInstalacao.set(year, monthOfYear, dayOfMonth);
                dataInstalacaoET.setText(formatoData.format(dataInstalacao.getTime()));
            }

        },dataHoraAtual.get(Calendar.YEAR), dataHoraAtual.get(Calendar.MONTH), dataHoraAtual.get(Calendar.DAY_OF_MONTH));
    }

    private boolean validarCamposObrigatorios() {
        if (!UtilActivity.isValidSpinnerValue(spinnerVencimentoFatura)) {
            UtilActivity.makeShortToast("Informe o vencimento da fatura.", this);
            return false;
        }
        if (!UtilActivity.isValidSpinnerValue(spinnerFormaPagamento)) {
            UtilActivity.makeShortToast("Informe a forma de pagamento.", this);
            return false;
        }
        if (spinnerBanco.isShown()) {
            if (!UtilActivity.isValidSpinnerValue(spinnerBanco)) {
                UtilActivity.makeShortToast("Selecione o banco.", this);
                return false;
            }
            if (txtAgencia.getText().length() < 4) {
                UtilActivity.makeShortToast("Informe a agencia.", this);
                return false;
            }
            if (txtConta.getText().length() < 4) {
                UtilActivity.makeShortToast("Informe a conta.", this);
                return false;
            }
            if (txtNomeTitular.getText().length() < 4) {
                UtilActivity.makeShortToast("Informe o nome do titular.", this);
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_forma_pagamento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            Intent activityAnterior = new Intent();
            activityAnterior.putExtra("idVenda", idVenda);
            activityAnterior.putExtra("edicao", edicao);
            activityAnterior.putExtra("isMock", isMock);
            activityAnterior.putExtra("isVoltar", true);
            activityAnterior.putExtra("telasAnteriores", telasAnteriores);
            activityAnterior.putExtra("produtosTipo", produtosTiposSelecionados);
            Class<?> cls = null;

            if (telasAnteriores.contains(EVendaFluxo.INTERNET)) {
                cls = VendaInternetActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.FONE_PORTABILIDADE)) {
                cls = VendaFonePortabilidadeActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.FONE)) {
                cls = VendaFoneActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.TV_ADICIONAL)) {
                cls = VendaTvAdicionalActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.TV)) {
                cls = VendaTvActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.CELULAR_DEPENDENTE)) {
                cls = VendaCelularDependenteActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.CELULAR)) {
                cls = VendaCelularActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.MOTIVOS_NAO_VENDA)) {
                cls = VendaPacoteNaoVendaActivity.class;
            } else {
                cls = VendaPacoteActivity.class;
            }

            activityAnterior.setComponent(new ComponentName(this, cls));
            startActivity(activityAnterior);
            VendaFormaPagamentoActivity.this.finish();
        }

        if (id == R.id.action_avancar) {
            avancar();
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaFormaPagamentoActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void avancar() {
        try {
            if (!validarCamposObrigatorios()) {
                return;
            }

            if (venda.getVendaFormaPagamento() != null
                    && venda.getVendaFormaPagamento().getId() != null) {
                entityManager.executeNativeQuery("DELETE FROM venda_forma_pagamento WHERE id = "
                        + venda.getVendaFormaPagamento().getId());

                if (venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco() != null
                        && venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco() != null) {
                    entityManager.executeNativeQuery("DELETE FROM venda_forma_pagamento_banco WHERE id = "
                            + venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco().getId());
                }
            }

            vendaFormaPagamento = popularDadosVendaFormaPagamento();

            if (vendaFormaPagamento.getVendaFormaPagamentoBanco() != null) {
                vendaFormaPagamento.setVendaFormaPagamentoBanco(
                        entityManager.save(vendaFormaPagamento.getVendaFormaPagamentoBanco()));
            }

            venda.setVendaFormaPagamento(entityManager.save(vendaFormaPagamento));

            if (UtilActivity.isValidSpinnerValue(spinnerPeriodoInstalacao)) {
                venda.setPeriodoInstalacao(periodos.get(spinnerPeriodoInstalacao.getSelectedItemPosition() - SistemaConstantes.UM));
            }
            if (UtilActivity.isValidSpinnerValue(spinnerMidia)) {
                venda.setMidia(midias.get(spinnerMidia.getSelectedItemPosition() - SistemaConstantes.UM));
            }
            if (!dataInstalacaoET.getText().toString().replaceAll("_", "").replaceAll("/", "").equals("")) {
                venda.setDataInstalacao(dataInstalacao);
            }

            venda.getCliente().setEmail(txtEmailFatura.getText().toString());
            entityManager.atualizar(venda.getCliente());

            entityManager.atualizar(venda);

            Tabulacao tabulacao = entityManager.getById(Tabulacao.class, idVenda);
            tabulacao.setCompleta(EBoolean.TRUE);
            entityManager.atualizar(tabulacao);

            if (!edicao && !isVoltar) {
                LogDiarioServico logDiarioServico = new LogDiarioServico(this);
                LogDiario logDiario = logDiarioServico.obterLogDiarioAtualPorUsuario(multiSales.getLogin());
                logDiario.setVendas(logDiario.getVendas() + SistemaConstantes.UM);
                entityManager.atualizar(logDiario);
            }
            irParaProximaTela();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar forma pagamento(s): " + e.getMessage(), this);
        }
    }

    private VendaFormaPagamento popularDadosVendaFormaPagamento() {
        VendaFormaPagamento vendaFormaPagamento = new VendaFormaPagamento();
        vendaFormaPagamento.setVencimentoFatura(vencimentos
                .get(spinnerVencimentoFatura.getSelectedItemPosition() - SistemaConstantes.UM));
        if (isVendaComDebitoContaCorrente()) {
            VendaFormaPagamentoBanco formaPagamentoBanco = new VendaFormaPagamentoBanco();
            formaPagamentoBanco.setAgencia(txtAgencia.getText().toString());
            formaPagamentoBanco.setConta(txtConta.getText().toString());
            formaPagamentoBanco.setNomeTitular(txtNomeTitular.getText().toString());
            if (UtilActivity.isValidSpinnerValue(spinnerBanco)) {
                formaPagamentoBanco.setBanco(bancos
                        .get(spinnerBanco.getSelectedItemPosition() - SistemaConstantes.UM));
            }
            vendaFormaPagamento.setVendaFormaPagamentoBanco(formaPagamentoBanco);
        }
        vendaFormaPagamento.setCpfNota(checkBoxCPFNota.isChecked() ? EBoolean.TRUE : EBoolean.FALSE);
        vendaFormaPagamento.setInformouSobreMulta(checkBoxInformouSobreMuta.isChecked() ? EBoolean.TRUE : EBoolean.FALSE);
        vendaFormaPagamento.setReceberCampanhaPublicitaria(checkBoxAceitaReceberCampanhaPublicitaria.isChecked() ? EBoolean.TRUE : EBoolean.FALSE);
        vendaFormaPagamento.setFaturaSomentePorEmail(checkBoxReceberFaturaSomentePorEmail.isChecked() ? EBoolean.TRUE : EBoolean.FALSE);

        return vendaFormaPagamento;
    }

    private void irParaProximaTela() throws Exception {
        Intent proximaTela = new Intent();
        proximaTela.putExtra("idVenda", idVenda);
        proximaTela.putExtra("isMock", isMock);
        proximaTela.putExtra("edicao", edicao);
        proximaTela.putExtra("produtosTipo", produtosTiposSelecionados);
        telasAnteriores.add(EVendaFluxo.PAGAMENTO);
        proximaTela.putExtra("telasAnteriores", telasAnteriores);
        Class<?> cls = null;

        if (edicao) {
            cls = MainActivity.class;
            UtilActivity.makeShortToast("Venda atualizada com sucesso!", this);
        } else {
            cls = VendaFinalizarActivity.class;
        }
        proximaTela.setComponent(new ComponentName(this, cls));
        startActivity(proximaTela);
    }

    private boolean isVendaComDebitoContaCorrente() {
        String formaPagamentoSelecionado = formasPagamento.get(spinnerFormaPagamento.getSelectedItemPosition());
        if (formaPagamentoSelecionado.equals(EFormaPagamento.DEBITO_CC.getDescricao())) {
            return true;
        }
        return false;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(hasFocus) {
            showPicker(view);
        }
    }

    @Override
    public void onClick(View view) {
        showPicker(view);
    }

    private void showPicker(View view) {
        if(view == dataInstalacaoET) {
            dataDPD.show();
        }
    }
}