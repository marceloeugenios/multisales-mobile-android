package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Concorrente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoMigracao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaInternet;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaInternetProdutoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaInternetActivity extends ActionBarActivity  implements View.OnFocusChangeListener, View.OnClickListener {

    private EntityManager entityManager;
    private Venda venda;
    private VendaInternet vendaInternet;

    private Integer idVenda;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private HashSet<EVendaFluxo> telasAnteriores;

    private List<Concorrente> concorrentes;
    private List<MotivoMigracao> motivosMigracao;
    private String[] descricoesProdutosAdicionais;
    private List<Integer> indexProdutosAdicionaisSelecionados;
    private List<Produto> planosInternet;
    private List<Produto> produtosAdicionais;

    //Componentes
    private Spinner spinnerPlanoInternet;
    private EditText txtObservacao;
    private Spinner spinnerConcorrentes;
    private Spinner spinnerMotivosMigracao;
    private EditText txtTaxaInstalacao;
    private EditText txtValorPromocional;
    private EditText txtValorPosPromocao;
    private EditText txtVigenciaPromocao;
    private EditText txtDataInstalacao;

    private DatePickerDialog dataVigenciaDPD;
    private DatePickerDialog dataInstalacaoDPD;
    private SimpleDateFormat formatoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_internet);
        criarCamposEntrada();
        formatoData = new SimpleDateFormat("dd/MM/yyyy");
        entityManager = new EntityManager(this);
        vendaInternet = new VendaInternet();
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>) getIntent()
                .getExtras().get("produtosTipo");
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);

        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.INTERNET);

        carregarProdutos();
        carregarProdutosAdicionais();
        carregarConcorrentes();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            venda = entityManager.getById(Venda.class, idVenda);
            if (venda.getVendaInternet() != null
                    && venda.getVendaInternet().getId() != null
                    && !edicao) {
                isVoltar = true;
            }

            if (venda.getVendaInternet() != null
                    && venda.getVendaInternet().getId() != null
                    && (edicao || isVoltar)) {
                vendaInternet = entityManager.getById(VendaInternet.class,
                        venda.getVendaInternet().getId());
                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }
        setDateTimeField();
    }

    private void popularValoresCamposEntrada() throws DataBaseException {
        if (vendaInternet.getProduto() != null) {
            for (Produto produto : planosInternet) {
                if (produto.getId().equals(vendaInternet.getProduto().getId())) {
                    spinnerPlanoInternet.setSelection(planosInternet
                            .indexOf(produto) + SistemaConstantes.UM);
                    break;
                }
            }
        }

        txtObservacao.setText(vendaInternet.getObservacao());

        if (vendaInternet.getConcorrenteMigracao() != null) {
            for (Concorrente concorrente : concorrentes) {
                if (concorrente.getId().equals(vendaInternet.getConcorrenteMigracao().getId())) {
                    spinnerConcorrentes.setSelection(concorrentes
                            .indexOf(concorrente) + SistemaConstantes.UM);
                    break;
                }
            }
        }

        if (edicao || isVoltar) {
            List<Produto> produtosAdicionais = entityManager.select(
                    Produto.class,
                    "SELECT p.* FROM produto p " +
                            "INNER JOIN venda_internet_produto_adicional vipa " +
                            "ON vipa._produto = p.id " +
                            "AND vipa._venda_internet = " + vendaInternet.getId());

            if (produtosAdicionais != null) {
                for (Produto produto : produtosAdicionais) {
                    indexProdutosAdicionaisSelecionados.add(
                            Arrays.asList(descricoesProdutosAdicionais).indexOf(produto.getDescricao()));
                }
            }
        }
        listarProdutosAdicionaisSelecionados();

        if (vendaInternet.getTaxaInstalacao() != null) {
            txtTaxaInstalacao.setText(UtilActivity.formatarMoeda(vendaInternet.getTaxaInstalacao().doubleValue()));
        }

        if (vendaInternet.getValorPromocional() != null) {
            txtValorPromocional.setText(UtilActivity.formatarMoeda(vendaInternet.getValorPromocional().doubleValue()));
        }

        if (vendaInternet.getValorPosPromocao() != null) {
            txtValorPosPromocao.setText(UtilActivity.formatarMoeda(vendaInternet.getValorPosPromocao().doubleValue()));
        }

        if (vendaInternet.getVigenciaPromocao() != null) {
            txtVigenciaPromocao.setText(formatoData.format(vendaInternet.getVigenciaPromocao().getTime()));
        }

        if (vendaInternet.getDataInstalacao() != null) {
            txtDataInstalacao.setText(formatoData.format(vendaInternet.getDataInstalacao().getTime()));
        }
    }

    private void popularMock() throws DataBaseException {
        vendaInternet = new VendaInternet();
        vendaInternet.setProduto(planosInternet.get(SistemaConstantes.ZERO));
        vendaInternet.setObservacao("OBSERVAÇÃO VENDA INTERNET");
        vendaInternet.setConcorrenteMigracao(concorrentes.get(SistemaConstantes.ZERO));
        vendaInternet.setMotivoMigracao(
                entityManager.select(MotivoMigracao.class,
                        "SELECT * FROM motivo_migracao WHERE situacao = 'ATIVO'").get(SistemaConstantes.ZERO));
        indexProdutosAdicionaisSelecionados.add(SistemaConstantes.ZERO);
        vendaInternet.setTaxaInstalacao(new BigDecimal(SistemaConstantes.CEM));
        vendaInternet.setValorPromocional(new BigDecimal(SistemaConstantes.OITENTA));
        vendaInternet.setValorPosPromocao(new BigDecimal(SistemaConstantes.CENTO_VINTE));
        vendaInternet.setVigenciaPromocao(Calendar.getInstance());
        vendaInternet.getVigenciaPromocao().set(2015, 12, 31);
        vendaInternet.setDataInstalacao(Calendar.getInstance());
    }

    private void carregarProdutos() {
        try {
            planosInternet = entityManager.select(
                    Produto.class,
                    "SELECT p.* " +
                    "FROM produto p " +
                    "INNER JOIN produto_tipo pt " +
                    "ON pt.id = p._produto_tipo " +
                    "AND pt.descricao = '" + EProdutoTipo.INTERNET.getDescricao() + "' " +
                    "ORDER BY p.descricao");

            List<String> descricoesPlanosInternet = new ArrayList<>();
            descricoesPlanosInternet.add(UtilActivity.SELECIONE);

            for (Produto produto : planosInternet) {
                descricoesPlanosInternet.add(produto.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerPlanoInternet, descricoesPlanosInternet);

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar planos de internet.", this);
        }
    }

    private void criarCamposEntrada() {
        spinnerPlanoInternet    = (Spinner) findViewById(R.id.spinnerPlanoInternet);
        txtObservacao           = (EditText) findViewById(R.id.txtObservacao);
        spinnerConcorrentes     = (Spinner) findViewById(R.id.spinnerConcorrentes);
        spinnerMotivosMigracao  = (Spinner) findViewById(R.id.spinnerMotivosMigracao);
        txtTaxaInstalacao       = (EditText) findViewById(R.id.txtTaxaInstalacao);
        UtilMask.setMascaraMoeda(txtTaxaInstalacao, 10);
        txtValorPromocional     = (EditText) findViewById(R.id.txtValorPromocional);
        UtilMask.setMascaraMoeda(txtValorPromocional, 10);
        txtValorPosPromocao     = (EditText) findViewById(R.id.txtValorPosPromocao);
        UtilMask.setMascaraMoeda(txtValorPosPromocao, 10);
        txtVigenciaPromocao     = (EditText) findViewById(R.id.txtVigenciaPromocao);
        txtDataInstalacao       = (EditText) findViewById(R.id.txtDataInstalacao);
    }

    private void setDateTimeField() {
        final Calendar dataHoraAtual = Calendar.getInstance();

        txtDataInstalacao.setOnFocusChangeListener(this);
        txtDataInstalacao.setOnClickListener(this);

        dataInstalacaoDPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                vendaInternet.setDataInstalacao(Calendar.getInstance());
                vendaInternet.getDataInstalacao().set(year, monthOfYear, dayOfMonth);
                txtDataInstalacao.setText(formatoData.format(vendaInternet.getDataInstalacao().getTime()));
            }

        },dataHoraAtual.get(Calendar.YEAR), dataHoraAtual.get(Calendar.MONTH), dataHoraAtual.get(Calendar.DAY_OF_MONTH));

        txtVigenciaPromocao.setOnFocusChangeListener(this);
        txtVigenciaPromocao.setOnClickListener(this);

        dataVigenciaDPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                vendaInternet.setVigenciaPromocao(Calendar.getInstance());
                vendaInternet.getVigenciaPromocao().set(year, monthOfYear, dayOfMonth);
                txtVigenciaPromocao.setText(formatoData.format(vendaInternet.getVigenciaPromocao().getTime()));
            }
        },dataHoraAtual.get(Calendar.YEAR), dataHoraAtual.get(Calendar.MONTH), dataHoraAtual.get(Calendar.DAY_OF_MONTH));
    }

    private void carregarProdutosAdicionais() {
        try {
            produtosAdicionais = entityManager.select(
                    Produto.class,
                    "SELECT p.* " +
                            "FROM produto p " +
                            "INNER JOIN produto_tipo pt " +
                            "ON pt.id = p._produto_tipo " +
                            "AND pt.descricao = '" + EProdutoTipo.INTERNET_ADICIONAL.getDescricao() + "' " +
                            "ORDER BY p.descricao");

            descricoesProdutosAdicionais = new String[produtosAdicionais.size()];
            indexProdutosAdicionaisSelecionados = new ArrayList<>();

            int index = SistemaConstantes.ZERO;
            for (Produto produtoAdicional : produtosAdicionais) {
                descricoesProdutosAdicionais[index] =  (produtoAdicional.getDescricao());
                index++;
            }

        } catch (Exception e ) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar produtos adicionais: " + e.getMessage(), this);
        }
    }

    private void carregarConcorrentes() {
        try {
            concorrentes = entityManager.getByWhere(Concorrente.class, "situacao = 'ATIVO'", "descricao");
            List<String> descricoesConcorrentes = new ArrayList<>();
            descricoesConcorrentes.add(UtilActivity.SELECIONE);
            for (Concorrente c : concorrentes) {
                descricoesConcorrentes.add(c.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerConcorrentes, descricoesConcorrentes);
            spinnerConcorrentes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    List<String> descricoesMotivosMigracao = new ArrayList<String>();

                    if (position > SistemaConstantes.ZERO) {
                        try {
                            motivosMigracao = entityManager.getByWhere(MotivoMigracao.class, "situacao = 'ATIVO'", "descricao");
                            descricoesMotivosMigracao.add(UtilActivity.SELECIONE);
                            for (MotivoMigracao motivo : motivosMigracao) {
                                descricoesMotivosMigracao.add(motivo.getDescricao());
                            }
                            spinnerMotivosMigracao.setEnabled(true);
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                            UtilActivity.makeShortToast("ERRO ao listar Motivos de Migração: " + e.getMessage(), getApplicationContext());
                        }
                    } else {
                        descricoesMotivosMigracao.add("SELECIONE O CONCORRENTE");
                        spinnerMotivosMigracao.setEnabled(false);
                    }

                    ArrayAdapter adapter = new ArrayAdapter<> (VendaInternetActivity.this, android.R.layout.simple_spinner_item, descricoesMotivosMigracao);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerMotivosMigracao.setAdapter(adapter);

                    if (edicao || isMock || isVoltar) {
                        if (vendaInternet.getMotivoMigracao() != null) {
                            for (MotivoMigracao motivo : motivosMigracao) {
                                if (motivo.getId().equals(vendaInternet.getMotivoMigracao().getId())) {
                                    spinnerMotivosMigracao.setSelection(motivosMigracao
                                            .indexOf(motivo) + SistemaConstantes.UM);
                                    vendaInternet.setMotivoMigracao(null);
                                    break;
                                }
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            UtilActivity.makeShortToast("ERRO ao listar Concorrentes: " + e.getMessage(), this);
        }
    }

    public void adicionarProdutoAdicional(View v) {

        boolean[] checkedItems = null;

        if (descricoesProdutosAdicionais != null) {
            checkedItems = new boolean[descricoesProdutosAdicionais.length];

            for (int i = SistemaConstantes.ZERO; i < descricoesProdutosAdicionais.length; i++) {
                if (indexProdutosAdicionaisSelecionados.contains(i)) {
                    checkedItems[i] = true;
                } else {
                    checkedItems[i] = false;
                }
            }
        }

        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder((this));

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setTitle("Adicionais de Internet");

        // Specify the list array, the items to be selected by default (null for none),
        // and the listener through which to receive callbacks when items are selected
        builder.setMultiChoiceItems(descricoesProdutosAdicionais, checkedItems,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which,
                                        boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            indexProdutosAdicionaisSelecionados.add(which);
                        } else if (indexProdutosAdicionaisSelecionados.contains(which)) {
                            // Else, if the item is already in the array, remove it
                            indexProdutosAdicionaisSelecionados.remove(Integer.valueOf(which));
                        }
                    }
                })
                // Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listarProdutosAdicionaisSelecionados();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //Fecha dialog
                    }
                });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        //Show dialog
        dialog.show();
    }

    private void listarProdutosAdicionaisSelecionados() {
        List<Map<String, Object>> itens = new ArrayList<Map<String, Object>>();
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.produtosAdicionaisLL);
        linearLayout.removeAllViews();

        if (indexProdutosAdicionaisSelecionados != null) {
            Collections.sort(indexProdutosAdicionaisSelecionados);
            for (Integer index : indexProdutosAdicionaisSelecionados) {
                View itemProdutoAdicional = getLayoutInflater().inflate(R.layout.item_produto_adicional, null);
                TextView textView = (TextView) itemProdutoAdicional.findViewById(R.id.txtNomeProdutoAdicional);
                textView.setText( produtosAdicionais.get(index).getDescricao());
                linearLayout.addView(itemProdutoAdicional);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_internet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            Intent activityAnterior = new Intent();
            activityAnterior.putExtra("idVenda", idVenda);
            activityAnterior.putExtra("edicao", edicao);
            activityAnterior.putExtra("isMock", isMock);
            activityAnterior.putExtra("isVoltar", true);
            activityAnterior.putExtra("telasAnteriores", telasAnteriores);
            activityAnterior.putExtra("produtosTipo", produtosTiposSelecionados);
            Class<?> cls = null;

            if (telasAnteriores.contains(EVendaFluxo.FONE_PORTABILIDADE)) {
                cls = VendaFonePortabilidadeActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.FONE)) {
                cls = VendaFoneActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.TV_ADICIONAL)) {
                cls = VendaTvAdicionalActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.TV)) {
                cls = VendaTvActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.CELULAR_DEPENDENTE)) {
                cls = VendaCelularDependenteActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.CELULAR)) {
                cls = VendaCelularActivity.class;
            } else if (telasAnteriores.contains(EVendaFluxo.MOTIVOS_NAO_VENDA)) {
                cls = VendaPacoteNaoVendaActivity.class;
            } else {
                cls = VendaPacoteActivity.class;
            }

            activityAnterior.setComponent(new ComponentName(this, cls));
            startActivity(activityAnterior);
            VendaInternetActivity.this.finish();
        }

        if (id == R.id.action_avancar) {
            avancar();
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaInternetActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void avancar() {
        try {
            if (!validarCamposObrigatorios()) {
                return;
            }

            if (venda.getVendaInternet() != null && venda.getVendaInternet().getId() != null) {
                entityManager.executeNativeQuery("DELETE FROM venda_internet WHERE id = "
                        + venda.getVendaInternet().getId());
                entityManager.executeNativeQuery(
                        "DELETE FROM venda_internet_produto_adicional WHERE _venda_internet = "
                                + venda.getVendaInternet().getId());
            }

            vendaInternet = entityManager.save(popularDadosVendaInternet());

            for (Integer i : indexProdutosAdicionaisSelecionados) {
                VendaInternetProdutoAdicional vipa =
                        new VendaInternetProdutoAdicional(vendaInternet, produtosAdicionais.get(i));
                entityManager.save(vipa);
            }

            venda.setVendaInternet(vendaInternet);
            entityManager.atualizar(venda);

            irParaProximaTela();

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar venda internet: " + e.getMessage(), this);
        }
    }

    private void irParaProximaTela() throws Exception {
        Intent proximaTela = new Intent();
        proximaTela.putExtra("idVenda", idVenda);
        proximaTela.putExtra("produtosTipo", produtosTiposSelecionados);
        proximaTela.putExtra("edicao", edicao);
        proximaTela.putExtra("isMock", isMock);

        telasAnteriores.add(EVendaFluxo.INTERNET);
        proximaTela.putExtra("telasAnteriores", telasAnteriores);

        Class<?> cls = VendaFormaPagamentoActivity.class;
        proximaTela.setComponent(new ComponentName(this, cls));
        startActivity(proximaTela);
        VendaInternetActivity.this.finish();
    }

    private boolean validarCamposObrigatorios() {
        if (!UtilActivity.isValidSpinnerValue(spinnerPlanoInternet)) {
            UtilActivity.makeShortToast("informe o plano de internet.", this);
            return false;
        }

        if (UtilActivity.isValidSpinnerValue(spinnerConcorrentes)) {
            if (UtilActivity.isValidSpinnerValue(spinnerMotivosMigracao)) {
                return true;
            }
            return false;
        }
        return true;
    }

    private VendaInternet popularDadosVendaInternet() {
        VendaInternet vendaInternet = new VendaInternet();
        vendaInternet.setProduto(planosInternet
                .get(spinnerPlanoInternet.getSelectedItemPosition() - SistemaConstantes.UM));
        vendaInternet.setObservacao(txtObservacao.getText().toString());

        if (UtilActivity.isValidSpinnerValue(spinnerConcorrentes)) {
            vendaInternet.setConcorrenteMigracao(
                    concorrentes.get(spinnerPlanoInternet.getSelectedItemPosition() - SistemaConstantes.UM));
            vendaInternet.setMotivoMigracao(
                    motivosMigracao.get(spinnerMotivosMigracao.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        if (txtTaxaInstalacao.getText() != null && !txtTaxaInstalacao.getText().toString().isEmpty()) {
            vendaInternet.setTaxaInstalacao(new BigDecimal(UtilMask.unmaskMoeda(txtTaxaInstalacao.getText().toString())));
        }

        if (txtValorPromocional.getText() != null && !txtValorPromocional.getText().toString().isEmpty()) {
            vendaInternet.setValorPromocional(new BigDecimal(UtilMask.unmaskMoeda(txtValorPromocional.getText().toString())));
        }

        if (txtValorPosPromocao.getText() != null && !txtValorPosPromocao.getText().toString().isEmpty()) {
            vendaInternet.setValorPosPromocao(new BigDecimal(UtilMask.unmaskMoeda(txtValorPosPromocao.getText().toString())));
        }

        vendaInternet.setVigenciaPromocao(UtilActivity.getCalendarValue(txtVigenciaPromocao));
        vendaInternet.setDataInstalacao(UtilActivity.getCalendarValue(txtDataInstalacao));

        return vendaInternet;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(hasFocus) {
            showPicker(view);
        }
    }

    @Override
    public void onClick(View view) {
        showPicker(view);
    }

    private void showPicker(View view) {
        if(view == txtDataInstalacao) {
            dataInstalacaoDPD.show();
        } else if (view == txtVigenciaPromocao) {
            dataVigenciaDPD.show();
        }
    }
}