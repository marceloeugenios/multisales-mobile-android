package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaInternet;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaInternetProdutoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

/**
 * Created by eric on 16-03-2015.
 */
public class VendaInternetFragment extends Fragment {

    private RelativeLayout relativeLayout;
    private RelativeLayout rlAdicionaisRL;
    private LinearLayout adicionaisLayout;
    private RelativeLayout rlConcorrenciaRL;

    private TextView txtPlanoInternetTV;
    private TextView txtConcorrenteTV;
    private TextView txtMotivoMigracaoTV;

    private VendaInternet vendaInternet;
    private EntityManager entityManager;
    private VendaInternetProdutoAdicional vendaInternetProdutoAdicional;
    private TextView txtObservacaoTV;
    private RelativeLayout relativeLayoutObservacao;
    private RelativeLayout rlObservacoesRL;
    private RelativeLayout linhaLayout;
    private Produto produto;
    private RelativeLayout relativeLayoutPlanoLayout;
    private RelativeLayout relativeLayoutConcorrencia;
    private TextView vigenciaPromocao;
    private TextView taxaInstalacaoTV;
    private TextView valorPosPromocao;
    private TextView valorPromocionalTV;
    private TextView dataInstalacao;
    private RelativeLayout relativeLayoutPagamento;
    private RelativeLayout relativeLayoutTaxaInstalacao;
    private RelativeLayout relativeValorPromocional;
    private RelativeLayout relativeLayoutValorPosPromocao;
    private RelativeLayout relativeLayoutDataInstalacao;
    private RelativeLayout relativeLayoutVigenciaPromocao;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        entityManager = new EntityManager(getActivity());
        this.vendaInternet = ((VendaDetalhesActivity) getActivity()).getVenda().getVendaInternet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        //relative Layout principal
        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_venda_internet, container, false);
        //relative Layout Pagamento
        relativeLayoutPagamento = (RelativeLayout) relativeLayout.findViewById(R.id.pagamentoRL);
        //relative Layout Plano internet
        relativeLayoutPlanoLayout = (RelativeLayout) relativeLayout.findViewById(R.id.planoInternetRL);
        //relative Layout Adicionais
        adicionaisLayout = (LinearLayout) relativeLayout.findViewById(R.id.adicionaisLL);
        //relative Layout Concorrencia
        relativeLayoutConcorrencia = (RelativeLayout) relativeLayout.findViewById(R.id.concorrenciaRL);
        //relatrive layout
        linhaLayout = (RelativeLayout) inflater.inflate(R.layout.item_produto_adicional, container, false);
        //relative Layout Observacao
        relativeLayoutObservacao = (RelativeLayout) relativeLayout.findViewById(R.id.observacaoRL);
        criarCamposEntrada(inflater,container,savedInstanceState);
        return relativeLayout;
    }

    public void criarCamposEntrada(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState){

        txtPlanoInternetTV = (TextView) relativeLayout.findViewById(R.id.planoInternetTV);
        if (vendaInternet.getProduto() != null) {
            txtPlanoInternetTV.setText(vendaInternet.getProduto().getDescricao());
        } else {
            txtPlanoInternetTV.setText("N/D");
        }

       //campos Pagamento
       if((vendaInternet.getTaxaInstalacao() != null && !vendaInternet.getTaxaInstalacao().equals(BigDecimal.ZERO) )
         ||  (vendaInternet.getValorPromocional() != null && !vendaInternet.getTaxaInstalacao().equals(BigDecimal.ZERO) )
         ||  (vendaInternet.getValorPosPromocao() !=null && !vendaInternet.getValorPosPromocao().equals(BigDecimal.ZERO) )
         ||  (vendaInternet.getVigenciaPromocao() != null) ) {


       this.taxaInstalacaoTV = (TextView) relativeLayoutPagamento.findViewById(R.id.taxaInstalacaoTV);
        if(vendaInternet.getTaxaInstalacao() != null
                &&  !vendaInternet.getTaxaInstalacao().equals(BigDecimal.ZERO) ){
            this.taxaInstalacaoTV.setText(UtilActivity.formatarMoeda(vendaInternet.getTaxaInstalacao().doubleValue()));
        } else {
            relativeLayoutTaxaInstalacao = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.taxaInstalacaoRL);
            relativeLayoutTaxaInstalacao.setVisibility(View.GONE);
        }

        this.valorPromocionalTV = (TextView) relativeLayoutPagamento.findViewById(R.id.valorPromocionalTV);
        if(vendaInternet.getValorPromocional() != null
                && !vendaInternet.getValorPromocional().equals(BigDecimal.ZERO)){
            this.valorPromocionalTV.setText(UtilActivity.formatarMoeda(vendaInternet.getValorPromocional().doubleValue()));
        } else {
            relativeValorPromocional = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.valorPromocionalRL);
            relativeValorPromocional.setVisibility(View.GONE);
        }

        this.valorPosPromocao = (TextView) relativeLayoutPagamento.findViewById(R.id.valorPosPromocaoTV);
        if(vendaInternet.getValorPosPromocao() != null
                && !vendaInternet.getValorPosPromocao().equals(BigDecimal.ZERO)){
            this.valorPosPromocao.setText(UtilActivity.formatarMoeda(vendaInternet.getValorPosPromocao().doubleValue()));
        } else {
            relativeLayoutValorPosPromocao = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.valorPosPromocaoRL);
            relativeLayoutValorPosPromocao.setVisibility(View.GONE);
        }

        this.vigenciaPromocao = (TextView) relativeLayoutPagamento.findViewById(R.id.vigenciaPromocaoTV);
        if(vendaInternet.getVigenciaPromocao() != null){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String data = simpleDateFormat.format(vendaInternet.getVigenciaPromocao().getTime());
            this.vigenciaPromocao.setText(data);
        } else {
            relativeLayoutVigenciaPromocao = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.vigenciaPromocaoRL);
            relativeLayoutVigenciaPromocao.setVisibility(View.GONE);
        }
        this.dataInstalacao = (TextView) relativeLayoutPagamento.findViewById(R.id.dataInstalacaoTV);
        if(vendaInternet.getDataInstalacao() != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String data = simpleDateFormat.format(vendaInternet.getDataInstalacao().getTime());
            this.dataInstalacao.setText(data);
        } else {
            relativeLayoutDataInstalacao = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.dataInstalacaoRL);
            relativeLayoutDataInstalacao.setVisibility(View.GONE);
        }
       }else {
           relativeLayoutPagamento.setVisibility(View.GONE);
       }

        txtObservacaoTV = (TextView) relativeLayoutObservacao.findViewById(R.id.observacoesTV);
        if(vendaInternet.getObservacao() != null
                && !vendaInternet.getObservacao().isEmpty()){
            txtObservacaoTV.setText(vendaInternet.getObservacao());
        } else {
            rlObservacoesRL = (RelativeLayout) relativeLayout.findViewById(R.id.observacaoRL);
            rlObservacoesRL.setVisibility(View.GONE);
        }

        if (vendaInternet.getProdutos().size() > 0) {
          for (int i = 0; i < vendaInternet.getProdutos().size(); i++) {
                produto = vendaInternet.getProdutos().get(i);
                try {
                    entityManager.initialize(produto.getProdutoTipo());
                    entityManager.initialize(produto.getProdutoPai());
                } catch (Exception e) {
                    e.printStackTrace();
                }

              RelativeLayout itemProduto = (RelativeLayout) inflater.inflate(R.layout.item_produto_adicional, container, false);
              TextView textView = (TextView) itemProduto.findViewById(R.id.txtNomeProdutoAdicional);
              textView.setText(produto.getDescricao());
              adicionaisLayout.addView(itemProduto);
              if (i != vendaInternet.getProdutos().size() - 1) {
                  LinearLayout separador = (LinearLayout) inflater.inflate(R.layout.separador, container, false);
                  adicionaisLayout.addView(separador);
              }
          }
       } else {
            rlAdicionaisRL = (RelativeLayout) relativeLayout.findViewById(R.id.adicionaisRL);
            rlAdicionaisRL.setVisibility(View.GONE);
        }

        rlConcorrenciaRL = (RelativeLayout) relativeLayout.findViewById(R.id.concorrenciaRL);
        if(vendaInternet.getMotivoMigracao() != null ) {

            txtConcorrenteTV =(TextView)  relativeLayout.findViewById(R.id.concorrenteTV);
            txtConcorrenteTV.setText(vendaInternet.getConcorrenteMigracao().getDescricao());

            txtMotivoMigracaoTV = (TextView) relativeLayout.findViewById(R.id.motivoMigracaoTV);
            txtMotivoMigracaoTV.setText(vendaInternet.getMotivoMigracao().getDescricao());

        } else {
            rlConcorrenciaRL.setVisibility(View.GONE);
        }

    }
}
