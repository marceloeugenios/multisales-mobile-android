

package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.BandeiraSistema;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.CombinacaoProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ETipoCliente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TipoContrato;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaPacoteActivity extends ActionBarActivity {

    private EntityManager entityManager;
    private HashSet<EVendaFluxo> telasAnteriores;
    private Venda venda;

    private Integer idVenda;
    private TipoContrato tipoContrato;
    private CombinacaoProdutoTipo combinacaoProdutoTipo;

    private List<ETipoCliente> tiposCliente;
    private List<TipoContrato> tiposContrato;
    private List<CombinacaoProdutoTipo> combinacoesProdutoTipo;

    private Spinner spinnerTipoCliente;
    private TextView txtViewNumeroContrato;
    private EditText txtNumeroContrato;
    private Spinner spinnerTipoContrato;
    private Spinner spinnerProduto;
    private CheckBox checkBoxFidelidade;
    private EditText txtObservacao;
    private EditText txtPerfilCombo;
    private EditText txtValorTotal;
    private EditText txtValorProm;
    private EditText txtvalorAdesao;
    private EditText txtnumeroParcelas;
    private EditText txtValorParcela;
    private CheckBox checkBoxAdicionaTv;
    private CheckBox checkBoxAdicionaInternet;
    private boolean edicao;
    private boolean isMock = false;
    private boolean isVoltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_pacote);
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);
        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.PACOTE);

        criarCamposEntrada();
        entityManager = new EntityManager(this);
        carregarTiposCliente();
        carregarTiposContrato();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            venda = entityManager.getById(Venda.class, idVenda);

            if (venda.getCombinacaoProdutoTipo() != null && !edicao) {
                isVoltar = true;
            }

            if (edicao || isVoltar) {
                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO: " + e.getMessage(), this);
        }
    }

    private void criarCamposEntrada() {
        spinnerTipoCliente          = (Spinner) findViewById(R.id.spinnerTipoCliente);
        txtViewNumeroContrato       = (TextView) findViewById(R.id.txtViewNumeroContrato);
        txtNumeroContrato           = (EditText) findViewById(R.id.txtNumeroContrato);
        spinnerTipoContrato         = (Spinner) findViewById(R.id.spinnerTipoContrato);
        spinnerProduto              = (Spinner) findViewById(R.id.spinnerProduto);
        checkBoxFidelidade          = (CheckBox) findViewById(R.id.checkBoxFidelidade);
        txtObservacao               = (EditText) findViewById(R.id.txtObservacao);
        txtPerfilCombo              = (EditText) findViewById(R.id.txtPerfilCombo);
        txtValorTotal               = (EditText) findViewById(R.id.txtValorTotal);
        UtilMask.setMascaraMoeda(txtValorTotal, 10);
        txtValorProm                = (EditText) findViewById(R.id.txtValorProm);
        UtilMask.setMascaraMoeda(txtValorProm, 10);
        txtvalorAdesao              = (EditText) findViewById(R.id.txtvalorAdesao);
        UtilMask.setMascaraMoeda(txtvalorAdesao, 10);
        txtnumeroParcelas           = (EditText) findViewById(R.id.txtnumeroParcelas);
        txtValorParcela             = (EditText) findViewById(R.id.txtValorParcela);
        UtilMask.setMascaraMoeda(txtValorParcela, 10);
        checkBoxAdicionaTv          = (CheckBox) findViewById(R.id.checkBoxAdicionaTv);
        checkBoxAdicionaInternet    = (CheckBox) findViewById(R.id.checkBoxAdicionaInternet);

        alterarVisibilidadeProdutosAdicionais(View.GONE);
    }

    private void carregarTiposCliente() {
        try {
            tiposCliente = Arrays.asList(ETipoCliente.values());
            List<String> rotulosTiposCliente = new ArrayList<String>();
            rotulosTiposCliente.add(UtilActivity.SELECIONE);
            for (ETipoCliente tipoCliente : tiposCliente) {
                rotulosTiposCliente.add(tipoCliente.getRotulo());
            }
            UtilActivity.setAdapter(this, spinnerTipoCliente, rotulosTiposCliente);

            spinnerTipoCliente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > SistemaConstantes.ZERO
                            && (tiposCliente.get(position - SistemaConstantes.UM) == ETipoCliente.BASE)) {
                        txtViewNumeroContrato.setVisibility(View.VISIBLE);
                        txtNumeroContrato.setVisibility(View.VISIBLE);
                    } else {
                        txtViewNumeroContrato.setVisibility(View.GONE);
                        txtNumeroContrato.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Ocorreu um ERRO ao listar tipos de cliente!", getApplicationContext());
        }
    }

    private void carregarTiposContrato() {
        try {
            tiposContrato = entityManager.getAll(TipoContrato.class);
            List<String> descricoesTiposContratos = new ArrayList<String>();
            descricoesTiposContratos.add(UtilActivity.SELECIONE);
            for (TipoContrato tipoContrato : tiposContrato) {
                descricoesTiposContratos.add(tipoContrato.getDescricao());
            }
            UtilActivity.setAdapter(this, spinnerTipoContrato, descricoesTiposContratos);

            spinnerTipoContrato.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    carregarCombinacoesProdutoTipo(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Ocorreu um ERRO ao listar tipos de contrato!", getApplicationContext());
        }
    }

    private void carregarCombinacoesProdutoTipo(int position) {
        try {

            List<String> descricoesCombinacoesProdutoTipo = new ArrayList<String>();

            if (position > SistemaConstantes.ZERO) {
                TipoContrato tipoContratoSelecionado =
                        tiposContrato.get(position - SistemaConstantes.UM);
                combinacoesProdutoTipo = entityManager.select(
                        CombinacaoProdutoTipo.class,
                        "SELECT cpt.* " +
                                "FROM combinacao_produto_tipo cpt " +
                                "WHERE cpt.id IN (" +
                                    "SELECT DISTINCT tccpt._combinacao_produto_tipo " +
                                    "FROM tipo_contrato_combinacao_produto_tipo tccpt " +
                                    "WHERE tccpt._tipo_contrato = " + tipoContratoSelecionado.getId() +
                                ") " +
                                "AND cpt.situacao = 'ATIVO'");
                descricoesCombinacoesProdutoTipo.add(UtilActivity.SELECIONE);
                for (CombinacaoProdutoTipo combinacao : combinacoesProdutoTipo) {
                    descricoesCombinacoesProdutoTipo.add(combinacao.getDescricao());
                }
                spinnerProduto.setEnabled(true);
            } else {
                descricoesCombinacoesProdutoTipo.add("SELECIONE O TIPO DE CONTRATO");
                spinnerProduto.setEnabled(false);
            }
            UtilActivity.setAdapter(this, spinnerProduto, descricoesCombinacoesProdutoTipo);
            if (edicao || isMock || isVoltar) {
                if (venda != null && venda.getCombinacaoProdutoTipo() != null) {
                    for (CombinacaoProdutoTipo cpt : combinacoesProdutoTipo) {
                        if (cpt.getId().equals(venda.getCombinacaoProdutoTipo().getId())) {
                            spinnerProduto.setSelection(
                                    combinacoesProdutoTipo.indexOf(cpt) + SistemaConstantes.UM);
                            venda.setCombinacaoProdutoTipo(null);
                            break;
                        }
                    }
                }
            }


            spinnerProduto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    tratarExibicaoProdutosAdicionais(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Ocorreu um ERRO ao listar combinações de produto!", getApplicationContext());
        }
    }

    private void tratarExibicaoProdutosAdicionais(int position) {

        alterarVisibilidadeProdutosAdicionais(View.GONE);

        if (position > SistemaConstantes.ZERO) {
            CombinacaoProdutoTipo combinacaoSelecionada =
                    combinacoesProdutoTipo.get(position - SistemaConstantes.UM);
            if (UtilActivity.MULTI_INTERNET.equals(combinacaoSelecionada.getDescricao())) {
                alterarVisibilidadeProdutosAdicionais(View.VISIBLE);
            }
        }
    }

    private void alterarVisibilidadeProdutosAdicionais(int visibilidade) {
        checkBoxAdicionaTv.setVisibility(visibilidade);
        checkBoxAdicionaInternet.setVisibility(visibilidade);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_pacote, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            try {
                Intent activityAnterior = new Intent();
                activityAnterior.putExtra("idVenda", idVenda);
                activityAnterior.putExtra("edicao", edicao);
                activityAnterior.putExtra("isMock", isMock);
                activityAnterior.putExtra("isVoltar", true);
                activityAnterior.putExtra("telasAnteriores", telasAnteriores);

                entityManager.initialize(venda.getCliente());

                boolean enderecoCobranca = false;
                if (venda.getCliente().getEnderecoCobranca() != null && venda.getCliente().getEnderecoCobranca().getId() != null) {
                    enderecoCobranca = true;
                }

                activityAnterior.putExtra("enderecoCobranca", enderecoCobranca);
                Class<?> cls = VendaEnderecoActivity.class;
                activityAnterior.setComponent(new ComponentName(this, cls));
                startActivity(activityAnterior);
                VendaPacoteActivity.this.finish();
            } catch (DataBaseException e) {
                e.printStackTrace();
            }
        }

        if (id == R.id.action_avancar) {
            avancar(null);
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaPacoteActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void cancelar(View view) {
        UtilActivity.makeShortToast("VOLTAR", this.getApplicationContext());
    }

    public void avancar(View view) {
        try {
            if (!validarCamposObrigatorios()) {
                return;
            }

            venda = popularDadosPacote(venda);
            entityManager.atualizar(venda);

            irParaProximaTela();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Ocorreu um ERRO ao salvar dados do pacote!",
                    this.getApplicationContext());
        }
    }

    private void irParaProximaTela() throws DataBaseException {

        HashSet<EProdutoTipo> produtosTipos = obterProdutosTipos();

        Intent intentProduto = new Intent();
        intentProduto.putExtra("idVenda", venda.getId());
        intentProduto.putExtra("produtosTipo", produtosTipos);
        intentProduto.putExtra("edicao", edicao);
        intentProduto.putExtra("isMock", isMock);
        telasAnteriores.add(EVendaFluxo.PACOTE);
        intentProduto.putExtra("telasAnteriores", telasAnteriores);

        //FIXME substituir pelas bandeiras da classe MultiSales
        List<BandeiraSistema> bandeiras = entityManager.select(BandeiraSistema.class, "SELECT b.* FROM bandeira_sistema b");
        List<String> descricoesBandeiras = new ArrayList<>();
        if (bandeiras != null) {
            for (BandeiraSistema bandeira : bandeiras) {
                descricoesBandeiras.add(bandeira.getDescricao());
            }
        }

        Class<?> cls = null;
        boolean deletarMotivosNaoVenda = true;
        if (!UtilActivity.MULTI_INTERNET.equals(venda.getCombinacaoProdutoTipo().getDescricao())
                && !UtilActivity.COMBO_MULTI.equals(venda.getCombinacaoProdutoTipo().getDescricao())
                && !descricoesBandeiras.contains("VIVO")
                ) {
            deletarMotivosNaoVenda = false;
            cls = VendaPacoteNaoVendaActivity.class;
        } else if (produtosTipos.contains(EProdutoTipo.MULTI)) {
            cls = VendaCelularActivity.class;
        } else if (produtosTipos.contains(EProdutoTipo.TV)) {
            cls = VendaTvActivity.class;
        } else if (produtosTipos.contains(EProdutoTipo.FONE)) {
            cls = VendaFoneActivity.class;
        } else if (produtosTipos.contains(EProdutoTipo.INTERNET)) {
            cls = VendaInternetActivity.class;
        }

        if (deletarMotivosNaoVenda) {
            entityManager.executeNativeQuery(
                    "DELETE FROM venda_motivo_nao_venda WHERE _venda = " + idVenda);
        }

        intentProduto.setComponent(new ComponentName(this, cls));
        startActivity(intentProduto);
        VendaPacoteActivity.this.finish();
    }

    private HashSet<EProdutoTipo> obterProdutosTipos() {
        HashSet<EProdutoTipo> eprodutosTipos = new HashSet<>();

        try {
            String sql =
                    "SELECT pt.* " +
                            "FROM produto_tipo pt " +
                            "INNER JOIN combinacao_produto_tipo_produto_tipo cptpt " +
                            "ON cptpt._produto_tipo = pt.id " +
                            "INNER JOIN combinacao_produto_tipo cpt " +
                            "ON cpt.id = cptpt._combinacao_produto_tipo " +
                            "AND cpt.id = " + venda.getCombinacaoProdutoTipo().getId();

            List<ProdutoTipo> produtosTipos = entityManager.select(ProdutoTipo.class, sql);

            for (ProdutoTipo pt : produtosTipos) {
                if (EProdutoTipo.TV.getDescricao().equals(pt.getDescricao())) {
                    eprodutosTipos.add(EProdutoTipo.TV);
                } else  if (EProdutoTipo.FONE.getDescricao().equals(pt.getDescricao())) {
                    eprodutosTipos.add(EProdutoTipo.FONE);
                } else  if (EProdutoTipo.MULTI.getDescricao().equals(pt.getDescricao())) {
                    eprodutosTipos.add(EProdutoTipo.MULTI);
                } else  if (EProdutoTipo.INTERNET.getDescricao().equals(pt.getDescricao())) {
                    eprodutosTipos.add(EProdutoTipo.INTERNET);
                }
            }

            if (checkBoxAdicionaTv.getVisibility() == View.VISIBLE
                    && checkBoxAdicionaTv.isChecked()) {
                eprodutosTipos.add(EProdutoTipo.TV);
            }

            if (checkBoxAdicionaInternet.getVisibility() == View.VISIBLE
                    && checkBoxAdicionaInternet.isChecked()) {
                eprodutosTipos.add(EProdutoTipo.INTERNET);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return eprodutosTipos;
    }

    public boolean validarCamposObrigatorios() {
        if (spinnerTipoContrato.getSelectedItemPosition() <= SistemaConstantes.ZERO) {
            UtilActivity.makeShortToast("Selecione o tipo de contrato.", this);
            return false;
        } else {
            tipoContrato = tiposContrato.get(
                    spinnerTipoContrato.getSelectedItemPosition() - SistemaConstantes.UM);
        }

        if (spinnerProduto.getSelectedItemPosition() <= SistemaConstantes.ZERO) {
            UtilActivity.makeShortToast("Selecione o(s) produto(s).", this);
            return false;
        } else {
            combinacaoProdutoTipo = combinacoesProdutoTipo.get(
                    spinnerProduto.getSelectedItemPosition() - SistemaConstantes.UM);
        }

        return true;
    }

    private Venda popularDadosPacote(Venda venda) {

        if (spinnerTipoCliente.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            venda.setTipoCliente(tiposCliente.get(
                    spinnerTipoCliente.getSelectedItemPosition() - SistemaConstantes.UM).getRotulo());
        }

        if (UtilActivity.isValidIntegerValue(txtNumeroContrato)) {
            venda.setNumeroContrato((long) Integer.parseInt(txtNumeroContrato.getText().toString()));
        } else {
            venda.setNumeroContrato(null);
        }

        venda.setTipoContrato(tipoContrato);
        venda.setCombinacaoProdutoTipo(combinacaoProdutoTipo);

        Boolean isFidelidade = checkBoxFidelidade.isChecked();
        EBoolean fidelidade = EBoolean.valueOf(isFidelidade.toString().toUpperCase());
        venda.setFidelidade(fidelidade);

        venda.setObservacao(txtObservacao.getText().toString());
        venda.setPerfilCombo(txtPerfilCombo.getText().toString());

        if (UtilActivity.isValidIntegerValue(txtValorProm)) {
            venda.setOferta(new BigDecimal(
                    UtilMask.unmaskMoeda(txtValorProm.getText().toString())));
        } else {
            venda.setOferta(null);
        }

        if (UtilActivity.isValidIntegerValue(txtValorTotal)) {
            venda.setValor(new BigDecimal(
                    UtilMask.unmaskMoeda(txtValorTotal.getText().toString())));
        } else {
            venda.setValor(null);
        }

        if (UtilActivity.isValidIntegerValue(txtvalorAdesao)) {
            venda.setValorParcelaAdesao(new BigDecimal(
                    UtilMask.unmaskMoeda(txtvalorAdesao.getText().toString())));
        } else {
            venda.setValorParcelaAdesao(null);
        }

        if (UtilActivity.isValidIntegerValue(txtValorParcela)) {
            venda.setValorTotalParcelaAdesao(new BigDecimal(
                    UtilMask.unmaskMoeda(txtValorParcela.getText().toString())));
        } else {
            venda.setValorTotalParcelaAdesao(null);
        }

        if (UtilActivity.isValidIntegerValue(txtnumeroParcelas)) {
            venda.setNumeroParcelasAdesao(Integer.parseInt(txtnumeroParcelas.getText().toString()));
        } else {
            venda.setNumeroParcelasAdesao(null);
        }

        return venda;
    }

    private void popularMock() {
        try {
            venda.setTipoCliente(ETipoCliente.BASE.getRotulo());
            venda.setNumeroContrato(123456789L);
            venda.setTipoContrato(
                    entityManager.select(
                            TipoContrato.class,
                            "SELECT tc.* FROM tipo_contrato tc WHERE tc.descricao = 'INDIVIDUAL' ")
                            .get(SistemaConstantes.ZERO));
            venda.setCombinacaoProdutoTipo(
                    entityManager.select(
                            CombinacaoProdutoTipo.class,
                            "SELECT cpt.* FROM combinacao_produto_tipo cpt WHERE cpt.descricao = 'COMBO MULTI' ")
                            .get(SistemaConstantes.ZERO));
            venda.setFidelidade(EBoolean.TRUE);
            venda.setObservacao("OBSERVAÇÃO VENDA");
            venda.setPerfilCombo("PERFIL COMBO VENDA");
            venda.setValorTotalParcelaAdesao(new BigDecimal("100.00"));
            venda.setValor(new BigDecimal("100.00"));
            venda.setValorParcelaAdesao(new BigDecimal("100.00"));
            venda.setNumeroParcelasAdesao(SistemaConstantes.CINCO);
            venda.setOferta(new BigDecimal("100.00"));
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao popular dados do mock: " + e.getMessage(), this);
        }
    }

    private void popularValoresCamposEntrada() {

        if (venda.getTipoCliente() != null) {
            spinnerTipoCliente.setSelection(
                    tiposCliente.indexOf(
                            ETipoCliente.valueOf(venda.getTipoCliente())) + SistemaConstantes.UM);

            if (ETipoCliente.BASE == ETipoCliente.valueOf(venda.getTipoCliente())) {
                txtNumeroContrato.setVisibility(View.VISIBLE);
            } else {
                txtNumeroContrato.setVisibility(View.GONE);
            }
        }

        if (venda.getNumeroContrato() != null) {
            txtNumeroContrato.setText(venda.getNumeroContrato().toString());
        }

        if (venda.getTipoContrato() != null) {
            for (TipoContrato tipoContrato : tiposContrato) {
                if (tipoContrato.getId().equals(venda.getTipoContrato().getId())) {
                    int position = tiposContrato.indexOf(tipoContrato) + SistemaConstantes.UM;
                    spinnerTipoContrato.setSelection(position);
                    break;
                }
            }
        }

        checkBoxFidelidade.setChecked((venda.getFidelidade() == EBoolean.TRUE));

        txtObservacao.setText(venda.getObservacao());
        txtPerfilCombo.setText(venda.getPerfilCombo());

        if (venda.getOferta() != null) {
            txtValorProm.setText(UtilActivity.formatarMoeda(venda.getOferta().doubleValue()));
        }

        if (venda.getValor() != null) {
            txtValorTotal.setText(UtilActivity.formatarMoeda(venda.getValor().doubleValue()));
        }

        if (venda.getValorParcelaAdesao() != null) {
            txtvalorAdesao.setText(UtilActivity.formatarMoeda(venda.getValorParcelaAdesao().doubleValue()));
        }

        if (venda.getValorTotalParcelaAdesao() != null) {
            txtValorParcela.setText(UtilActivity.formatarMoeda(venda.getValorTotalParcelaAdesao().doubleValue()));
        }

        if (venda.getNumeroParcelasAdesao() != null) {
            txtnumeroParcelas.setText(venda.getNumeroParcelasAdesao().toString());
        }
    }
}