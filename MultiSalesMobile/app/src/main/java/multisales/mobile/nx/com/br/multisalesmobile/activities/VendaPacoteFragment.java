package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

/**
 * Created by eric on 27-02-2015.
 */
public class VendaPacoteFragment extends Fragment{

    private RelativeLayout relativeLayout;
    private Venda venda;
    private TextView txtTipoClienteTV;
    private TextView txtnumeroContratoTV;
    private TextView txtTipoComtratoTv;
    private TextView txtProdutoTv;
    private TextView txtFidelidade;
    private TextView txtPerfilComboTV;
    private TextView txtValorTotal;
    private TextView txtValorPromocional;
    private TextView txtValorAdesao;
    private TextView txtNumeroParcelas;
    private TextView txtValorParcelas;
    private TextView txtObservacoes;
    private RelativeLayout txtNumeroContratoRL;
    private DecimalFormat decimalFormat;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decimalFormat = new DecimalFormat("#,##0.00");
        this.venda = ((VendaDetalhesActivity) getActivity()).getVenda();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        relativeLayout= (RelativeLayout) inflater.inflate(R.layout.fragment_venda_pacote, container, false);
        criarCamposEntrada();
        return relativeLayout;
    }

    public void criarCamposEntrada(){
        txtTipoClienteTV            = (TextView) relativeLayout.findViewById(R.id.tipoClienteTV);
        if(venda.getTipoCliente() != null
                && !venda.getTipoCliente().isEmpty()){
            txtTipoClienteTV.setText(venda.getTipoCliente());
        } else {
            txtTipoClienteTV.setText("N/D");
        }

        txtnumeroContratoTV         = (TextView) relativeLayout.findViewById(R.id.numeroContratoTV);
        if(venda.getNumeroContrato() != null) {
            txtnumeroContratoTV.setText(venda.getNumeroContrato().toString());
        } else {
            txtNumeroContratoRL = (RelativeLayout) relativeLayout.findViewById(R.id.numeroContratoRL);
            txtNumeroContratoRL.setVisibility(View.GONE);
        }

        txtTipoComtratoTv           = (TextView) relativeLayout.findViewById(R.id.tipoContratoTV);
        if(venda.getTipoContrato() != null){
            txtTipoComtratoTv.setText(venda.getTipoContrato().getDescricao());
        } else {
            txtTipoComtratoTv.setText("N/D");
        }

        txtProdutoTv                = (TextView) relativeLayout.findViewById(R.id.produtoTV);
        if(venda.getCombinacaoProdutoTipo() != null){
            txtProdutoTv.setText(venda.getCombinacaoProdutoTipo().getDescricao());
        } else {
            txtProdutoTv.setText("N/D");
        }

        txtFidelidade               = (TextView) relativeLayout.findViewById(R.id.estadoCivilTV);
        if(venda.getFidelidade() != null) {
            String fidelidade = venda.getFidelidade().toString();
            if(venda.getFidelidade().toString() == "FALSE"){
                txtFidelidade.setText("NÃO");
            } else {
                txtFidelidade.setText("SIM");
            }
        } else {
            txtFidelidade.setText("N/D");
        }

        txtPerfilComboTV            = (TextView) relativeLayout.findViewById(R.id.perfilComboTV);
        if(venda.getPerfilCombo() != null
                && !venda.getPerfilCombo().isEmpty()){
            txtPerfilComboTV.setText(venda.getPerfilCombo());
        } else {
            txtPerfilComboTV.setText("N/D");
        }

        txtValorTotal               = (TextView) relativeLayout.findViewById(R.id.cpfCnpjTV);
        if(venda.getValor() != null
                && !venda.getValor().toString().isEmpty()) {
            txtValorTotal.setText(UtilActivity.formatarMoeda(venda.getValor().doubleValue()));
        } else {
            txtValorTotal.setText("N/D");
        }

        txtValorPromocional         = (TextView) relativeLayout.findViewById(R.id.valorPromocionalTV);
        if(venda.getOferta() != null
                && !venda.getOferta().toString().isEmpty()){
            txtValorPromocional.setText(UtilActivity.formatarMoeda(venda.getOferta().doubleValue()));
        } else {
            txtValorPromocional.setText("N/D");
        }

        txtValorAdesao              = (TextView) relativeLayout.findViewById(R.id.valorAdesaoTV);
        if(venda.getValorTotalParcelaAdesao() != null){
            txtValorAdesao.setText(UtilActivity.formatarMoeda(venda.getValorTotalParcelaAdesao().doubleValue()));
        } else {
            txtValorAdesao.setText("N/D");
        }

        txtNumeroParcelas           = (TextView) relativeLayout.findViewById(R.id.numeroParcelasAdesaoTV);
        if(venda.getNumeroParcelasAdesao() != null
                && venda.getNumeroParcelasAdesao() > 0){
            txtNumeroParcelas.setText(venda.getNumeroParcelasAdesao().toString());
        } else {
            txtNumeroParcelas.setText("N/D");
        }

        txtValorParcelas            = (TextView) relativeLayout.findViewById(R.id.valorParcelasTV);
        if(venda.getValorTotalParcelaAdesao() != null){
            txtValorParcelas.setText(UtilActivity.formatarMoeda(venda.getValorTotalParcelaAdesao().doubleValue()));
        } else {
            txtValorParcelas.setText("N/D");
        }

        txtObservacoes              = (TextView) relativeLayout.findViewById(R.id.observacoesTV);
        if(venda.getObservacao() != null
                && !venda.getObservacao().isEmpty()){
            txtObservacoes.setText(venda.getObservacao());
        } else {
            txtObservacoes.setText("N/D");
        }


    }

}
