package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Concorrente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoMigracao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoNaoVenda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaMotivoNaoVenda;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.OnItemSelectListener;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class VendaPacoteNaoVendaActivity extends ActionBarActivity implements OnItemSelectListener {

    private EntityManager entityManager;

    //Extras
    private Integer idVenda;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private boolean edicao;
    private boolean isMock = false;
    private boolean isVoltar;
    private HashSet<EVendaFluxo> telasAnteriores;

    private MotivoNaoVenda motivoConcorrencia;
    private Concorrente concorrente;
    private MotivoMigracao motivoMigracao;

    private List<MotivoNaoVenda> motivosNaoVenda;
    private ItemSelecionavelAdapter<MotivoNaoVenda> listAdapter;

    private List<Concorrente> concorrentes;
    private List<MotivoMigracao> motivosMigracao;

    private ListView motivoNaoVendaLV;
    private Spinner spinnerConcorrentes;
    private Spinner spinnerMotivosMigracao;
    private LinearLayout concorrenteLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_pacote_nao_venda);
        entityManager = new EntityManager(this);
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados =
                (HashSet<EProdutoTipo>) getIntent().getExtras().get("produtosTipo");
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);
        edicao = getIntent().getBooleanExtra("edicao", false);

        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.MOTIVOS_NAO_VENDA);

        criarCamposEntrada();
        carregarMotivosNaoVenda();
        carregarConcorrentes();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            Venda venda = entityManager.getById(Venda.class, idVenda);
            entityManager.initialize(venda.getCliente());

            List<VendaMotivoNaoVenda> vendaMotivosNaoVendas =
                    entityManager.select(
                            VendaMotivoNaoVenda.class,
                            "SELECT * FROM venda_motivo_nao_venda WHERE _venda = " + idVenda);

            if (vendaMotivosNaoVendas != null && !vendaMotivosNaoVendas.isEmpty() && !edicao) {
                isVoltar = true;
            }

            if (edicao || isMock || isVoltar) {
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO: " + e.getMessage(), this);
        }
    }

    private void popularValoresCamposEntrada() throws DataBaseException {
        if (isMock) {
            for (MotivoNaoVenda motivo : motivosNaoVenda) {
                if (motivo.getDescricao().contains("CONCORRENCIA")) {
                    motivo.setSelecionado(true);
                    spinnerConcorrentes.setSelection(SistemaConstantes.UM);
                    break;
                }
            }
            alterarVisibilidadeCamposConcorrencia(View.VISIBLE);
            listAdapter = new ItemSelecionavelAdapter<MotivoNaoVenda>(this, motivosNaoVenda);
            listAdapter.setOnItemSelectListener(this);
            motivoNaoVendaLV.setAdapter(listAdapter);
        } else if (edicao || isVoltar) {
            List<MotivoNaoVenda> motivosNaoVendaSelecionados =
                    entityManager.select(MotivoNaoVenda.class,
                    "SELECT mnv.* " +
                    "FROM motivo_nao_venda mnv " +
                    "INNER JOIN venda_motivo_nao_venda vmnv " +
                    "ON vmnv._motivo_nao_venda = mnv.id AND vmnv._venda = "
                    + idVenda);

            boolean isConcorrencia = false;
            for (MotivoNaoVenda motivo : motivosNaoVenda) {
                for (MotivoNaoVenda motivoSelecionado : motivosNaoVendaSelecionados) {
                    if (motivo.getId().equals(motivoSelecionado.getId())) {
                        motivo.setSelecionado(true);
                        if (motivoSelecionado.getDescricao().contains("CONCORRENCIA")) {
                            isConcorrencia = true;
                        }
                    }
                }
            }

            if (isConcorrencia) {
                VendaMotivoNaoVenda vendaMotivoNaoVenda = entityManager.select(
                        VendaMotivoNaoVenda.class,
                        "SELECT * FROM venda_motivo_nao_venda WHERE _venda = "
                                + idVenda + "AND _concorrente IS NOT NULL").get(SistemaConstantes.ZERO);

                motivoMigracao = vendaMotivoNaoVenda.getMotivoMigracao();

                for (Concorrente c : concorrentes) {
                    if (c.getId().equals(vendaMotivoNaoVenda.getConcorrenteMigracao().getId())) {
                        spinnerConcorrentes.setSelection(concorrentes.indexOf(c) + SistemaConstantes.UM);
                        break;
                    }
                }

                alterarVisibilidadeCamposConcorrencia(View.VISIBLE);
            }

            listAdapter = new ItemSelecionavelAdapter<MotivoNaoVenda>(this, motivosNaoVenda);
            listAdapter.setOnItemSelectListener(this);
            motivoNaoVendaLV.setAdapter(listAdapter);
        }
    }

    private void criarCamposEntrada() {
        motivoNaoVendaLV        = (ListView) findViewById(R.id.motivoNaoVendaLV);
        spinnerConcorrentes     = (Spinner) findViewById(R.id.spinnerConcorrentes);
        spinnerMotivosMigracao  = (Spinner) findViewById(R.id.spinnerMotivosMigracao);
        concorrenteLL           = (LinearLayout) findViewById(R.id.concorrenteLL);
    }

    private void alterarVisibilidadeCamposConcorrencia(int visibility) {
        concorrenteLL.setVisibility(visibility);
    }

    private void carregarMotivosNaoVenda() {
        try {
            motivosNaoVenda = entityManager.select(
                    MotivoNaoVenda.class,
                    "SELECT mnv.* FROM motivo_nao_venda mnv INNER JOIN produto_tipo pt ON pt.id = mnv._produto_tipo AND pt.descricao = 'MULTI'");
            listAdapter = new ItemSelecionavelAdapter<MotivoNaoVenda>(this, motivosNaoVenda);
            listAdapter.setOnItemSelectListener(this);
            motivoNaoVendaLV.setAdapter(listAdapter);
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Ocorreu um ERRO ao listar motivos nao venda!", getApplicationContext());
        }
    }

    private void carregarConcorrentes() {
        try {
            concorrentes = entityManager.getByWhere(Concorrente.class, "situacao = 'ATIVO'", "descricao");
            List<String> descricoesConcorrentes = new ArrayList<>();
            descricoesConcorrentes.add(UtilActivity.SELECIONE);
            for (Concorrente c : concorrentes) {
                descricoesConcorrentes.add(c.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerConcorrentes, descricoesConcorrentes);
            spinnerConcorrentes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    List<String> descricoesMotivosMigracao = new ArrayList<String>();

                    if (position > SistemaConstantes.ZERO) {
                        try {
                            motivosMigracao = entityManager.getByWhere(MotivoMigracao.class, "situacao = 'ATIVO'", "descricao");
                            descricoesMotivosMigracao.add(UtilActivity.SELECIONE);
                            for (MotivoMigracao motivo : motivosMigracao) {
                                descricoesMotivosMigracao.add(motivo.getDescricao());
                            }
                            spinnerMotivosMigracao.setEnabled(true);
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                            UtilActivity.makeShortToast("ERRO ao listar Motivos de Migração: " + e.getMessage(), getApplicationContext());
                        }
                    } else {
                        descricoesMotivosMigracao.add("SELECIONE O CONCORRENTE");
                        spinnerMotivosMigracao.setEnabled(false);
                    }

                    ArrayAdapter adapter = new ArrayAdapter<> (VendaPacoteNaoVendaActivity.this, android.R.layout.simple_spinner_item, descricoesMotivosMigracao);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerMotivosMigracao.setAdapter(adapter);

                    if (isMock) {
                        spinnerMotivosMigracao.setSelection(SistemaConstantes.UM);
                    }

                    if (edicao) {
                        if (motivoMigracao != null) {
                            for (MotivoMigracao motivo : motivosMigracao) {
                                if (motivo.getId().equals(motivoMigracao.getId())) {
                                    spinnerMotivosMigracao.setSelection(motivosMigracao.indexOf(motivo) + SistemaConstantes.UM);
                                    break;
                                }
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            UtilActivity.makeShortToast("ERRO ao listar Concorrentes: " + e.getMessage(), this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_pacote_nao_venda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            try {
                Intent activityAnterior = new Intent();
                activityAnterior.putExtra("idVenda", idVenda);
                activityAnterior.putExtra("edicao", edicao);
                activityAnterior.putExtra("isMock", isMock);
                activityAnterior.putExtra("isVoltar", true);
                activityAnterior.putExtra("isMock", isMock);
                activityAnterior.putExtra("telasAnteriores", telasAnteriores);
                Class<?> cls = VendaPacoteActivity.class;
                activityAnterior.setComponent(new ComponentName(this, cls));
                startActivity(activityAnterior);
                VendaPacoteNaoVendaActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (id == R.id.action_avancar) {
            avancar();
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaPacoteNaoVendaActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void avancar() {

        if (!salvarMotivosNaoVenda()) {
            return;
        }

        Intent intentProduto = new Intent();
        intentProduto.putExtra("idVenda", idVenda);
        intentProduto.putExtra("produtosTipo", produtosTiposSelecionados);
        intentProduto.putExtra("edicao", edicao);
        intentProduto.putExtra("isMock", isMock);
        intentProduto.putExtra("telasAnteriores", telasAnteriores);

        Class<?> cls = null;

        if (produtosTiposSelecionados.contains(EProdutoTipo.MULTI)) {
            cls = VendaCelularActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.TV)) {
            cls = VendaTvActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.FONE)) {
            cls = VendaFoneActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.INTERNET)) {
            cls = VendaInternetActivity.class;
        }

        intentProduto.setComponent(new ComponentName(this, cls));
        startActivity(intentProduto);
        VendaPacoteNaoVendaActivity.this.finish();
    }

    private boolean salvarMotivosNaoVenda() {
        try {

            entityManager.executeNativeQuery("DELETE FROM venda_motivo_nao_venda WHERE _venda = " + idVenda);

            int qtdItens = listAdapter.getCount();
            boolean existeMotivoSelecionado = false;

            if (motivoConcorrencia != null) {
                if (UtilActivity.isValidSpinnerValue(spinnerConcorrentes)
                        && UtilActivity.isValidSpinnerValue(spinnerMotivosMigracao)) {
                    concorrente = concorrentes.get(spinnerConcorrentes.getSelectedItemPosition() - SistemaConstantes.UM);
                    motivoMigracao = motivosMigracao.get(spinnerMotivosMigracao.getSelectedItemPosition() - SistemaConstantes.UM);
                } else {
                    UtilActivity.makeShortToast("Selecione o Concorrente e o Motivo de Migração.", this);
                    return false;
                }
            }

            for (int i = SistemaConstantes.ZERO; i < qtdItens; i++) {
                if (listAdapter.getItem(i).isSelecionado()) {
                    VendaMotivoNaoVenda vmnv = new VendaMotivoNaoVenda(new Venda(idVenda), listAdapter.getItem(i));
                    if (listAdapter.getItem(i).getDescricao().contains("CONCORRENCIA")) {
                        vmnv.setConcorrenteMigracao(concorrente);
                        vmnv.setMotivoMigracao(motivoMigracao);
                    }
                    existeMotivoSelecionado = true;
                }
            }

            if (!existeMotivoSelecionado) {
                UtilActivity.makeShortToast("Selecione ao menos um motivo de não venda.", this);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao salvar motivo(s) não venda: " + e.getMessage(), this);
            return false;
        }
        return true;
    }

    @Override
    public void onItemSelect(int position) {
        boolean encontrou = false;
        for (MotivoNaoVenda motivoNaoVenda : listAdapter.getItens()) {
            if (motivoNaoVenda.isSelecionado()) {
                if (motivoNaoVenda.getDescricao().contains("CONCORRENCIA")) {
                    motivoConcorrencia = motivoNaoVenda;
                    encontrou = true;
                    alterarVisibilidadeCamposConcorrencia(View.VISIBLE);
                    break;
                }
            }
        }
        if (!encontrou) {
            alterarVisibilidadeCamposConcorrencia(View.GONE);
            motivoConcorrencia = null;
        }
    }
}
