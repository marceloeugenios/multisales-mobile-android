package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaFormaPagamento;

/**
 * Created by eric on 27-02-2015.
 */
public class VendaPagamentoFragment extends Fragment{

    private RelativeLayout relativeLayout;
    private Venda venda;
    private RelativeLayout rlPagamentoLayout;
    private RelativeLayout rlBancoLayout;
    private RelativeLayout rlFaturaLayout;
    private RelativeLayout rlInstalacaoLayout;
    private TextView txtVencimentoFaturaTV;
    private VendaFormaPagamento vendaFormaPagamento;
    private TextView txtFormaPagamentoTV;
    private TextView txtMultaTV;
    private TextView txtMidiaTV;
    private TextView txtBancoTV;
    private TextView txtSomenteEmailTV;
    private TextView txtEmailTV;
    private TextView txtCpfNotaTV;
    private TextView txtAutorizaPublicidadeTV;
    private TextView txtDataInstalacaoTV;
    private TextView txtPeriodoInstalacaoTV;
    private TextView agenciaTV;
    private TextView contaTV;
    private TextView nomeTitularTV;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.venda = ((VendaDetalhesActivity) getActivity()).getVenda();
        this.vendaFormaPagamento = ((VendaDetalhesActivity) getActivity()).getVenda().getVendaFormaPagamento();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        relativeLayout= (RelativeLayout) inflater.inflate(R.layout.fragment_venda_pagamento, container, false);
        //relative Layout pagamento
        rlPagamentoLayout = (RelativeLayout) relativeLayout.findViewById(R.id.pagamentoRL);
        //relative Layout  banco
        rlBancoLayout = (RelativeLayout) relativeLayout.findViewById(R.id.informacaoBancoRL);
        //relative Layout fatura
        rlFaturaLayout = (RelativeLayout) relativeLayout.findViewById(R.id.informacaoFaturaRL);
        //relative Layout instalacao
        rlInstalacaoLayout = (RelativeLayout) relativeLayout.findViewById(R.id.instalacaoRL);

        criarCamposEntrada();
        return relativeLayout;
    }

    public void criarCamposEntrada(){

        //campos pagmento
        txtVencimentoFaturaTV = (TextView) rlPagamentoLayout.findViewById(R.id.vencimentoFaturaTV);
        if (vendaFormaPagamento.getVencimentoFatura() != null) {
            txtVencimentoFaturaTV.setText(vendaFormaPagamento.getVencimentoFatura().getVencimento().toString());
        } else {
            txtVencimentoFaturaTV.setText("N/D");
        }

        txtFormaPagamentoTV = (TextView) rlPagamentoLayout.findViewById(R.id.formaPagamentoTV);
        if(vendaFormaPagamento.getVendaFormaPagamentoBanco() != null) {
            txtFormaPagamentoTV.setText("DEBITO C/C");
        } else {
            txtFormaPagamentoTV.setText("BOLETO");
            rlBancoLayout.setVisibility(View.GONE);
        }

        txtMultaTV = (TextView) rlPagamentoLayout.findViewById(R.id.multaTV);
        if (vendaFormaPagamento.getInformouSobreMulta().toString() == "FALSE") {
            txtMultaTV.setText("NÃO");
        } else {
            txtMultaTV.setText("SIM");
        }

        txtMidiaTV = (TextView) rlPagamentoLayout.findViewById(R.id.midiaTV);
        if (venda.getMidia() != null) {
            txtMidiaTV.setText(venda.getMidia().getDescricao());
        } else {
            txtMidiaTV.setText("N/D");
        }

        //campos informacoes do banco

        if (vendaFormaPagamento.getVendaFormaPagamentoBanco() != null ) {

            txtBancoTV = (TextView) rlBancoLayout.findViewById(R.id.bancoTV);
            if (vendaFormaPagamento.getVendaFormaPagamentoBanco().getBanco().getDescricao() != null){
                txtBancoTV.setText(vendaFormaPagamento.getVendaFormaPagamentoBanco().getBanco().getDescricao());
            }

            this.agenciaTV = (TextView) rlBancoLayout.findViewById(R.id.agenciaTV);
            this.agenciaTV.setText(vendaFormaPagamento.getVendaFormaPagamentoBanco().getAgencia());

            this.contaTV = (TextView) rlBancoLayout.findViewById(R.id.contaTV);
            this.contaTV.setText(vendaFormaPagamento.getVendaFormaPagamentoBanco().getConta());

            this.nomeTitularTV = (TextView) rlBancoLayout.findViewById(R.id.nomeTitularTV);
            this.nomeTitularTV.setText(vendaFormaPagamento.getVendaFormaPagamentoBanco().getNomeTitular());
        } else {
            rlBancoLayout.setVisibility(View.GONE);
        }


        //FATURA

        txtSomenteEmailTV = (TextView) rlFaturaLayout.findViewById(R.id.somenteEmailTV);
        if (vendaFormaPagamento.getFaturaSomentePorEmail().toString() == "FALSE") {
            txtSomenteEmailTV.setText("NÃO");
        } else {
            txtSomenteEmailTV.setText("SIM");
        }

        txtEmailTV = (TextView) rlFaturaLayout.findViewById(R.id.emailTV);
        if (venda.getCliente().getEmail() != null
                && !venda.getCliente().getEmail().isEmpty()){
            txtEmailTV.setText(venda.getCliente().getEmail());
        } else {
            txtEmailTV.setText("N/D");
        }

        txtCpfNotaTV = (TextView) rlFaturaLayout.findViewById(R.id.cpfNotaTV);
        if (vendaFormaPagamento.getCpfNota().toString() == "FALSE") {
            txtCpfNotaTV.setText("NÃO");
        } else {
            txtCpfNotaTV.setText("SIM");
        }

        txtAutorizaPublicidadeTV = (TextView)  rlFaturaLayout.findViewById(R.id.nomePublicidadeTV);
        if (vendaFormaPagamento.getReceberCampanhaPublicitaria().toString() == "FALSE" ) {
            txtAutorizaPublicidadeTV.setText("NÃO");
        } else {
            txtAutorizaPublicidadeTV.setText("SIM");
        }

       //DATA INSTALAÇÃO

        txtDataInstalacaoTV = (TextView) rlInstalacaoLayout.findViewById(R.id.dataInstalacaoTV);
        if (venda.getDataInstalacao() != null) {
            SimpleDateFormat sdfDataEmissao = new SimpleDateFormat("dd/MM/yyyy");
            String dataInstalacao = sdfDataEmissao.format(venda.getDataInstalacao().getTime());
            txtDataInstalacaoTV.setText(dataInstalacao);
        } else {
            txtDataInstalacaoTV.setText("N/D");
        }

        txtPeriodoInstalacaoTV = (TextView) rlInstalacaoLayout.findViewById(R.id.periodoInstalacaoTV);
        if(venda.getPeriodoInstalacao() != null) {
            txtPeriodoInstalacaoTV.setText(venda.getPeriodoInstalacao().getDescricao());
        } else {
            txtPeriodoInstalacaoTV.setText("N/D");
        }

    }

}
