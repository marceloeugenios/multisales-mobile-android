package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.BandeiraSistema;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Concorrente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoMigracao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTv;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTvPontoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTvProdutoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaTvActivity extends ActionBarActivity implements View.OnFocusChangeListener, View.OnClickListener {

    private EntityManager entityManager;

    private Integer idVenda;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private SimpleDateFormat formatoData;

    private Venda venda;
    private VendaTv vendaTv;

    private List<BandeiraSistema> bandeirasSistema;
    private List<Produto> produtosTv;
    private List<Concorrente> concorrentes;
    private List<MotivoMigracao> motivosMigracao;
    private Produto produtoTvSelecionado;
    private Concorrente concorrenteSelecionado;
    private MotivoMigracao motivoMigracaoSelecionado;

    private Spinner spinnerBandeirasSistema;
    private Spinner spinnerProdutosTv;
    private Spinner spinnerConcorrentes;
    private Spinner spinnerMotivosMigracao;
    private CheckBox checkBoxAdicionaisTv;
    private EditText txtDegustacao;
    private EditText txtTaxaInstalacao;
    private EditText txtValorPromocional;
    private EditText txtValorPosPromocao;
    private EditText txtVigenciaPromocao;
    private EditText txtDataInstalacao;

    private DatePickerDialog dataVigenciaDPD;
    private DatePickerDialog dataInstalacaoDPD;

    private HashSet<EVendaFluxo> telasAnteriores;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_tv);
        vendaTv = new VendaTv();
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>) getIntent().getExtras().get("produtosTipo");
        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.TV);
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);
        formatoData = new SimpleDateFormat("dd/MM/yyyy");

        criarCamposEntrada();
        entityManager = new EntityManager(this);

        carregarBandeirasSistema();
        carregarConcorrentes();
        setDateTimeField();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            venda = entityManager.getById(Venda.class, idVenda);

            if (venda.getVendaTv() != null
                    && venda.getVendaTv().getId() != null
                    && !edicao) {
                isVoltar = true;
            }

            if (venda.getVendaTv() != null
                    && venda.getVendaTv().getId() != null
                    && (edicao || isVoltar)) {
                vendaTv = entityManager.getById(VendaTv.class, venda.getVendaTv().getId());
                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
                popularValoresCamposEntrada();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }
    }

    private void setDateTimeField() {
        final Calendar dataHoraAtual = Calendar.getInstance();

        txtDataInstalacao.setOnFocusChangeListener(this);
        txtDataInstalacao.setOnClickListener(this);

        dataInstalacaoDPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                vendaTv.setDataInstalacao(Calendar.getInstance());
                vendaTv.getDataInstalacao().set(year, monthOfYear, dayOfMonth);
                txtDataInstalacao.setText(formatoData.format(vendaTv.getDataInstalacao().getTime()));
            }

        },dataHoraAtual.get(Calendar.YEAR), dataHoraAtual.get(Calendar.MONTH), dataHoraAtual.get(Calendar.DAY_OF_MONTH));

        txtVigenciaPromocao.setOnFocusChangeListener(this);
        txtVigenciaPromocao.setOnClickListener(this);

        dataVigenciaDPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                vendaTv.setVigenciaPromocao(Calendar.getInstance());
                vendaTv.getVigenciaPromocao().set(year, monthOfYear, dayOfMonth);
                txtVigenciaPromocao.setText(formatoData.format(vendaTv.getVigenciaPromocao().getTime()));
            }
        },dataHoraAtual.get(Calendar.YEAR), dataHoraAtual.get(Calendar.MONTH), dataHoraAtual.get(Calendar.DAY_OF_MONTH));
    }

    private void popularMock() throws Exception {
        vendaTv = new VendaTv();
        isMock = true;
        vendaTv.setProduto(
                entityManager.select(Produto.class,
                "SELECT p.* " +
                        "FROM produto p " +
                        "INNER JOIN produto_tipo pt " +
                        "ON pt.id = p._produto_tipo " +
                        "WHERE pt.descricao = '" + EProdutoTipo.TV.getDescricao() + "' " +
                        "AND p.situacao = 'ATIVO' ")
                .get(SistemaConstantes.ZERO));
        vendaTv.setDegustacao("DEGUSTAÇÃO TV");
        vendaTv.setConcorrenteMigracao(
                entityManager.select(Concorrente.class,
                        "SELECT * FROM concorrente WHERE situacao = 'ATIVO'")
                        .get(SistemaConstantes.ZERO));
        vendaTv.setMotivoMigracao(
                entityManager.select(MotivoMigracao.class,
                        "SELECT * FROM motivo_migracao WHERE situacao = 'ATIVO'")
                        .get(SistemaConstantes.ZERO));

        vendaTv.setTaxaInstalacao(new BigDecimal(SistemaConstantes.CEM));
        vendaTv.setValorPromocional(new BigDecimal(SistemaConstantes.OITENTA));
        vendaTv.setValorPosPromocao(new BigDecimal(SistemaConstantes.CENTO_VINTE));
        vendaTv.setVigenciaPromocao(Calendar.getInstance());
        vendaTv.getVigenciaPromocao().set(2015, 12, 31);
        vendaTv.setDataInstalacao(Calendar.getInstance());
    }

    private void popularValoresCamposEntrada() throws Exception {

        if (vendaTv.getProduto() != null) {
            BandeiraSistema bandeira = entityManager.select(BandeiraSistema.class,
                    "SELECT bs.* FROM bandeira_sistema bs " +
                            "INNER JOIN bandeira_sistema_produto bsp " +
                            "ON bsp._bandeira_sistema = bs.id " +
                            "AND bsp._produto = " + vendaTv.getProduto().getId())
                    .get(SistemaConstantes.ZERO);

            for (BandeiraSistema bandeiraSistema : bandeirasSistema) {
                if (bandeira.getId().equals(bandeiraSistema.getId())) {
                    spinnerBandeirasSistema
                            .setSelection(bandeirasSistema
                                    .indexOf(bandeiraSistema) + SistemaConstantes.UM);
                }
            }
        }

        if (vendaTv.getConcorrenteMigracao() != null) {
            for (Concorrente concorrente : concorrentes) {
                if (concorrente.getId().equals(vendaTv.getConcorrenteMigracao().getId())) {
                    spinnerConcorrentes.setSelection(concorrentes.indexOf(concorrente) + SistemaConstantes.UM);
                    break;
                }
            }
        }

        List<VendaTvProdutoAdicional> produtosAdicionais = entityManager
                .select(VendaTvProdutoAdicional.class,
                "SELECT * FROM venda_tv_produto_adicional WHERE _venda_tv = " + vendaTv.getId());
        List<VendaTvPontoAdicional> pontosAdicionais = entityManager
                .select(VendaTvPontoAdicional.class,
                        "SELECT * FROM venda_tv_ponto_adicional WHERE _venda_tv = " + vendaTv.getId());

        if ((produtosAdicionais != null && !produtosAdicionais.isEmpty())
                || (pontosAdicionais != null && !pontosAdicionais.isEmpty())
                || (isMock)) {
            checkBoxAdicionaisTv.setChecked(true);
        }

        txtDegustacao.setText(vendaTv.getDegustacao());

        if (vendaTv.getTaxaInstalacao() != null) {
            txtTaxaInstalacao.setText(UtilActivity.formatarMoeda(vendaTv.getTaxaInstalacao().doubleValue()));
        }

        if (vendaTv.getValorPromocional() != null) {
            txtValorPromocional.setText(UtilActivity.formatarMoeda(vendaTv.getValorPromocional().doubleValue()));
        }

        if (vendaTv.getValorPosPromocao() != null) {
            txtValorPosPromocao.setText(UtilActivity.formatarMoeda(vendaTv.getValorPosPromocao().doubleValue()));
        }

        if (vendaTv.getVigenciaPromocao() != null) {
            txtVigenciaPromocao.setText(formatoData.format(vendaTv.getVigenciaPromocao().getTime()));
        }

        if (vendaTv.getDataInstalacao() != null) {
            txtDataInstalacao.setText(formatoData.format(vendaTv.getDataInstalacao().getTime()));
        }
    }

    private void criarCamposEntrada() {
        spinnerBandeirasSistema = (Spinner) findViewById(R.id.spinnerBandeirasSistema);
        spinnerProdutosTv       = (Spinner) findViewById(R.id.spinnerProdutosTv);
        spinnerConcorrentes     = (Spinner) findViewById(R.id.spinnerConcorrentes);
        spinnerMotivosMigracao  = (Spinner) findViewById(R.id.spinnerMotivosMigracao);
        checkBoxAdicionaisTv    = (CheckBox) findViewById(R.id.checkBoxAdicionaisTv);
        txtDegustacao           = (EditText) findViewById(R.id.txtDegustacao);
        txtTaxaInstalacao       = (EditText) findViewById(R.id.txtTaxaInstalacao);
        UtilMask.setMascaraMoeda(txtTaxaInstalacao, 10);
        txtValorPromocional     = (EditText) findViewById(R.id.txtValorPromocional);
        UtilMask.setMascaraMoeda(txtValorPromocional, 10);
        txtValorPosPromocao     = (EditText) findViewById(R.id.txtValorPosPromocao);
        UtilMask.setMascaraMoeda(txtValorPosPromocao, 10);
        txtVigenciaPromocao     = (EditText) findViewById(R.id.txtVigenciaPromocao);
        txtDataInstalacao       = (EditText) findViewById(R.id.txtDataInstalacao);
    }

    private void carregarBandeirasSistema() {
        try {
            //bandeirasSistema = entityManager.getByWhere(BandeiraSistema.class, "situacao = 'ATIVO'", "descricao");
            bandeirasSistema = entityManager.getAll(BandeiraSistema.class);
            List<String> descricoesBandeirasSistema = new ArrayList<>();
            descricoesBandeirasSistema.add(UtilActivity.SELECIONE);
            for (BandeiraSistema bs : bandeirasSistema) {
                descricoesBandeirasSistema.add(bs.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerBandeirasSistema, descricoesBandeirasSistema);
            spinnerBandeirasSistema.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    List<String> descricoesProdutosTv = new ArrayList<String>();

                    if (position > SistemaConstantes.ZERO) {
                        BandeiraSistema bandeiraSelecionada =
                                bandeirasSistema.get(spinnerBandeirasSistema.getSelectedItemPosition() - SistemaConstantes.UM);

                        try {
                            String sql =
                                    "SELECT p.* " +
                                    "FROM produto p " +
                                    "INNER JOIN produto_tipo pt " +
                                        "ON pt.id = p._produto_tipo " +
                                    "INNER JOIN bandeira_sistema_produto bsp " +
                                        "ON bsp._produto = p.id " +
                                    "INNER JOIN bandeira_sistema bs " +
                                        "ON bs.id = bsp._bandeira_sistema " +
                                    "WHERE bs.id = " + bandeiraSelecionada.getId() + " " +
                                    "AND pt.descricao = 'TV' " +
                                    "AND p.situacao = 'ATIVO' " +
                                    "ORDER BY p.descricao";
                            produtosTv = entityManager.select(Produto.class, sql);
                            descricoesProdutosTv.add(UtilActivity.SELECIONE);
                            for (Produto produto : produtosTv) {
                                descricoesProdutosTv.add(produto.getDescricao());
                            }
                            spinnerProdutosTv.setEnabled(true);
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        descricoesProdutosTv.add("SELECIONE A BANDEIRA");
                        spinnerProdutosTv.setEnabled(false);
                    }

                    ArrayAdapter adapter = new ArrayAdapter<> (VendaTvActivity.this, android.R.layout.simple_spinner_item, descricoesProdutosTv);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerProdutosTv.setAdapter(adapter);

                    if (edicao || isMock || isVoltar) {
                        if (vendaTv.getProduto() != null) {
                            for (Produto produto : produtosTv) {
                                if (produto.getId().equals(vendaTv.getProduto().getId())) {
                                    spinnerProdutosTv.setSelection(
                                            produtosTv.indexOf(produto) + SistemaConstantes.UM);
                                    vendaTv.setProduto(null);
                                    break;
                                }
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            UtilActivity.makeShortToast("ERRO ao listar bandeiras: " + e.getMessage(), this);
        }
    }

    private void carregarConcorrentes() {
        try {
            concorrentes = entityManager.getByWhere(Concorrente.class, "situacao = 'ATIVO'", "descricao");
            List<String> descricoesConcorrentes = new ArrayList<>();
            descricoesConcorrentes.add(UtilActivity.SELECIONE);
            for (Concorrente c : concorrentes) {
                descricoesConcorrentes.add(c.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerConcorrentes, descricoesConcorrentes);
            spinnerConcorrentes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    List<String> descricoesMotivosMigracao = new ArrayList<String>();

                    if (position > SistemaConstantes.ZERO) {
                        try {
                            motivosMigracao = entityManager.getByWhere(MotivoMigracao.class, "situacao = 'ATIVO'", "descricao");
                            descricoesMotivosMigracao.add(UtilActivity.SELECIONE);
                            for (MotivoMigracao motivo : motivosMigracao) {
                                descricoesMotivosMigracao.add(motivo.getDescricao());
                            }
                            spinnerMotivosMigracao.setEnabled(true);
                        } catch (DataBaseException e) {
                            e.printStackTrace();
                            UtilActivity.makeShortToast("ERRO ao listar Motivos de Migração: " + e.getMessage(), getApplicationContext());
                        }
                    } else {
                        descricoesMotivosMigracao.add("SELECIONE O CONCORRENTE");
                        spinnerMotivosMigracao.setEnabled(false);
                    }

                    ArrayAdapter adapter = new ArrayAdapter<> (VendaTvActivity.this, android.R.layout.simple_spinner_item, descricoesMotivosMigracao);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerMotivosMigracao.setAdapter(adapter);

                    if (edicao || isMock || isVoltar) {
                        if (vendaTv.getMotivoMigracao() != null) {
                            for (MotivoMigracao motivoMigracao : motivosMigracao) {
                                if (motivoMigracao.getId().equals(vendaTv.getMotivoMigracao().getId())) {
                                    spinnerMotivosMigracao.setSelection(
                                            motivosMigracao.indexOf(motivoMigracao) + SistemaConstantes.UM);
                                    vendaTv.setMotivoMigracao(null);
                                    break;
                                }
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            UtilActivity.makeShortToast("ERRO ao listar Concorrentes: " + e.getMessage(), this);
        }
    }

    public void avancar(View view) {
        if (!validarCamposObrigatorios()) {
            return;
        }
        try {
            vendaTv = entityManager.save(popularDadosVendaTv());

            if (venda.getVendaTv() != null && venda.getVendaTv().getId() != null) {
                deletarVendaTv();

                if (checkBoxAdicionaisTv.isChecked()) {
                    entityManager.executeNativeQuery("UPDATE venda_tv_produto_adicional SET _venda_tv = "
                            + vendaTv.getId()
                            + " WHERE _venda_tv = "
                            + venda.getVendaTv().getId());

                    entityManager.executeNativeQuery("UPDATE venda_tv_ponto_adicional SET _venda_tv = "
                            + vendaTv.getId()
                            + " WHERE _venda_tv = "
                            + venda.getVendaTv().getId());
                } else {
                    entityManager.executeNativeQuery("DELETE FROM venda_tv_produto_adicional "
                            + "WHERE _venda_tv = "
                            + venda.getVendaTv().getId());

                    entityManager.executeNativeQuery("DELETE FROM venda_tv_ponto_adicional "
                            + "WHERE _venda_tv = "
                            + venda.getVendaTv().getId());
                }
            }

            venda.setVendaTv(vendaTv);
            entityManager.atualizar(venda);
            irParaProximaTela();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Erro ao salvar dados da TV: " + e.getMessage(), this);
        }
    }

    private void deletarVendaTv() throws DataBaseException {
        entityManager.executeNativeQuery("DELETE FROM venda_tv WHERE id = " + venda.getVendaTv().getId());
    }

    private boolean validarCamposObrigatorios() {
        if (spinnerProdutosTv.getSelectedItemPosition() < SistemaConstantes.UM) {
            UtilActivity.makeShortToast("Selecione o produto.", this);
            return false;
        } else {
            produtoTvSelecionado = produtosTv
                    .get((spinnerProdutosTv.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        if (spinnerConcorrentes.getSelectedItemPosition() > SistemaConstantes.ZERO
                && spinnerMotivosMigracao.getSelectedItemPosition() < SistemaConstantes.UM) {
            UtilActivity.makeShortToast("Selecione o motivo de migração.", this);
            return false;
        } else if (spinnerConcorrentes.getSelectedItemPosition() > SistemaConstantes.ZERO
                && spinnerMotivosMigracao.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            concorrenteSelecionado = concorrentes
                    .get((spinnerConcorrentes.getSelectedItemPosition() - SistemaConstantes.UM));
            motivoMigracaoSelecionado = motivosMigracao
                    .get((spinnerMotivosMigracao.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        return true;
    }

    private VendaTv popularDadosVendaTv() {
        VendaTv vendaTv = new VendaTv();
        vendaTv.setProduto(produtoTvSelecionado);
        vendaTv.setConcorrenteMigracao(concorrenteSelecionado);
        vendaTv.setMotivoMigracao(motivoMigracaoSelecionado);
        vendaTv.setDegustacao(txtDegustacao.getText().toString());

        if (txtTaxaInstalacao.getText() != null ) {
            vendaTv.setTaxaInstalacao(new BigDecimal(UtilMask.unmaskMoeda(txtTaxaInstalacao.getText().toString())));
        }

        if (txtValorPromocional.getText() != null) {
            vendaTv.setValorPromocional(new BigDecimal(UtilMask.unmaskMoeda(txtValorPromocional.getText().toString())));
        }

        if (txtValorPosPromocao.getText() != null ) {
            vendaTv.setValorPosPromocao(new BigDecimal(UtilMask.unmaskMoeda(txtValorPosPromocao.getText().toString())));
        }

        vendaTv.setVigenciaPromocao(UtilActivity.getCalendarValue(txtVigenciaPromocao));
        vendaTv.setDataInstalacao(UtilActivity.getCalendarValue(txtDataInstalacao));

        return vendaTv;
    }

    private void irParaProximaTela() throws Exception {
        Intent proximaTela = new Intent();
        proximaTela.putExtra("idVenda", idVenda);
        proximaTela.putExtra("produtosTipo", produtosTiposSelecionados);
        proximaTela.putExtra("edicao", edicao);
        proximaTela.putExtra("isMock", isMock);

        telasAnteriores.add(EVendaFluxo.TV);
        proximaTela.putExtra("telasAnteriores", telasAnteriores);

        Class<?> cls = null;

        if (checkBoxAdicionaisTv.isChecked()) {
            cls = VendaTvAdicionalActivity.class;
        } else {
            if (produtosTiposSelecionados.contains(EProdutoTipo.FONE)) {
                cls = VendaFoneActivity.class;
            } else if (produtosTiposSelecionados.contains(EProdutoTipo.INTERNET)) {
                cls = VendaInternetActivity.class;
            } else {
                cls = VendaFormaPagamentoActivity.class;
            }
        }
        proximaTela.setComponent(new ComponentName(this, cls));
        startActivity(proximaTela);
        VendaTvActivity.this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_tv, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            try {
                Intent activityAnterior = new Intent();
                activityAnterior.putExtra("idVenda", idVenda);
                activityAnterior.putExtra("edicao", edicao);
                activityAnterior.putExtra("isMock", isMock);
                activityAnterior.putExtra("isVoltar", true);
                activityAnterior.putExtra("telasAnteriores", telasAnteriores);
                activityAnterior.putExtra("produtosTipo", produtosTiposSelecionados);
                Class<?> cls = null;

                if (telasAnteriores.contains(EVendaFluxo.CELULAR_DEPENDENTE)) {
                    cls = VendaCelularDependenteActivity.class;
                } else if (telasAnteriores.contains(EVendaFluxo.CELULAR)) {
                    cls = VendaCelularActivity.class;
                } else if (telasAnteriores.contains(EVendaFluxo.MOTIVOS_NAO_VENDA)) {
                    cls = VendaPacoteNaoVendaActivity.class;
                } else {
                    cls = VendaPacoteActivity.class;
                }

//                if (produtosTiposSelecionados.contains(EProdutoTipo.MULTI)) {
//                    entityManager.initialize(venda.getVendaCelular());
//                    List<VendaCelularDependente> dependentes =
//                            entityManager.select(
//                                    VendaCelularDependente.class,
//                                    "SELECT * FROM venda_celular_dependente WHERE _venda_celular = "
//                                            + venda.getVendaCelular().getId());
//                    //verificar se tem dependentes
//                    if (dependentes != null && !dependentes.isEmpty()) {
//                        cls = VendaCelularDependenteActivity.class;
//                    } else {
//                        cls = VendaCelularActivity.class;
//                    }
//                } else {
//                    //Caso bandeira seja vivo
//                    boolean isBandeiraVivo = false;
//                    for (BandeiraSistema bandeira : multiSales.getBandeiras()) {
//                        if (bandeira.getDescricao().contains("VIVO")) {
//                            isBandeiraVivo = true;
//                            break;
//                        }
//                    }
//                    if (isBandeiraVivo) {
//                        cls = VendaPacoteActivity.class;
//                    } else {
//                        cls = VendaPacoteNaoVendaActivity.class;
//                    }
//                }

                activityAnterior.setComponent(new ComponentName(this, cls));
                startActivity(activityAnterior);
                VendaTvActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (id == R.id.action_avancar) {
            avancar(null);
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaTvActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(hasFocus) {
            showPicker(view);
        }
    }

    @Override
    public void onClick(View view) {
        showPicker(view);
    }

    private void showPicker(View view) {
        if(view == txtDataInstalacao) {
            dataInstalacaoDPD.show();
        } else if (view == txtVigenciaPromocao) {
            dataVigenciaDPD.show();
        }
    }
}
