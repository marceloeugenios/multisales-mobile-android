package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EVendaFluxo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TipoPontoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTv;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTvPontoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTvProdutoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilMask;

public class VendaTvAdicionalActivity extends ActionBarActivity {

    private EntityManager entityManager;
    private VendaTv vendaTv;

    //Extras
    private Integer idVenda;
    private HashSet<EProdutoTipo> produtosTiposSelecionados;
    private boolean edicao;
    private boolean isMock;
    private boolean isVoltar;
    private HashSet<EVendaFluxo> telasAnteriores;

    private List<Produto> produtosAdicionais;
    private List<Produto> produtosAdicionaisSelecionados;
    private String[] descricoesProdutosAdicionais;
    private List<Integer> indexProdutosAdicionaisSelecionados;
    private List<TipoPontoAdicional> pontosAdicionais;
    private List<VendaTvPontoAdicional> vendasPontosAdicionais;
    private ListView listViewPontosAdicionais;

    //Componentes
    private Spinner spinnerPontosAdicionais;
    private EditText txtValorPontoAdicional;
    private TextView txtViewValorTotalPontosAdicionaisTv;
    private EditText txtValorTotalPontosAdicionaisTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda_tv_adicional);

        criarCamposEntrada();
        entityManager = new EntityManager(this);
        idVenda = getIntent().getIntExtra("idVenda", SistemaConstantes.ZERO);
        produtosTiposSelecionados = (HashSet<EProdutoTipo>)
                getIntent().getExtras().get("produtosTipo");
        edicao = getIntent().getBooleanExtra("edicao", false);
        isMock = getIntent().getBooleanExtra("isMock", false);
        isVoltar = getIntent().getBooleanExtra("isVoltar", false);

        telasAnteriores = (HashSet<EVendaFluxo>) getIntent().getExtras().get("telasAnteriores");
        telasAnteriores.remove(EVendaFluxo.TV_ADICIONAL);

        carregarProdutosAdicionais();
        carregarPontosAdicionais();
        vendasPontosAdicionais = new ArrayList<>();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            Venda venda = entityManager.getById(Venda.class, idVenda);
            vendaTv = entityManager.getById(VendaTv.class, venda.getVendaTv().getId());

            vendasPontosAdicionais   = entityManager.select(
                    VendaTvPontoAdicional.class,
                    "SELECT * FROM venda_tv_ponto_adicional WHERE _venda_tv = " + vendaTv.getId());
            produtosAdicionaisSelecionados = entityManager.select(
                    Produto.class,
                    "SELECT p.* FROM produto p INNER JOIN venda_tv_produto_adicional vtpa ON vtpa._produto = p.id AND vtpa._venda_tv = " + vendaTv.getId());

            if ((vendasPontosAdicionais != null && !vendasPontosAdicionais.isEmpty())
                    || (produtosAdicionaisSelecionados != null && !produtosAdicionaisSelecionados.isEmpty())
                    && !edicao) {
                isVoltar = true;
            }

            if (edicao || isVoltar) {
                popularValoresCamposEntrada();
            } else if (isMock) {
                popularMock();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO " + e.getMessage(), this);
        }
    }

    private void popularValoresCamposEntrada() throws Exception {
        if (produtosAdicionais != null) {
            for (Produto produto : produtosAdicionais) {
                indexProdutosAdicionaisSelecionados.add(
                        Arrays.asList(descricoesProdutosAdicionais).indexOf(produto.getDescricao()));
            }
        }

        listarProdutosAdicionaisSelecionados();
        listarPontosAdicionaisSelecionados();
    }

    private void popularMock() {
        try {
            indexProdutosAdicionaisSelecionados.add(SistemaConstantes.ZERO);
            VendaTvPontoAdicional pontoAdicional = new VendaTvPontoAdicional(pontosAdicionais.get(SistemaConstantes.ZERO), new BigDecimal("100"));
            vendasPontosAdicionais.add(pontoAdicional);

            listarProdutosAdicionaisSelecionados();
            listarPontosAdicionaisSelecionados();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao popular mock: " + e.getMessage(), this);
        }
    }

    private void criarCamposEntrada() {
        spinnerPontosAdicionais             = (Spinner) findViewById(R.id.spinnerPontosAdicionais);
        txtValorPontoAdicional              = (EditText) findViewById(R.id.txtValorPontoAdicional);
        UtilMask.setMascaraMoeda(txtValorPontoAdicional, 10);
        listViewPontosAdicionais            = (ListView) findViewById(R.id.listViewPontosAdicionais);
        txtViewValorTotalPontosAdicionaisTv = (TextView) findViewById(R.id.txtViewValorTotalPontosAdicionaisTv);
        txtValorTotalPontosAdicionaisTv     = (EditText) findViewById(R.id.txtValorTotalPontosAdicionaisTv);

        txtViewValorTotalPontosAdicionaisTv.setVisibility(View.GONE);
        txtValorTotalPontosAdicionaisTv.setVisibility(View.GONE);

        listViewPontosAdicionais.setLongClickable(true);
        listViewPontosAdicionais.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    vendasPontosAdicionais.remove(position);
                    listarPontosAdicionaisSelecionados();
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
    }

    private void carregarProdutosAdicionais() {
        try {
            produtosAdicionais = entityManager.select(
                    Produto.class,
                    "SELECT p.* " +
                            "FROM produto p " +
                            "INNER JOIN produto_tipo pt " +
                            "ON pt.id = p._produto_tipo " +
                            "AND pt.descricao = 'TV ADICIONAL' " +
                            "ORDER BY p.descricao");

            descricoesProdutosAdicionais = new String[produtosAdicionais.size()];
            indexProdutosAdicionaisSelecionados = new ArrayList<>();

            int index = SistemaConstantes.ZERO;
            for (Produto produtoAdicional : produtosAdicionais) {
                descricoesProdutosAdicionais[index] =  (produtoAdicional.getDescricao());
                index++;
            }

        } catch (Exception e ) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar produtos adicionais: " + e.getMessage(), this);
        }
    }

    private void carregarPontosAdicionais() {
        try {
            pontosAdicionais = entityManager.select(
                    TipoPontoAdicional.class,
                    "SELECT tpa.* " +
                            "FROM tipo_ponto_adicional tpa " +
                            "WHERE tpa.situacao = 'ATIVO' " +
                            "ORDER BY tpa.descricao");

            List<String> descricoesPontosAdicionais = new ArrayList<>();
            descricoesPontosAdicionais.add(UtilActivity.SELECIONE);

            for (TipoPontoAdicional tpa : pontosAdicionais) {
                descricoesPontosAdicionais.add(tpa.getDescricao());
            }

            UtilActivity.setAdapter(this, spinnerPontosAdicionais, descricoesPontosAdicionais);
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao listar pontos adicionais: " + e.getMessage(), this);
        }
    }

    public void adicionarProdutoAdicional(View v) {

        boolean[] checkedItems = null;

        if (descricoesProdutosAdicionais != null) {
            checkedItems = new boolean[descricoesProdutosAdicionais.length];

            for (int i = SistemaConstantes.ZERO; i < descricoesProdutosAdicionais.length; i++) {
                if (indexProdutosAdicionaisSelecionados.contains(i)) {
                    checkedItems[i] = true;
                } else {
                    checkedItems[i] = false;
                }
            }
        }

        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder((this));

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setTitle("Produtos Adicionais");

        // Specify the list array, the items to be selected by default (null for none),
        // and the listener through which to receive callbacks when items are selected
        builder.setMultiChoiceItems(descricoesProdutosAdicionais, checkedItems,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which,
                                        boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            indexProdutosAdicionaisSelecionados.add(which);
                        } else if (indexProdutosAdicionaisSelecionados.contains(which)) {
                            // Else, if the item is already in the array, remove it
                            indexProdutosAdicionaisSelecionados.remove(Integer.valueOf(which));
                        }
                    }
                })
                // Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listarProdutosAdicionaisSelecionados();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //Fecha dialog
                    }
                });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        //Show dialog
        dialog.show();
    }

    private void listarProdutosAdicionaisSelecionados() {
        List<Map<String, Object>> itens = new ArrayList<Map<String, Object>>();
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.produtosAdicionaisLL);
        linearLayout.removeAllViews();

        if (indexProdutosAdicionaisSelecionados != null) {
            Collections.sort(indexProdutosAdicionaisSelecionados);
            for (Integer index : indexProdutosAdicionaisSelecionados) {
                View itemProdutoAdicional = getLayoutInflater().inflate(R.layout.item_produto_adicional, null);
                TextView textView = (TextView) itemProdutoAdicional.findViewById(R.id.txtNomeProdutoAdicional);
                textView.setText( produtosAdicionais.get(index).getDescricao());
                linearLayout.addView(itemProdutoAdicional);
            }
        }
    }

    public void adicionarPontoAdicional(View view) {

        try {

            if (spinnerPontosAdicionais.getSelectedItemPosition() < SistemaConstantes.UM) {
                UtilActivity.makeShortToast("Selecione o tipo do ponto adicional.", this);
                return;
            }

            if (txtValorPontoAdicional.getText() == null
                    || txtValorPontoAdicional.getText().toString().trim().isEmpty() ) {
                UtilActivity.makeShortToast("Informe o valor do ponto adicional.", this);
                return;
            }

            VendaTvPontoAdicional vpa = new VendaTvPontoAdicional(
                    pontosAdicionais.get(
                            spinnerPontosAdicionais.getSelectedItemPosition() - SistemaConstantes.UM),
                    new BigDecimal(UtilMask.unmaskMoeda(txtValorPontoAdicional.getText().toString())));
            vendasPontosAdicionais.add(vpa);

            txtValorPontoAdicional.setText("");
            spinnerPontosAdicionais.setSelection(SistemaConstantes.ZERO);
            listarPontosAdicionaisSelecionados();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("ERRO ao adicionar ponto adicional: " + e.getMessage(), this);
        }
    }

    private void listarPontosAdicionaisSelecionados() throws DataBaseException {
        List<Map<String, Object>> itens = new ArrayList<Map<String, Object>>();

        if (vendasPontosAdicionais != null) {
            BigDecimal valorTotal = new BigDecimal(0.00);
            valorTotal.setScale(SistemaConstantes.DOIS);

            for (VendaTvPontoAdicional vpa : vendasPontosAdicionais) {
                Map<String, Object> item = new HashMap<>();
                entityManager.initialize(vpa.getTipoPontoAdicional());
                item.put("Tipo", vpa.getTipoPontoAdicional().getDescricao());
                item.put("Valor", UtilActivity.formatarMoeda(vpa.getCustoPontoAdicional().doubleValue()));
                valorTotal = valorTotal.add(vpa.getCustoPontoAdicional().setScale(SistemaConstantes.DOIS));
                itens.add(item);
            }
            txtValorTotalPontosAdicionaisTv.setText(UtilActivity.formatarMoeda(valorTotal.doubleValue()));
            txtViewValorTotalPontosAdicionaisTv.setVisibility(View.VISIBLE);
            txtValorTotalPontosAdicionaisTv.setVisibility(View.VISIBLE);
        } else {
            txtViewValorTotalPontosAdicionaisTv.setVisibility(View.GONE);
            txtValorTotalPontosAdicionaisTv.setVisibility(View.GONE);
        }

        String[] de = {"Tipo", "Valor"};
        int[] para = {R.id.txtDescricaoPontoAdicional,R.id.txtValorPontoAdicional};
        SimpleAdapter adapter = new SimpleAdapter(this, itens, R.layout.item_venda_tv_ponto_adicional, de, para);
        listViewPontosAdicionais.setAdapter(adapter);

        UtilActivity.setListViewHeightBasedOnItems(listViewPontosAdicionais);
    }

    public void avancar(View view) {
        try {
            deletarAdicionaisTv();
            salvarProdutosAdicionais();
            salvarPontosAdicionais();
            irParaProximaTela();
        } catch (Exception e) {
            e.printStackTrace();
            UtilActivity.makeShortToast("Ocorreu um ERRO: " + e.getMessage(), this);
        }
    }

    private void deletarAdicionaisTv() throws Exception {
        Venda venda = entityManager.getById(Venda.class, idVenda);
        entityManager.executeNativeQuery(
                "DELETE FROM venda_tv_produto_adicional WHERE _venda_tv = " + venda.getVendaTv().getId());
        entityManager.executeNativeQuery(
                "DELETE FROM venda_tv_ponto_adicional WHERE _venda_tv = " + venda.getVendaTv().getId());
    }

    private void irParaProximaTela() {
        Intent proximaTela = new Intent();
        proximaTela.putExtra("idVenda", idVenda);
        proximaTela.putExtra("produtosTipo", produtosTiposSelecionados);
        proximaTela.putExtra("edicao", edicao);
        proximaTela.putExtra("isMock", isMock);

        telasAnteriores.add(EVendaFluxo.TV_ADICIONAL);
        proximaTela.putExtra("telasAnteriores", telasAnteriores);

        Class<?> cls = null;

        if (produtosTiposSelecionados.contains(EProdutoTipo.FONE)) {
            cls = VendaFoneActivity.class;
        } else if (produtosTiposSelecionados.contains(EProdutoTipo.INTERNET)) {
            cls = VendaInternetActivity.class;
        } else {
            cls = VendaFormaPagamentoActivity.class;
        }

        proximaTela.setComponent(new ComponentName(this, cls));
        startActivity(proximaTela);
        VendaTvAdicionalActivity.this.finish();
    }

    private void salvarProdutosAdicionais() throws DataBaseException {
        try {
            if (indexProdutosAdicionaisSelecionados != null) {
                for (Integer index : indexProdutosAdicionaisSelecionados) {
                    VendaTvProdutoAdicional vendaTvProdutoAdicional = new VendaTvProdutoAdicional(produtosAdicionais.get(index).getId(), vendaTv.getId());
                    entityManager.save(vendaTvProdutoAdicional);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void salvarPontosAdicionais() throws DataBaseException {
        for (VendaTvPontoAdicional vendaTvPontoAdicional : vendasPontosAdicionais) {
            vendaTvPontoAdicional.setVendaTv(vendaTv);
            entityManager.save(vendaTvPontoAdicional);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_venda_tv_adicional, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            try {
                Intent activityAnterior = new Intent();
                activityAnterior.putExtra("idVenda", idVenda);
                activityAnterior.putExtra("edicao", edicao);
                activityAnterior.putExtra("isMock", isMock);
                activityAnterior.putExtra("isVoltar", true);
                activityAnterior.putExtra("telasAnteriores", telasAnteriores);
                activityAnterior.putExtra("produtosTipo", produtosTiposSelecionados);

                Class<?> cls = VendaTvActivity.class;
                activityAnterior.setComponent(new ComponentName(this, cls));
                startActivity(activityAnterior);
                VendaTvAdicionalActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (id == R.id.action_avancar) {
            avancar(null);
        }

        if (id == R.id.action_cancelar) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            UtilActivity.irParaHome(VendaTvAdicionalActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja realmente cancelar a venda?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
