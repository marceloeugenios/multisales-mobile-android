package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTv;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VendaTvPontoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

/**
 * Created by eric on 02-03-2015.
 */
public class VendaTvFragment extends Fragment {

    private TextView txtBandeiraTV;
    private TextView txtProdutoTV;

    private RelativeLayout rlprodutosAdicionaisRL;
    private LinearLayout linearLayout;
    private RelativeLayout relativeLayout;

    private VendaTvPontoAdicional vendaTvPontoAdicional;
    private VendaTv vendaTv;
    private EntityManager entityManager;
    private TextView txtDegustacaoTV;
    private RelativeLayout rlConcorrenciaRL;
    private TextView txtConcorrente;
    private TextView txtMotivoTV;
    private Produto produto;
    private RelativeLayout relativeLayoutObservacao;
    private LinearLayout linearLayoutPontoAdicional;
    private RelativeLayout relativeLayoutPagamento;
    private RelativeLayout relativeLayoutDataInstalacao;
    private RelativeLayout relativeLayoutTaxaInstalacao;
    private TextView valorPromocionalTV;
    private TextView taxaInstalacaoTV;
    private TextView valorPosPromocaoTV;
    private RelativeLayout relativeValorPromocional;
    private RelativeLayout relativeLayoutValorPosPromocao;
    private RelativeLayout relativeLayoutVigenciaPromocao;
    private TextView vigenciaPromocaoTV;
    private TextView dataInstalacaoTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        entityManager = new EntityManager(getActivity());
        this.vendaTv = ((VendaDetalhesActivity) getActivity()).getVenda().getVendaTv();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }


        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_venda_tv, container, false);
        relativeLayoutPagamento = (RelativeLayout) relativeLayout.findViewById(R.id.pagamentoRL);
        linearLayout = (LinearLayout) relativeLayout.findViewById(R.id.produtosAdicionaisLL);
        linearLayoutPontoAdicional= (LinearLayout) relativeLayout.findViewById(R.id.pontosAdicionaisLL);

        criarCamposEntrada(inflater, container, savedInstanceState);
        return relativeLayout;
    }

    public void criarCamposEntrada(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState) {


        txtProdutoTV = (TextView) relativeLayout.findViewById(R.id.produtoTV);
        if (vendaTv.getProduto() != null) {
            txtProdutoTV.setText(vendaTv.getProduto().getDescricao());
        } else {
            txtProdutoTV.setText("N/D");
        }

        txtDegustacaoTV = (TextView) relativeLayout.findViewById(R.id.degustacaoTV);
        if (vendaTv.getDegustacao() != null
                && !vendaTv.getDegustacao().isEmpty()){
            txtDegustacaoTV.setText(vendaTv.getDegustacao());
        }else {
            txtDegustacaoTV.setText("N/D");
        }
        //pagamento
        if(vendaTv.getTaxaInstalacao() != null
                || vendaTv.getValorPromocional() != null
                || vendaTv.getValorPosPromocao() !=null
                || vendaTv.getVigenciaPromocao() != null ){


            this.taxaInstalacaoTV = (TextView) relativeLayoutPagamento.findViewById(R.id.taxaInstalacaoTV);
            if(vendaTv.getTaxaInstalacao() != null){
                this.taxaInstalacaoTV.setText(UtilActivity.formatarMoeda(vendaTv.getTaxaInstalacao().doubleValue()));
            } else {
                relativeLayoutTaxaInstalacao = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.taxaInstalacaoRL);
                relativeLayoutTaxaInstalacao.setVisibility(View.GONE);
            }

            this.valorPromocionalTV = (TextView) relativeLayoutPagamento.findViewById(R.id.valorPromocionalTV);
            if(vendaTv.getValorPromocional() != null){
                this.valorPromocionalTV.setText(UtilActivity.formatarMoeda(vendaTv.getValorPromocional().doubleValue()));
            } else {
                relativeValorPromocional = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.valorPromocionalRL);
                relativeValorPromocional.setVisibility(View.GONE);
            }


            this.valorPosPromocaoTV = (TextView) relativeLayoutPagamento.findViewById(R.id.valorPosPromocaoTV);
            if(vendaTv.getValorPosPromocao() != null){
                this.valorPosPromocaoTV.setText(UtilActivity.formatarMoeda(vendaTv.getValorPosPromocao().doubleValue()));
            } else {
                relativeLayoutValorPosPromocao = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.valorPosPromocaoRL);
                relativeLayoutValorPosPromocao.setVisibility(View.GONE);
            }

            this.vigenciaPromocaoTV = (TextView) relativeLayoutPagamento.findViewById(R.id.vigenciaPromocaoTV);
            if(vendaTv.getVigenciaPromocao() != null){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String data = simpleDateFormat.format(vendaTv.getVigenciaPromocao().getTime());
                this.vigenciaPromocaoTV .setText(data);
            } else {
                relativeLayoutVigenciaPromocao = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.vigenciaPromocaoRL);
                relativeLayoutVigenciaPromocao.setVisibility(View.GONE);
            }
            this.dataInstalacaoTV = (TextView) relativeLayoutPagamento.findViewById(R.id.dataInstalacaoTV);
            if(vendaTv.getDataInstalacao() != null) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String data = simpleDateFormat.format(vendaTv.getDataInstalacao().getTime());
                this.dataInstalacaoTV.setText(data);
            } else {
                relativeLayoutDataInstalacao = (RelativeLayout) relativeLayoutPagamento.findViewById(R.id.dataInstalacaoRL);
                relativeLayoutDataInstalacao.setVisibility(View.GONE);
            }
        }else {
            relativeLayoutPagamento.setVisibility(View.GONE);
        }

        //concorrencia
        if (vendaTv.getConcorrenteMigracao() != null) {
            txtConcorrente = (TextView)  relativeLayout.findViewById(R.id.concorrenteTV);
            if(vendaTv.getConcorrenteMigracao() != null) {
                txtConcorrente.setText(vendaTv.getConcorrenteMigracao().getDescricao());
            }
            txtMotivoTV = (TextView) relativeLayout.findViewById(R.id.motivoTV);
            if(vendaTv.getMotivoMigracao() != null){
                txtMotivoTV.setText(vendaTv.getMotivoMigracao().getDescricao());
            }

        } else {
            rlConcorrenciaRL = (RelativeLayout) relativeLayout.findViewById(R.id.concorrenciaRL);
            rlConcorrenciaRL.setVisibility(View.GONE);
        }

        //lista de produtos
        if (vendaTv.getProdutos().size() > 0) {
            for (int i = 0; i < vendaTv.getProdutos().size(); i++) {
                produto = vendaTv.getProdutos().get(i);

                try {
                    entityManager.initialize(produto.getProdutoPai());
                    entityManager.initialize(produto.getProdutoTipo());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                RelativeLayout itemProduto = (RelativeLayout) inflater.inflate(R.layout.item_produto_adicional, container, false);
                TextView textView = (TextView) itemProduto.findViewById(R.id.txtNomeProdutoAdicional);
                textView.setText(produto.getDescricao());
                linearLayout.addView(itemProduto);
            }
        } else {
            rlprodutosAdicionaisRL  = (RelativeLayout) relativeLayout.findViewById(R.id.produtosAdicionaisRL);
            rlprodutosAdicionaisRL.setVisibility(View.GONE);
        }


        //pontos adicionais
        if (vendaTv.getVendaTvPontosAdicionais().size() > 0) {
            for (int i = 0; i < vendaTv.getVendaTvPontosAdicionais().size(); i++) {
                vendaTvPontoAdicional = vendaTv.getVendaTvPontosAdicionais().get(i);
                try {
                    entityManager.initialize(vendaTvPontoAdicional.getTipoPontoAdicional());
                    entityManager.initialize(vendaTvPontoAdicional.getVendaTv());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                RelativeLayout itemPontoAdicional = (RelativeLayout) inflater.inflate(R.layout.item_venda_tv_ponto_adicional, container , false);

                TextView textView = (TextView) itemPontoAdicional.findViewById(R.id.txtDescricaoPontoAdicional);
                textView.setText(vendaTvPontoAdicional.getTipoPontoAdicional().getDescricao());

                TextView textViewValor = (TextView) itemPontoAdicional.findViewById(R.id.txtValorPontoAdicional);
                textViewValor.setText(UtilActivity.formatarMoeda(vendaTvPontoAdicional.getCustoPontoAdicional().doubleValue()));

                linearLayoutPontoAdicional.addView(itemPontoAdicional);
            }
        } else {
            relativeLayoutObservacao  = (RelativeLayout) relativeLayout.findViewById(R.id.pontosAdicionaisRL);
            relativeLayoutObservacao.setVisibility(View.GONE);
        }


    }
}

