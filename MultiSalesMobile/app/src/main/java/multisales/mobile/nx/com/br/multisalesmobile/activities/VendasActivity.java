package multisales.mobile.nx.com.br.multisalesmobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

public class VendasActivity extends ActionBarActivity implements AdapterView.OnItemClickListener  {

    private ListView listView;
    private RelativeLayout syncRL;
    private EntityManager entityManager;
    private List<Tabulacao> tabulacoes;
    private Integer alturaSync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendas);
        entityManager = new EntityManager(this);

        syncRL = (RelativeLayout) findViewById(R.id.syncRL);

        if (UtilActivity.isOnline(this)) {

            ViewTreeObserver vto = syncRL.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    alturaSync = syncRL.getMeasuredHeight();
                }
            });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    syncRL.animate().setDuration(500);
                    syncRL.animate().translationY(alturaSync);
                }
            }, 5000);

        } else {

            syncRL.setVisibility(View.INVISIBLE);

        }

        TabulacaoListAdapter adapter = new TabulacaoListAdapter(this, listarVendas());

        listView = (ListView) findViewById(R.id.listView);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private List<Tabulacao> listarVendas() {
        try {
            List<MotivoTabulacaoTipo> motivosTabulacaoTipo = entityManager.getByWhere(MotivoTabulacaoTipo.class, "descricao = '" + EMotivoTabulacaoTipo.VENDA.getDescricao() + "'", null);
            if (motivosTabulacaoTipo != null
                    && !motivosTabulacaoTipo.isEmpty()) {

                MotivoTabulacaoTipo motivoVenda = motivosTabulacaoTipo.get(SistemaConstantes.ZERO);

                tabulacoes = new ArrayList<>();
                StringBuilder stringBuilder =  new StringBuilder();
                stringBuilder.append("SELECT tab.* ");
                stringBuilder.append("FROM ");
                stringBuilder.append("tabulacao tab ");
                stringBuilder.append("INNER JOIN ");
                stringBuilder.append("motivo_tabulacao mtab ");
                stringBuilder.append("ON ");
                stringBuilder.append("tab._motivo_tabulacao = mtab.id ");
                stringBuilder.append("WHERE ");
                stringBuilder.append("mtab._motivo_tabulacao_tipo =  " + motivoVenda.getId() + " ");
                stringBuilder.append("AND ");
                stringBuilder.append("tab.completa =  \"" + EBoolean.TRUE.toString() + "\"");
                tabulacoes = entityManager.select(Tabulacao.class, stringBuilder.toString());

                for (Tabulacao tabulacao : tabulacoes) {
                    entityManager.initialize(tabulacao.getMotivoTabulacao());
                    entityManager.initialize(tabulacao.getVenda());
                    entityManager.initialize(tabulacao.getVenda().getCliente());
                    if (tabulacao.getVenda().getCombinacaoProdutoTipo() != null) {
                        entityManager.initialize(tabulacao.getVenda().getCombinacaoProdutoTipo());
                    }
                }

                return tabulacoes;
            }
        } catch (Exception e) {
            UtilActivity.makeShortToast("Erro ao listar as vendas", this);
            finish();
        }

        return null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vendas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        if (id == R.id.novo) {
            Intent intentVenda = new Intent(this, VendaClienteActivity.class);
            startActivity(intentVenda);
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentDetalhes = new Intent(this, VendaDetalhesActivity.class);
        intentDetalhes.putExtra("idVenda", tabulacoes.get(position).getVenda().getId());
        startActivity(intentDetalhes);
    }
}
