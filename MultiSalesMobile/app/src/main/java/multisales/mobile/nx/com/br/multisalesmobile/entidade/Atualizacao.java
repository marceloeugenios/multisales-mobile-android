package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.Calendar;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;

/**
 * Created by eric on 30-03-2015.
 */
public class Atualizacao {

    @Id
    private Integer id;
    @Column(name = "dataHora")
    private Calendar dataHora;
    private String login;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Calendar getDataHora() {
        return dataHora;
    }

    public void setDataHora(Calendar dataHora) {
        this.dataHora = dataHora;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
