package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;

/**
 * Created by samara on 24/02/15.
 */

@Table(name = "bandeira_sistema")
public class BandeiraSistema {

    @Id
    private Integer id;
    private String descricao;
    private String icone;
    private ESituacao situacao = ESituacao.ATIVO;

    @Transient
    @XmlTransient
    private boolean selecionada;

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public ESituacao getSituacao() {
        return situacao;
    }

    public void setSituacao(ESituacao situacao) {
        this.situacao = situacao;
    }

    public BandeiraSistema() {

    }

    public BandeiraSistema(String descricao, String icone) {
        this.descricao = descricao;
        this.icone = icone;
    }

    public BandeiraSistema(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getIcone() {
        return icone;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BandeiraSistema other = (BandeiraSistema) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public boolean isSelecionada() {
        return selecionada;
    }

    public void setSelecionada(boolean selecionada) {
        this.selecionada = selecionada;
    }
}
