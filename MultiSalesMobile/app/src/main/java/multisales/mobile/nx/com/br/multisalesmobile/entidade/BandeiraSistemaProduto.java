package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.EmbeddedId;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

/**
 * Created by samara on 24/02/15.
 */
@Table(name = "bandeira_sistema_produto")
public class BandeiraSistemaProduto {

    @EmbeddedId
    private BandeiraSistemaProdutoPK id;

    public BandeiraSistemaProduto() {

    }

    public BandeiraSistemaProduto(BandeiraSistema bandeiraSistema, Produto produto) {
        this.id = new BandeiraSistemaProdutoPK(
                bandeiraSistema.getId(), produto.getId());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BandeiraSistemaProduto other = (BandeiraSistemaProduto) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
