package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;

/**
 * Created by samara on 24/02/15.
 */

public class BandeiraSistemaProdutoPK {

    @Column(name = "_bandeira_sistema")
    private Integer bandeiraSistema;
    @Column(name = "_produto")
    private Integer produto;

    public BandeiraSistemaProdutoPK() {

    }

    public BandeiraSistemaProdutoPK(Integer bandeiraSistema, Integer produto) {
        this.bandeiraSistema = bandeiraSistema;
        this.produto = produto;
    }

    public Integer getBandeiraSistema() {
        return bandeiraSistema;
    }

    public Integer getProduto() {
        return produto;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + bandeiraSistema;
        result = prime * result + produto;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BandeiraSistemaProdutoPK other = (BandeiraSistemaProdutoPK) obj;
        if (bandeiraSistema != other.bandeiraSistema) {
            return false;
        }
        if (produto != other.produto) {
            return false;
        }
        return true;
    }

}
