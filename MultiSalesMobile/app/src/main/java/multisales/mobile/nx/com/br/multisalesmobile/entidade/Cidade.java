package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.io.Serializable;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;

public class Cidade {

    @Id
	private Integer id;

	private String nome;

	private String codigo;

	private String sigla;

    @JoinColumn(name = "_estado")
	private Estado estado;

	public Cidade() {
	}

	public Cidade(String nome, Estado estado, String codigo, String sigla) {
		this.nome = nome;
		this.estado = estado;
		this.codigo = codigo;
		this.sigla = sigla;
	}

	public Cidade(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return this.sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Cidade other = (Cidade) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}