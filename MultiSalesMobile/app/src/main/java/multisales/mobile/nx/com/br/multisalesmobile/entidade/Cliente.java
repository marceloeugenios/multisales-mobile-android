package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.math.BigDecimal;
import java.util.Calendar;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

public class Cliente {

    @Id
	private Integer id;

	private String nome;

    @Column(name = "cpf_cnpj")
	private String cpfCnpj;

    @Column(name = "data_emissao")
	private Calendar dataEmissao;

    @Column(name = "data_nascimento")
	private Calendar dataNascimento;

	private String email;

	private String emissor;

    @Column(name = "faixa_salarial")
	private BigDecimal faixaSalarial = new BigDecimal(0);

	private String nacionalidade;

    @Column(name = "nome_mae")
	private String nomeMae;

    @Column(name = "nome_pai")
	private String nomePai;

	private String profissao;

    @Column(name = "rg_inscricao_estadual")
	private String rgInscricaoEstadual;

	private ESexo sexo;

    @Column(name = "telefone_celular")
	private String telefoneCelular;

    @Column(name = "telefone_comercial")
	private String telefoneComercial;

    @Column(name = "telefone_residencial")
	private String telefoneResidencial;

    @JoinColumn(name = "_endereco")
	private Endereco endereco;

    @JoinColumn(name = "_endereco_cobranca")
	private Endereco enderecoCobranca;

    @JoinColumn(name = "_escolaridade")
	private Escolaridade escolaridade;

    @JoinColumn(name = "_estado_civil")
	private EstadoCivil estadoCivil;

    @Column(name = "responsavel_legal")
    @XmlElement(name = "responsavel_legal")
    private String responsavelLegal;

    @Column(name = "procurador")
    private String procurador;


    public Cliente() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Endereco getEndereco() {
		return this.endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Endereco getEnderecoCobranca() {
		return this.enderecoCobranca;
	}

	public void setEnderecoCobranca(Endereco enderecoCobranca) {
		this.enderecoCobranca = enderecoCobranca;
	}

	public Escolaridade getEscolaridade() {
		return this.escolaridade;
	}

	public void setEscolaridade(Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}

	public EstadoCivil getEstadoCivil() {
		return this.estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getCpfCnpj() {
		return this.cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public Calendar getDataEmissao() {
		return this.dataEmissao;
	}

	public void setDataEmissao(Calendar dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Calendar getDataNascimento() {
		return this.dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmissor() {
		return this.emissor;
	}

	public void setEmissor(String emissor) {
		this.emissor = emissor;
	}

	public BigDecimal getFaixaSalarial() {
		return this.faixaSalarial;
	}

	public void setFaixaSalarial(BigDecimal faixaSalarial) {
		this.faixaSalarial = faixaSalarial;
	}

	public String getNacionalidade() {
		return this.nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeMae() {
		return this.nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getNomePai() {
		return this.nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getProfissao() {
		return this.profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getRgInscricaoEstadual() {
		return this.rgInscricaoEstadual;
	}

	public void setRgInscricaoEstadual(String rgInscricaoEstadual) {
		this.rgInscricaoEstadual = rgInscricaoEstadual;
	}

	public ESexo getSexo() {
		return this.sexo;
	}

	public void setSexo(ESexo sexo) {
		this.sexo = sexo;
	}

	public String getTelefoneCelular() {
		return this.telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getTelefoneComercial() {
		return this.telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	public String getTelefoneResidencial() {
		return this.telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

    public String getResponsavelLegal() {
        return responsavelLegal;
    }

    public void setResponsavelLegal(String responsavelLegal) {
        this.responsavelLegal = responsavelLegal;
    }

    public String getProcurador() {
        return procurador;
    }

    public void setProcurador(String procurador) {
        this.procurador = procurador;
    }

    @Override
	public String toString() {
		return "Cliente [id=" + id + ", nome=" + nome + ", cpfCnpj=" + cpfCnpj
				+ ", dataEmissao=" + dataEmissao + ", dataNascimento="
				+ dataNascimento + ", email=" + email + ", emissor=" + emissor
				+ ", faixaSalarial=" + faixaSalarial + ", nacionalidade="
				+ nacionalidade + ", nomeMae=" + nomeMae + ", nomePai="
				+ nomePai + ", profissao=" + profissao
				+ ", rgInscricaoEstadual=" + rgInscricaoEstadual + ", sexo="
				+ sexo + ", telefoneCelular=" + telefoneCelular
				+ ", telefoneComercial=" + telefoneComercial
				+ ", telefoneResidencial=" + telefoneResidencial + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    public void setNullId() {
        this.id = null;
    }
}