package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;

@Table(name = "combinacao_produto_tipo")
public class CombinacaoProdutoTipo {

    @Id
	private Integer id;

	private String descricao;

    @Transient
	private List<CombinacaoProdutoTipoProdutoTipo> combinacaoProdutoTipoProdutoTipo;

    @Transient
	private List<Venda> vendas;

    @Transient
	private List<TipoContratoCombinacaoProdutoTipo> tiposContratosCombinacoesProdutosTipo;

	private ESituacao situacao = ESituacao.ATIVO;
	
	public CombinacaoProdutoTipo() {
	}

	public CombinacaoProdutoTipo(String descricao) {
		this.descricao = descricao;
	}

	public CombinacaoProdutoTipo(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ESituacao getSituacao() {
		return this.situacao;
	}

	public List<Venda> getVendas() {
		return this.vendas;
	}

	public void setVendas(List<Venda> vendas) {
		this.vendas = vendas;
	}

	public List<CombinacaoProdutoTipoProdutoTipo> getCombinacaoProdutoTipoProdutoTipo() {
		return combinacaoProdutoTipoProdutoTipo;
	}

	public void setCombinacaoProdutoTipoProdutoTipo(
			List<CombinacaoProdutoTipoProdutoTipo> combinacaoProdutoTipoProdutoTipo) {
		this.combinacaoProdutoTipoProdutoTipo = combinacaoProdutoTipoProdutoTipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CombinacaoProdutoTipo other = (CombinacaoProdutoTipo) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public List<TipoContratoCombinacaoProdutoTipo> getTiposContratosCombinacoesProdutosTipo() {
		return tiposContratosCombinacoesProdutosTipo;
	}

	public void setTiposContratosCombinacoesProdutosTipo(
			List<TipoContratoCombinacaoProdutoTipo> tiposContratosCombinacoesProdutosTipo) {
		this.tiposContratosCombinacoesProdutosTipo = tiposContratosCombinacoesProdutosTipo;
	}
}