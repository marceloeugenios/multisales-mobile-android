package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.EmbeddedId;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "combinacao_produto_tipo_produto_tipo")
public class CombinacaoProdutoTipoProdutoTipo {

    @EmbeddedId
	private CombinacaoProdutoTipoProdutoTipoPK id;

	public CombinacaoProdutoTipoProdutoTipo() {

	}
	
	public CombinacaoProdutoTipoProdutoTipo(
			CombinacaoProdutoTipo combinacaoProdutoTipo, ProdutoTipo produtoTipo) {
		this.id = new CombinacaoProdutoTipoProdutoTipoPK(
				combinacaoProdutoTipo.getId(), produtoTipo.getId());
	}

	public CombinacaoProdutoTipoProdutoTipoPK getId() {
		return this.id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CombinacaoProdutoTipoProdutoTipo other = (CombinacaoProdutoTipoProdutoTipo) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "CombinacaoProdutoTipoProdutoTipo [id=" + id + "]";
	}
}