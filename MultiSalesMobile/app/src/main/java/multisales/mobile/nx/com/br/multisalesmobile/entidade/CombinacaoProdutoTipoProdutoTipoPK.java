package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;

/**
 * The primary key class for the combinacao_produto_tipo_produto_tipo database table.
 * 
 */

public class CombinacaoProdutoTipoProdutoTipoPK {

    @Column(name = "_combinacao_produto_tipo")
	private Integer combinacaoProdutoTipo;

    @Column(name = "_produto_tipo")
	private Integer produtoTipo;

	public CombinacaoProdutoTipoProdutoTipoPK(Integer combinacaoProdutoTipo, Integer produtoTipo) {
		this.combinacaoProdutoTipo = combinacaoProdutoTipo;
		this.produtoTipo = produtoTipo;
	}

	public CombinacaoProdutoTipoProdutoTipoPK() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + combinacaoProdutoTipo;
		result = prime * result + produtoTipo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CombinacaoProdutoTipoProdutoTipoPK other = (CombinacaoProdutoTipoProdutoTipoPK) obj;
		if (combinacaoProdutoTipo != other.combinacaoProdutoTipo) {
			return false;
		}
		if (produtoTipo != other.produtoTipo) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "CoordenacaoCoordenadorPK [combinacaoProdutoTipo=" + combinacaoProdutoTipo
				+ ", produtoTipo=" + produtoTipo + "]";
	}

	public Integer getCombinacaoProdutoTipo() {
		return combinacaoProdutoTipo;
	}

	public void setCombinacaoProdutoTipo(Integer c) {
		this.combinacaoProdutoTipo = c;
	}
	
	public Integer getProdutoTipo() {
		return produtoTipo;
	}

	public void setProdutoTipo(Integer p) {
		this.produtoTipo = p;
	}
}