package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;

public class Detalhamento {

    @Id
	private Integer id;

	private String descricao;
	
	private String codigo;

    @JoinColumn(name = "_ocorrencia")
	private Ocorrencia ocorrencia;

	private ESituacao situacao = ESituacao.ATIVO;
	
	public Detalhamento() {
	}

	public Detalhamento(String codigo, String descricao) {
		this.descricao = descricao;
		this.codigo = codigo;
	}

	public Detalhamento(String codigo, String descricao, Ocorrencia ocorrencia) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.ocorrencia = ocorrencia;
	}

	public Integer getId() {
		return this.id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Ocorrencia getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(Ocorrencia ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public ESituacao getSituacao() {
		return situacao;
	}
}