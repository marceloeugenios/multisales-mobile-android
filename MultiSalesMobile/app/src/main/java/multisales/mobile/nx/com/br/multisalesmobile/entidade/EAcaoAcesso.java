package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by eric on 10-03-2015.
 */
public enum EAcaoAcesso {
    LOGIN("1"),
    LOGOFF("2");

    private String codigo;

    EAcaoAcesso(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
