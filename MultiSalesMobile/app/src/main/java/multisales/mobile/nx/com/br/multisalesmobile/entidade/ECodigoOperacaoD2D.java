package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by eric on 10/02/15.
 */
public enum ECodigoOperacaoD2D {
    AUTENTICAR("1"),
    NAO_VENDA_CODIGO("2"),
    VENDA_CODIGO("3"),
    VERIFICAR_PACOTES("4"),
    AGENDAMENTO("5"),
    CONDOMINIO("6"),
    PAP_CODIGO_HP("7"),
    PAP_CODIGO_CONDOMINIO("8"),
    CONDOMINIO_PLANTAO("9"),
    VERIFICAR_ATUALIZACOES("10"),
    VERIFICAR_CONEXAO("11");

    private String codigo;

    private ECodigoOperacaoD2D(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
