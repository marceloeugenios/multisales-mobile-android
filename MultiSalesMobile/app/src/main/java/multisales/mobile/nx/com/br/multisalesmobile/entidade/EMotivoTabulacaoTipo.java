package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by eric on 26-02-2015.
 */
public enum EMotivoTabulacaoTipo {
    TELEFONIA("TELEFONIA"),
    NAO_VENDA("NAO-VENDA"),
    VENDA("VENDA"),
    AGENDAMENTO("AGENDAMENTO");

    private String descricao;

    EMotivoTabulacaoTipo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
