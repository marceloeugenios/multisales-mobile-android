package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by eric on 27-03-2015.
 */
public enum ENotificacaoCodigo {
    ATUALIZACAO(1),
    CONEXAO(2);

    private Integer codigo;

    ENotificacaoCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getCodigo() {
        return codigo;
    }
}
