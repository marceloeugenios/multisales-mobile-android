package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by samara on 25/02/15.
 */
public enum EProdutoTipo {
    TV("TV"),
    TV_ADICIONAL("TV ADICIONAL"),
    FONE("FONE"),
    MULTI("MULTI"),
    INTERNET("INTERNET"),
    INTERNET_ADICIONAL("INTERNET ADICIONAL");

    private final String descricao;

    EProdutoTipo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
