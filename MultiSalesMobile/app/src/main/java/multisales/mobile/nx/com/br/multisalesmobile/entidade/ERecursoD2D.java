package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by eric on 10/02/15.
 */
public enum ERecursoD2D {
    PROCESSAR("processar");

    private String codigo;

    private ERecursoD2D(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
