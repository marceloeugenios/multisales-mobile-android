package multisales.mobile.nx.com.br.multisalesmobile.entidade;

public enum ESexo {

	M("MASCULINO"), 
	F("FEMININO"),
	J("JURÍDICO");

	private final String descricao;

	private ESexo(String sexo) {
		this.descricao = sexo;
	}

	public String getDescricao() {
		return this.descricao;
	}
}