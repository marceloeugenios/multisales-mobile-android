package multisales.mobile.nx.com.br.multisalesmobile.entidade;

public enum ESistema {
	
	SALESCALL("Sales <strong>Call</strong>", 
			"0800 136 0136", "logo-salescall.png", "cliente/img-home.jpg", "sales.salescall.com.br"), 
	MULTISALES("Multi <strong>Sales</strong>", "43 33510414", 
			"logo_multisales.png", "img-home.jpg", "multisales.com.br/multisales"),
	AMBOS("Multi <strong>Sales</strong>", "43 33510414", 
			"logo_multisales.png", "img-home.jpg", "");
	
	private final String nome;
	private final String telefone;
	private final String header;
	private final String home;
	private final String url;
	
	private ESistema(String nome, String telefone, String header, String home, String url) {
		this.nome = nome;
		this.telefone = telefone;
		this.header = header;
		this.home = home;
		this.url = url;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public String getHeader() {
		return header;
	}
	
	public String getHome() {
		return home;
	}
	
	public String getUrl() {
		return url;
	}
}
