package multisales.mobile.nx.com.br.multisalesmobile.entidade;

public enum ETipoAgendamento {

	PUBLICO("PÚBLICO"),
	PESSOAL("PESSOAL");

	private final String label;

	private ETipoAgendamento(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
}
