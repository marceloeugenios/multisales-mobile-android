package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by samara on 04/03/15.
 */
public enum ETipoAquisicao {

    PORTABILIDADE,
    NOVO;
}
