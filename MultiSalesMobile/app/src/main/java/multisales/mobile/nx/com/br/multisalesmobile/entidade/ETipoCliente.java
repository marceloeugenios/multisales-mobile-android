package multisales.mobile.nx.com.br.multisalesmobile.entidade;

public enum ETipoCliente {

	BASE("BASE"),
	PROSPECT("PROSPECT");

	private final String rotulo;
	
	private ETipoCliente(String rotulo) {
		this.rotulo = rotulo;
	}
	
	public String getRotulo() {
		return rotulo;
	}
}
