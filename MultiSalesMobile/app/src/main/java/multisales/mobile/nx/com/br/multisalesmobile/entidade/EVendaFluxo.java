package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by samara on 27/03/15.
 */
public enum EVendaFluxo {
    CLIENTE,
    ENDERECO,
    ENDERECO_COBRANCA,
    PACOTE,
    MOTIVOS_NAO_VENDA,
    CELULAR,
    CELULAR_DEPENDENTE,
    TV,
    TV_ADICIONAL,
    FONE,
    FONE_PORTABILIDADE,
    INTERNET,
    PAGAMENTO;
}
