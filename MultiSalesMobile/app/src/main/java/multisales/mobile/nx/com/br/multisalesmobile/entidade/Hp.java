package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;

public class Hp {


	private Long id;

    @Id
    @XmlTransient
    @Column(name = "id_local")
    private Long idLocal;

	private String nome;

	private String telefone1;

	private String telefone2;

	private String telefone3;

	private String telefone4;

	private String telefone5;
	
	private String telefone6;
	
	private String telefone7;
	
	private String telefone8;

	private String telefone9;
	
	private String telefone10;
	
	private String observacao;

    @Column(name = "data_baixa")
	private Calendar dataBaixa;

	private String cep;

	private String bairro;

	private String complemento;

	private String email;

	private String logradouro;
	
	private String numero;

    @Transient
    private Double latitude;

    @Transient
    private Double longitude;

    @JoinColumn(name = "_ultimo_motivo_tabulacao")
	private MotivoTabulacao ultimoMotivoTabulacao;

    @JoinColumn(name = "_cidade")
	private Cidade cidade;

    @JoinColumn(name = "_condominio")
    private Condominio condominio;

	public Hp() {
	}

	public Hp(Long id) {
		this.id = id;
	}
	
	public void resetarIdDataBaixa() {
		this.id = null;
		this.dataBaixa = null;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cidade getCidade() {
		return this.cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogradouro() {
		return this.logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

    public Double getLatitude() { return latitude;}

    public void setLatitude(Double latitude) { this.latitude = latitude;}

    public Double getLongitude() { return longitude;}

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

	public String getTelefone1() {
		if (this.telefone1 == null) {
			this.telefone1 = "";
		}
		return this.telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		if (this.telefone2 == null) {
			this.telefone2 = "";
		}
		return this.telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getTelefone3() {
		if (this.telefone3 == null) {
			this.telefone3 = "";
		}
		return this.telefone3;
	}

	public void setTelefone3(String telefone3) {
		this.telefone3 = telefone3;
	}

	public String getTelefone4() {
		if (this.telefone4 == null) {
			this.telefone4 = "";
		}
		return this.telefone4;
	}

	public void setTelefone4(String telefone4) {
		this.telefone4 = telefone4;
	}

	public String getTelefone5() {
		if (this.telefone5 == null) {
			this.telefone5 = "";
		}
		return this.telefone5;
	}

	public void setTelefone5(String telefone5) {
		this.telefone5 = telefone5;
	}

	public String getTelefone6() {
		if (this.telefone6 == null) {
			this.telefone6 = "";
		}
		return telefone6;
	}
	
	public void setTelefone6(String telefone6) {
		this.telefone6 = telefone6;
	}
	
	public String getTelefone7() {
		if (this.telefone7 == null) {
			this.telefone7 = "";
		}
		return telefone7;
	}
	
	public void setTelefone7(String telefone7) {
		this.telefone7 = telefone7;
	}
	
	public String getTelefone8() {
		if (this.telefone8 == null) {
			this.telefone8 = "";
		}
		return telefone8;
	}
	
	public void setTelefone8(String telefone8) {
		this.telefone8 = telefone8;
	}
	
	public String getTelefone9() {
		if (this.telefone9 == null) {
			this.telefone9 = "";
		}
		return telefone9;
	}
	
	public void setTelefone9(String telefone9) {
		this.telefone9 = telefone9;
	}
	
	public String getTelefone10() {
		if (this.telefone10 == null) {
			this.telefone10 = "";
		}
		return telefone10;
	}
	
	public void setTelefone10(String telefone10) {
		this.telefone10 = telefone10;
	}
	
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setUltimoMotivoTabulacao(MotivoTabulacao ultimoMotivoTabulacao) {
		this.ultimoMotivoTabulacao = ultimoMotivoTabulacao;
	}

	public Calendar getDataBaixa() {
		return dataBaixa;
	}

	public Hp baixarHp() {
		this.dataBaixa = Calendar.getInstance();
		return this;
	}

	public Set<String> getTelefonesValidos() {
		Set<String> telefonesValidos = new HashSet<String>();
		if (!getTelefone1().isEmpty()) {
			telefonesValidos.add(telefone1);
		}
		if (!getTelefone2().isEmpty()) {
			telefonesValidos.add(telefone2);
		}
		if (!getTelefone3().isEmpty()) {
			telefonesValidos.add(telefone3);
		}
		if (!getTelefone4().isEmpty()) {
			telefonesValidos.add(telefone4);
		}
		if (!getTelefone5().isEmpty()) {
			telefonesValidos.add(telefone5);
		}
		if (!getTelefone6().isEmpty()) {
			telefonesValidos.add(telefone6);
		}
		if (!getTelefone7().isEmpty()) {
			telefonesValidos.add(telefone7);
		}
		if (!getTelefone8().isEmpty()) {
			telefonesValidos.add(telefone8);
		}
		if (!getTelefone9().isEmpty()) {
			telefonesValidos.add(telefone9);
		}
		if (!getTelefone10().isEmpty()) {
			telefonesValidos.add(telefone10);
		}
		return telefonesValidos;
	}
	
	public MotivoTabulacao getUltimoMotivoTabulacao() {
		return ultimoMotivoTabulacao;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getNumero() {
		return numero;
	}

    public Condominio getCondominio() {
        return condominio;
    }

    public void setCondominio(Condominio condominio) {
        this.condominio = condominio;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Hp other = (Hp) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}