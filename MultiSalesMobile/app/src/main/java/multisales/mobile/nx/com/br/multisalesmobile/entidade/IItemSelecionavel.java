package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by eric on 06-03-2015.
 */
public interface IItemSelecionavel {
    public String getLabel();
    public Boolean isSelecionado ();
    public void setSelecionado(Boolean selecionado);
}
