package multisales.mobile.nx.com.br.multisalesmobile.entidade;

/**
 * Created by fabionx on 17/03/15.
 */
public class Localizacao {

    private Double latitude;
    private Double longitude;
    private Integer raio;


    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getRaio() {
        return raio;
    }

    public void setRaio(Integer raio) {
        this.raio = raio;
    }
}
