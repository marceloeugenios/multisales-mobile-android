package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.Calendar;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.DateFormat;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

/**
 * Created by eric on 10-03-2015.
 */
@Table(name = "log_acesso")
public class LogAcesso {

    @Id
    private Integer id;
    @Column(name = "acao_login")
    private EAcaoAcesso acaoLogin;
    private String login;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Column(name = "id_agente_autorizado")
    private Integer idAgenteAutorizado;
    private String imei;
    @DateFormat(format = "dd-MM-yyyy")
    @Column(name = "data_login")
    private Calendar dataLogin;
    @DateFormat(format = "HH:mm:ss")
    @Column(name = "hora_login")
    private Calendar horaLogin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EAcaoAcesso getAcaoLogin() {
        return acaoLogin;
    }

    public void setAcaoLogin(EAcaoAcesso acaoLogin) {
        this.acaoLogin = acaoLogin;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Calendar getDataLogin() {
        return dataLogin;
    }

    public void setDataLogin(Calendar dataLogin) {
        this.dataLogin = dataLogin;
    }

    public Calendar getHoraLogin() {
        return horaLogin;
    }

    public void setHoraLogin(Calendar horaLogin) {
        this.horaLogin = horaLogin;
    }

    public void setIdAgenteAutorizado(Integer idAgenteAutorizado) {
        this.idAgenteAutorizado = idAgenteAutorizado;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdAgenteAutorizado() {
        return idAgenteAutorizado;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImei() {
        return imei;
    }
}
