package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.Calendar;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.DateFormat;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

/**
 * Created by eric on 10-03-2015.
 */
@Table(name = "log_diario")
public class LogDiario {

    @Id
    private Integer id;
    private String login;
    @DateFormat(format = "dd-MM-yyyy")
    @Column(name = "data_log")
    private Calendar dataLog;
    private Integer vendas = 0;
    @Column(name = "nao_vendas")
    private Integer naoVendas = 0;
    private Integer agendamentos = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Calendar getDataLog() {
        return dataLog;
    }

    public void setDataLog(Calendar dataLog) {
        this.dataLog = dataLog;
    }

    public Integer getVendas() {
        return vendas;
    }

    public void setVendas(Integer vendas) {
        this.vendas = vendas;
    }

    public Integer getNaoVendas() {
        return naoVendas;
    }

    public void setNaoVendas(Integer naoVendas) {
        this.naoVendas = naoVendas;
    }

    public Integer getAgendamentos() {
        return agendamentos;
    }

    public void setAgendamentos(Integer agendamentos) {
        this.agendamentos = agendamentos;
    }
}
