package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.io.Serializable;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "motivo_migracao")
public class MotivoMigracao {

    @Id
	private Integer id;
	private String descricao;
	private ESituacao situacao = ESituacao.ATIVO;
	
	public MotivoMigracao() {
		
	}
	
	public MotivoMigracao(String descricao, ESituacao situacao) {
		this.descricao = descricao;
		this.situacao = situacao;
	}

	public MotivoMigracao(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ESituacao getSituacao() {
		return situacao;
	}
	
	@Override
	public String toString() {
		return "MotivoMigracao [id=" + id + ", descricao=" + descricao
				+ ", situacao=" + situacao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MotivoMigracao other = (MotivoMigracao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}