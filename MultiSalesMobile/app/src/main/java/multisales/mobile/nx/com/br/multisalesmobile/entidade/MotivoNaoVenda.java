package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.io.Serializable;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;

@Table(name = "motivo_nao_venda")
public class MotivoNaoVenda implements IItemSelecionavel {

    @Id
	private Integer id;
	
	private String descricao;
	
	private ESituacao situacao = ESituacao.ATIVO;

    @JoinColumn(name = "_produto_tipo")
    @XmlElement(name = "produto_tipo")
	private ProdutoTipo produtoTipo;

    @XmlTransient
    @Transient
    private Boolean selecionado = false;
	
	public MotivoNaoVenda() {
		
	}
	
	public MotivoNaoVenda(String descricao, ProdutoTipo produtoTipo) {
		this.descricao = descricao;
		this.produtoTipo = produtoTipo;
	}

	public MotivoNaoVenda(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public ESituacao getSituacao() {
		return this.situacao;
	}

	public Integer getId() {
		return id;
	}

	public ProdutoTipo getProdutoTipo() {
		return produtoTipo;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setSituacao(ESituacao situacao) {
		this.situacao = situacao;
	}

	public void setProdutoTipo(ProdutoTipo produtoTipo) {
		this.produtoTipo = produtoTipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((produtoTipo == null) ? 0 : produtoTipo.hashCode());
		result = prime * result
				+ ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MotivoNaoVenda other = (MotivoNaoVenda) obj;
		if (descricao == null) {
			if (other.descricao != null) {
				return false;
			}
		} else if (!descricao.equals(other.descricao)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (produtoTipo == null) {
			if (other.produtoTipo != null) {
				return false;
			}
		} else if (!produtoTipo.equals(other.produtoTipo)) {
			return false;
		}
		if (situacao != other.situacao) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "MotivoNaoVenda [id=" + id + ", descricao=" + descricao
				+ ", situacao=" + situacao + ", produtoTipo=" + produtoTipo
				+ "]";
	}

    @Override
    public String getLabel() {
        return this.descricao;
    }

    @Override
    public Boolean isSelecionado() {
        return this.selecionado;
    }

    @Override
    public void setSelecionado(Boolean selecionado) {
        this.selecionado = selecionado;
    }
}
