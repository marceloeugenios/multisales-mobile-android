package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.io.Serializable;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

@Table(name = "motivo_tabulacao")
public class MotivoTabulacao {

    @Id
	private Integer id;
	private String descricao;
	private ESituacao situacao = ESituacao.ATIVO;
    @Column(name = "deve_baixar_hp")
    @XmlElement(name = "deve_baixar_hp")
	private EBoolean deveBaixarHp = EBoolean.TRUE;
    @JoinColumn(name = "_motivo_tabulacao_tipo")
    @XmlElement(name = "motivo_tabulacao_tipo")
	private MotivoTabulacaoTipo motivoTabulacaoTipo;
	private EBoolean concorrencia = EBoolean.FALSE;

	public MotivoTabulacao() {

	}
	
	public MotivoTabulacao(String descricao,
			MotivoTabulacaoTipo motivoTabulacaoTipo, EBoolean concorrencia, 
			EBoolean deveBaixarHp, ESituacao situacao) {
	
		this.descricao = descricao;
		this.motivoTabulacaoTipo = motivoTabulacaoTipo;
		this.concorrencia = concorrencia;
		this.deveBaixarHp = deveBaixarHp;
		this.situacao = situacao;
	}
	
	public MotivoTabulacao(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public MotivoTabulacaoTipo getMotivoTabulacaoTipo() {
		return motivoTabulacaoTipo;
	}

	public void setMotivoTabulacaoTipo(MotivoTabulacaoTipo motivoTabulacaoTipo) {
		this.motivoTabulacaoTipo = motivoTabulacaoTipo;
	}

	public EBoolean getDeveBaixarHp() {
		return deveBaixarHp;
	}
	
	public EBoolean getConcorrencia() {
		return concorrencia;
	}

	public void setConcorrencia(EBoolean concorrencia) {
		this.concorrencia = concorrencia;
	}
	
	public ESituacao getSituacao() {
		return situacao;
	}
	
	public void setSituacao(ESituacao situacao) {
		this.situacao = situacao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MotivoTabulacao other = (MotivoTabulacao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}