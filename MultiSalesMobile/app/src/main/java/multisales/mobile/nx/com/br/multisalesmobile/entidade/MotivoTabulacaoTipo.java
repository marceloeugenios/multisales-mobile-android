package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.io.Serializable;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "motivo_tabulacao_tipo")
public class MotivoTabulacaoTipo {

    @Id
	private Integer id;
	private String descricao;
	private ESituacao situacao = ESituacao.ATIVO;
	private String icone;
	private EBoolean retroalimentavel = EBoolean.FALSE;

	public MotivoTabulacaoTipo() {

	}

	public MotivoTabulacaoTipo(String descricao, String icone, EBoolean retroalimentacao) {
		this.descricao = descricao;
		this.icone = icone;
		this.retroalimentavel = retroalimentacao;
	}

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ESituacao getSituacao() {
		return situacao;
	}

	public String getIcone() {
		return icone;
	}

	public void setIcone(String icone) {
		this.icone = icone;
	}

	public EBoolean getRetroalimentavel() {
		return retroalimentavel;
	}
	
	public boolean isVenda() {
		return descricao.equals("VENDA");
	}
	
	public boolean isNaoVenda() {
		return descricao.equals("NAO-VENDA");
	}
	
	public boolean isAgendamento() {
		return descricao.equals("AGENDAMENTO");
	}
	
	public boolean isTelefonia() {
		return descricao.equals("TELEFONIA");
	}
}