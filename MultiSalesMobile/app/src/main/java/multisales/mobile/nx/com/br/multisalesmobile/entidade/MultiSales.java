package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import android.app.Application;

import java.util.List;

/**
 * Created by eric on 21-03-2015.
 */
public class MultiSales extends Application {
    private String login;
    private Integer idUsuario;
    private Integer idAgenteAutorizado;
    private String imei;
    private Integer idCondominio;
    private List<BandeiraSistema> bandeiras;
    private Boolean redeDados;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdAgenteAutorizado() {
        return idAgenteAutorizado;
    }

    public void setIdAgenteAutorizado(Integer idAgenteAutorizado) {
        this.idAgenteAutorizado = idAgenteAutorizado;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public List<BandeiraSistema> getBandeiras() {
        return bandeiras;
    }

    public void setBandeiras(List<BandeiraSistema> bandeiras) {
        this.bandeiras = bandeiras;
    }

    public void setIdCondominio(Integer idCondominio) {
        this.idCondominio = idCondominio;
    }

    public Integer getIdCondominio() {
        return idCondominio;
    }

    public Boolean getRedeDados() {
        return redeDados;
    }

    public void setRedeDados(Boolean redeDados) {
        this.redeDados = redeDados;
    }
}

