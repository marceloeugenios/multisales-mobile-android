package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;

public class Ocorrencia {

    @Id
	private Integer id;
	private String descricao;
	private String codigo;
	private String cor;
	private Integer sequencia;
	private ESituacao situacao = ESituacao.ATIVO;
	
	public Ocorrencia() {

	}

	public Ocorrencia(String descricao, String codigo, ESituacao situacao, String cor, Integer sequencia) {
		this.descricao = descricao;
		this.codigo = codigo;
		this.situacao = situacao;
		this.cor = cor;
		this.sequencia = sequencia;
	}

	public Ocorrencia(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public ESituacao getSituacao() {
		return situacao;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Integer getSequencia() {
		return sequencia;
	}
	
	public void setSequencia(Integer sequencia) {
		this.sequencia = sequencia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Ocorrencia other = (Ocorrencia) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}