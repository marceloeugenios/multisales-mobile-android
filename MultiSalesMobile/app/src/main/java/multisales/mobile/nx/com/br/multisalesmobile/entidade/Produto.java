package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;

public class Produto {

    @Id
	private Integer id;

	private String descricao;

    @JoinColumn(name = "_produto_pai")
	private Produto produtoPai;

    @JoinColumn(name = "_produto_tipo")
	private ProdutoTipo produtoTipo;
	
	private ESituacao situacao = ESituacao.ATIVO;
	
	private ESistema sistema = ESistema.AMBOS;
	

	public Produto() {
	}

	public Produto(ProdutoTipo tipo, String descricao, ESistema sistema) {
		this.produtoTipo = tipo;
		this.descricao = descricao;
		this.sistema = sistema;
	}

	public Produto(Integer id) {
		this.id = id;
	}

	public Produto(String descricao) {
		this.descricao = descricao;
	}

	public Integer getId() {
		return this.id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setSituacao(ESituacao situacao) {
		this.situacao = situacao;
	}

	public Produto getProdutoPai() {
		return produtoPai;
	}

	public void setProdutoPai(Produto produtoPai) {
		this.produtoPai = produtoPai;
	}

	public ProdutoTipo getProdutoTipo() {
		return produtoTipo;
	}

	public void setProdutoTipo(ProdutoTipo produtoTipo) {
		this.produtoTipo = produtoTipo;
	}
	
	public ESistema getSistema() {
		return sistema;
	}

	public void setSistema(ESistema sistema) {
		this.sistema = sistema;
	}

	
	public ESituacao getSituacao() {
		return situacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Produto other = (Produto) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Produto [id=" + id + "]";
	}
}