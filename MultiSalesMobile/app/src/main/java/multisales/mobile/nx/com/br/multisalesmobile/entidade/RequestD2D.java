package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.io.Serializable;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioPlantao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

/**
 * Created by eric on 06/02/15.
 */
public class RequestD2D implements Serializable{
    private String usuario;
    private String senha;
    private String operacao;
    @XmlElement(name = "id_usuario")
    private Integer idUsuario;
    @XmlElement(name = "id_agente_autorizado")
    private Integer idAgenteAutorizado;
    @XmlElement(name = "versao_pacote")
    private Float versaoPacote;
    @XmlElement(name = "versao_aplicacao")
    private Float versaoAplicacao;
    @XmlElement(name = "tabulacao")
    private Tabulacao tabulacao;
    @XmlElement(name = "tabulacao_agendamento")
    private TabulacaoAgendamento tabulacaoAgendamento;
    @XmlElement(name = "condominio")
    private Condominio condominio;
    @XmlElement(name = "condominio_plantao")
    private CondominioPlantao condominioPlantao;
    private Localizacao localizacao;
    private List<Condominio> condominios;

    public Tabulacao getTabulacao() {
        return tabulacao;
    }

    public void setTabulacao(Tabulacao tabulacao) {
        this.tabulacao = tabulacao;
    }

    public void setTabulacaoAgendamento(TabulacaoAgendamento tabulacaoAgendamento) {
        this.tabulacaoAgendamento = tabulacaoAgendamento;
    }

    public TabulacaoAgendamento getTabulacaoAgendamento() {
        return tabulacaoAgendamento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Float getVersaoAplicacao() {
        return versaoAplicacao;
    }

    public void setVersaoAplicacao(Float versaoAplicacao) {
        this.versaoAplicacao = versaoAplicacao;
    }

    public Float getVersaoPacote() {
        return versaoPacote;
    }

    public void setVersaoPacote(Float versaoPacote) {
        this.versaoPacote = versaoPacote;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Condominio getCondominio() {
        return condominio;
    }

    public void setCondominio(Condominio condominio) {
        this.condominio = condominio;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public CondominioPlantao getCondominioPlantao() {
        return condominioPlantao;
    }

    public void setCondominioPlantao(CondominioPlantao condominioPlantao) {
        this.condominioPlantao = condominioPlantao;
    }
    public Integer getIdAgenteAutorizado() {
        return idAgenteAutorizado;
    }

    public void setIdAgenteAutorizado(Integer idAgenteAutorizado) {
        this.idAgenteAutorizado = idAgenteAutorizado;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public List<Condominio> getCondominios() {
        return condominios;
    }

    public void setCondominios(List<Condominio> condominios) {
        this.condominios = condominios;
    }
}
