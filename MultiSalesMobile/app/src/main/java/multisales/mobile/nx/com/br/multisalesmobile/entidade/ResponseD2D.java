package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.List;
import java.util.Map;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.Condominio;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

public class ResponseD2D {

	private ED2DCodigoResponse codigo;
	private String mensagem;
    @XmlElement(name = "id_usuario")
    private Integer idUsuario;
    @XmlElement(name = "id_agente_autorizado")
    private Integer idAgenteAutorizado;
    private List<Hp> hps;
    private List<Condominio> condominios;
    private Map<String, Object> extras;

    public ResponseD2D() {
    }

    public ED2DCodigoResponse getCodigo() {
        return codigo;
    }

    public String getMensagem() {
		return mensagem;
	}

    public Map<String, Object> getExtras() {
        return extras;
    }

    public void setCodigo(ED2DCodigoResponse codigo) {
        this.codigo = codigo;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public List<Hp> getHps() {
        return hps;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdAgenteAutorizado() {
        return idAgenteAutorizado;
    }

    public void setIdAgenteAutorizado(Integer idAgenteAutorizado) {
        this.idAgenteAutorizado = idAgenteAutorizado;
    }
    public List<Condominio> getCondominios() {
        return condominios;
    }

    public void setCondominios(List<Condominio> condominios) {
        this.condominios = condominios;
    }
}