package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

/**
 * Created by eric on 12-03-2015.
 */
@Table(name = "status_sincronizacao")
public class StatusSincronizacao {

    @Id
    private Integer id;
    @Column(name = "motivo_tabulacao_tipo")
    private EMotivoTabulacaoTipo motivoTabulacaoTipo;
    @Column(name = "em_sincronizacao")
    private EBoolean emSincronizacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EMotivoTabulacaoTipo getMotivoTabulacaoTipo() {
        return motivoTabulacaoTipo;
    }

    public void setMotivoTabulacaoTipo(EMotivoTabulacaoTipo motivoTabulacaoTipo) {
        this.motivoTabulacaoTipo = motivoTabulacaoTipo;
    }

    public EBoolean getEmSincronizacao() {
        return emSincronizacao;
    }

    public void setEmSincronizacao(EBoolean emSincronizacao) {
        this.emSincronizacao = emSincronizacao;
    }
}
