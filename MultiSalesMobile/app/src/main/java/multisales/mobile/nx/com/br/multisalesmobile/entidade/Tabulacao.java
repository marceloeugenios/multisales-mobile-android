package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;

public class Tabulacao {


	private Integer id;

    @Id
    @XmlTransient
    @Column(name = "id_local")
    private Integer idLocal;

    @XmlTransient
    private EBoolean completa = EBoolean.FALSE;

    @Transient
    @XmlElement(name = "vendas")
    private List<Venda> vendas;

    @XmlTransient
    @JoinColumn(name = "_venda")
    private Venda venda;

    @JoinColumn(name = "_motivo_tabulacao")
    @XmlElement(name = "motivo_tabulacao")
	private MotivoTabulacao motivoTabulacao;

    @XmlTransient
    @JoinColumn(name = "_usuario")
	private Usuario usuario;

    @Column(name = "data_cadastro")
    @XmlElement(name = "data_cadastro")
	private Calendar dataCadastro;

    @JoinColumn(name = "_hp")
	private Hp hp;

    @JoinColumn(name = "_concorrente")
    @XmlElement(name = "concorrente_migracao")
	private Concorrente concorrenteMigracao;

    @JoinColumn(name = "_motivo_migracao")
    @XmlElement(name = "motivo_migracao")
	private MotivoMigracao motivoMigracao;
	
	public Tabulacao() {
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public MotivoTabulacao getMotivoTabulacao() {
		return this.motivoTabulacao;
	}

	public void setMotivoTabulacao(MotivoTabulacao motivoTabulacao) {
		this.motivoTabulacao = motivoTabulacao;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Calendar getDataCadastro() {
		return this.dataCadastro;
	}

    public void setDataCadastro(Calendar dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
	
	public Hp getHp() {
		return hp;
	}

	public void setHp(Hp hp) {
		this.hp = hp;
	}

	public Concorrente getConcorrenteMigracao() {
		return concorrenteMigracao;
	}

	public void setConcorrenteMigracao(Concorrente concorrenteMigracao) {
		this.concorrenteMigracao = concorrenteMigracao;
	}

	public MotivoMigracao getMotivoMigracao() {
		return motivoMigracao;
	}

	public void setMotivoMigracao(MotivoMigracao motivoMigracao) {
		this.motivoMigracao = motivoMigracao;
	}

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public EBoolean getCompleta() {
        return completa;
    }

    public void setCompleta(EBoolean completa) {
        this.completa = completa;
    }

    public List<Venda> getVendas() {
        return vendas;
    }

    public void setVendas(List<Venda> vendas) {
        this.vendas = vendas;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Tabulacao other = (Tabulacao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    public void addVenda() {
        this.vendas = new ArrayList<>();
        this.vendas.add(venda);
    }
}