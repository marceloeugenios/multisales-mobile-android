package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.Calendar;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;

@Table(name = "tabulacao_agendamento")
public class TabulacaoAgendamento{

	private Integer id;
    @Id
    @Column(name = "id_local")
    @XmlTransient
    private Integer idLocal;
    @JoinColumn(name = "_tabulacao")
	private Tabulacao tabulacao;
    @Column(name = "data_retorno")
    @XmlElement(name = "data_retorno")
	private Calendar dataRetorno;
    @Column(name = "tipo_agendamento")
    @XmlElement(name = "tipo_agendamento")
	private ETipoAgendamento tipoAgendamento;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Tabulacao getTabulacao() {
		return tabulacao;
	}

	public void setTabulacao(Tabulacao tabulacao) {
		this.tabulacao = tabulacao;
	}

	public Calendar getDataRetorno() {
		return dataRetorno;
	}
	
	public void setDataRetorno(Calendar dataRetorno) {
		this.dataRetorno = dataRetorno;
	}
	
	public ETipoAgendamento getTipoAgendamento() {
		return tipoAgendamento;
	}
	
	public void setTipoAgendamento(ETipoAgendamento tipoAgendamento) {
		this.tipoAgendamento = tipoAgendamento;
	}

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }
}
