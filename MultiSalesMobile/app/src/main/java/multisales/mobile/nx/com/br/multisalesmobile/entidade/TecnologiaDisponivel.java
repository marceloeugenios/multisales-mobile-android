package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

@Table(name = "tecnologia_disponivel")
public class TecnologiaDisponivel {

    @Id
    private Integer id;

    private String descricao;

    private ESituacao situacao = ESituacao.ATIVO;

    @XmlElement(name = "produto_tipo")
    @JoinColumn(name = "_produto_tipo")
    private ProdutoTipo produtoTipo;

    public TecnologiaDisponivel() {

    }

    public TecnologiaDisponivel(String descricao) {
        this.descricao = descricao;
    }

    public TecnologiaDisponivel(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ESituacao getSituacao() {
        return situacao;
    }

    public ProdutoTipo getProdutoTipo() {
        return produtoTipo;
    }

    public void setProdutoTipo(ProdutoTipo produtoTipo) {
        this.produtoTipo = produtoTipo;
    }

    @Override
    public String toString() {
        return "Banco [id=" + id + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TecnologiaDisponivel other = (TecnologiaDisponivel) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}