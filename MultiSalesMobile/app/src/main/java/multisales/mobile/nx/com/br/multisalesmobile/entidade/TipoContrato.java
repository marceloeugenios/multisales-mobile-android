package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;

@Table(name = "tipo_contrato")
public class TipoContrato {

    @Id
	private Integer id;

	private String descricao;

	private ESituacao situacao = ESituacao.ATIVO;

    @Transient
    @XmlTransient
	private List<Venda> vendas;

    @Transient
    @XmlTransient
	private List<TipoContratoCombinacaoProdutoTipo> tiposContratoCombinacoesProdutoTipo;

	public TipoContrato() {

	}

	public TipoContrato(String descricao) {
		this.descricao = descricao;
	}

	public TipoContrato(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ESituacao getSituacao() {
		return this.situacao;
	}

	public List<Venda> getVendas() {
		return this.vendas;
	}

	public void setVendas(List<Venda> vendas) {
		this.vendas = vendas;
	}

	public List<TipoContratoCombinacaoProdutoTipo> getTiposContratoCombinacoesProdutoTipo() {
		return tiposContratoCombinacoesProdutoTipo;
	}

	public void setTiposContratoCombinacoesProdutoTipo(
			List<TipoContratoCombinacaoProdutoTipo> tiposContratoCombinacoesProdutoTipo) {
		this.tiposContratoCombinacoesProdutoTipo = tiposContratoCombinacoesProdutoTipo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TipoContrato other = (TipoContrato) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}