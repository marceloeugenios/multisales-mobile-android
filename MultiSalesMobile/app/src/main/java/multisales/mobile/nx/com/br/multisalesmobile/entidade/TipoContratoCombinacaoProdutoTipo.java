package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.EmbeddedId;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "tipo_contrato_combinacao_produto_tipo")
public class TipoContratoCombinacaoProdutoTipo {

    @EmbeddedId
	private TipoContratoCombinacaoProdutoTipoPK id;

	public TipoContratoCombinacaoProdutoTipo() {

	}

	public TipoContratoCombinacaoProdutoTipo(TipoContrato tipoContrato,
			CombinacaoProdutoTipo combinacaoProdutoTipo) {
		this.id = new TipoContratoCombinacaoProdutoTipoPK(tipoContrato.getId(),
				combinacaoProdutoTipo.getId());
	}

	public TipoContratoCombinacaoProdutoTipoPK getId() {
		return id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TipoContratoCombinacaoProdutoTipo other = (TipoContratoCombinacaoProdutoTipo) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}