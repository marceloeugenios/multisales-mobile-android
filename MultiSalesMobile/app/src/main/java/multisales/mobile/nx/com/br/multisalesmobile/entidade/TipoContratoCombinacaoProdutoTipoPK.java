package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;

public class TipoContratoCombinacaoProdutoTipoPK {

    @Column(name = "_tipo_contrato")
	private Integer tipoContrato;
    @Column(name = "_combinacao_produto_tipo")
	private Integer combinacaoProdutoTipo;

	public TipoContratoCombinacaoProdutoTipoPK() {

	}

	public TipoContratoCombinacaoProdutoTipoPK(Integer tipoContrato, Integer combinacaoProdutoTipo) {
		this.tipoContrato = tipoContrato;
		this.combinacaoProdutoTipo = combinacaoProdutoTipo;
	}

	public Integer getCombinacaoProdutoTipo() {
		return combinacaoProdutoTipo;
	}

	public Integer getTipoContrato() {
		return tipoContrato;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + combinacaoProdutoTipo;
		result = prime * result + tipoContrato;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TipoContratoCombinacaoProdutoTipoPK other = (TipoContratoCombinacaoProdutoTipoPK) obj;
		if (combinacaoProdutoTipo != other.combinacaoProdutoTipo) {
			return false;
		}
		if (tipoContrato != other.tipoContrato) {
			return false;
		}
		return true;
	}
}