package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;

@Table(name = "tipo_ponto_adicional")
public class TipoPontoAdicional {

    @Id
	private Integer id;

	private String descricao;

	private ESituacao situacao = ESituacao.ATIVO;

    @Transient
	private List<VendaTvPontoAdicional> vendaTvPontoAdicionals;

	public TipoPontoAdicional() {
	}
	
	public TipoPontoAdicional(Integer id) {
		this.id = id;
	}

	public TipoPontoAdicional(String descricao) {
		this.descricao = descricao;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ESituacao getSituacao() {
		return this.situacao;
	}

	public List<VendaTvPontoAdicional> getVendaTvPontoAdicionals() {
		return this.vendaTvPontoAdicionals;
	}

	public void setVendaTvPontoAdicionals(
			List<VendaTvPontoAdicional> vendaTvPontoAdicionals) {
		this.vendaTvPontoAdicionals = vendaTvPontoAdicionals;
	}

	public VendaTvPontoAdicional addVendaTvPontoAdicional(
			VendaTvPontoAdicional vendaTvPontoAdicional) {
		getVendaTvPontoAdicionals().add(vendaTvPontoAdicional);
		vendaTvPontoAdicional.setTipoPontoAdicional(this);

		return vendaTvPontoAdicional;
	}

	public VendaTvPontoAdicional removeVendaTvPontoAdicional(
			VendaTvPontoAdicional vendaTvPontoAdicional) {
		getVendaTvPontoAdicionals().remove(vendaTvPontoAdicional);
		vendaTvPontoAdicional.setTipoPontoAdicional(null);

		return vendaTvPontoAdicional;
	}

}