package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;

public class Usuario {

    @Id
    private Integer id;

    private String nome;

    private String login;

    private String senha;

    private String imei;

    @JoinColumn(name = "_nivel")
    private Nivel nivel;

    public Usuario() {
    }

    public Usuario(Integer id) {
        this.id = id;
    }
    
    public Integer getId() {
		return id;
	}

    public String getNome() {
		return nome;
	}
    public String getLogin() {
		return login;
	}
    
    public String getSenha() {
		return senha;
	}
    
    public String getImei() {
		return imei;
	}
    
    public void setImei(String imei) {
		this.imei = imei;
	}
    
    public Nivel getNivel() {
		return nivel;
	}
}