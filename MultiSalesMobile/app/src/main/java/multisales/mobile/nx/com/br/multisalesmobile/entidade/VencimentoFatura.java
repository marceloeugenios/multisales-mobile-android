package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "vencimento_fatura")
public class VencimentoFatura {

	@Id
	private Integer vencimento;
	
	private ESituacao situacao = ESituacao.ATIVO;

	public VencimentoFatura() {
	}

	public VencimentoFatura(Integer vencimento) {
		this.vencimento = vencimento;
	}

	public Integer getVencimento() {
		return vencimento;
	}

	public void setVencimento(Integer vencimento) {
		this.vencimento = vencimento;
	}

	public ESituacao getSituacao() {
		return situacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((vencimento == null) ? 0 : vencimento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VencimentoFatura other = (VencimentoFatura) obj;
		if (vencimento == null) {
			if (other.vencimento != null) {
				return false;
			}
		} else if (!vencimento.equals(other.vencimento)) {
			return false;
		}
		return true;
	}
}