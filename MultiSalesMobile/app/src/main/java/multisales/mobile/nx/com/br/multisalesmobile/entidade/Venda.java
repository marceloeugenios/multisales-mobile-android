package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;

public class Venda implements Serializable {

    @Id
	private Integer id;

    @Column(name = "data_cadastro")
    @XmlElement(name = "data_cadastro")
	private Calendar dataCadastro;

    @Column(name = "data_instalacao")
    @XmlElement(name = "data_instalacao")
	private Calendar dataInstalacao;

	private EBoolean fidelidade;
	
	private String observacao;

	private BigDecimal oferta = new BigDecimal(0);

    @Column(name = "perfil_combo")
    @XmlElement(name = "perfil_combo")
	private String perfilCombo;

    @Column(name = "tipo_cliente")
    @XmlElement(name = "tipo_cliente")
	private String tipoCliente;

    @Column(name = "numero_parcelas_adesao")
    @XmlElement(name = "numero_parcelas_adesao")
	private Integer numeroParcelasAdesao;
	
	private BigDecimal valor = new BigDecimal(0);

    @Column(name = "valor_parcela_adesao")
    @XmlElement(name = "valor_parcela_adesao")
	private BigDecimal valorParcelaAdesao = new BigDecimal(0);

    @Column(name = "valor_total_parcela_adesao")
    @XmlElement(name = "valor_total_parcela_adesao")
	private BigDecimal valorTotalParcelaAdesao = new BigDecimal(0);

    @JoinColumn(name = "_cliente")
	private Cliente cliente;

    @JoinColumn(name = "_midia")
	private Midia midia;

    @JoinColumn(name = "_periodo_instalacao")
    @XmlElement(name = "periodo_instalacao")
	private PeriodoInstalacao periodoInstalacao;

    @JoinColumn(name = "_combinacao_produto_tipo")
    @XmlElement(name = "combinacao_produto_tipo")
	private CombinacaoProdutoTipo combinacaoProdutoTipo;

    @JoinColumn(name = "_tipo_contrato")
    @XmlElement(name = "tipo_contrato")
	private TipoContrato tipoContrato;

    @JoinColumn(name = "_ultima_ocorrencia")
    @XmlElement(name = "ultima_ocorrencia")
	private Ocorrencia ultimaOcorrencia;

    @Column(name = "data_ultima_alteracao")
    @XmlElement(name = "ultima_alteracao")
	private Calendar ultimaAlteracao;

    @Transient
    @XmlTransient
    private List<VendaInternet> vendasInternet;

    @XmlElement(name = "venda_internet")
    @JoinColumn(name = "_venda_internet")
	private VendaInternet vendaInternet;

    @Transient
    @XmlTransient
    private List<VendaTv> vendasTv;

    @XmlElement(name = "venda_tv")
    @JoinColumn(name = "_venda_tv")
	private VendaTv vendaTv;

    @Transient
    @XmlTransient
    private List<VendaFone> vendasFone;

    @JoinColumn(name = "_venda_fone")
    @XmlElement(name = "venda_fone")
	private VendaFone vendaFone;

    @Transient
    @XmlTransient
    private List<VendaCelular> vendasCelular;

    @JoinColumn(name = "_venda_celular")
    @XmlElement(name = "venda_celular")
	private VendaCelular vendaCelular;

    @Transient
    @XmlTransient
    private List<VendaFormaPagamento> vendaFormasPagamento;

    @JoinColumn(name = "_venda_forma_pagamento")
    @XmlElement(name = "venda_forma_pagamento")
	private VendaFormaPagamento vendaFormaPagamento;

    @Transient
    @XmlTransient
	private List<VendaInteracao> vendasInteracao;

    @Transient
    @XmlElement(name = "venda_motivos_nao_venda")
	private List<VendaMotivoNaoVenda> vendaMotivosNaoVenda;

    @Column(name = "numero_contrato")
    @XmlElement(name = "numero_contrato")
	private Long numeroContrato;

    @Column(name = "numero_proposta")
    @XmlElement(name = "numero_proposta")
	private Long numeroProposta;

    @XmlElement(name = "usuario_bloqueio")
    @Column(name = "_usuario_bloqueio")
	private Usuario usuarioBloqueio;

    @Column(name = "data_hora_bloqueio")
    @XmlElement(name = "data_hora_bloqueio")
	private Calendar dataHoraBloqueio;

	public Venda() {

	}

    public Venda(Integer idVenda) {
        this.id = idVenda;
    }

    public void setNullId() {
        this.setId(null);
    }

	public Integer getId() {
		return id;
	}

	public Midia getMidia() {
		return this.midia;
	}

	public void setMidia(Midia midia) {
		this.midia = midia;
	}

	public PeriodoInstalacao getPeriodoInstalacao() {
		return this.periodoInstalacao;
	}

	public void setPeriodoInstalacao(PeriodoInstalacao periodoInstalacao) {
		this.periodoInstalacao = periodoInstalacao;
	}

	public Calendar getDataCadastro() {
		return this.dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataInstalacao() {
		return this.dataInstalacao;
	}

	public void setDataInstalacao(Calendar dataInstalacao) {
		this.dataInstalacao = dataInstalacao;
	}

	public Integer getNumeroParcelasAdesao() {
		return this.numeroParcelasAdesao;
	}

	public void setNumeroParcelasAdesao(Integer numeroParcelasAdesao) {
		this.numeroParcelasAdesao = numeroParcelasAdesao;
	}

	public String getPerfilCombo() {
		return this.perfilCombo;
	}

	public void setPerfilCombo(String perfilCombo) {
		this.perfilCombo = perfilCombo;
	}

	public Calendar getUltimaAlteracao() {
		return this.ultimaAlteracao;
	}
	
	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValorParcelaAdesao() {
		return this.valorParcelaAdesao;
	}

	public void setValorParcelaAdesao(BigDecimal valorParcelaAdesao) {
		this.valorParcelaAdesao = valorParcelaAdesao;
	}

	public BigDecimal getValorTotalParcelaAdesao() {
		return this.valorTotalParcelaAdesao;
	}

	public void setValorTotalParcelaAdesao(BigDecimal valorTotalParcelaAdesao) {
		this.valorTotalParcelaAdesao = valorTotalParcelaAdesao;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public CombinacaoProdutoTipo getCombinacaoProdutoTipo() {
		return this.combinacaoProdutoTipo;
	}

	public void setCombinacaoProdutoTipo(
			CombinacaoProdutoTipo combinacaoProdutoTipo) {
		this.combinacaoProdutoTipo = combinacaoProdutoTipo;
	}

	public TipoContrato getTipoContrato() {
		return this.tipoContrato;
	}

	public void setTipoContrato(TipoContrato tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	public VendaCelular getVendaCelular() {
		return this.vendaCelular;
	}

	public void setVendaCelular(VendaCelular vendaCelular) {
		this.vendaCelular = vendaCelular;
	}

	public VendaFone getVendaFone() {
		return this.vendaFone;
	}

	public void setVendaFone(VendaFone vendaFone) {
		this.vendaFone = vendaFone;
	}

	public VendaFormaPagamento getVendaFormaPagamento() {
		return this.vendaFormaPagamento;
	}

	public void setVendaFormaPagamento(VendaFormaPagamento vendaFormaPagamento) {
		this.vendaFormaPagamento = vendaFormaPagamento;
	}

	public VendaInternet getVendaInternet() {
		return this.vendaInternet;
	}

	public void setVendaInternet(VendaInternet vendaInternet) {
		this.vendaInternet = vendaInternet;
	}

	public VendaTv getVendaTv() {
		return this.vendaTv;
	}

	public void setVendaTv(VendaTv vendaTv) {
		this.vendaTv = vendaTv;
	}

	public Ocorrencia getUltimaOcorrencia() {
		return ultimaOcorrencia;
	}

	public void setUltimaOcorrencia(Ocorrencia ultimaOcorrencia) {
		this.ultimaOcorrencia = ultimaOcorrencia;
	}

	public List<VendaInteracao> getVendasInteracao() {
		if (vendasInteracao == null) {
			return new ArrayList<VendaInteracao>();
		}
		return vendasInteracao;
	}

	public void setVendasInteracao(List<VendaInteracao> vendasInteracao) {
		this.vendasInteracao = vendasInteracao;
	}

	public VendaInteracao adicionarVendaInteracao(VendaInteracao vendaInteracao) {
		getVendasInteracao().add(vendaInteracao);
		vendaInteracao.setVenda(this);
		return vendaInteracao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    public void setUltimaAlteracao(Calendar ultimaAlteracao) {
        this.ultimaAlteracao = ultimaAlteracao;
    }

    public void setVendasInternet(List<VendaInternet> vendasInternet) {
        this.vendasInternet = vendasInternet;
    }

    public List<VendaFone> getVendasFone() {
        return vendasFone;
    }

    public void setVendasFone(List<VendaFone> vendasFone) {
        this.vendasFone = vendasFone;
    }

    public List<VendaCelular> getVendasCelular() {
        return vendasCelular;
    }

    public void setVendasCelular(List<VendaCelular> vendasCelular) {
        this.vendasCelular = vendasCelular;
    }

    public List<VendaFormaPagamento> getVendaFormasPagamento() {
        return vendaFormasPagamento;
    }

    public void setVendaFormasPagamento(List<VendaFormaPagamento> vendaFormasPagamento) {
        this.vendaFormasPagamento = vendaFormasPagamento;
    }

    @Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Venda other = (Venda) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public EBoolean getFidelidade() {
		return fidelidade;
	}

	public void setFidelidade(EBoolean fidelidade) {
		this.fidelidade = fidelidade;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public BigDecimal getOferta() {
		return oferta;
	}

	public void setOferta(BigDecimal oferta) {
		this.oferta = oferta;
	}

	public Usuario getUsuarioBloqueio() {
		return usuarioBloqueio;
	}

	public Calendar getDataHoraBloqueio() {
		return dataHoraBloqueio;
	}

	public void setUsuarioBloqueio(Usuario usuarioBloqueio) {
		this.usuarioBloqueio = usuarioBloqueio;
	}

	public void setDataHoraBloqueio(Calendar dataHoraBloqueio) {
		this.dataHoraBloqueio = dataHoraBloqueio;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<VendaMotivoNaoVenda> getVendaMotivosNaoVenda() {
		return vendaMotivosNaoVenda;
	}

	public void setVendaMotivosNaoVenda(
			List<VendaMotivoNaoVenda> vendaMotivosNaoVenda) {
		this.vendaMotivosNaoVenda = vendaMotivosNaoVenda;
	}

	public Long getNumeroContrato() {
		return numeroContrato;
	}

	public Long getNumeroProposta() {
		return numeroProposta;
	}

	public void setNumeroContrato(Long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public void setNumeroProposta(Long numeroProposta) {
		this.numeroProposta = numeroProposta;
	}

    public List<VendaInternet> getVendasInternet() {
        return vendasInternet;
    }

    public List<VendaTv> getVendasTv() {
        return vendasTv;
    }

    public void setVendasTv(List<VendaTv> vendasTv) {
        this.vendasTv = vendasTv;
    }

    public void addVendaCelular() {
        this.vendasCelular = new ArrayList<>();
        this.vendasCelular.add(vendaCelular);
    }

    public void addVendaTv() {
        this.vendasTv = new ArrayList<>();
        this.vendasTv.add(vendaTv);
    }

    public void addVendaFone() {
        this.vendasFone = new ArrayList<>();
        this.vendasFone.add(vendaFone);
    }

    public void addVendaInternet() {
        this.vendasInternet = new ArrayList<>();
        this.vendasInternet.add(vendaInternet);
    }

    public void addVendaFormaPagamento() {
        this.vendaFormasPagamento = new ArrayList<>();
        this.vendaFormasPagamento.add(vendaFormaPagamento);
    }
}