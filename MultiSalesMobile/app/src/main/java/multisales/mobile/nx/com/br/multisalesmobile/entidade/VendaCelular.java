package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;

@Table(name = "venda_celular")
public class VendaCelular {

    @Id
	private Integer id;

    @JoinColumn(name = "_operadora_portabilidade")
	private OperadoraTelefonia operadoraPortabilidade;

	private String telefone;

    @JoinColumn(name = "_produto")
	private Produto produto;

    @JoinColumn(name = "_tamanho_chip")
	private TamanhoChip tamanhoChip;

    @Transient
	private List<VendaCelularDependente> vendaCelularDependentes;

	public VendaCelular() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OperadoraTelefonia getOperadoraPortabilidade() {
		return this.operadoraPortabilidade;
	}

	public void setOperadoraPortabilidade(OperadoraTelefonia operadoraPortabilidade) {
		this.operadoraPortabilidade = operadoraPortabilidade;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public TamanhoChip getTamanhoChip() {
		return this.tamanhoChip;
	}

	public void setTamanhoChip(TamanhoChip tamanhoChip) {
		this.tamanhoChip = tamanhoChip;
	}

	public List<VendaCelularDependente> getVendaCelularDependentes() {
		if (this.vendaCelularDependentes == null) {
			this.vendaCelularDependentes = new ArrayList<VendaCelularDependente>();
		}
		return this.vendaCelularDependentes;
	}

	public void setVendaCelularDependentes(List<VendaCelularDependente> vendaCelularDependentes) {
		this.vendaCelularDependentes = vendaCelularDependentes;
	}

	public VendaCelularDependente addVendaCelularDependente(
			VendaCelularDependente vendaCelularDependente) {
		getVendaCelularDependentes().add(vendaCelularDependente);
		return vendaCelularDependente;
	}

	public VendaCelularDependente removeVendaCelularDependente(
			VendaCelularDependente vendaCelularDependente) {
		getVendaCelularDependentes().remove(vendaCelularDependente);
		return vendaCelularDependente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VendaCelular other = (VendaCelular) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    public void setNullId() {
        this.id = null;
    }
}