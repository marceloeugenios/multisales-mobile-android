package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

@Table(name = "venda_fone")
public class VendaFone {

    @Id
	private Integer id;

    @Column(name = "linha_tronco")
	private String linhaTronco;

	private String observacao;

    @Column(name = "taxa_habilitacao")
    @XmlElement(name = "taxa_habilitacao")
    private BigDecimal taxaHabilitacao = new BigDecimal(0);

    @Column(name = "valor_plano")
    @XmlElement(name = "valor_plano")
    private BigDecimal valorPlano = new BigDecimal(0);

    @JoinColumn(name = "_tecnologia_disponivel")
    @XmlElement(name = "tecnologia_disponivel")
    private TecnologiaDisponivel tecnologiaDisponivel;


    @Transient
	private List<VendaFoneLinha> vendaFoneLinhas;

    @Transient
	private List<VendaFonePortabilidade> vendaFonePortabilidades;

    @Transient
    private Venda venda;

	public VendaFone() {

	}

	public Integer getId() {
		return id;
	}

	public String getLinhaTronco() {
		return this.linhaTronco;
	}

	public void setLinhaTronco(String linhaTronco) {
		this.linhaTronco = linhaTronco;
	}

	public String getObservacao() {
		return this.observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public List<VendaFoneLinha> getVendaFoneLinhas() {
		if (this.vendaFoneLinhas == null) {
			this.vendaFoneLinhas = new ArrayList<VendaFoneLinha>();
		}
		return this.vendaFoneLinhas;
	}

	public void setVendaFoneLinhas(List<VendaFoneLinha> vendaFoneLinhas) {
		this.vendaFoneLinhas = vendaFoneLinhas;
	}

	public VendaFoneLinha addVendaFoneLinha(VendaFoneLinha vendaFoneLinha) {
		getVendaFoneLinhas().add(vendaFoneLinha);
		return vendaFoneLinha;
	}

	public VendaFoneLinha removeVendaFoneLinha(VendaFoneLinha vendaFoneLinha) {
		getVendaFoneLinhas().remove(vendaFoneLinha);
		return vendaFoneLinha;
	}

	public List<VendaFonePortabilidade> getVendaFonePortabilidades() {
		if (this.vendaFonePortabilidades == null) {
			this.vendaFonePortabilidades = new ArrayList<VendaFonePortabilidade>();
		}
		return this.vendaFonePortabilidades;
	}

	public void setVendaFonePortabilidades(
			List<VendaFonePortabilidade> vendaFonePortabilidades) {
		this.vendaFonePortabilidades = vendaFonePortabilidades;
	}

	public VendaFonePortabilidade addVendaFonePortabilidade(
			VendaFonePortabilidade vendaFonePortabilidade) {
		getVendaFonePortabilidades().add(vendaFonePortabilidade);
		vendaFonePortabilidade.setVendaFone(this);
		return vendaFonePortabilidade;
	}

	public VendaFonePortabilidade removeVendaFonePortabilidade(
			VendaFonePortabilidade vendaFonePortabilidade) {
		getVendaFonePortabilidades().remove(vendaFonePortabilidade);
		vendaFonePortabilidade.setVendaFone(null);
		return vendaFonePortabilidade;
	}

    public BigDecimal getTaxaHabilitacao() {
        return taxaHabilitacao;
    }

    public void setTaxaHabilitacao(BigDecimal taxaHabilitacao) {
        this.taxaHabilitacao = taxaHabilitacao;
    }

    public BigDecimal getValorPlano() {
        return valorPlano;
    }

    public void setValorPlano(BigDecimal valorPlano) {
        this.valorPlano = valorPlano;
    }

    public TecnologiaDisponivel getTecnologiaDisponivel() {
        return tecnologiaDisponivel;
    }

    public void setTecnologiaDisponivel(TecnologiaDisponivel tecnologiaDisponivel) {
        this.tecnologiaDisponivel = tecnologiaDisponivel;
    }

    @Override
	public String toString() {
		return "VendaFone [id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VendaFone other = (VendaFone) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    public void setNullId() {
        this.id = null;
    }
}