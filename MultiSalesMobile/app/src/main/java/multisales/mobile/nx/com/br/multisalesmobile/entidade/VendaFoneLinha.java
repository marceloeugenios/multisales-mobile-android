package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.math.BigDecimal;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

@Table(name = "venda_fone_linha")
public class VendaFoneLinha {

    @Id
	private Integer id;

	private Integer extensoes;

    @Column(name = "publicar_numero")
	private EBoolean publicarNumero = EBoolean.TRUE;

    @JoinColumn(name = "_produto")
	private Produto produto;

    @JoinColumn(name = "_venda_fone")
    private VendaFone vendaFone;

    @Column(name = "mensalidade")
    private BigDecimal mensalidade = new BigDecimal(0);

    @Column(name = "minuto_excedido_para_fixo")
    @XmlElement(name = "minuto_excedido_para_fixo")
    private BigDecimal minutoExcedidoParaFixo = new BigDecimal(0);

    @Column(name = "habilitacao")
    @XmlElement(name = "habilitacao")
    private BigDecimal habilitacao = new BigDecimal(0);



	public VendaFoneLinha() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getExtensoes() {
		return this.extensoes;
	}

	public void setExtensoes(Integer extensoes) {
		this.extensoes = extensoes;
	}

	public EBoolean getPublicarNumero() {
		return this.publicarNumero;
	}

	public void setPublicarNumero(EBoolean publicarNumero) {
		this.publicarNumero = publicarNumero;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

    public VendaFone getVendaFone() {
        return vendaFone;
    }

    public void setVendaFone(VendaFone vendaFone) {
        this.vendaFone = vendaFone;
    }

    public BigDecimal getMensalidade() {
        return mensalidade;
    }

    public void setMensalidade(BigDecimal mensalidade) {
        this.mensalidade = mensalidade;
    }

    public BigDecimal getMinutoExcedidoParaFixo() {
        return minutoExcedidoParaFixo;
    }

    public void setMinutoExcedidoParaFixo(BigDecimal minutoExcedidoParaFixo) {
        this.minutoExcedidoParaFixo = minutoExcedidoParaFixo;
    }

    public BigDecimal getHabilitacao() {
        return habilitacao;
    }

    public void setHabilitacao(BigDecimal habilitacao) {
        this.habilitacao = habilitacao;
    }

    public void setNullId() {
        this.id = null;
    }
}