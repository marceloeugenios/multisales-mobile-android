package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "venda_fone_portabilidade")
public class VendaFonePortabilidade {

    @Id
	private Integer id;

    @JoinColumn(name = "_operadora_portabilidade")
	private OperadoraTelefonia operadoraPortabilidade;

    @Column(name = "telefone_portado")
	private String telefonePortado;

    @JoinColumn(name = "_venda_fone")
	private VendaFone vendaFone;

	public VendaFonePortabilidade() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OperadoraTelefonia getOperadoraPortabilidade() {
		return this.operadoraPortabilidade;
	}

	public void setOperadoraPortabilidade(OperadoraTelefonia operadoraPortabilidade) {
		this.operadoraPortabilidade = operadoraPortabilidade;
	}

	public String getTelefonePortado() {
		return this.telefonePortado;
	}

	public void setTelefonePortado(String telefonePortado) {
		this.telefonePortado = telefonePortado;
	}

	public VendaFone getVendaFone() {
		return this.vendaFone;
	}

	public void setVendaFone(VendaFone vendaFone) {
		this.vendaFone = vendaFone;
	}

    public void setNullId() {
        this.id = null;
    }
}