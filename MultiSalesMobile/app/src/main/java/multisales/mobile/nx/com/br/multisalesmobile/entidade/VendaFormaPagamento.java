package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "venda_forma_pagamento")
public class VendaFormaPagamento {

    @Id
	private Integer id;

    @Column(name = "cpf_na_nota")
	private EBoolean cpfNota = EBoolean.FALSE;

    @Column(name = "fatura_somente_por_email")
	private EBoolean faturaSomentePorEmail = EBoolean.FALSE;

    @Column(name = "informou_sobre_multa")
	private EBoolean informouSobreMulta = EBoolean.FALSE;

    @Column(name = "receber_campanha_publicitaria")
	private EBoolean receberCampanhaPublicitaria = EBoolean.FALSE;

    @JoinColumn(name = "_vencimento_fatura")
	private VencimentoFatura vencimentoFatura;

    @JoinColumn(name = "_venda_forma_pagamento_banco")
	private VendaFormaPagamentoBanco vendaFormaPagamentoBanco;

	public VendaFormaPagamento() {
		
	}

	public Integer getId() {
		return id;
	}

	public VencimentoFatura getVencimentoFatura() {
		return this.vencimentoFatura;
	}

	public void setVencimentoFatura(VencimentoFatura vencimentoFatura) {
		this.vencimentoFatura = vencimentoFatura;
	}

	public EBoolean getCpfNota() {
		return this.cpfNota;
	}

	public void setCpfNota(EBoolean cpfNota) {
		this.cpfNota = cpfNota;
	}

	public EBoolean getFaturaSomentePorEmail() {
		return this.faturaSomentePorEmail;
	}

	public void setFaturaSomentePorEmail(EBoolean faturaSomentePorEmail) {
		this.faturaSomentePorEmail = faturaSomentePorEmail;
	}

	public EBoolean getInformouSobreMulta() {
		return this.informouSobreMulta;
	}

	public void setInformouSobreMulta(EBoolean informouSobreMulta) {
		this.informouSobreMulta = informouSobreMulta;
	}

	public EBoolean getReceberCampanhaPublicitaria() {
		return this.receberCampanhaPublicitaria;
	}

	public void setReceberCampanhaPublicitaria(
			EBoolean receberCampanhaPublicitaria) {
		this.receberCampanhaPublicitaria = receberCampanhaPublicitaria;
	}

	public VendaFormaPagamentoBanco getVendaFormaPagamentoBanco() {
		return this.vendaFormaPagamentoBanco;
	}

	public void setVendaFormaPagamentoBanco(
			VendaFormaPagamentoBanco vendaFormaPagamentoBanco) {
		this.vendaFormaPagamentoBanco = vendaFormaPagamentoBanco;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}