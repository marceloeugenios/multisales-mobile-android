package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "venda_forma_pagamento_banco")
public class VendaFormaPagamentoBanco {

    @Id
	private Integer id;

    @JoinColumn(name = "_banco")
	private Banco banco;

	private String agencia;

	private String conta;

    @Column(name = "nome_titular")
	private String nomeTitular;

	public VendaFormaPagamentoBanco() {
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public Banco getBanco() {
		return this.banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return this.agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return this.conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getNomeTitular() {
		return this.nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VendaFormaPagamentoBanco other = (VendaFormaPagamentoBanco) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}