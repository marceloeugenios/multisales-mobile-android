package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;

@Table(name = "venda_interacao")
public class VendaInteracao {

    @Id
	private Integer id;

    @JoinColumn(name = "_usuario")
	private Usuario usuario;

    @JoinColumn(name = "_venda")
	private Venda venda;

    @Column(name = "data_interacao")
	private Calendar dataInteracao;

    @Transient
	private List<VendaInteracaoDetalhamento> vendasInteracaoDetalhamento;

	public VendaInteracao() {
	}	

	public Integer getId() {
		return id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Calendar getDataInteracao() {
		return dataInteracao;
	}
	
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public Venda getVenda() {
		return venda;
	}

	private void setDataInteracao() {
		this.dataInteracao = Calendar.getInstance();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VendaInteracao other = (VendaInteracao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public List<VendaInteracaoDetalhamento> getVendasInteracaoDetalhamento() {
		return vendasInteracaoDetalhamento;
	}

	public void setVendasInteracaoDetalhamento(
			List<VendaInteracaoDetalhamento> vendasInteracaoDetalhamento) {
		this.vendasInteracaoDetalhamento = vendasInteracaoDetalhamento;
	}
}