package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.EmbeddedId;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "venda_interacao_detalhamento")
public class VendaInteracaoDetalhamento {

    @EmbeddedId
	private VendaInteracaoDetalhamentoPK id;

	public VendaInteracaoDetalhamento() {
		
	}
	
	public VendaInteracaoDetalhamento(VendaInteracao vendaInteracao, Detalhamento detalhamento) {
		this.id = new VendaInteracaoDetalhamentoPK(vendaInteracao.getId(), detalhamento.getId());
	}
	
	public VendaInteracaoDetalhamentoPK getId() {
		return id;
	}

	public VendaInteracaoDetalhamento(VendaInteracaoDetalhamentoPK id) {
		super();
		this.id = id;
	}
}