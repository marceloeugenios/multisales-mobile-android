package multisales.mobile.nx.com.br.multisalesmobile.entidade;

public class VendaInteracaoDetalhamentoPK {

	private Integer vendaInteracao;
	private Integer detalhamento;

	public VendaInteracaoDetalhamentoPK(Integer vendaInteracao, Integer detalhamento) {
		this.vendaInteracao = vendaInteracao;
		this.detalhamento = detalhamento;
	}

	public VendaInteracaoDetalhamentoPK() {
	}

	public Integer getVendaInteracao() {
		return vendaInteracao;
	}

	public void setVendaInteracao(Integer vendaInteracao) {
		this.vendaInteracao = vendaInteracao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + detalhamento;
		result = prime * result + vendaInteracao;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VendaInteracaoDetalhamentoPK other = (VendaInteracaoDetalhamentoPK) obj;
		if (detalhamento != other.detalhamento) {
			return false;
		}
		if (vendaInteracao != other.vendaInteracao) {
			return false;
		}
		return true;
	}
}