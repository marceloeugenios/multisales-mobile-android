package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

/**
 * Created by samara on 14/03/15.
 */

@Table(name = "venda_internet_produto_adicional")
public class VendaInternetProdutoAdicional {

    @JoinColumn(name = "_venda_internet")
    private VendaInternet vendaInternet;

    @JoinColumn(name = "_produto")
    private Produto produto;

    public VendaInternetProdutoAdicional() {

    }

    public VendaInternetProdutoAdicional(VendaInternet vendaInternet, Produto produto) {
        this.vendaInternet = vendaInternet;
        this.produto = produto;
    }

    public VendaInternet getVendaInternet() {
        return vendaInternet;
    }

    public void setVendaInternet(VendaInternet vendaInternet) {
        this.vendaInternet = vendaInternet;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VendaInternetProdutoAdicional that = (VendaInternetProdutoAdicional) o;

        if (produto != null ? !produto.equals(that.produto) : that.produto != null) return false;
        if (vendaInternet != null ? !vendaInternet.equals(that.vendaInternet) : that.vendaInternet != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = vendaInternet != null ? vendaInternet.hashCode() : 0;
        result = 31 * result + (produto != null ? produto.hashCode() : 0);
        return result;
    }
}
