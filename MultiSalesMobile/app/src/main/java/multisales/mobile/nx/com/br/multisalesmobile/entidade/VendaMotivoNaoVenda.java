package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.EmbeddedId;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "venda_motivo_nao_venda")
public class VendaMotivoNaoVenda {

    @EmbeddedId
	private VendaMotivoNaoVendaPk  id;

    @JoinColumn(name = "_concorrente_migracao")
	private Concorrente concorrenteMigracao;

    @JoinColumn(name = "_motivo_migracao")
	private MotivoMigracao motivoMigracao;
	
	public VendaMotivoNaoVenda() {
		
	}

	public VendaMotivoNaoVenda(Venda venda, MotivoNaoVenda motivoNaoVenda) {
		this.id = new VendaMotivoNaoVendaPk(motivoNaoVenda.getId(), venda.getId());
	}

	public VendaMotivoNaoVendaPk getId() {
		return id;
	}

	public void setId(VendaMotivoNaoVendaPk id) {
		this.id = id;
	}

	public void setMotivoNaoVenda(MotivoNaoVenda motivoNaoVenda) {
		this.id.setMotivoNaoVenda(motivoNaoVenda.getId());
	}

	public Concorrente getConcorrenteMigracao() {
		return concorrenteMigracao;
	}

	public void setConcorrenteMigracao(Concorrente concorrenteMigracao) {
		this.concorrenteMigracao = concorrenteMigracao;
	}

	public MotivoMigracao getMotivoMigracao() {
		return motivoMigracao;
	}

	public void setMotivoMigracao(MotivoMigracao motivoMigracao) {
		this.motivoMigracao = motivoMigracao;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VendaMotivoNaoVenda that = (VendaMotivoNaoVenda) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
	public String toString() {
		return "VendaMotivoNaoVenda [id=" + id + ", "
				+ ", concorrenteMigracao=" + concorrenteMigracao.getDescricao()
				+ ", motivoMigracao=" + motivoMigracao.getDescricao() + "]";
	}
}
