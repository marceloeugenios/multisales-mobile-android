package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;

public class VendaMotivoNaoVendaPk {

    @JoinColumn(name = "_venda")
	private Integer venda;

    @JoinColumn(name = "_motivo_nao_venda")
	private Integer motivoNaoVenda;
	
	public VendaMotivoNaoVendaPk() {
		
	}

	public VendaMotivoNaoVendaPk(Integer idMotivo, Integer idVenda) {
		this.motivoNaoVenda = idMotivo;
		if (idVenda != null) {
			this.venda = idVenda;
		}
	}

	public int getVenda() {
		return venda;
	}

	public int getMotivoNaoVenda() {
		return motivoNaoVenda;
	}

	public void setVenda(int venda) {
		this.venda = venda;
	}

	public void setMotivoNaoVenda(int motivoNaoVenda) {
		this.motivoNaoVenda = motivoNaoVenda;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + motivoNaoVenda;
		result = prime * result + venda;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VendaMotivoNaoVendaPk other = (VendaMotivoNaoVendaPk) obj;
		if (motivoNaoVenda != other.motivoNaoVenda) {
			return false;
		}
		if (venda != other.venda) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "VendaMotivoNaoVendaPk [venda=" + venda + ", motivoNaoVenda="
				+ motivoNaoVenda + "]";
	}
}
