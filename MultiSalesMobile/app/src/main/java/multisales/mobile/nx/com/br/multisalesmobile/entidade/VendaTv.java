package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

@Table(name = "venda_tv")
public class VendaTv {

    @Id
	private Integer id;

	private String degustacao;

    @Column(name = "descricao_a_la_carte")
	private String descricaoALaCarte;

    @JoinColumn(name = "_concorrente")
	private Concorrente concorrenteMigracao;

    @JoinColumn(name = "_motivo_migracao")
	private MotivoMigracao motivoMigracao;

    @JoinColumn(name = "_produto")
	private Produto produto;

    @Transient
	private List<Produto> produtos;

    @Transient
	private List<VendaTvPontoAdicional> vendaTvPontosAdicionais;

    @Column(name = "taxa_instalacao")
    @XmlElement(name = "taxa_instalacao")
    private BigDecimal taxaInstalacao = new BigDecimal(0);

    @Column(name = "valor_promocional")
    @XmlElement(name = "valor_promocional")
    private BigDecimal valorPromocional = new BigDecimal(0);

    @Column(name = "valor_pos_promocao")
    @XmlElement(name = "valor_pos_promocao")
    private BigDecimal valorPosPromocao = new BigDecimal(0);

    @Column(name = "data_instalacao")
    @XmlElement(name = "data_instalacao")
    private Calendar dataInstalacao;

    @Column(name = "vigencia_promocao")
    @XmlElement(name = "vigencia_promocao")
    private Calendar vigenciaPromocao;

	public VendaTv() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Concorrente getConcorrenteMigracao() {
		return this.concorrenteMigracao;
	}

	public void setConcorrenteMigracao(Concorrente concorrenteMigracao) {
		this.concorrenteMigracao = concorrenteMigracao;
	}

	public String getDegustacao() {
		return this.degustacao;
	}

	public void setDegustacao(String degustacao) {
		this.degustacao = degustacao;
	}

	public String getDescricaoALaCarte() {
		return this.descricaoALaCarte;
	}

	public void setDescricaoALaCarte(String descricaoALaCarte) {
		this.descricaoALaCarte = descricaoALaCarte;
	}

	public MotivoMigracao getMotivoMigracao() {
		return this.motivoMigracao;
	}

	public void setMotivoMigracao(MotivoMigracao motivoMigracao) {
		this.motivoMigracao = motivoMigracao;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public void setVenda(Venda venda) {
		this.id = venda.getId();
	}

	public List<Produto> getProdutos() {
		if (this.produtos == null) {
			this.produtos = new ArrayList<Produto>();
		}
		return this.produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public List<VendaTvPontoAdicional> getVendaTvPontosAdicionais() {
		if (this.vendaTvPontosAdicionais == null) {
			this.vendaTvPontosAdicionais = new ArrayList<VendaTvPontoAdicional>();
		}
		return this.vendaTvPontosAdicionais;
	}

	public void setVendaTvPontosAdicionais(
			List<VendaTvPontoAdicional> vendaTvPontoAdicionals) {
		this.vendaTvPontosAdicionais = vendaTvPontoAdicionals;
	}

	public VendaTvPontoAdicional addVendaTvPontoAdicional(
			VendaTvPontoAdicional vendaTvPontoAdicional) {
		getVendaTvPontosAdicionais().add(vendaTvPontoAdicional);
		return vendaTvPontoAdicional;
	}

	public VendaTvPontoAdicional removeVendaTvPontoAdicional(
			VendaTvPontoAdicional vendaTvPontoAdicional) {
		getVendaTvPontosAdicionais().remove(vendaTvPontoAdicional);
		return vendaTvPontoAdicional;
	}

    public BigDecimal getTaxaInstalacao() {
        return taxaInstalacao;
    }

    public void setTaxaInstalacao(BigDecimal taxaInstalacao) {
        this.taxaInstalacao = taxaInstalacao;
    }

    public BigDecimal getValorPromocional() {
        return valorPromocional;
    }

    public void setValorPromocional(BigDecimal valorPromocional) {
        this.valorPromocional = valorPromocional;
    }

    public Calendar getDataInstalacao() {
        return dataInstalacao;
    }

    public void setDataInstalacao(Calendar dataInstalacao) {
        this.dataInstalacao = dataInstalacao;
    }

    public BigDecimal getValorPosPromocao() {
        return valorPosPromocao;
    }

    public void setValorPosPromocao(BigDecimal valorPosPromocao) {
        this.valorPosPromocao = valorPosPromocao;
    }

    public Calendar getVigenciaPromocao() {
        return vigenciaPromocao;
    }

    public void setVigenciaPromocao(Calendar vigenciaPromocao) {
        this.vigenciaPromocao = vigenciaPromocao;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VendaTv other = (VendaTv) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    public void setNullId() {
        this.id = null;
    }
}