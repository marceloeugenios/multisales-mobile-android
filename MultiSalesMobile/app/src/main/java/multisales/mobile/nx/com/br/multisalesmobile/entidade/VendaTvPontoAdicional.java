package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.math.BigDecimal;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "venda_tv_ponto_adicional")
public class VendaTvPontoAdicional {

    @Id
	private Integer id;

    @JoinColumn(name = "_venda_tv")
    private VendaTv vendaTv;

    @JoinColumn(name = "_tipo_ponto_adicional")
	private TipoPontoAdicional tipoPontoAdicional;

    @Column(name = "custo_ponto_adicional")
	private BigDecimal custoPontoAdicional = new BigDecimal(0);


	public VendaTvPontoAdicional() {
	}

    public VendaTvPontoAdicional(TipoPontoAdicional tipoPontoAdicional,
                                 BigDecimal custoPontoAdicional) {
        this.tipoPontoAdicional = tipoPontoAdicional;
        this.custoPontoAdicional = custoPontoAdicional;
    }

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TipoPontoAdicional getTipoPontoAdicional() {
		return this.tipoPontoAdicional;
	}

	public void setTipoPontoAdicional(TipoPontoAdicional tipoPontoAdicional) {
		this.tipoPontoAdicional = tipoPontoAdicional;
	}

	public BigDecimal getCustoPontoAdicional() {
		return custoPontoAdicional;
	}

	public void setCustoPontoAdicional(BigDecimal custoPontoAdicional) {
		this.custoPontoAdicional = custoPontoAdicional;
	}

    public VendaTv getVendaTv() {
        return vendaTv;
    }

    public void setVendaTv(VendaTv vendaTv) {
        this.vendaTv = vendaTv;
    }

    public void setNullId() {
        this.id = null;
    }
}