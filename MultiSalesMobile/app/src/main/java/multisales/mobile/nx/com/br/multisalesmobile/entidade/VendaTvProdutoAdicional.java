package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.EmbeddedId;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

/**
 * Created by samara on 27/02/15.
 */

@Table(name = "venda_tv_produto_adicional")
public class VendaTvProdutoAdicional {

    @EmbeddedId
    private VendaTvProdutoAdicionalPk id;

    public VendaTvProdutoAdicional() {

    }

    public VendaTvProdutoAdicional(Integer produto, Integer vendaTv) {
        this.id = new VendaTvProdutoAdicionalPk(produto, vendaTv);
    }

    public VendaTvProdutoAdicionalPk getId() {
        return id;
    }

    public void setId(VendaTvProdutoAdicionalPk id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VendaTvProdutoAdicional that = (VendaTvProdutoAdicional) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "VendaTvProdutoAdicional{" +
                "id=" + id.toString() +
                '}';
    }
}
