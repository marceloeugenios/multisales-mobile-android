package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;

/**
 * Created by samara on 27/02/15.
 */
public class VendaTvProdutoAdicionalPk {

    @Column(name = "_produto")
    private Integer produto;

    @Column(name = "_venda_tv")
    private Integer vendaTv;

    public VendaTvProdutoAdicionalPk() {

    }

    public VendaTvProdutoAdicionalPk(Integer produto, Integer vendaTv) {
        this.produto = produto;
        this.vendaTv = vendaTv;
    }

    public Integer getProduto() {
        return produto;
    }

    public void setProduto(Integer produto) {
        this.produto = produto;
    }

    public Integer getVendaTv() {
        return vendaTv;
    }

    public void setVendaTv(Integer vendaTv) {
        this.vendaTv = vendaTv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VendaTvProdutoAdicionalPk that = (VendaTvProdutoAdicionalPk) o;

        if (!produto.equals(that.produto)) return false;
        if (!vendaTv.equals(that.vendaTv)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = produto.hashCode();
        result = 31 * result + vendaTv.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "VendaTvProdutoAdicionalPk{" +
                "produto=" + produto +
                ", vendaTv=" + vendaTv +
                '}';
    }
}
