package multisales.mobile.nx.com.br.multisalesmobile.entidade;

import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContatoCargo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioOperacaoComercial;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioPlantaoSituacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioStatusInfra;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlElement;

@Table(name = "versao_pacote")
public class VersaoPacote {
    @Id
	private Integer id;
	private Float versao;
    @Column(name = "data_cadastro")
    @XmlElement(name = "data_cadastro")
	private Calendar dataCadastro;
    @Column(name = "data_atualizacao")
    private Calendar dataAtualizacao;
	private String descricao;
    @Transient
    private List<Midia> midias;
    @Transient
    @XmlElement(name = "motivos_tabulacao")
    private List<MotivoTabulacao> motivosTabulacao;
    @Transient
    @XmlElement(name = "motivos_tabulacao_tipos")
    private List<MotivoTabulacaoTipo> motivoTabulacaoTipos;
    @Transient
    private List<Concorrente> concorrentes;
    @Transient
    @XmlElement(name = "motivos_migracao")
    private List<MotivoMigracao> motivosMigracao;
    @Transient
    @XmlElement(name = "cidades")
    private List<Cidade> cidades;
    @Transient
    @XmlElement(name = "estados")
    private List<Estado> estados;
    @Transient
    @XmlElement(name = "estados_civil")
    private List<EstadoCivil> estadosCivil;
    @Transient
    @XmlElement(name = "escolaridades")
    private List<Escolaridade> escolaridades;
    @Transient
    @XmlElement(name = "motivos_nao_venda")
    private List<MotivoNaoVenda> motivosNaoVenda;
    @Transient
    @XmlElement(name = "tipos_contratos")
    private List<TipoContrato> tiposContratos;
    @Transient
    @XmlElement(name = "produtos")
    private List<Produto> produtos;
    @Transient
    @XmlElement(name = "produtos_tipos")
    private List<ProdutoTipo> produtosTipos;
    @Transient
    @XmlElement(name = "combinacoes_produto_tipo")
    private List<CombinacaoProdutoTipo> combinacoesProdutoTipo;
    @Transient
    @XmlElement(name = "combinacoes_produto_tipo_produto_tipo")
    private List<CombinacaoProdutoTipoProdutoTipo> combinacoesProdutoTipoProdutoTipo;
    @Transient
    @XmlElement(name = "tipos_contrato_combinacao_produto_tipo")
    private List<TipoContratoCombinacaoProdutoTipo> tiposContratoCombinacaoProdutoTipo;
    @Transient
    @XmlElement(name = "bandeiras_sistema")
    private List<BandeiraSistema> bandeirasSistema;
    @Transient
    @XmlElement(name = "bandeiras_sistema_produtos")
    private List<BandeiraSistemaProduto> bandeirasSistemaProdutos;
    @Transient
    @XmlElement(name = "tipos_pontos_adicionais")
    private List<TipoPontoAdicional> tiposPontosAdicionais;
    @Transient
    @XmlElement(name = "operadoras_telefonia")
    private List<OperadoraTelefonia> operadorasTelefonia;
    @Transient
    @XmlElement(name = "tamanhos_chip")
    private List<TamanhoChip> tamanhosChip;
    @Transient
    @XmlElement(name = "tipos_compartilhamento")
    private List<TipoCompartilhamento> tiposCompartilhamento;
    @Transient
    @XmlElement(name = "vencimentos_fatura")
    private List<VencimentoFatura> vencimentosFatura;
    @Transient
    @XmlElement(name = "bancos")
    private List<Banco> bancos;
    @Transient
    @XmlElement(name = "condominio_contato_cargo")
    private List<CondominioContatoCargo> condominioContatoCargos;
    @Transient
    @XmlElement(name = "condominio_operacao_comercial")
    private List<CondominioOperacaoComercial> condominioOperacoesComerciais;
    @Transient
    @XmlElement(name = "condominio_plantao_situacao")
    private List<CondominioPlantaoSituacao> condominioPlantoesSituacoes;
    @Transient
    @XmlElement(name = "periodos_instalacao")
    private List<PeriodoInstalacao> periodosInstalacao;
    @Transient
    @XmlElement(name = "condominio_status_infra")
    private List<CondominioStatusInfra> condominioStatusInfras;
    @Transient
    @XmlElement(name = "tecnologias_disponiveis")
    private List<TecnologiaDisponivel> tecnologiasDisponiveis;

    public String getDescricao() {
		return descricao;
	}

	public Integer getId() {
		return id;
	}

	public Float getVersao() {
		return versao;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

    public Calendar getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Calendar dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public List<Midia> getMidias() {
        return midias;
    }

    public List<MotivoTabulacao> getMotivosTabulacao() {
        return motivosTabulacao;
    }

    public List<MotivoTabulacaoTipo> getMotivoTabulacaoTipos() {
        return motivoTabulacaoTipos;
    }

    public List<Concorrente> getConcorrentes() {
        return concorrentes;
    }

    public List<MotivoMigracao> getMotivosMigracao() {
        return motivosMigracao;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public List<EstadoCivil> getEstadosCivil() {
        return estadosCivil;
    }

    public List<Escolaridade> getEscolaridades() {
        return escolaridades;
    }

    public List<MotivoNaoVenda> getMotivosNaoVenda() {
        return motivosNaoVenda;
    }

    public List<TipoContrato> getTiposContratos() {
        return tiposContratos;
    }

    public void setTiposContratos(List<TipoContrato> tiposContratos) {
        this.tiposContratos = tiposContratos;
    }

    public List<CombinacaoProdutoTipo> getCombinacoesProdutoTipo() {
        return combinacoesProdutoTipo;
    }

    public void setCombinacoesProdutoTipo(List<CombinacaoProdutoTipo> combinacoesProdutoTipo) {
        this.combinacoesProdutoTipo = combinacoesProdutoTipo;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public List<ProdutoTipo> getProdutosTipos() {
        return produtosTipos;
    }

    public void setProdutosTipos(List<ProdutoTipo> produtosTipos) {
        this.produtosTipos = produtosTipos;
    }

    public List<CombinacaoProdutoTipoProdutoTipo> getCombinacoesProdutoTipoProdutoTipo() {
        return combinacoesProdutoTipoProdutoTipo;
    }

    public void setCombinacoesProdutoTipoProdutoTipo(List<CombinacaoProdutoTipoProdutoTipo> combinacoesProdutoTipoProdutoTipo) {
        this.combinacoesProdutoTipoProdutoTipo = combinacoesProdutoTipoProdutoTipo;
    }

    public List<TipoContratoCombinacaoProdutoTipo> getTiposContratoCombinacaoProdutoTipo() {
        return tiposContratoCombinacaoProdutoTipo;
    }

    public void setTiposContratoCombinacaoProdutoTipo(List<TipoContratoCombinacaoProdutoTipo> tiposContratoCombinacaoProdutoTipo) {
        this.tiposContratoCombinacaoProdutoTipo = tiposContratoCombinacaoProdutoTipo;
    }

    public List<BandeiraSistema> getBandeirasSistema() {
        return bandeirasSistema;
    }

    public void setBandeirasSistema(List<BandeiraSistema> bandeirasSistema) {
        this.bandeirasSistema = bandeirasSistema;
    }

    public List<BandeiraSistemaProduto> getBandeirasSistemaProdutos() {
        return bandeirasSistemaProdutos;
    }

    public void setBandeirasSistemaProdutos(List<BandeiraSistemaProduto> bandeirasSistemaProdutos) {
        this.bandeirasSistemaProdutos = bandeirasSistemaProdutos;
    }

    public List<TipoPontoAdicional> getTiposPontosAdicionais() {
        return tiposPontosAdicionais;
    }

    public void setTiposPontosAdicionais(List<TipoPontoAdicional> tiposPontosAdicionais) {
        this.tiposPontosAdicionais = tiposPontosAdicionais;
    }

    public List<OperadoraTelefonia> getOperadorasTelefonia() {
        return operadorasTelefonia;
    }

    public void setOperadorasTelefonia(List<OperadoraTelefonia> operadorasTelefonia) {
        this.operadorasTelefonia = operadorasTelefonia;
    }

    public List<TamanhoChip> getTamanhosChip() {
        return tamanhosChip;
    }

    public void setTamanhosChip(List<TamanhoChip> tamanhosChip) {
        this.tamanhosChip = tamanhosChip;
    }

    public List<TipoCompartilhamento> getTiposCompartilhamento() {
        return tiposCompartilhamento;
    }

    public void setTiposCompartilhamento(List<TipoCompartilhamento> tiposCompartilhamento) {
        this.tiposCompartilhamento = tiposCompartilhamento;
    }

    public List<VencimentoFatura> getVencimentosFatura() {
        return vencimentosFatura;
    }

    public List<Banco> getBancos() {
        return bancos;
    }

    public List<CondominioContatoCargo> getCondominioContatoCargos() {
        return condominioContatoCargos;
    }

    public void setCondominioContatoCargos(List<CondominioContatoCargo> condominioContatoCargos) {
        this.condominioContatoCargos = condominioContatoCargos;
    }

    public List<CondominioOperacaoComercial> getCondominioOperacoesComerciais() {
        return condominioOperacoesComerciais;
    }

    public void setCondominioOperacoesComerciais(List<CondominioOperacaoComercial> condominioOperacoesComerciais) {
        this.condominioOperacoesComerciais = condominioOperacoesComerciais;
    }

    public List<CondominioPlantaoSituacao> getCondominioPlantoesSituacoes() {
        return condominioPlantoesSituacoes;
    }

    public void setCondominioPlantoesSituacoes(List<CondominioPlantaoSituacao> condominioPlantoesSituacoes) {
        this.condominioPlantoesSituacoes = condominioPlantoesSituacoes;
    }

    public List<CondominioStatusInfra> getCondominioStatusInfras() {
        return condominioStatusInfras;
    }

    public List<PeriodoInstalacao> getPeriodosInstalacao() {
        return periodosInstalacao;
    }

    public void setCondominioStatusInfras(List<CondominioStatusInfra> condominioStatusInfras) {
        this.condominioStatusInfras = condominioStatusInfras;
    }

    public List<TecnologiaDisponivel> getTecnologiasDisponiveis() {
        return tecnologiasDisponiveis;
    }

    public void setTecnologiasDisponiveis(List<TecnologiaDisponivel> tecnologiasDisponiveis) {
        this.tecnologiasDisponiveis = tecnologiasDisponiveis;
    }
}
