package multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio;

import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.ESituacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Transient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;


public class Condominio {


    @Id
    @Column(name = "id_local")
    @XmlTransient
    private Integer idLocal;

    private Integer id;

	private String nome;

	private ESituacao situacao = ESituacao.ATIVO;

    @Column(name = "cod_fttx")
	private String codFttx;

    @Column(name = "chave_endereco")
	private String chaveEndereco;

    @Column(name = "classe_social")
	private String classeSocial;

    @Column(name = "tipo_condominio")
	private String tipoCondominio;


	private String segmento;

	private ECondominioLayout layout;

	private String prioridade;

    @Column(name = "segmentacao_mkt")
	private String segmentacaoMkt;

    @Column(name = "cod_log")
	private String codLog;

    @Column(name = "chave_cep")
	private String chaveCep;

	@Column(name = "quantidade_bloco")
	private Integer quantidadeBloco;

	@Column(name = "quantidade_apartamento")
	private String quantidadeApartamento;

	private String cnl;

	@Column(name = "area_telefonica")
	private String areaTelefonica;

    @JoinColumn(name = "_endereco")
    private Endereco endereco;

    @JoinColumn(name = "_condominio_status_infra")
	private CondominioStatusInfra condominioStatusInfra;

    @Column(name = "data_concl_infra")
    private Calendar dataConclInfra;

	@Column(name = "capacidade_equipamento")
	private Integer capacidadeEquipamento;

	@Column(name = "ocupacao_equipamento")
	private Integer ocupacaoEquipamento;

	@Column(name = "fibra_livre")
	private Integer fibraLivre;

	@Column(name = "percentual_ocupacao")
	private Double percentualOcupacao;

	@Column(name = "quantidade_andares_bloco")
	private Integer quantidadeAndaresBloco;

	@Column(name = "quantidade_apartamento_andar")
	private Integer quantidadeApartamentoAndar;

	@Column(name = "quantidade_dormitorio_apartamento")
	private Integer quantidadeDormitorioApartamento;

	@Column(name = "idade_edificio")
	private Integer idadeEdificio;

	@Column(name = "metragem_apartamento")
	private Double metragemApartamento;

    @Transient
    private List<CondominioContato> condominioContatos;

	@JoinColumn(name = "_condominio_operacao_comercial" )
	private CondominioOperacaoComercial condominioOperacaoComercial;

	private Double latitude;

	private Double longitude;

 	private String observacao;
	
	@Column(name = "telefone_portaria")
	private String telefone_portaria;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodFttx() {
		return codFttx;
	}

	public void setCodFttx(String codFttx) {
		this.codFttx = codFttx;
	}

	public String getChaveEndereco() {
		return chaveEndereco;
	}

	public void setChaveEndereco(String chaveEndereco) {
		this.chaveEndereco = chaveEndereco;
	}

	public String getClasseSocial() {
		return classeSocial;
	}

	public void setClasseSocial(String classeSocial) {
		this.classeSocial = classeSocial;
	}

	public String getTipoCondominio() {
		return tipoCondominio;
	}

	public void setTipoCondominio(String tipoCondominio) {
		this.tipoCondominio = tipoCondominio;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public ECondominioLayout getLayout() {
		return layout;
	}

	public void setLayout(ECondominioLayout layout) {
		this.layout = layout;
	}

	public String getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	public String getSegmentacaoMkt() {
		return segmentacaoMkt;
	}

	public void setSegmentacaoMkt(String segmentacaoMkt) {
		this.segmentacaoMkt = segmentacaoMkt;
	}

	public String getCodLog() {
		return codLog;
	}

	public void setCodLog(String codLog) {
		this.codLog = codLog;
	}

	public String getChaveCep() {
		return chaveCep;
	}

	public void setChaveCep(String chaveCep) {
		this.chaveCep = chaveCep;
	}

	public Integer getQuantidadeBloco() {
		return quantidadeBloco;
	}

	public void setQuantidadeBloco(Integer quantidadeBloco) {
		this.quantidadeBloco = quantidadeBloco;
	}

	public String getQuantidadeApartamento() {
		return quantidadeApartamento;
	}

	public void setQuantidadeApartamento(String quantidadeApartamento) {
		this.quantidadeApartamento = quantidadeApartamento;
	}

	public String getCnl() {
		return cnl;
	}

	public void setCnl(String cnl) {
		this.cnl = cnl;
	}

	public String getAreaTelefonica() {
		return areaTelefonica;
	}

	public void setAreaTelefonica(String areaTelefonica) {
		this.areaTelefonica = areaTelefonica;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public CondominioStatusInfra getCondominioStatusInfra() {
		return condominioStatusInfra;
	}

	public void setCondominioStatusInfra(CondominioStatusInfra condominioStatusInfra) {
		this.condominioStatusInfra = condominioStatusInfra;
	}

	public Integer getCapacidadeEquipamento() {
		return capacidadeEquipamento;
	}

	public void setCapacidadeEquipamento(Integer capacidadeEquipamento) {
		this.capacidadeEquipamento = capacidadeEquipamento;
	}

	public Integer getOcupacaoEquipamento() {
		return ocupacaoEquipamento;
	}

	public void setOcupacaoEquipamento(Integer ocupacaoEquipamento) {
		this.ocupacaoEquipamento = ocupacaoEquipamento;
	}

	public Integer getFibraLivre() {
		return fibraLivre;
	}

	public void setFibraLivre(Integer fibraLivre) {
		this.fibraLivre = fibraLivre;
	}

	public Double getPercentualOcupacao() {
		return percentualOcupacao;
	}

	public void setPercentualOcupacao(Double percentualOcupacao) {
		this.percentualOcupacao = percentualOcupacao;
	}

	public Integer getQuantidadeAndaresBloco() {
		return quantidadeAndaresBloco;
	}

	public void setQuantidadeAndaresBloco(Integer quantidadeAndaresBloco) {
		this.quantidadeAndaresBloco = quantidadeAndaresBloco;
	}

	public Integer getQuantidadeApartamentoAndar() {
		return quantidadeApartamentoAndar;
	}

	public void setQuantidadeApartamentoAndar(Integer quantidadeApartamentoAndar) {
		this.quantidadeApartamentoAndar = quantidadeApartamentoAndar;
	}

	public Integer getQuantidadeDormitorioApartamento() {
		return quantidadeDormitorioApartamento;
	}

	public void setQuantidadeDormitorioApartamento(
			Integer quantidadeDormitorioApartamento) {
		this.quantidadeDormitorioApartamento = quantidadeDormitorioApartamento;
	}

	public Integer getIdadeEdificio() {
		return idadeEdificio;
	}

	public void setIdadeEdificio(Integer idadeEdificio) {
		this.idadeEdificio = idadeEdificio;
	}

	public Double getMetragemApartamento() {
		return metragemApartamento;
	}

	public void setMetragemApartamento(Double metragemApartamento) {
		this.metragemApartamento = metragemApartamento;
	}


	public CondominioOperacaoComercial getCondominioOperacaoComercial() {
		return condominioOperacaoComercial;
	}

	public void setCondominioOperacaoComercial(
			CondominioOperacaoComercial condominioOperacaoComercial) {
		this.condominioOperacaoComercial = condominioOperacaoComercial;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getId() {
		return id;
	}

    public Calendar getDataConclInfra() {
        return dataConclInfra;
    }

    public void setDataConclInfra(Calendar dataConclInfra) {
        this.dataConclInfra = dataConclInfra;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CondominioContato> getCondominioContatos() {
        return condominioContatos;
    }

    public void setCondominioContatos(List<CondominioContato> condominioContatos) {
        this.condominioContatos = condominioContatos;
    }
}
