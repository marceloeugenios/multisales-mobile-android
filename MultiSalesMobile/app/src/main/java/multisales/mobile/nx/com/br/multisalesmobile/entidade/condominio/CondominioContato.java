package multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio;

import java.util.Calendar;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Endereco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "condominio_contato")
public class CondominioContato {

	@Id
	private Integer id;
	
	@Column(name = "nome")
	private String nome;
	
	@JoinColumn(name = "_condominio")
	private Condominio condominio;
	
	@Column(name = "data_nascimento")
	private Calendar dataNascimento;
	
	@Column(name = "numero_apartamento")
	private String numeroApartamento;
	
	@Column(name = "telefone_residencial")
	private String telefoneResidencial;
	
	@Column(name = "telefone_celular")
	private String telefoneCelular;
	
	@Column(name = "telefone_comercial")
	private String telefoneComercial;

    @Column(name = "telefone_portaria")
    private String telefonePortaria;
	
	@JoinColumn(name = "_condominio_contato_cargo")
	private CondominioContatoCargo condominioContatoCargo;

    private String clube;

    @Column(name = "cortesia")
    private EBoolean cortesia;

    @Column(name = "data_limite_mandato")
    private Calendar dataLimiteMandato;

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNumeroApartamento() {
		return numeroApartamento;
	}

	public void setNumeroApartamento(String numeroApartamento) {
		this.numeroApartamento = numeroApartamento;
	}

	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

    public String getTelefonePortaria() {
        return telefonePortaria;
    }

    public void setTelefonePortaria(String telefonePortaria) {
        this.telefonePortaria = telefonePortaria;
    }

	public CondominioContatoCargo getCondominioContatoCargo() {
		return condominioContatoCargo;
	}

	public void setCondominioContatoCargo(
			CondominioContatoCargo condominioContatoCargo) {
		this.condominioContatoCargo = condominioContatoCargo;
	}

    public void setNullId() {
        this.id = null;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CondominioContato other = (CondominioContato) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

    public String getClube() {
        return clube;
    }

    public void setClube(String time) {
        this.clube = time;
    }

    public EBoolean getCortesia() {
        return cortesia;
    }

    public void setCortesia(EBoolean cortesia) {
        this.cortesia = cortesia;
    }

    public Calendar getDataLimiteMandato() {
        return dataLimiteMandato;
    }

    public void setDataLimiteMandato(Calendar dataLimiteMandato) {
        this.dataLimiteMandato = dataLimiteMandato;
    }
}
