package multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio;

import java.util.Calendar;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.Usuario;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Column;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.EmbeddedId;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.JoinColumn;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.XmlTransient;

@Table(name = "condominio_plantao")
public class CondominioPlantao {

    @Id
    @Column(name = "id_local")
    @XmlTransient
    private Integer idLocal;

    private Integer id;

    @Column(name = "data_inicio")
    private Calendar dataInicio;

    @Column(name = "_usuario")
    private Usuario usuario;

    @Column(name = "data_final")
	private Calendar dataFinal;
	
	@JoinColumn(name = "_condominio_plantao_situacao")
	private CondominioPlantaoSituacao condominioPlantaoSituacao;
	
	@JoinColumn(name = "_condominio")
	private Condominio condominio;

	public Calendar getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	public CondominioPlantaoSituacao getCondominioPlantaoSituacao() {
		return condominioPlantaoSituacao;
	}
	
	public void setCondominioPlantaoSituacao(
			CondominioPlantaoSituacao condominioPlantaoSituacao) {
		this.condominioPlantaoSituacao = condominioPlantaoSituacao;
	}
	
	public Condominio getCondominio() {
		return condominio;
	}
	
	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Calendar getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Calendar dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
