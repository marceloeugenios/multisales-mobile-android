package multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.ESituacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Id;
import multisales.mobile.nx.com.br.multisalesmobile.utils.annotations.Table;

@Table(name = "condominio_status_infra")
public class CondominioStatusInfra {
	

	@Id
	private Integer id;

	private String descricao;

	private ESituacao situacao = ESituacao.ATIVO;
	
	public Integer getId() {
		return this.id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ESituacao getSituacao() {
		return situacao;
	}
	
	public String toString() {
		return "CondominioStatusInfra [id=" + id + "]";
	}


}
