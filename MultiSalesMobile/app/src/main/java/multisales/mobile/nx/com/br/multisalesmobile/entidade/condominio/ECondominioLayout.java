package multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio;

public enum ECondominioLayout {
	VERTICAL, 
	HORIZONTAL, 
	MISTO;
}
