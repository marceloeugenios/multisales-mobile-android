package multisales.mobile.nx.com.br.multisalesmobile.exception;

/**
 * Created by samara on 25/02/15.
 */
public class CampoObrigatorioException extends Throwable {
    public CampoObrigatorioException(String message) {
        super(message);
    }
}
