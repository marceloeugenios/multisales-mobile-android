package multisales.mobile.nx.com.br.multisalesmobile.servico;

import android.content.Context;

import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.Atualizacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Banco;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.BandeiraSistema;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.BandeiraSistemaProduto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cidade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.CombinacaoProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.CombinacaoProdutoTipoProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Concorrente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Escolaridade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Estado;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EstadoCivil;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogAcesso;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Midia;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoMigracao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoNaoVenda;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.OperadoraTelefonia;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.PeriodoInstalacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Produto;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TamanhoChip;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TecnologiaDisponivel;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TipoCompartilhamento;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TipoContrato;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TipoContratoCombinacaoProdutoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TipoPontoAdicional;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VencimentoFatura;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.VersaoPacote;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioContatoCargo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioOperacaoComercial;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioPlantaoSituacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.condominio.CondominioStatusInfra;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;

/**
 * Created by eric on 26-03-2015.
 */
public class AtualizacaoServico {

    private EntityManager entityManager;

    public AtualizacaoServico(Context context) {
        entityManager = new EntityManager(context);
    }

    public void instalarPacotes(VersaoPacote resposta) throws DataBaseException {
        entityManager.dropBasicTables();
        //}
        entityManager.createBasicTables();
        for (Midia midia : resposta.getMidias()) {
            entityManager.save(midia);
        }
        for (MotivoTabulacaoTipo motivoTabulacaoTipo : resposta.getMotivoTabulacaoTipos()) {
            entityManager.save(motivoTabulacaoTipo);
        }
        for (MotivoTabulacao motivoTabulacao : resposta.getMotivosTabulacao()) {
            entityManager.save(motivoTabulacao);
        }
        for (MotivoMigracao motivoMigracao : resposta.getMotivosMigracao()) {
            entityManager.save(motivoMigracao);
        }
        for (Concorrente concorrente : resposta.getConcorrentes()) {
            //retira vivo dos concorrentes
            if (!concorrente.getDescricao().contains("VIVO")) {
                entityManager.save(concorrente);
            }
        }
        for (Estado estado : resposta.getEstados()) {
            entityManager.save(estado);
        }
        for (Cidade cidade : resposta.getCidades()) {
            entityManager.save(cidade);
        }
        for (EstadoCivil estadoCivil : resposta.getEstadosCivil()) {
            entityManager.save(estadoCivil);
        }
        for (Escolaridade escolaridade : resposta.getEscolaridades()) {
            entityManager.save(escolaridade);
        }
        for (MotivoNaoVenda motivoNaoVenda : resposta.getMotivosNaoVenda()) {
            entityManager.save(motivoNaoVenda);
        }
        for (TipoContrato tipoContrato : resposta.getTiposContratos()) {
            entityManager.save(tipoContrato);
        }
        for (BandeiraSistema bandeiraSistema : resposta.getBandeirasSistema()) {
            entityManager.save(bandeiraSistema);
        }
        for (ProdutoTipo produtoTipo : resposta.getProdutosTipos()) {
            entityManager.save(produtoTipo);
        }
        for (Produto produto : resposta.getProdutos()) {
            entityManager.save(produto);
        }
        for (BandeiraSistemaProduto bandeiraSistemaProduto
                : resposta.getBandeirasSistemaProdutos()) {
            entityManager.save(bandeiraSistemaProduto);
        }
        for (CombinacaoProdutoTipo combinacaoProdutoTipo
                : resposta.getCombinacoesProdutoTipo()) {
            entityManager.save(combinacaoProdutoTipo);
        }
        for (CombinacaoProdutoTipoProdutoTipo combinacaoProdutoTipoProdutoTipo
                : resposta.getCombinacoesProdutoTipoProdutoTipo()) {
            entityManager.save(combinacaoProdutoTipoProdutoTipo);
        }
        for (TipoContratoCombinacaoProdutoTipo tipoContratoCombinacaoProdutoTipo
                : resposta.getTiposContratoCombinacaoProdutoTipo()) {
            entityManager.save(tipoContratoCombinacaoProdutoTipo);
        }
        for (TipoPontoAdicional tipoPontoAdicional : resposta.getTiposPontosAdicionais()) {
            entityManager.save(tipoPontoAdicional);
        }
        for (OperadoraTelefonia operadora : resposta.getOperadorasTelefonia()) {
            entityManager.save(operadora);
        }
        for (TamanhoChip tamanhoChip : resposta.getTamanhosChip()) {
            entityManager.save(tamanhoChip);
        }
        for (TipoCompartilhamento tipoCompartilhamento : resposta.getTiposCompartilhamento()) {
            entityManager.save(tipoCompartilhamento);
        }
        for (VencimentoFatura vencimentoFatura : resposta.getVencimentosFatura()) {
            entityManager.save(vencimentoFatura);
        }
        for (Banco banco : resposta.getBancos()) {
            entityManager.save(banco);
        }
        for (CondominioContatoCargo condominioContatoCargo : resposta.getCondominioContatoCargos()) {
            entityManager.save(condominioContatoCargo);
        }
        for (CondominioOperacaoComercial condominioOperacaoComercial : resposta.getCondominioOperacoesComerciais()) {
            entityManager.save(condominioOperacaoComercial);
        }
        for (CondominioPlantaoSituacao condominioPlantaoSituacao : resposta.getCondominioPlantoesSituacoes()) {
            entityManager.save(condominioPlantaoSituacao);
        }
        for (PeriodoInstalacao periodoInstalacao : resposta.getPeriodosInstalacao()) {
            entityManager.save(periodoInstalacao);
        }
        for (CondominioStatusInfra condominioStatusInfra : resposta.getCondominioStatusInfras()) {
            entityManager.save(condominioStatusInfra);
        }
        for (TecnologiaDisponivel tecnologia : resposta.getTecnologiasDisponiveis()) {
            entityManager.save(tecnologia);
        }
        resposta.setDataAtualizacao(Calendar.getInstance());
        entityManager.save(resposta);

        List<MotivoTabulacao> motivos = entityManager.getAll(MotivoTabulacao.class);

        MotivoTabulacao teste = motivos.get(0);

        entityManager.initialize(teste.getMotivoTabulacaoTipo());
    }

    public Atualizacao obterUltimaAtualizacao (String login) throws DataBaseException {
        Atualizacao atualizacao = null;
        List<Atualizacao> atualizacoes = entityManager.select(Atualizacao.class, "SELECT * FROM atualizacao WHERE id = (SELECT MAX(id) FROM atualizacao)");
        if (atualizacoes.size() > 0) {
            atualizacao = atualizacoes.get(0);
        }
        return atualizacao;
    }


}
