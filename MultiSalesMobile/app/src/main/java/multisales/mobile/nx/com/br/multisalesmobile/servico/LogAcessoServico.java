package multisales.mobile.nx.com.br.multisalesmobile.servico;

import android.content.Context;

import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.EAcaoAcesso;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogAcesso;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;

/**
 * Created by eric on 11-03-2015.
 */
public class LogAcessoServico {

    EntityManager entityManager;

    public LogAcessoServico (Context context) {
        this.entityManager = new EntityManager(context);
    }

    public LogAcesso obterUltimoLogAcesso() throws DataBaseException {
        LogAcesso logAcesso = null;
        List<LogAcesso> logs = entityManager.select(LogAcesso.class, "SELECT * FROM log_acesso ORDER BY id DESC");
        if (logs.size() > 0) {
           logAcesso = logs.get(0);
        }
        return logAcesso;
    }

    public LogAcesso salvarAcaoAcesso(Integer idUsuario, Integer idAgenteAutorizado, String login, EAcaoAcesso acaoAcesso) throws DataBaseException {
        LogAcesso logAcesso = new LogAcesso();
        logAcesso.setDataLogin(Calendar.getInstance());
        logAcesso.setHoraLogin(Calendar.getInstance());
        logAcesso.setAcaoLogin(acaoAcesso);
        logAcesso.setLogin(login);
        logAcesso.setIdUsuario(idUsuario);
        logAcesso.setIdAgenteAutorizado(idAgenteAutorizado);
        return entityManager.save(logAcesso);
    }

}
