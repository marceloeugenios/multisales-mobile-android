package multisales.mobile.nx.com.br.multisalesmobile.servico;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.LogDiario;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;

/**
 * Created by eric on 11-03-2015.
 */
public class LogDiarioServico {

    EntityManager entityManager;

    public LogDiarioServico (Context context) {
        this.entityManager = new EntityManager(context);
    }

    public LogDiario obterLogDiarioPorData(Calendar data, String login) throws DataBaseException {
        LogDiario retorno = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        List<LogDiario> logs = entityManager.getByWhere(LogDiario.class, "data_log = \"" + simpleDateFormat.format(data.getTime()) + "\" AND login = \"" + login + "\"", null);
        if (logs.size() > 0) {
           retorno =  logs.get(0);
        }
        return retorno;
    }

    public LogDiario obterLogDiarioPorMes(Calendar data, String login) throws DataBaseException {
        LogDiario retorno = new LogDiario();
        retorno.setAgendamentos(SistemaConstantes.ZERO);
        retorno.setNaoVendas(SistemaConstantes.ZERO);
        retorno.setVendas(SistemaConstantes.ZERO);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        Calendar primeiroDiaMes = Calendar.getInstance();
        primeiroDiaMes.set(Calendar.DAY_OF_MONTH, 1);
        Calendar ultimoDiaMes = Calendar.getInstance();
        ultimoDiaMes.set(Calendar.DAY_OF_MONTH,ultimoDiaMes.getActualMaximum(Calendar.DAY_OF_MONTH));

        List<LogDiario> logs = entityManager.getByWhere(LogDiario.class, " login = '" + login + "'", null);
        if (logs != null && logs.size() > 0) {
            for (LogDiario log : logs) {
                retorno.setAgendamentos(retorno.getAgendamentos() + (log.getAgendamentos() != null ? log.getAgendamentos() : SistemaConstantes.ZERO));
                retorno.setNaoVendas(retorno.getNaoVendas() + (log.getNaoVendas() != null ? log.getNaoVendas() : SistemaConstantes.ZERO));
                retorno.setVendas(retorno.getVendas() + (log.getVendas() != null ? log.getVendas() : SistemaConstantes.ZERO));
            }
        }
        return retorno;
    }

    public LogDiario obterLogDiarioAtualPorUsuario(String login) throws DataBaseException {
        Calendar calendar = Calendar.getInstance();
        LogDiario logDiario = obterLogDiarioPorData(calendar, login);
        if (logDiario == null) {
            logDiario = new LogDiario();
            logDiario.setLogin(login);
            logDiario.setDataLog(Calendar.getInstance());
            logDiario = entityManager.save(logDiario);
        }
        return logDiario;
    }

    public void apagarLogsMesAnterior() throws Exception {
        List<LogDiario> logs = entityManager.getAll(LogDiario.class);
        int mesAtual = Calendar.getInstance().get(Calendar.MONTH);
        if (logs != null && logs.size() > 0) {
            for (LogDiario log : logs) {
                if (log.getDataLog().get(Calendar.MONTH) != mesAtual) {
                    entityManager.delete(log);
                }
            }
        }
    }
}
