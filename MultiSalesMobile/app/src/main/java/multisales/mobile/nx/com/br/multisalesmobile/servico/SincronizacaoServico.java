package multisales.mobile.nx.com.br.multisalesmobile.servico;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ECodigoOperacaoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ED2DCodigoResponse;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.RequestD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ResponseD2D;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.StatusSincronizacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.TabulacaoAgendamento;
import multisales.mobile.nx.com.br.multisalesmobile.utils.D2DRestClient;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.JsonParser;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

/**
 * Created by eric on 12-03-2015.
 */
public class SincronizacaoServico {
    private Context context;
    private EntityManager entityManager;
    private JsonParser jsonParser = new JsonParser();
    private D2DRestClient d2DRestClient = new D2DRestClient();
    private TabulacaoServico tabulacaoServico;
    private StatusSincronizacaoServico statusSincronizacaoServico;
    private TabulacaoAgendamentoServico tabulacaoAgendamentoServico;

    private Tabulacao tabulacaoNaoVendaAtual;
    private TabulacaoAgendamento tabulacaoAgendamentoAtual;
    private List<Tabulacao> naoVendasNaoSincronizadas;
    private List<TabulacaoAgendamento> agendamentosNaoSincronizados;
    private int contadorSincronizacaoNaoVendas = 0;
    private int contadorSincronizacaoAgendamentos = 0;
    public AsyncTask<Void, Void, ResponseD2D> taskNaoVenda;

    private RequestD2D requestD2DNaoVenda = new RequestD2D();
    private RequestD2D requestD2DAgendamento = new RequestD2D();

    private MultiSales multiSales;

    public SincronizacaoServico (Context context) {
        try {
            this.entityManager = new EntityManager(context);
            this.context = context;
            multiSales = (MultiSales)context;
            tabulacaoServico = new TabulacaoServico(context);
            statusSincronizacaoServico = new StatusSincronizacaoServico(context);
            tabulacaoAgendamentoServico = new TabulacaoAgendamentoServico(context);
            naoVendasNaoSincronizadas = tabulacaoServico.listarNaovendasNaoSincronizadas();
            agendamentosNaoSincronizados = tabulacaoAgendamentoServico.obterTabulacoesAgendamentoNaoSincronizadas();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sincronizarTodasTabulacoes () {
        sincronizarNaoVendas(false, false);
        sincronizarAgendamentos(false, false);
    }

    public void sincronizarNaoVendas (final boolean showToasts, final boolean continuacao) {
        try {
            StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.NAO_VENDA);
            if ((statusSincronizacao.getEmSincronizacao().equals(EBoolean.FALSE) || continuacao) && multiSales.getIdUsuario() != null) {
                if (naoVendasNaoSincronizadas.size() > 0) {
                    statusSincronizacao.setEmSincronizacao(EBoolean.TRUE);
                    entityManager.atualizar(statusSincronizacao);
                    tabulacaoNaoVendaAtual = naoVendasNaoSincronizadas.get(contadorSincronizacaoNaoVendas);
                    entityManager.initialize(tabulacaoNaoVendaAtual.getHp());

                    taskNaoVenda = new AsyncTask<Void, Void, ResponseD2D>() {
                        @Override
                        protected ResponseD2D doInBackground(Void... params) {
                            JSONObject jsonEnvio;
                            JSONObject jsonRetorno;
                            ResponseD2D responseD2D;
                            try {
                                requestD2DNaoVenda.setOperacao(ECodigoOperacaoD2D.NAO_VENDA_CODIGO.getCodigo());
                                requestD2DNaoVenda.setTabulacao(tabulacaoNaoVendaAtual);
                                requestD2DNaoVenda.setIdUsuario(multiSales.getIdUsuario());
                                requestD2DNaoVenda.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                                jsonEnvio = jsonParser.parseObjectToJson(requestD2DNaoVenda);
                                jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                                responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                            } catch (Exception e) {
                                responseD2D = new ResponseD2D();
                                responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                                responseD2D.setMensagem(e.getMessage());
                                return responseD2D;
                            }
                            return responseD2D;
                        }

                        @Override
                        protected void onPostExecute(ResponseD2D resposta) {
                            super.onPostExecute(resposta);
                            if (resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                                try {
                                    tabulacaoNaoVendaAtual.setId((Integer) resposta.getExtras().get("idNaoVenda"));
                                    entityManager.atualizar(tabulacaoNaoVendaAtual);
                                    contadorSincronizacaoNaoVendas++;
                                    if (naoVendasNaoSincronizadas.size() == contadorSincronizacaoNaoVendas) {
                                        StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.NAO_VENDA);
                                        statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                                        entityManager.atualizar(statusSincronizacao);
                                        naoVendasNaoSincronizadas = tabulacaoServico.listarNaovendasNaoSincronizadas();
                                        if (naoVendasNaoSincronizadas.size() == 0) {
                                            if (showToasts) {
                                                UtilActivity.makeShortToast("Todas tabulações sincronizadas com sucesso!", context);
                                            }
                                        }
                                        contadorSincronizacaoNaoVendas = 0;
                                    } else {
                                        sincronizarNaoVendas(showToasts, true);
                                    }
                                    Intent broadcast = new Intent("SINCRONIZACAO_CONCLUIDA");
                                    context.sendBroadcast(broadcast);
                                } catch (Exception e) {
                                    try {
                                        StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.NAO_VENDA);
                                        statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                                        entityManager.atualizar(statusSincronizacao);
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                    if (showToasts) {
                                        UtilActivity.makeShortToast("ERRO ao salvar Tabulação!", context);
                                    }
                                }
                            } else {
                                try {
                                    StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.NAO_VENDA);
                                    statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                                    entityManager.atualizar(statusSincronizacao);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    };
                    taskNaoVenda.execute();
                }
            } else {
                if (showToasts) {
                    UtilActivity.makeShortToast("Sincronização em andamento", context);
                }
            }

        } catch (DataBaseException e) {
            try {
                StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.NAO_VENDA);
                statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                entityManager.atualizar(statusSincronizacao);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }

    }

    public void sincronizarAgendamentos(final boolean showToasts, final boolean continuacao) {
        try {
            StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.AGENDAMENTO);
            if ((statusSincronizacao.getEmSincronizacao().equals(EBoolean.FALSE) || continuacao) && multiSales.getIdUsuario() != null) {
                if (agendamentosNaoSincronizados.size() > 0) {
                    statusSincronizacao.setEmSincronizacao(EBoolean.TRUE);
                    entityManager.atualizar(statusSincronizacao);
                    tabulacaoAgendamentoAtual = agendamentosNaoSincronizados.get(contadorSincronizacaoAgendamentos);
                    entityManager.initialize(tabulacaoAgendamentoAtual.getTabulacao());
                    entityManager.initialize(tabulacaoAgendamentoAtual.getTabulacao().getHp());

                    AsyncTask<Void, Void, ResponseD2D> taskAgendamento = new AsyncTask<Void, Void, ResponseD2D>() {
                        @Override
                        protected ResponseD2D doInBackground(Void... params) {
                            JSONObject jsonEnvio;
                            JSONObject jsonRetorno;
                            ResponseD2D responseD2D;
                            try {
                                requestD2DAgendamento.setOperacao(ECodigoOperacaoD2D.AGENDAMENTO.getCodigo());
                                requestD2DAgendamento.setTabulacao(tabulacaoAgendamentoAtual.getTabulacao());
                                requestD2DAgendamento.setTabulacaoAgendamento(tabulacaoAgendamentoAtual);
                                requestD2DAgendamento.setIdUsuario(multiSales.getIdUsuario());
                                requestD2DAgendamento.setIdAgenteAutorizado(multiSales.getIdAgenteAutorizado());
                                jsonEnvio = jsonParser.parseObjectToJson(requestD2DAgendamento);
                                jsonRetorno = d2DRestClient.sendPost(jsonEnvio, ERecursoD2D.PROCESSAR);
                                responseD2D = jsonParser.parseJsonToObject(jsonRetorno, ResponseD2D.class);
                            } catch (Exception e) {
                                responseD2D = new ResponseD2D();
                                responseD2D.setCodigo(ED2DCodigoResponse.ERROR);
                                responseD2D.setMensagem(e.getMessage());
                                return responseD2D;
                            }
                            return responseD2D;
                        }

                        @Override
                        protected void onPostExecute(ResponseD2D resposta) {
                            super.onPostExecute(resposta);
                            if (resposta.getCodigo().equals(ED2DCodigoResponse.OK)) {
                                try {
                                    tabulacaoAgendamentoAtual.getTabulacao().setId((Integer) resposta.getExtras().get("idAgendamento"));
                                    entityManager.atualizar(tabulacaoAgendamentoAtual.getTabulacao());
                                    tabulacaoAgendamentoAtual.setId((Integer) resposta.getExtras().get("idAgendamento"));
                                    entityManager.atualizar(tabulacaoAgendamentoAtual);

                                    contadorSincronizacaoAgendamentos++;
                                    if (agendamentosNaoSincronizados.size() == contadorSincronizacaoAgendamentos) {
                                        StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.AGENDAMENTO);
                                        statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                                        entityManager.atualizar(statusSincronizacao);
                                        agendamentosNaoSincronizados = tabulacaoAgendamentoServico.obterTabulacoesAgendamentoNaoSincronizadas();
                                        if (agendamentosNaoSincronizados.size() == 0) {
                                            if (showToasts) {
                                                UtilActivity.makeShortToast("Todas tabulações sincronizadas com sucesso!", context);
                                            }
                                        }
                                    } else {
                                        sincronizarAgendamentos(showToasts, true);
                                    }
                                    Intent broadcast = new Intent("SINCRONIZACAO_CONCLUIDA");
                                    context.sendBroadcast(broadcast);
                                } catch (Exception e) {
                                    try {
                                        StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.AGENDAMENTO);
                                        statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                                        entityManager.atualizar(statusSincronizacao);
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                    if (showToasts) {
                                        UtilActivity.makeShortToast("ERRO ao salvar Tabulação!", context);
                                    }
                                }
                            } else {
                                try {
                                    StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.NAO_VENDA);
                                    statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                                    entityManager.atualizar(statusSincronizacao);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    };
                    taskAgendamento.execute();
                } else {
                    if (showToasts) {
                        UtilActivity.makeShortToast("Não há tabulações para sincronizar", context);
                    }
                }
            }

        } catch (Exception e) {
            try {
                StatusSincronizacao statusSincronizacao = statusSincronizacaoServico.obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo.AGENDAMENTO);
                statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
                entityManager.atualizar(statusSincronizacao);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public Boolean verificarSincronizacoesPendentes() {
        return (agendamentosNaoSincronizados != null && agendamentosNaoSincronizados.size() > 0) ||
                (agendamentosNaoSincronizados != null && naoVendasNaoSincronizadas.size() > 0);
    }
}
