package multisales.mobile.nx.com.br.multisalesmobile.servico;

import android.content.Context;

import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.EBoolean;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.StatusSincronizacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;

/**
 * Created by eric on 12-03-2015.
 */
public class StatusSincronizacaoServico {
    private EntityManager entityManager;

    public StatusSincronizacaoServico(Context context) {
        this.entityManager = new EntityManager(context);
    }

    public StatusSincronizacao obterStatusSincronizacaoPorTipo(EMotivoTabulacaoTipo motivoTabulacaoTipo) throws DataBaseException {
        List<StatusSincronizacao> listStatusSincronizacao = entityManager.getByWhere(StatusSincronizacao.class, "motivo_tabulacao_tipo = '" + motivoTabulacaoTipo.toString() + "'", null);
        if (listStatusSincronizacao.size() > 0) {
            return listStatusSincronizacao.get(0);
        } else {
            StatusSincronizacao statusSincronizacao = new StatusSincronizacao();
            statusSincronizacao.setEmSincronizacao(EBoolean.FALSE);
            statusSincronizacao.setMotivoTabulacaoTipo(motivoTabulacaoTipo);
            return entityManager.save(statusSincronizacao);
        }
    }

}
