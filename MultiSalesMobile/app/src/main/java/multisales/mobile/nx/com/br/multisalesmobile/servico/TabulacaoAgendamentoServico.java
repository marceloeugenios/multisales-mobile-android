package multisales.mobile.nx.com.br.multisalesmobile.servico;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.TabulacaoAgendamento;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;

/**
 * Created by eric on 12-03-2015.
 */
public class TabulacaoAgendamentoServico {

    EntityManager entityManager;

    public TabulacaoAgendamentoServico(Context context) {
        this.entityManager = new EntityManager(context);
    }

    public List<TabulacaoAgendamento> obterTabulacoesAgendamentoNaoSincronizadas() throws DataBaseException {
        List<TabulacaoAgendamento> agendamentosNaoSincronizados = new ArrayList<>();
        for (TabulacaoAgendamento tabulacaoAgendamento : entityManager.getAll(TabulacaoAgendamento.class)) {
            if (tabulacaoAgendamento.getId() == null) {
                agendamentosNaoSincronizados.add(tabulacaoAgendamento);
            }
        }
        return agendamentosNaoSincronizados;
    }

}
