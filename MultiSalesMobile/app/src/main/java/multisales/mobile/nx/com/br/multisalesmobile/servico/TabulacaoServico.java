package multisales.mobile.nx.com.br.multisalesmobile.servico;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.EMotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MotivoTabulacaoTipo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.utils.DataBaseException;
import multisales.mobile.nx.com.br.multisalesmobile.utils.EntityManager;
import multisales.mobile.nx.com.br.multisalesmobile.utils.UtilActivity;

/**
 * Created by eric on 12-03-2015.
 */
public class TabulacaoServico {
    private Context context;
    private EntityManager entityManager;

    public TabulacaoServico (Context context) {
        this.entityManager = new EntityManager(context);
        this.context = context;
    }

    public List<Tabulacao> listarNaoVendas() throws DataBaseException {
        List<MotivoTabulacaoTipo> motivosTabulacaoTipo = entityManager.getByWhere(MotivoTabulacaoTipo.class, "descricao = '" + EMotivoTabulacaoTipo.NAO_VENDA.getDescricao() + "'", null);
        if (motivosTabulacaoTipo != null
                && !motivosTabulacaoTipo.isEmpty()) {

            MotivoTabulacaoTipo motivoNaoVenda = motivosTabulacaoTipo.get(SistemaConstantes.ZERO);

            List<Tabulacao> tabulacoes = new ArrayList<>();
            StringBuilder stringBuilder =  new StringBuilder();
            stringBuilder.append("SELECT tab.* ");
            stringBuilder.append("FROM ");
            stringBuilder.append("tabulacao tab ");
            stringBuilder.append("INNER JOIN ");
            stringBuilder.append("motivo_tabulacao mtab ");
            stringBuilder.append("ON ");
            stringBuilder.append("tab._motivo_tabulacao = mtab.id ");
            stringBuilder.append("WHERE ");
            stringBuilder.append("mtab._motivo_tabulacao_tipo =  " + motivoNaoVenda.getId());
            tabulacoes = entityManager.select(Tabulacao.class, stringBuilder.toString());

            for (Tabulacao tabulacao : tabulacoes) {
                entityManager.initialize(tabulacao.getHp());
                entityManager.initialize(tabulacao.getMotivoTabulacao());
            }

            return tabulacoes;
        }

        return new ArrayList<>();
    }

    public List<Tabulacao> listarNaovendasNaoSincronizadas() throws DataBaseException {
        List<Tabulacao> naoSincronizadas = new ArrayList<>();
        for (Tabulacao tabulacao : listarNaoVendas()) {
            if (tabulacao.getId() == null) {
                naoSincronizadas.add(tabulacao);
            }
        }
        return naoSincronizadas;
    }
}
