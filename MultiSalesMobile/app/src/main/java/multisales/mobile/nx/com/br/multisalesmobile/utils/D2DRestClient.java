package multisales.mobile.nx.com.br.multisalesmobile.utils;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.ERecursoD2D;

/**
 * Created by eric on 10/02/15.
 */
public class D2DRestClient {
    public static final String URL = "http://172.16.1.174:8080/rest/d2d/";

    public JSONObject sendPost(JSONObject jsonObject, ERecursoD2D recurso) throws Exception {
        JSONObject jsonReturn = null;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        defaultHttpClient.getParams().setIntParameter("http.socket.timeout", 10000);
        defaultHttpClient.getParams().setIntParameter("http.connection.timeout", 60000);
        HttpPost httpPost = new HttpPost(URL + recurso.getCodigo());
        StringEntity stringEntity = new StringEntity(jsonObject.toString(), "UTF-8");

        httpPost.setEntity(stringEntity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        HttpResponse httpResponse = defaultHttpClient.execute(httpPost);
        if (httpResponse.getStatusLine().getStatusCode() == 200) {
            String stringReturn = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            jsonReturn = new JSONObject(stringReturn);
            Log.d("JSON", "JSON RETORNO: " + stringReturn);
        } else {
            throw new Exception("Falha na requisição: Código: " + httpResponse.getStatusLine().getStatusCode());
        }
        return jsonReturn;
    }
}