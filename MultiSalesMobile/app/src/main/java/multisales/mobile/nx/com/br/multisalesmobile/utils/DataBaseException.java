package multisales.mobile.nx.com.br.multisalesmobile.utils;

/**
 * Created by eric on 11/02/15.
 */
public class DataBaseException extends Exception {

    public DataBaseException(String message) {
        super(message);
    }

}
