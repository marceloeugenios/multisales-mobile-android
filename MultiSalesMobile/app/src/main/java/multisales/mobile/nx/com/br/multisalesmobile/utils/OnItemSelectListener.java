package multisales.mobile.nx.com.br.multisalesmobile.utils;

import android.view.View;

/**
 * Created by eric on 19-03-2015.
 */
public interface OnItemSelectListener {
    void onItemSelect(int position);
}
