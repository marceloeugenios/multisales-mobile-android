package multisales.mobile.nx.com.br.multisalesmobile.utils;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.activities.ItemComboAdapter;
import multisales.mobile.nx.com.br.multisalesmobile.activities.MainActivity;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ENotificacaoCodigo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Tabulacao;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Venda;

/**
 * Created by samara on 20/02/15.
 */
public class UtilActivity {

    public static final String SELECIONE = "SELECIONE";
    public static final String MULTI_INTERNET = "MULTI INTERNET";
    public static final String COMBO_MULTI = "COMBO MULTI";

    public static void makeShortToast(String mensagem, Context context) {
        Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
    }

    public static void setAdapter(ActionBarActivity activity, Spinner spinner, List<String> strings) {
        ItemComboAdapter adapter = new ItemComboAdapter(activity, R.layout.item_combo, strings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public static Calendar getCalendarValue(EditText editText) {
        if (editText != null
                && editText.getText() != null
                && !editText.getText().toString().isEmpty()) {
            String textoData = editText.getText().toString();
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date data;
            try {
                data = df.parse(textoData);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(data);
                return calendar;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Calendar ConverterStringParaCalendar(String strData) throws ParseException {
        if(strData.trim().equals("")){
            return null;
        }
        DateFormat formatter;
        Date date;
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        date = (Date) formatter.parse(strData);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static boolean isValidSpinnerValue(Spinner spinner) {
        if (spinner != null
                && spinner.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            return true;
        }
        return false;
    }

    public static boolean isValidIntegerValue(EditText editText) {
        if (editText.getText() != null
                && !editText.getText().toString().isEmpty()) {
            return true;
        }
        return false;
    }

    public static void deletarVenda(EntityManager entityManager, Integer idVenda)
            throws Exception {
        try {

            //Tabulação
            entityManager.delete(entityManager.getById(Tabulacao.class, idVenda));

            //Venda
            Venda venda = entityManager.getById(Venda.class, idVenda);
            entityManager.initialize(venda.getCliente());

            //Endereço
            if (venda.getCliente().getEndereco() != null) {
                entityManager.delete(venda.getCliente().getEndereco());
            }

            if (venda.getCliente().getEnderecoCobranca() != null) {
                //Endereço Cobrança
                entityManager.delete(venda.getCliente().getEnderecoCobranca());
            }

            //Cliente
            entityManager.delete(venda.getCliente());

            //VendaTv
            deletarTv(entityManager, idVenda);

            //VendaCelular
            deletarCelular(entityManager, idVenda);

            //VendaFone
            deletarFone(entityManager, idVenda);

            //Venda Internet
            deletarInternet(entityManager, idVenda);

            //Motivos não venda
            deletarVendasMotivosNaoVenda(entityManager, idVenda);

            //Forma Pagamento
            deletarFormaPagamento(entityManager, idVenda);

            //Venda itself
            entityManager.delete(venda);

        } catch (Exception e) {
            throw e;
        }
    }

    private static void deletarVendasMotivosNaoVenda(EntityManager entityManager,
                                                     Integer idVenda) throws Exception {
        entityManager.executeNativeQuery("DELETE " +
                "FROM venda_motivo_nao_venda " +
                "WHERE _venda = "
                + idVenda);
    }

    public static void deletarTv(EntityManager entityManager, Integer idVenda) throws Exception {
        entityManager.executeNativeQuery(
                "DELETE " +
                        "FROM venda_tv_ponto_adicional " +
                        "WHERE _venda_tv =  "
                        + idVenda);

        entityManager.executeNativeQuery(
                "DELETE " +
                        "FROM venda_tv_produto_adicional " +
                        "WHERE _venda_tv = "
                        + idVenda);

        entityManager.executeNativeQuery(
                "DELETE " +
                        "FROM venda_tv_ponto_adicional " +
                        "WHERE _venda_tv = "
                        + idVenda);

        entityManager.executeNativeQuery(
                "DELETE " +
                        "FROM venda_tv " +
                        "WHERE id = "
                        + idVenda);
    }

    public static void deletarCelular(EntityManager entityManager, Integer idVenda) throws Exception {
        entityManager.executeNativeQuery(
                "DELETE " +
                        "FROM venda_celular " +
                        "WHERE id =  "
                        + idVenda);

        entityManager.executeNativeQuery(
                "DELETE " +
                        "FROM venda_celular_dependente " +
                        "WHERE _venda_celular =  "
                        + idVenda);
    }

    public static void deletarFone(EntityManager entityManager, Integer idVenda) throws Exception {
        Venda venda = entityManager.getById(Venda.class, idVenda);
        if (venda.getVendaFone() != null
                && venda.getVendaFone().getId() != null) {
            entityManager.executeNativeQuery("DELETE FROM venda_fone_linha WHERE _venda_fone = " + venda.getVendaFone().getId());
            entityManager.executeNativeQuery("DELETE FROM venda_fone_portabilidade WHERE _venda_fone = " + venda.getVendaFone().getId());
            entityManager.delete(venda.getVendaFone());
        }
    }

    public static void deletarFormaPagamento(EntityManager entityManager, Integer idVenda)
            throws Exception {
        Venda venda = entityManager.getById(Venda.class, idVenda);
        if (venda.getVendaFormaPagamento() != null) {
            entityManager.executeNativeQuery("DELETE FROM venda_forma_pagamento WHERE _venda = " + venda.getVendaFormaPagamento().getId());
            entityManager.initialize(venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco());
            if (venda.getVendaFormaPagamento().getVendaFormaPagamentoBanco() != null) {
                entityManager.executeNativeQuery("DELETE FROM venda_forma_pagamento_banco WHERE _venda = " + venda.getVendaFone().getId());
            }
        }
    }

    public static void deletarInternet(EntityManager entityManager, Integer idVenda) throws Exception {
        Venda venda = entityManager.getById(Venda.class, idVenda);

        if (venda.getVendaFone() != null && venda.getVendaFone().getId() != null) {
            entityManager.executeNativeQuery(
                    "DELETE FROM venda_fone_linha WHERE _venda_fone = " + venda.getVendaFone().getId());
            entityManager.executeNativeQuery(
                    "DELETE FROM venda_fone_portabilidade WHERE _venda_fone = " + venda.getVendaFone().getId());
            entityManager.delete(venda.getVendaFone());
        }
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected())
            return true;
        else
            return false;
    }

    public static void irParaHome(Activity context) {
        Intent main = new Intent(context, MainActivity.class);
        context.startActivity(main);
        context.finish();
    }

    public static String formatarMoeda(Double valor) {
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        return "R$ " + decimalFormat.format(valor);
    }

    public static String serializarImagem(Bitmap bitmap) throws UnsupportedEncodingException {
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static Bitmap deserializarImagem (String text) throws UnsupportedEncodingException {
        try {
            byte[] encodeByte = Base64.decode(text, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static void makeNotification(Context context, ENotificacaoCodigo eNotificacaoCodigo, int logo, String titulo, String texto, String aviso, Intent intent, Boolean permanente) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(logo)
                        .setContentTitle(titulo)
                        .setContentText(texto)
                        .setTicker(aviso)
                        .setAutoCancel(true);
        int mNotificationId = eNotificacaoCodigo.getCodigo();
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = mBuilder.build();
        if (intent != null) {
            PendingIntent myIntent = PendingIntent.getActivity(context, 0, intent, 0);
            notification.setLatestEventInfo(context, titulo, texto, myIntent);
        }
        if (permanente) {
            notification.flags = Notification.FLAG_ONGOING_EVENT;
        }
        mNotifyMgr.notify(mNotificationId, notification);
    }

    public static void cancelNotification(Context context, ENotificacaoCodigo notificacaoCodigo) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) context.getSystemService(ns);
        nMgr.cancel(notificacaoCodigo.getCodigo());
    }

    public static String formatarTelefone(String telefone) {
        String telefoneFormatado=" ";
        String numeroInicial = telefone.substring(0,4);
        if(telefone.length() == 10) {
            telefoneFormatado = "(" + telefone.substring(0,2) + ")" + telefone.substring(2,6) +"-"+ telefone.substring(6,10);
        }
        if(telefone.length() == 11 && !numeroInicial.equals("0800")  ){
            telefoneFormatado = "(" + telefone.substring(0,2) + ")" + telefone.substring(2,7) +"-"+ telefone.substring(7,11);
        }
        if( telefone.length() < 10 || telefone.length() >11 || numeroInicial.equals("0800")) {
            telefoneFormatado = telefone;
        }
        return telefoneFormatado;
    }

    public static String formatarCPF(String cpf) {
        if(cpf.length() > 11 || cpf.length() < 11){
            return cpf;
        } else {
            cpf = cpf.substring(0,3) + "." + cpf.substring(3,6) + "." + cpf.substring(6,9) + "-" + cpf.substring(9,11);
            return cpf;
        }
    }
}
