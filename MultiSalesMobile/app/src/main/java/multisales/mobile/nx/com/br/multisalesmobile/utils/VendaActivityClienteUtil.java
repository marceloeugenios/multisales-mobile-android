package multisales.mobile.nx.com.br.multisalesmobile.utils;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import multisales.mobile.nx.com.br.multisalesmobile.entidade.Cliente;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ESexo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.Escolaridade;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.EstadoCivil;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.SistemaConstantes;

/**
 * Created by marceloeugenio on 3/15/15.
 */
public class VendaActivityClienteUtil {

    private Integer idVenda;
    public EditText txtNomeCliente;
    public EditText txtDataNascimentoCliente;
    public Spinner spinnerSexo;
    public Spinner spinnerEstadoCivil;
    public EditText txtCpfCnpj;
    public EditText txtNomeMae;
    public EditText txtNomePai;
    public EditText txtRgInscEstadual;
    public EditText txtEmissorRG;
    public EditText txtDataEmissaoRG;
    public Spinner spinnerEscolaridade;
    public EditText txtNacionalidade;
    public EditText txtTelResidencial;
    public EditText txtTelComercial;
    public EditText txtTelCelular;
    public EditText txtEmail;
    public EditText txtProfissao;
    public EditText txtFaixaSalarial;
    private DatePickerDialog dataNascDPD;
    private DatePickerDialog dataEmissaoDPD;
    private SimpleDateFormat formatoData;

    private List<Escolaridade> escolaridades;
    private List<EstadoCivil> estadosCivis;
    private ESexo[] sexos;

    private Cliente cliente;

    public VendaActivityClienteUtil() {
        formatoData = new SimpleDateFormat("dd/MM/yyyy");
    }

    private Cliente popularDadosCliente() {
        cliente.setId(idVenda);
        cliente.setNome(txtNomeCliente.getText().toString());
        cliente.setDataNascimento(UtilActivity.getCalendarValue(txtDataNascimentoCliente));

        if (spinnerSexo.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            cliente.setSexo(Arrays.asList(sexos)
                    .get(spinnerSexo.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        if (spinnerEstadoCivil.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            cliente.setEstadoCivil(estadosCivis
                    .get(spinnerEstadoCivil.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        cliente.setCpfCnpj(txtCpfCnpj.getText().toString());
        cliente.setNomeMae(txtNomeMae.getText().toString());
        cliente.setNomePai(txtNomePai.getText().toString());
        cliente.setRgInscricaoEstadual(txtRgInscEstadual.getText().toString());
        cliente.setDataEmissao(UtilActivity.getCalendarValue(txtDataEmissaoRG));

        if (spinnerEscolaridade.getSelectedItemPosition() > SistemaConstantes.ZERO) {
            cliente.setEscolaridade(escolaridades
                    .get(spinnerEscolaridade.getSelectedItemPosition() - SistemaConstantes.UM));
        }

        cliente.setNacionalidade(txtNacionalidade.getText().toString());
        cliente.setTelefoneResidencial(txtTelResidencial.getText().toString());
        cliente.setTelefoneComercial(txtTelComercial.getText().toString());
        cliente.setTelefoneCelular(txtTelCelular.getText().toString());
        cliente.setEmail(txtEmail.getText().toString());
        cliente.setProfissao(txtProfissao.getText().toString());
        cliente.setFaixaSalarial(new BigDecimal(txtFaixaSalarial.getText().toString()));

        return cliente;
    }

    private void popularValoresCamposEntrada() {
        txtNomeCliente.setText(cliente.getNome());
        if (cliente.getDataNascimento() != null) {
            txtDataNascimentoCliente.setText(formatoData.format(cliente.getDataNascimento().getTime()));
        }
        int i;
        if (cliente.getSexo() != null) {
            for (i = 0; i < sexos.length; i++) {
                if (sexos[i].equals(cliente.getSexo())) {
                    spinnerSexo.setSelection(i + 1);
                    break;
                }
            }
        }
        if (cliente.getEstadoCivil() != null) {
            for (i = 0; i < estadosCivis.size(); i++) {
                if (estadosCivis.get(i).getId().equals(cliente.getEstadoCivil().getId())) {
                    spinnerEstadoCivil.setSelection(i + 1);
                    break;
                }
            }
        }
        if (cliente.getCpfCnpj() != null) {
            txtCpfCnpj.setText(cliente.getCpfCnpj());
        }
        if (cliente.getNomeMae() != null) {
            txtNomeMae.setText(cliente.getNomeMae());
        }
        if (cliente.getNomePai() != null) {
            txtNomePai.setText(cliente.getNomePai());
        }
        if (cliente.getRgInscricaoEstadual() != null) {
            txtRgInscEstadual.setText(cliente.getRgInscricaoEstadual());
        }
        if (cliente.getEmissor() != null) {
            txtEmissorRG.setText(cliente.getEmissor());
        }
        if (cliente.getDataEmissao() != null) {
            txtDataEmissaoRG.setText(formatoData.format(cliente.getDataEmissao().getTime()));
        }
        if (cliente.getEscolaridade() != null) {
            for (i = 0; i < escolaridades.size(); i++) {
                if (escolaridades.get(i).getId().equals(cliente.getEscolaridade().getId())) {
                    spinnerEscolaridade.setSelection(i + 1);
                    break;
                }
            }
        }
        if (cliente.getNacionalidade() != null) {
            txtNacionalidade.setText(cliente.getNacionalidade());
        }
        if (cliente.getTelefoneResidencial() != null) {
            txtTelResidencial.setText(cliente.getTelefoneResidencial());
        }
        if (cliente.getTelefoneComercial() != null) {
            txtTelComercial.setText(cliente.getTelefoneComercial());
        }
        if (cliente.getTelefoneCelular() != null) {
            txtTelCelular.setText(cliente.getTelefoneCelular());
        }
        if (cliente.getEmail() != null) {
            txtEmail.setText(cliente.getEmail());
        }
        if (cliente.getProfissao() != null) {
            txtProfissao.setText(cliente.getProfissao());
        }
        if (cliente.getFaixaSalarial() != null) {
            txtFaixaSalarial.setText(cliente.getFaixaSalarial().toString());
        }
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
