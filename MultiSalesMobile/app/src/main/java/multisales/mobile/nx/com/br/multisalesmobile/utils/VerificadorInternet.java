package multisales.mobile.nx.com.br.multisalesmobile.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import multisales.mobile.nx.com.br.multisalesmobile.R;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.ENotificacaoCodigo;
import multisales.mobile.nx.com.br.multisalesmobile.entidade.MultiSales;
import multisales.mobile.nx.com.br.multisalesmobile.servico.SincronizacaoServico;

/**
 * Created by eric on 12-03-2015.
 */
public class VerificadorInternet extends BroadcastReceiver {

    private SincronizacaoServico sincronizacaoServico;
    private MultiSales multiSales;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        multiSales = (MultiSales)context.getApplicationContext();

        sincronizacaoServico = new SincronizacaoServico(context.getApplicationContext());
        if (UtilActivity.isOnline(context)) {
            multiSales.setRedeDados(true);
            sincronizacaoServico.sincronizarTodasTabulacoes();
            UtilActivity.cancelNotification(context, ENotificacaoCodigo.CONEXAO);
        } else {
            multiSales.setRedeDados(false);
            UtilActivity.makeNotification(context,
                    ENotificacaoCodigo.CONEXAO,
                    R.drawable.ic_stat_notify_app,
                    "Multisales",
                    "Trabalhando offline",
                    "Trabalhando offline",
                    null,
                    true);
        }
        Intent broadcast = new Intent("ALTERACAO_REDE");
        context.sendBroadcast(broadcast);
    }

}
